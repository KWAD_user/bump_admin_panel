// Change FRONT-END PORT here
var PORT = 80;

// Change directory "public" here
var FolderName = '/' + 'dist';

/*Command line arguments
 * Command node app.js [PORT] [PUBLIC-DIR-PATH]*/
if (process.argv.length > 1) {
  PORT = (process.argv[2] && !isNaN(process.argv[2]) && +process.argv[2] ) || PORT;
  FolderName = process.argv[3] || FolderName;
}

var express = require('express');
var path = require('path');

// Setup server
var app = express();
var server = require('http').createServer(app);

process.on('uncaughtException', function(err) {
  console.error("Uncaught Exception: " + err);
  console.error("Stack: " + err.stack);
  process.exit(1);
});

app.use(express.static(path.join(__dirname, FolderName)));
app.set('appPath', __dirname + FolderName);

// Insert routes below

// All undefined asset or api routes should return a 404
app.route('/:url(api|components|app|bower_components|assets)/*')
  .get(function (req, res) {
    res.send("Error 404");
  });

// All other routes should redirect to the index.html
app.route('/*')
  .get(function (req, res) {
    res.sendFile(app.get('appPath') + '/index.html');
  });


/**
 * Server init and start
 */

server.listen(PORT);

function stopWebServer() {
  server.close(function() {
    console.info('Webserver shutdown');
    process.exit();
  });
}

var gracefulShutdown = function() {
  console.info("Received kill signal, shutting down Webserver gracefully.");
  stopWebServer();
  // if after
  setTimeout(function() {
    console.error("Could not close connections in time, forcefully shutting down webserver");
    process.exit();
  }, 10 * 1000);
};

// Ctrl + C
process.on('SIGINT', gracefulShutdown);

// listen for TERM signal .e.g. kill
process.on('SIGTERM', gracefulShutdown);

process.on('uncaughtException', function(err) {
  console.error("Uncaught Exception: " + err);
  process.exit(1);
});

console.info('Express server listening on port: %s', PORT);
