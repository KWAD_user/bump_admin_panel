'use strict';

describe('Unit: ForgetPassword Controller', function () {
  var ForgetPassword,
    $controller,
    $rootScope,
    $q,
    $httpBackend,
    ForgetPasswordApiService,
    ToastrMessage,
    MessageService,
    LoaderService = {Loader: {}};

  beforeEach(module('kwadWeb'));
  beforeEach(inject(function (_$controller_, _$rootScope_, _$q_, _$httpBackend_, _ToastrMessage_) {
    $controller = _$controller_;
    $rootScope = _$rootScope_;
    $q = _$q_;
    $httpBackend = _$httpBackend_;
    ToastrMessage = _ToastrMessage_;
    ForgetPasswordApiService = jasmine.createSpyObj('ForgetPasswordApiService', ['forgetPassword']);
    MessageService = jasmine.createSpyObj('MessageService', ['showSuccessfulMessage', 'showFailedMessage']);
    LoaderService.Loader = jasmine.createSpyObj('LoaderService.Loader', ['hideSpinner', 'showSpinner']);
    MessageService.showSuccessfulMessage.and.callFake(function () {
    });
    MessageService.showFailedMessage.and.callFake(function () {
    });
    LoaderService.Loader.hideSpinner.and.callFake(function () {
    });
    LoaderService.Loader.showSpinner.and.callFake(function () {
    });
  }));
  beforeEach(function () {
    ForgetPassword = $controller('ForgetPasswordCtrl', {
      ForgetPasswordApiService: ForgetPasswordApiService,
      ToastrMessage: ToastrMessage,
      MessageService: MessageService,
      LoaderService: LoaderService
    });
    $httpBackend.expectGET('app/login/login.html').respond(200);
  });

  describe('Units Should be defined', function () {
    it('ForgetPassword should be defined', function () {
      expect(ForgetPassword).toBeDefined();
    });
    it('ForgetPasswordApiService should be defined', function () {
      expect(ForgetPasswordApiService).toBeDefined();
    });
    it('ToastrMessage should be defined', function () {
      expect(ToastrMessage).toBeDefined();
    });
    it('MessageService should be defined', function () {
      expect(MessageService).toBeDefined();
    });
    it('LoaderService should be defined', function () {
      expect(LoaderService).toBeDefined();
    });
  });

  describe('Function Unit: ForgetPassword.sendEmail', function () {
    it('Should be defined and be a function', function () {
      expect(ForgetPassword.sendEmail).toBeDefined();
      expect(typeof ForgetPassword.sendEmail).toEqual('function');
    });
    it('Should send a mail regarding to forget password', function () {
      ForgetPasswordApiService.forgetPassword.and.callFake(function () {
        var deferred = $q.defer();
        deferred.resolve({status: 'SUCCESS'});
        deferred.$promise = deferred.promise;
        return deferred;
      });
      ForgetPassword.email = 'sandeep.kumar1@tothenew.com';
      ForgetPassword.sendEmail();
      $rootScope.$digest();
      expect(ForgetPassword.isEmailSent).toEqual(true);
    });

    describe('Should not send an email', function () {
      it('when ForgetPassword.email field is empty', function () {
        ForgetPassword.email = '';
        ForgetPassword.sendEmail();
        $rootScope.$digest();
        expect(ForgetPassword.isEmailSent).toEqual(false);
      });
      it('when ForgetPasswordApiService forgetPassword returns 200 with status \'ERROR\'', function () {
        ForgetPasswordApiService.forgetPassword.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve({status: 'ERROR'});
          deferred.$promise = deferred.promise;
          return deferred;
        });
        ForgetPassword.email = 'sandeep.kumar1@tothenew.com';
        ForgetPassword.sendEmail();
        $rootScope.$digest();
        expect(ForgetPassword.isEmailSent).toEqual(false);
      });
      it('when ForgetPasswordApiService forgetPassword send server error', function () {
        ForgetPasswordApiService.forgetPassword.and.callFake(function () {
          var deferred = $q.defer();
          deferred.reject();
          deferred.$promise = deferred.promise;
          return deferred;
        });
        ForgetPassword.email = 'sandeep.kumar1@tothenew.com';
        ForgetPassword.sendEmail();
        $rootScope.$digest();
        expect(ForgetPassword.isEmailSent).toEqual(false);
      });
    })
  });
});