'use strict';

(function (angular) {
  angular.module('kwadWeb')
    .controller('ForgetPasswordCtrl', ['ForgetPasswordApiService', 'LoaderService', 'ToastrMessage', 'MessageService',
      function (ForgetPasswordApiService, LoaderService, ToastrMessage, MessageService) {

        var ForgetPassword = this;

        ForgetPassword.email = '';
        ForgetPassword.isEmailSent = false;

        ForgetPassword.sendEmail = function () {
          if (!ForgetPassword.email) {
            MessageService.showFailedMessage(ToastrMessage.LOGIN.BLANK_EMAIL_FIELD);
            return;
          }
          LoaderService.Loader.showSpinner();
          var _payload = {
            email: ForgetPassword.email
          };
          ForgetPasswordApiService.forgetPassword(_payload).$promise.then(function (response) {
            LoaderService.Loader.hideSpinner();
            if (response.status === 'SUCCESS') {
              ForgetPassword.isEmailSent = true;
            } else {
              MessageService.showFailedMessage();
            }
          }, function () {
            LoaderService.Loader.hideSpinner();
            MessageService.showFailedMessage();
          });
        };
      }]);
})(angular);
