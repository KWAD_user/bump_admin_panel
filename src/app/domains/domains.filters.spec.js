'use strict';

describe('Unit: domainsStatus Filter', function () {

  var $filter, Universities;

  beforeAll(function () {
    Universities = [{
      'name': 'Albion College',
      'universityId': '54c3d0a8e4b0d6dec07e5398',
      'domains': [{'name': 'albion.edu', 'status': 'INACTIVE'}]
    }, {
      'name': 'Alderson Broaddus University',
      'universityId': '54c3d0a8e4b0d6dec07e5399',
      'domains': [{'name': 'Alderson.edu', 'status': 'ACTIVE'}]
    }];
  });

  beforeEach(function () {
    module('kwadWeb');
    inject(function (_$filter_) {
      $filter = _$filter_;
    });
  });

  it('Should return array of all Universities is domain status set as ALL', function () {
    // Arrange.
    var status = 'ALL', result;

    // Act.
    result = $filter('domainsStatus')(Universities, status);

    // Assert.
    expect(result).toEqual(Universities);
    expect(result.length).toEqual(2);
  });

  it('Should return array of INACTIVE Universities is domain status set as INACTIVE', function () {
    // Arrange.
    var status = 'INACTIVE', result;

    // Act.
    result = $filter('domainsStatus')(Universities, status);

    // Assert.
    expect(result[0]).toEqual(Universities[0]);
    expect(result.length).toEqual(1);
  });

  it('Should return array of ACTIVE Universities is domain status set as ACTIVE', function () {
    // Arrange.
    var status = 'ACTIVE', result;

    // Act.
    result = $filter('domainsStatus')(Universities, status);

    // Assert.
    expect(result[0]).toEqual(Universities[1]);
    expect(result.length).toEqual(1);
  });
});
