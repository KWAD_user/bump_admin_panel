'use strict';

(function (angular) {
    angular.module('kwadWeb')
            .controller('DomainsCtrl', ['$rootScope', '$scope', '$compile', '$modal', 'CookieService', 'AuthService', 'EnumsService', 'DTOptionsBuilder', 'DTColumnBuilder', 'DomainApiService', 'DomainAddEditApiService', 'MessageService', 'ToastrMessage', 'LoaderService', 'HOST',
                function ($rootScope, $scope, $compile, $modal, CookieService, AuthService, EnumsService, DTOptionsBuilder, DTColumnBuilder, DomainApiService, DomainAddEditApiService, MessageService, ToastrMessage, LoaderService, HOST) {
                    // Define variables
                    var Domains = this;

                    Domains.USER_ROLES_OBJ = null;
                    Domains.USER_ROLES = null;
                    Domains.currentUser = null;
                    Domains.viewName = 'Domains';
                    Domains.serchForStatus = 'ALL';  
                    Domains.allDomains = [];
                    Domains.newUniversity = {
                        'universityName': '',
                        'domainList': [{
                                'name': '',
                                'status': 'ACTIVE'
                            }]
                    };
                    Domains.dtInstance = {};
					Domains.showDT = false; //fix for only showing table when data is ready
                    
                    var userToken = CookieService.userSession.get();
                    
                    var modalInstance;

                    // Methods                  
                    var _successUpdateUniversity = function (response) {
                        if (response.status === 'SUCCESS') {
                            MessageService.showSuccessfulMessage(ToastrMessage.DOMAIN[response.status]);
                            if (response.data.universityId) {
                                angular.forEach(response.data.domains, function (domains) {
                                    domains.name = domains.name.substring(0, domains.name.length - 4);
                                });
                                $rootScope.$evalAsync(function () {
                                    var index = Domains.allDomains.map(function (obj) {
                                        return obj.universityId;
                                    }).indexOf(response.data.universityId);
                                    Domains.allDomains.splice(index, 1, response.data);
                                });
                            }
                        } else {
                            MessageService.showFailedMessage(ToastrMessage.DOMAIN[response.status]);
                        }
                        LoaderService.Loader.hideSpinner();
                        Domains.ok();
                    };
                    var _failUpdateUniversity = function () {
                        MessageService.showFailedMessage();
                        LoaderService.Loader.hideSpinner();
                    };

                    var _successAddUniversity = function (response) {
                        if (response.status === 'SUCCESS') {
                            MessageService.showSuccessfulMessage(ToastrMessage.DOMAIN[response.status]);
                            if (response.data.universityId) {
                                angular.forEach(response.data.domains, function (domains) {
                                    domains.name = domains.name.substring(0, domains.name.length - 4);
                                });
                                $rootScope.$evalAsync(function () {
                                    Domains.allDomains.push(response.data);
                                    Domains.addMode = false;
                                });
                            }
                        } else {
                            MessageService.showFailedMessage(ToastrMessage.DOMAIN[response.status]);
                        }
                        LoaderService.Loader.hideSpinner();
                    };
                    var _failAddUniversity = function () {
                        MessageService.showFailedMessage();
                        LoaderService.Loader.hideSpinner();
                    };


                    var userRoleEnumsSuccess = function (userRoles) {
                        Domains.USER_ROLES = angular.copy(userRoles);
                        Domains.USER_ROLES_OBJ = userRoles.toObject();
                        AuthService.getCurrentUser().then(function (user) {
                            Domains.currentUser = user;
                            getDomains(user.role);
                        }, function () {
                            Domains.currentUser = null;
                            MessageService.showFailedMessage();
                            LoaderService.Loader.hideSpinner();
                        });
                    };
                    var userRoleEnumsFailure = function () {
                        MessageService.showFailedMessage();
                        LoaderService.Loader.hideSpinner();
                    };

                    function getDomains(role) {
                        var _payload = {
                            query: ''
                        };
                        switch (role) {
                            case Domains.USER_ROLES_OBJ.ADMIN:
                            case Domains.USER_ROLES_OBJ.EDITOR:
                                
                                Domains.dtOptions = DTOptionsBuilder.newOptions()
                                        .withOption('ajax', {
                                            url: HOST.HOST_URL + '/api/ext/s/domainsearch',
                                            type: 'POST',
                                            headers: {'Authorization': 'Bearer ' + btoa(userToken)},
                                            data: function (data) {
                                                _payload.limit = data.length;
                                                _payload.offSet = data.start;                                                
                                                _payload.query = data.search.value;

                                                return JSON.stringify(_payload);
                                            },
                                            dataFilter: function (data) {                                                                                                                                                
                                                var json = jQuery.parseJSON(data);
                                                
                                                //remove .edu from the end of the domain name
                                                angular.forEach(json.data, function (university) {
                                                    angular.forEach(university.domains, function (domains) {
                                                        domains.name = domains.name.substring(0, domains.name.length - 4);
                                                    });
                                                });                                               
                                                
                                                json.recordsTotal = json.count;
                                                json.recordsFiltered = json.count;
                                                                                                                                               
                                                return JSON.stringify(json);
                                            },
                                            dataType: 'json',
                                            contentType: 'application/json'
                                        })
                                        .withDataProp('data')
                                        .withOption('processing', true)
                                        .withOption('serverSide', true)
                                        .withOption('createdRow', createdRow)
                                        .withPaginationType('full_numbers');

                                Domains.dtColumns = [                                    
                                    DTColumnBuilder.newColumn('name', 'Name'),
                                    DTColumnBuilder.newColumn('domains').withTitle('Domains').withClass('domains').renderWith(function (data, type, full, meta) {
                                        var html = '<ul>';
                                        for (var i = 0; i < data.length; i++) {
                                            html += '<li class="list-unstyled">'+data[i].name + '</li>';
                                        }          
                                        html += '</ul>';
                                        
                                        return html;
                                    }),
                                    DTColumnBuilder.newColumn('domains').withTitle('Status').renderWith(function (data, type, full, meta) {
                                        var html = '<ul>';
                                        for (var i = 0; i < data.length; i++) {
                                            html += '<li class="list-unstyled">'+data[i].status + '</li>';
                                        }          
                                        html += '</ul>';
                                        
                                        return html;
                                    })
                                ];

                                Domains.showDT = true;

                                LoaderService.Loader.hideSpinner();		
                                
                                break;
                            default :
                                LoaderService.Loader.hideSpinner();
                                break;
                        }
                    }

					function createdRow(row, data, dataIndex) {				
                        $(row).addClass('edit table-row cursor-pointer');
                        $(row).unbind('click');
                        $(row).bind('click', function () {
                            $scope.$apply(function () {
                                Domains.editDomainDetails(dataIndex, data);
                            });
                        });                        
                        
						// Recompiling so we can bind Angular directive to the DT
						$compile(angular.element(row).contents())($scope);
					}

                    // Events
                    Domains.init = function () {
                        LoaderService.Loader.showSpinner();
                        if (CookieService.userSession.get()) {
                            $rootScope.isLoggedInUser = true;
                        }
                        EnumsService.getUserRoleEnums().then(userRoleEnumsSuccess, userRoleEnumsFailure);
                    };

                    Domains.createCopy = function (source, destination) {
                        angular.copy(source, destination);
                    };

                    Domains.addDomian = function (university) {
                        university.domains.push({
                            name: '',
                            status: 'ACTIVE'
                        });
                    };

                    Domains.addDomianNew = function () {
                        Domains.newUniversity.domainList.push({
                            name: '',
                            status: 'ACTIVE'
                        });
                    };

                    Domains.updateUniversity = function (university) {
                        if (Domains.currentUser.role !== Domains.USER_ROLES_OBJ.ADMIN) {
                            MessageService.showFailedMessage(ToastrMessage.DOMAIN.INSUFFICIENT_RIGHTS);
                            return;
                        }

                        if (!university.name) {
                            MessageService.showFailedMessage(ToastrMessage.DATA_VALIDATE.EMPTY_UNIVERSITY);
                            return;
                        }
                        LoaderService.Loader.showSpinner();
                        var newDomainsList = [];
                        var _payload = {
                            'universityName': university.name,
                            'universityId': university.universityId,
                            'domainList': []
                        };
                        angular.forEach(university.domains, function (domain) {
                            if (domain.name) {
                                newDomainsList.push({
                                    'name': domain.name,
                                    'status': domain.status
                                });
                                var domainName = domain.name + '.edu';
                                _payload.domainList.push({
                                    'name': domainName,
                                    'status': domain.status
                                });
                            }
                        });
                        university.domains = newDomainsList;
                        DomainAddEditApiService.addEditDomain(_payload).$promise.then(_successUpdateUniversity, _failUpdateUniversity);
                    };

                    Domains.addUniversity = function () {
                        if (Domains.currentUser.role !== Domains.USER_ROLES_OBJ.ADMIN && Domains.currentUser.role !== Domains.USER_ROLES_OBJ.EDITOR) {
                            MessageService.showFailedMessage(ToastrMessage.DOMAIN.INSUFFICIENT_RIGHTS);
                            return;
                        }

                        if (!Domains.newUniversity.universityName) {
                            MessageService.showFailedMessage(ToastrMessage.DATA_VALIDATE.EMPTY_UNIVERSITY);
                            return;
                        }
                        LoaderService.Loader.showSpinner();
                        var _payload = {
                            'universityName': Domains.newUniversity.universityName,
                            'domainList': []
                        };
                        angular.forEach(Domains.newUniversity.domainList, function (domain) {
                            if (domain.name) {
                                var domainName = domain.name + '.edu';
                                _payload.domainList.push({
                                    'name': domainName,
                                    'status': domain.status
                                });
                            }
                        });
                        DomainAddEditApiService.addEditDomain(_payload).$promise.then(_successAddUniversity, _failAddUniversity);
                    };
                    
                    /**
                     * edit the selected domains details
                     * @returns {undefined}
                     */
                    Domains.editDomainDetails = function (dataIndex, data){                                                                                               
                        Domains.modalDomain = data;
                        
                        $rootScope.Domains = Domains;
                        
                        modalInstance = $modal.open({
                            templateUrl: 'components/modals/editDomain/editDomain.html',                            
                            controller: 'DomainsCtrl',
                            size: 'lg',
                            scope: $rootScope
                        });
                    };
                    
                    Domains.ok = function () {
                        Domains.dtInstance.rerender();
                        modalInstance.dismiss('cancel');
                    };

                    Domains.cancel = function () {                        
                        modalInstance.dismiss('cancel');
                    };
                }]);
})(angular);
