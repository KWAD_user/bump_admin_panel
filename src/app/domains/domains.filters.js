'use strict';

/**
 * @ngdoc function
 * @name kwadWeb.Filter:domainsStatus filter
 * @description
 * # used to filter domains by status
 */
(function (angular) {
  angular.module('kwadWeb')
    .filter('domainsStatus', function () {
      return function (Universities, status) {
        if (status === 'ALL') {
          return Universities;
        }
        var output = [];
        angular.forEach(Universities, function (University) {
          University.domains.some(function (domain) {
            if (domain.status === status) {
              output.push(University);
              return true;
            }
          });
        });
        return output;
      };
    });
})(angular);
