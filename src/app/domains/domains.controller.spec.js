'use strict';

describe('Unit: Domains Controller', function () {
  var Domains,
    $rootScope,
    $scope,
    $q,
    $controller,
    $httpBackend,
    CookieService = {},
    AuthService,
    DomainApiService,
    DomainAddEditApiService,
    ToastrMessage,
    MessageService,
    LoaderService = {Loader: {}},
    fakeResponse,
    EnumsService;

  beforeAll(function () {
    fakeResponse = {
      fakeDomains: [{
        'name': 'Albion College',
        'universityId': '54c3d0a8e4b0d6dec07e5398',
        'domains': [{'name': 'albion.edu', 'status': 'INACTIVE'}]
      }],
      fakeDomain: {
        status: 'SUCCESS',
        data: {
          'name': 'Albion College',
          'universityId': '54c3d0a8e4b0d6dec07e5398',
          'domains': [{'name': 'albion.edu', 'status': 'INACTIVE'}]
        }
      },
      fakeExistingDomain: {
        status: 'UNIVERSITY_ALREADY_EXISTS'
      },
      fakeUniversity: {
        'universityName': 'Albion College',
        'domainList': [{
          'name': 'albion',
          'status': 'ACTIVE'
        }]
      },
      fakeUniversityToUpdate: {
        'name': 'Amherst College',
        'universityId': '54c3d0a8e4b0d6dec07e5398',
        'domains': [{'name': 'Amherst', 'status': 'INACTIVE'}]
      },
      fakeUpdateUniversityResponse: {
        status: 'SUCCESS',
        data: {
          'name': 'Amherst College',
          'universityId': '54c3d0a8e4b0d6dec07e5398',
          'domains': [{'name': 'Amherst.edu', 'status': 'INACTIVE'}]
        }
      },
      fakeRoles: ['ANONYMOUS', 'REGULAR', 'LAUNCHSQUAD', 'ADMIN', 'BUSINESS', 'BUSINESS_INDIVIDUAL', 'EDITOR'],
      fakeRolesObj: {
        ANONYMOUS: 'ANONYMOUS',
        REGULAR: 'REGULAR',
        LAUNCHSQUAD: 'LAUNCHSQUAD',
        ADMIN: 'ADMIN',
        BUSINESS: 'BUSINESS',
        BUSINESS_INDIVIDUAL: 'BUSINESS_INDIVIDUAL',
        EDITOR: 'EDITOR'
      },
      fakeAdminUser: {
        attributes: [],
        college: '',
        first_name: 'admin',
        full_name: 'admin',
        last_name: 'admin',
        role: 'ADMIN',
        state: 'ENABLED'
      },
      fakeLaunchsquadUser: {
        attributes: [],
        college: '',
        first_name: 'bump',
        full_name: 'bump user',
        last_name: 'user',
        role: 'LAUNCHSQUAD',
        state: 'ENABLED'
      }
    };
  });
  beforeEach(module('kwadWeb'));
  beforeEach(inject(function (_$rootScope_, _$controller_, _$q_, _ToastrMessage_, _$httpBackend_) {
    $rootScope = _$rootScope_;
    $scope = _$rootScope_.$new();
    $q = _$q_;
    $controller = _$controller_;
    $httpBackend = _$httpBackend_;
    CookieService.userSession = jasmine.createSpyObj('userSession', ['get']);
    AuthService = jasmine.createSpyObj('AuthService', ['getCurrentUser']);
    DomainApiService = jasmine.createSpyObj('DomainApiService', ['getDomains']);
    DomainAddEditApiService = jasmine.createSpyObj('DomainAddEditApiService', ['addEditDomain']);
    ToastrMessage = _ToastrMessage_;
    MessageService = jasmine.createSpyObj('MessageService', ['showSuccessfulMessage', 'showFailedMessage']);
    LoaderService.Loader = jasmine.createSpyObj('LoaderService.Loader', ['hideSpinner', 'showSpinner']);
    EnumsService = jasmine.createSpyObj('EnumsService', ['getUserRoleEnums']);
    MessageService.showSuccessfulMessage.and.callFake(function () {
    });
    MessageService.showFailedMessage.and.callFake(function () {
    });
    LoaderService.Loader.hideSpinner.and.callFake(function () {
    });
    LoaderService.Loader.showSpinner.and.callFake(function () {
    });
  }));
  beforeEach(function () {
    Domains = $controller('DomainsCtrl', {
      $scope: $scope,
      CookieService: CookieService,
      AuthService: AuthService,
      DomainApiService: DomainApiService,
      DomainAddEditApiService: DomainAddEditApiService,
      ToastrMessage: ToastrMessage,
      MessageService: MessageService,
      LoaderService: LoaderService,
      EnumsService: EnumsService
    });
    $httpBackend.expectGET('app/login/login.html').respond(200);
  });

  describe('Units Should be defined', function () {
    it('Domains should be defined', function () {
      expect(Domains).toBeDefined();
    });
    it('DomainApiService should be defined', function () {
      expect(DomainApiService).toBeDefined();
    });
    it('DomainAddEditApiService should be defined', function () {
      expect(DomainAddEditApiService).toBeDefined();
    });
    it('ToastrMessage should be defined', function () {
      expect(ToastrMessage).toBeDefined();
    });
    it('CookieService should be defined', function () {
      expect(CookieService).toBeDefined();
    });
  });

  describe('Function Unit: Domains.init', function () {
    beforeEach(function () {
      CookieService.userSession.get.and.callFake(function () {
        return true;
      });
    });
    it('Should be defined and be a function', function () {
      expect(Domains.init).toBeDefined();
      expect(typeof Domains.init).toEqual('function');
    });
    it('Should assign values when current user is admin/editor.', function () {
      EnumsService.getUserRoleEnums.and.callFake(function () {
        var deferred = $q.defer();
        deferred.resolve(fakeResponse.fakeRoles);
        return deferred.promise;
      });
      DomainApiService.getDomains.and.callFake(function () {
        var deferred = $q.defer();
        deferred.resolve(fakeResponse.fakeDomains);
        deferred.$promise = deferred.promise;
        return deferred;
      });
      AuthService.getCurrentUser.and.callFake(function () {
        var deferred = $q.defer();
        deferred.resolve(fakeResponse.fakeAdminUser);
        return deferred.promise;
      });
      Domains.init();
      $rootScope.$digest();
      $httpBackend.flush();
      expect($rootScope.isLoggedInUser).toBeTruthy();
      expect(Domains.allDomains.length).toBeGreaterThan(0);
    });
    describe('Should not assign value to Domains.allDomains', function () {
      it('When DomainApiService.getDomains return failure', function () {
        EnumsService.getUserRoleEnums.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve(fakeResponse.fakeRoles);
          return deferred.promise;
        });
        DomainApiService.getDomains.and.callFake(function () {
          var deferred = $q.defer();
          deferred.reject();
          deferred.$promise = deferred.promise;
          return deferred;
        });
        AuthService.getCurrentUser.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve(fakeResponse.fakeAdminUser);
          return deferred.promise;
        });
        Domains.init();
        $rootScope.$digest();
        $httpBackend.flush();
        expect(Domains.allDomains.length).toEqual(0);
      });
      it('When AuthService.getCurrentUser return failure', function () {
        EnumsService.getUserRoleEnums.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve(fakeResponse.fakeRoles);
          return deferred.promise;
        });
        AuthService.getCurrentUser.and.callFake(function () {
          var deferred = $q.defer();
          deferred.reject();
          return deferred.promise;
        });
        Domains.init();
        $rootScope.$digest();
        $httpBackend.flush();
        expect(Domains.currentUser).toEqual(null);
        expect(Domains.allDomains.length).toEqual(0);
      });
      it('When EnumsService.getUserRoleEnums return failure', function () {
        EnumsService.getUserRoleEnums.and.callFake(function () {
          var deferred = $q.defer();
          deferred.reject();
          return deferred.promise;
        });
        Domains.init();
        $rootScope.$digest();
        $httpBackend.flush();
        expect(Domains.currentUser).toEqual(null);
        expect(Domains.allDomains.length).toEqual(0);
      });
      it('When current user is other then admin/editor ', function () {
        EnumsService.getUserRoleEnums.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve(fakeResponse.fakeRoles);
          return deferred.promise;
        });
        DomainApiService.getDomains.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve(fakeResponse.fakeDomains);
          deferred.$promise = deferred.promise;
          return deferred;
        });
        AuthService.getCurrentUser.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve(fakeResponse.fakeLaunchsquadUser);
          return deferred.promise;
        });
        Domains.init();
        $rootScope.$digest();
        $httpBackend.flush();
        expect(Domains.allDomains.length).toEqual(0);
      });
    });
  });

  describe('Function Unit: Domains.createCopy', function () {
    it('Should be defined and be a function', function () {
      expect(Domains.createCopy).toBeDefined();
      expect(typeof Domains.createCopy).toEqual('function');
    });
    it('Should assign copy object to source object.', function () {
      var source = {
        'name': 'Albion College',
        'universityId': '54c3d0a8e4b0d6dec07e5398',
        'domains': [{'name': 'albion.edu', 'status': 'INACTIVE'}]
      };
      var destination = {};
      Domains.createCopy(source, destination);
      $rootScope.$digest();
      $httpBackend.flush();
      expect(source).toEqual(destination);
    });
  });

  describe('Function Unit: Domains.addDomian', function () {
    it('Should be defined and be a function', function () {
      expect(Domains.addDomian).toBeDefined();
      expect(typeof Domains.addDomian).toEqual('function');
    });
    it('Should add a domain', function () {
      var university = {
        'name': 'Albion College',
        'universityId': '54c3d0a8e4b0d6dec07e5398',
        'domains': [{'name': 'albion', 'status': 'INACTIVE'}]
      };
      Domains.addDomian(university);
      $rootScope.$digest();
      $httpBackend.flush();
      expect(university.domains.length).toEqual(2);
    });
  });

  describe('Function Unit: Domains.addDomianNew', function () {
    it('Should be defined and be a function', function () {
      expect(Domains.addDomianNew).toBeDefined();
      expect(typeof Domains.addDomianNew).toEqual('function');
    });
    it('Should add a new domain', function () {
      Domains.newUniversity = fakeResponse.fakeUniversity;
      Domains.addDomianNew();
      $rootScope.$digest();
      $httpBackend.flush();
      expect(Domains.newUniversity.domainList.length).toBeGreaterThan(1);
    });
  });

  describe('Function Unit: Domains.addUniversity', function () {
    it('Should be defined and be a function', function () {
      expect(Domains.addUniversity).toBeDefined();
      expect(typeof Domains.addUniversity).toEqual('function');
    });
    it('Should add a new record in Universities when DomainAddEditApiService.addEditDomain get resolved.', function () {
      Domains.currentUser = fakeResponse.fakeAdminUser;
      Domains.USER_ROLES_OBJ = fakeResponse.fakeRolesObj;
      Domains.newUniversity = fakeResponse.fakeUniversity;

      DomainAddEditApiService.addEditDomain.and.callFake(function () {
        var deferred = $q.defer();
        deferred.resolve(fakeResponse.fakeDomain);
        deferred.$promise = deferred.promise;
        return deferred;
      });
      Domains.addUniversity();
      $httpBackend.flush();
      $rootScope.$digest();
      expect(Domains.allDomains.length).toBeGreaterThan(0);
    });
    describe('Should not add a new record in Universities', function () {
      it('When new universityName pass empty.', function () {
        Domains.currentUser = fakeResponse.fakeAdminUser;
        Domains.USER_ROLES_OBJ = fakeResponse.fakeRolesObj;
        Domains.newUniversity = {
          'universityName': '',
          'domainList': [{
            'name': '',
            'status': 'ACTIVE'
          }]
        };
        Domains.addUniversity();
        $rootScope.$digest();
        $httpBackend.flush();
        expect(Domains.allDomains.length).toEqual(0);
      });
      it('When current user is bump.', function () {
        Domains.currentUser = fakeResponse.fakeLaunchsquadUser;
        Domains.USER_ROLES_OBJ = fakeResponse.fakeRolesObj;
        Domains.addUniversity();
        $rootScope.$digest();
        $httpBackend.flush();
        expect(Domains.allDomains.length).toEqual(0);
      });
      it('When DomainAddEditApiService.addEditDomain get rejected.', function () {
        Domains.currentUser = fakeResponse.fakeAdminUser;
        Domains.USER_ROLES_OBJ = fakeResponse.fakeRolesObj;
        Domains.newUniversity = {
          'universityName': 'Albion College',
          'domainList': [{
            'name': '',
            'status': 'ACTIVE'
          }]
        };
        DomainAddEditApiService.addEditDomain.and.callFake(function () {
          var deferred = $q.defer();
          deferred.reject();
          deferred.$promise = deferred.promise;
          return deferred;
        });
        Domains.addUniversity(Domains.newUniversity);
        $rootScope.$digest();
        $httpBackend.flush();
        expect(Domains.allDomains.length).toEqual(0);
      });
      it('When DomainAddEditApiService.addEditDomain get resolved with out SUCCESS status .', function () {
        Domains.currentUser = fakeResponse.fakeAdminUser;
        Domains.USER_ROLES_OBJ = fakeResponse.fakeRolesObj;
        Domains.newUniversity = fakeResponse.fakeUniversity;
        DomainAddEditApiService.addEditDomain.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve(fakeResponse.fakeExistingDomain);
          deferred.$promise = deferred.promise;
          return deferred;
        });
        Domains.addUniversity();
        $rootScope.$digest();
        $httpBackend.flush();
        expect(Domains.allDomains.length).toEqual(0);
      });
    });
  });

  describe('Function Unit: Domains.updateUniversity', function () {
    it('Should be defined and be a function', function () {
      expect(Domains.updateUniversity).toBeDefined();
      expect(typeof Domains.updateUniversity).toEqual('function');
    });
    it('Should update University when DomainAddEditApiService.addEditDomain get resolved.', function () {
      Domains.currentUser = fakeResponse.fakeAdminUser;
      Domains.USER_ROLES_OBJ = fakeResponse.fakeRolesObj;
      Domains.allDomains = [{
        'name': 'Albion College',
        'universityId': '54c3d0a8e4b0d6dec07e5398',
        'domains': [{'name': 'albion', 'status': 'INACTIVE'}]
      }];
      DomainAddEditApiService.addEditDomain.and.callFake(function () {
        var deferred = $q.defer();
        deferred.resolve(fakeResponse.fakeUpdateUniversityResponse);
        deferred.$promise = deferred.promise;
        return deferred;
      });
      Domains.updateUniversity(fakeResponse.fakeUniversityToUpdate);
      $httpBackend.flush();
      $rootScope.$digest();
      expect(Domains.allDomains.length).toBeGreaterThan(0);
      expect(Domains.allDomains[0].name).toEqual('Amherst College');
    });
    describe('Should not update University', function () {
      it('When current user is other then admin.', function () {
        var university = {
          'name': '',
          'universityId': '54c3d0a8e4b0d6dec07e5398',
          'domains': [{'name': 'Amherst', 'status': 'INACTIVE'}]
        };
        Domains.currentUser = fakeResponse.fakeLaunchsquadUser;
        Domains.USER_ROLES_OBJ = fakeResponse.fakeRolesObj;
        Domains.updateUniversity(university);
        $rootScope.$digest();
        $httpBackend.flush();
        expect(Domains.currentUser.role).not.toEqual(Domains.USER_ROLES_OBJ.ADMIN);
        expect(Domains.currentUser.role).toEqual(Domains.USER_ROLES_OBJ.LAUNCHSQUAD);
      });
      it('When new universityName pass empty.', function () {
        Domains.allDomains = [{
          'name': 'Albion College',
          'universityId': '54c3d0a8e4b0d6dec07e5398',
          'domains': [{'name': 'albion', 'status': 'INACTIVE'}]
        }];
        var university = {
          'name': '',
          'universityId': '54c3d0a8e4b0d6dec07e5398',
          'domains': [{'name': 'Amherst', 'status': 'INACTIVE'}]
        };
        Domains.currentUser = fakeResponse.fakeAdminUser;
        Domains.USER_ROLES_OBJ = fakeResponse.fakeRolesObj;
        Domains.updateUniversity(university);
        $rootScope.$digest();
        $httpBackend.flush();
        expect(Domains.allDomains.length).toBeGreaterThan(0);
        expect(Domains.allDomains[0].name).toEqual('Albion College');
      });
      it('When DomainAddEditApiService.addEditDomain get reject.', function () {
        Domains.allDomains = [{
          'name': 'Albion College',
          'universityId': '54c3d0a8e4b0d6dec07e5398',
          'domains': [{'name': 'albion', 'status': 'INACTIVE'}]
        }];
        Domains.currentUser = fakeResponse.fakeAdminUser;
        Domains.USER_ROLES_OBJ = fakeResponse.fakeRolesObj;
        DomainAddEditApiService.addEditDomain.and.callFake(function () {
          var deferred = $q.defer();
          deferred.reject();
          deferred.$promise = deferred.promise;
          return deferred;
        });
        Domains.updateUniversity(fakeResponse.fakeUniversityToUpdate);
        $rootScope.$digest();
        $httpBackend.flush();
        expect(Domains.allDomains.length).toBeGreaterThan(0);
        expect(Domains.allDomains[0].name).toEqual('Albion College');
      });
      it('When DomainAddEditApiService.addEditDomain get resolved with out SUCCESS status .', function () {
        Domains.allDomains = [{
          'name': 'Albion College',
          'universityId': '54c3d0a8e4b0d6dec07e5398',
          'domains': [{'name': 'albion', 'status': 'INACTIVE'}]
        }];
        var university = {
          'name': 'Albion College',
          'universityId': '54c3d0a8e4b0d6dec07e5398',
          'domains': [{'name': 'Amherst', 'status': 'INACTIVE'}]
        };
        Domains.currentUser = fakeResponse.fakeAdminUser;
        Domains.USER_ROLES_OBJ = fakeResponse.fakeRolesObj;
        DomainAddEditApiService.addEditDomain.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve(fakeResponse.fakeExistingDomain);
          deferred.$promise = deferred.promise;
          return deferred;
        });
        Domains.updateUniversity(university);
        $rootScope.$digest();
        $httpBackend.flush();
        expect(Domains.allDomains.length).toBeGreaterThan(0);
        expect(Domains.allDomains[0].name).toEqual('Albion College');
      });
    });
  });
});
