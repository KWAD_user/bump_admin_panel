'use strict';

describe('Unit: UserDetailsCtrl Controller', function () {
  var UserDetails,
    $rootScope,
    $scope,
    $controller,
    $state,
    $stateParams,
    $modal,
    $q,
    $httpBackend,
    AuthService,
    EventPlaceApiService,
    UsersApiService,
    UserDetailsService,
    AddEditUserAPIService,
    DTOptionsBuilder,
    DTColumnDefBuilder,
    ToastrMessage,
    MessageService,
    LoaderService = {Loader: {}},
    EnumsService,
    fakeResponse,
    UserProfileData;

  beforeAll(function () {
    UserProfileData = {};
    fakeResponse = {
      fakeRoles: ['ANONYMOUS', 'REGULAR', 'LAUNCHSQUAD', 'ADMIN', 'BUSINESS', 'BUSINESS_INDIVIDUAL', 'EDITOR'],
      fakeRolesObj: {
        ANONYMOUS: 'ANONYMOUS',
        REGULAR: 'REGULAR',
        LAUNCHSQUAD: 'LAUNCHSQUAD',
        ADMIN: 'ADMIN',
        BUSINESS: 'BUSINESS',
        BUSINESS_INDIVIDUAL: 'BUSINESS_INDIVIDUAL',
        EDITOR: 'EDITOR'
      },
      fakeBusinessTypes: ['PROFIT', 'NON_PROFIT', 'UNIVERSITY'],
      fakeAuthorizedStates: ['ENABLED', 'DISABLED', 'AWAITING_VERIFICATION', 'CLOSED'],
      fakeEventCategories: {
        '54b83a2be4b06b211e892748': 'Music',
        '54b83a2be4b06b211e892750': 'Group',
        '54b83a2be4b06b211e89274a': 'Sports',
        '54b83a2be4b06b211e892746': 'Bar/Club',
        '54b83a2be4b06b211e892752': 'Outdoors',
        '54b83a2be4b06b211e892744': 'Coffee',
        '54b83a2be4b06b211e892754': 'General',
        '54b83a2be4b06b211e89274e': 'Party',
        '54b83a2be4b06b211e892742': 'Food',
        '54b83a2be4b06b211e89274c': 'School'
      },
      fakeAdminUser: {
        attributes: [],
        college: '',
        email: 'usertest2@gmail.com',
        first_name: 'user',
        full_name: 'user test2',
        last_name: 'test2',
        id: '5677b0c2e4b02b658d41cb37',
        role: 'ADMIN',
        state: 'ENABLED'
      },
      fakeLaunchsquadUser: {
        attributes: [],
        college: '',
        email: 'usertest2@gmail.com',
        first_name: 'user',
        full_name: 'user test2',
        last_name: 'test2',
        id: '5677b0c2e4b02b658d41cb37',
        role: 'LAUNCHSQUAD',
        state: 'ENABLED'
      },
      fakeRegularUser: {
        attributes: [],
        college: '',
        email: 'usertest2@gmail.com',
        first_name: 'user',
        full_name: 'user test2',
        last_name: 'test2',
        id: '5677b0c2e4b02b658d41cb37',
        role: 'REGULAR',
        state: 'ENABLED'
      },
      fakeEditorUser: {
        email: 'usertest2@gmail.com',
        first_name: 'user',
        full_name: 'user test2',
        last_name: 'test2',
        id: '5677b0c2e4b02b658d41cb37',
        registrationDate: '2015-12-21T07:56:50.403+0000',
        role: 'Editor',
        state: 'ENABLED',
        year: 0
      },
      fakeBusinessUser: {
        'id': '56497837e4b0290cecbedfca',
        'email': 'mycar@gmail.com',
        'role': 'BUSINESS',
        'status': 'DISABLED',
        'registrationDate': '2015-11-16T06:31:19.968+0000',
        'contactNumber': '1234567890',
        'businessName': 'mycar',
        'bPlaces': 0,
        'businessType': 'PROFIT',
        'contactNames': ['Sandy Chhapola'],
        'address': 'mycar, delhi',
        'website': 'www.mycar.com',
        'notes': null,
        'associatedBusinessId': null,
        'associatedBusinessName': null,
        'firstName': null,
        'lastName': null,
        'friends': 0,
        'sessions': 0,
        'events': 0,
        'places': 0
      },
      fakeBusinessIndividuals: [{
        associatedBusinessId: '5665ace3e4b018f03ef8ab47',
        email: 'usertest2@gmail.com',
        first_name: 'user',
        friend: false,
        full_name: 'user test2',
        id: '5677b0c2e4b02b658d41cb37',
        last_name: 'test2',
        registrationDate: '2015-12-21T07:56:50.403+0000',
        role: 'BUSINESS_INDIVIDUAL',
        state: 'ENABLED',
        year: 0
      }],
      fakePlaceList: [{
        'epId': '546aff5be4b0370c32328fbb',
        'name': 'Phoenix Rock Gym',
        'place': '1353 E University Drive, Tempe AZ',
        'creatorName': 'Demo  User',
        'creatorId': '546aff58e4b0370c32328f40',
        'feedImpression': 0,
        'detailImpression': 0,
        'appCheckinCount': 26,
        'geoCheckinCount': 8,
        'shares': 26,
        'status': 'ENABLED',
        'associatedEvents': 2,
        'creatorRole': 'ADMIN',
        'eventDate': null,
        'eventTimeLine': 'UNDETERMINED'
      }, {
        'epId': '546aff5be4b0370c32328fbf',
        'name': 'The Vue on Apache',
        'place': '922 E Apache Blvd, Tempe AZ',
        'creatorName': 'Demo  User',
        'creatorId': '546aff58e4b0370c32328f40',
        'feedImpression': 6,
        'detailImpression': 0,
        'appCheckinCount': 24,
        'geoCheckinCount': 283,
        'shares': 24,
        'status': 'ENABLED',
        'associatedEvents': 0,
        'creatorRole': 'ADMIN',
        'eventDate': null,
        'eventTimeLine': 'UNDETERMINED'
      }],
      fakeEventList: [{
        'epId': '5594cf91e4b044fb28d25c1a',
        'name': 'New Songs',
        'place': '1353 E University Drive, Tempe AZ',
        'creatorName': 'Demo  User ',
        'creatorId': '546aff58e4b0370c32328f40',
        'feedImpression': 0,
        'detailImpression': 0,
        'appCheckinCount': 1,
        'geoCheckinCount': 0,
        'shares': 1,
        'status': 'ENABLED',
        'associatedEvents': 0,
        'creatorRole': 'ADMIN',
        'eventDate': '2015-07-02T18:42:00.637+0000',
        'eventTimeLine': 'PAST'
      }, {
        'epId': '55fa4d3ee4b04e131361c62b',
        'name': 'Rock Climbing Student Day',
        'place': '1353 E University Drive, Tempe AZ',
        'creatorName': 'Demo  User ',
        'creatorId': '546aff58e4b0370c32328f40',
        'feedImpression': 0,
        'detailImpression': 0,
        'appCheckinCount': 1,
        'geoCheckinCount': 0,
        'shares': 1,
        'status': 'ENABLED',
        'associatedEvents': 0,
        'creatorRole': 'ADMIN',
        'eventDate': '2015-09-20T21:00:12.000+0000',
        'eventTimeLine': 'UPCOMING'
      }],
      fakeUserList: [{
        'id': '5600d67ce4b037ce4d25269c',
        'email': 'vkvang@ucdavis.edu',
        'role': 'REGULAR',
        'status': 'ENABLED',
        'registrationDate': '2015-09-22T04:18:04.277+0000',
        'contactNumber': '2092440733',
        'businessName': null,
        'bPlaces': 0,
        'type': null,
        'contactNames': [],
        'address': null,
        'website': null,
        'notes': null,
        'associatedBusinessId': null,
        'associatedBusinessName': null,
        'firstName': 'Kazoua',
        'lastName': 'Vang',
        'friends': 0,
        'sessions': 0,
        'events': 0,
        'places': 0
      }, {
        'id': '5605f79be4b0a9be00b29c0a',
        'email': 'katrinao@uw.edu',
        'role': 'REGULAR',
        'status': 'DISABLED',
        'registrationDate': '2015-09-26T01:40:43.858+0000',
        'contactNumber': '4088354776',
        'businessName': null,
        'bPlaces': 0,
        'type': null,
        'contactNames': [],
        'address': null,
        'website': null,
        'notes': null,
        'associatedBusinessId': null,
        'associatedBusinessName': null,
        'firstName': 'Katrina',
        'lastName': 'Odfina',
        'friends': 0,
        'sessions': 0,
        'events': 0,
        'places': 0
      }]
    };
  });
  beforeEach(module('kwadWeb'));
  beforeEach(inject(function (_$rootScope_, _$controller_, _$stateParams_, _$q_, _UserDetailsService_, _DTOptionsBuilder_, _DTColumnDefBuilder_, _ToastrMessage_, _$httpBackend_) {
    $rootScope = _$rootScope_;
    $scope = _$rootScope_.$new();
    $controller = _$controller_;
    $state = jasmine.createSpyObj('$state', ['go']);
    $stateParams = _$stateParams_;
    $modal = jasmine.createSpyObj('$modal', ['open']);
    $q = _$q_;
    $httpBackend = _$httpBackend_;
    AuthService = jasmine.createSpyObj('AuthService', ['getCurrentUser']);
    EventPlaceApiService = jasmine.createSpyObj('EventPlaceApiService', ['getEventsList', 'getPlacesList']);
    UsersApiService = jasmine.createSpyObj('UsersApiService', ['getUsers']);
    UserDetailsService = _UserDetailsService_;
    AddEditUserAPIService = jasmine.createSpyObj('AddEditUserAPIService', ['addEditUser']);
    DTOptionsBuilder = _DTOptionsBuilder_;
    DTColumnDefBuilder = _DTColumnDefBuilder_;
    ToastrMessage = _ToastrMessage_;
    MessageService = jasmine.createSpyObj('MessageService', ['showSuccessfulMessage', 'showFailedMessage']);
    LoaderService.Loader = jasmine.createSpyObj('LoaderService.Loader', ['hideSpinner', 'showSpinner']);
    EnumsService = jasmine.createSpyObj('EnumsService', ['getBusinessTypeEnums', 'getUserRoleEnums', 'getAuthorizationStateEnums']);
    MessageService.showSuccessfulMessage.and.callFake(function () {
    });
    MessageService.showFailedMessage.and.callFake(function () {
    });
    LoaderService.Loader.hideSpinner.and.callFake(function () {
    });
    LoaderService.Loader.showSpinner.and.callFake(function () {
    });
    $state.go.and.callFake(function () {
    });
  }));
  beforeEach(function () {
    UserDetails = $controller('UserDetailsCtrl', {
      $scope: $scope,
      $state: $state,
      $stateParams: $stateParams,
      $modal: $modal,
      AuthService: AuthService,
      EventPlaceApiService: EventPlaceApiService,
      UsersApiService: UsersApiService,
      UserDetailsService: UserDetailsService,
      AddEditUserAPIService: AddEditUserAPIService,
      DTOptionsBuilder: DTOptionsBuilder,
      DTColumnDefBuilder: DTColumnDefBuilder,
      ToastrMessage: ToastrMessage,
      MessageService: MessageService,
      LoaderService: LoaderService,
      EnumsService: EnumsService,
      UserProfileData: UserProfileData
    });
    $httpBackend.expectGET('app/login/login.html').respond(200);
    $httpBackend.flush();
  });

  describe('Units Should be defined', function () {
    it('UserDetails should be defined', function () {
      expect(UserDetails).toBeDefined();
    });
    it('EventPlaceApiService should be defined', function () {
      expect(EventPlaceApiService).toBeDefined();
    });
    it('ToastrMessage should be defined', function () {
      expect(ToastrMessage).toBeDefined();
    });
  });

  describe('Function Unit: UserDetails.editPlaceDetails', function () {
    it('Should be defined and be a function', function () {
      expect(UserDetails.editPlaceDetails).toBeDefined();
      expect(typeof UserDetails.editPlaceDetails).toEqual('function');
    });
    it('Should change send to place details page', function () {
      var placeData = fakeResponse.fakePlaceList[0];
      UserDetails.editPlaceDetails(placeData);
      $rootScope.$digest();
      expect($state.go).toHaveBeenCalledWith('placeDetails', {placeId: placeData.epId});
    });
  });

  describe('Function Unit: UserDetails.editEventDetails', function () {
    it('Should be defined and be a function', function () {
      expect(UserDetails.editEventDetails).toBeDefined();
      expect(typeof UserDetails.editEventDetails).toEqual('function');
    });
    it('Should change send to place details page', function () {
      var eventData = fakeResponse.fakeEventList[0];
      UserDetails.editEventDetails(eventData);
      $rootScope.$digest();
      expect($state.go).toHaveBeenCalledWith('eventDetails', {eventId: eventData.epId});
    });
  });

  describe('Function Unit: UserDetails.resetPassword ', function () {
    it('Should be defined and be a function', function () {
      expect(UserDetails.resetPassword).toBeDefined();
      expect(typeof  UserDetails.resetPassword).toEqual('function');
    });
    it('when $modal.open invoked and clicked Ok', function () {
      $modal.open.and.callFake(function (obj) {
        var deferred = $q.defer();
        obj.resolve.UserInfo();
        deferred.resolve('SUCCESS');
        return {
          result: deferred.promise
        };
      });
      UserDetails.resetPassword(fakeResponse.fakeRegularUser);
      $rootScope.$digest();
      expect($modal.open).toHaveBeenCalled()
    });
    it('when $modal.open invoked and clicked Cancel', function () {
      $modal.open.and.callFake(function (obj) {
        var deferred = $q.defer();
        obj.resolve.UserInfo();
        deferred.reject('cancel');
        return {
          result: deferred.promise
        };
      });
      UserDetails.resetPassword(fakeResponse.fakeRegularUser);
      $rootScope.$digest();
      expect($modal.open).toHaveBeenCalled()
    });
  });

  describe('Function Unit: UserDetails.backToUserList ', function () {
    beforeEach(function () {
      UserDetails.USER_ROLES_OBJ = fakeResponse.fakeRolesObj;
    });
    it('Should be defined and be a function', function () {
      expect(UserDetails.backToUserList).toBeDefined();
      expect(typeof   UserDetails.backToUserList).toEqual('function');
    });
    it('$state should be \'users.regular\' by default', function () {
      UserDetails.userProfileData = {role: 'BUSINESS_INDIVIDUAL'};
      UserDetails.backToUserList();
      $rootScope.$digest();
      expect($state.go).toHaveBeenCalledWith('users.regular')
    });
    it('$state should be \'users.regular\' when role is REGULAR', function () {
      UserDetails.userProfileData = {role: 'REGULAR'};
      UserDetails.backToUserList();
      $rootScope.$digest();
      expect($state.go).toHaveBeenCalledWith('users.regular')
    });
    it('$state should be \'users.bump\' when role is LAUNCHSQUAD', function () {
      UserDetails.userProfileData = {role: 'LAUNCHSQUAD'};
      UserDetails.backToUserList();
      $rootScope.$digest();
      expect($state.go).toHaveBeenCalledWith('users.bump')
    });
    it('$state should be \'users.admin\' when role is ADMIN', function () {
      UserDetails.userProfileData = {role: 'ADMIN'};
      UserDetails.backToUserList();
      $rootScope.$digest();
      expect($state.go).toHaveBeenCalledWith('users.admin')
    });
    it('$state should be \'users.editor\' when role is EDITOR', function () {
      UserDetails.userProfileData = {role: 'EDITOR'};
      UserDetails.backToUserList();
      $rootScope.$digest();
      expect($state.go).toHaveBeenCalledWith('users.editor')
    });
    it('$state should be \'users.editor\' when role is BUSINESS', function () {
      UserDetails.userProfileData = {role: 'BUSINESS'};
      UserDetails.backToUserList();
      $rootScope.$digest();
      expect($state.go).toHaveBeenCalledWith('users.business')
    });
  });

  describe('Function Unit: UserDetails.getUserEventsList', function () {
    it('Should be defined and be a function', function () {
      expect(UserDetails.getUserEventsList).toBeDefined();
      expect(typeof UserDetails.getUserEventsList).toEqual('function');
    });
    it('Should fetch places of userId \'546aff58e4b0370c32328f40\'', function () {
      EventPlaceApiService.getEventsList.and.callFake(function () {
        var deferred = $q.defer();
        deferred.resolve(fakeResponse.fakeEventList);
        deferred.$promise = deferred.promise;
        return deferred;
      });
      $stateParams.userId = '546aff58e4b0370c32328f40';
      UserDetails.getUserEventsList();
      $rootScope.$digest();
      expect(UserDetails.userEventList.length).toBeGreaterThan(0);
    });
    it('Should not fetch places when EventPlaceApiService.getEventsList rejected', function () {
      EventPlaceApiService.getEventsList.and.callFake(function () {
        var deferred = $q.defer();
        deferred.reject();
        deferred.$promise = deferred.promise;
        return deferred;
      });
      $stateParams.userId = '546aff58e4b0370c32328f40';
      UserDetails.getUserEventsList();
      $rootScope.$digest();
      expect(UserDetails.userEventList).toEqual(null);
    });
  });

  describe('Function Unit: UserDetails.getUserPlacesList', function () {
    it('Should be defined and be a function', function () {
      expect(UserDetails.getUserPlacesList).toBeDefined();
      expect(typeof UserDetails.getUserPlacesList).toEqual('function');
    });
    it('Should fetch places of userId \'546aff58e4b0370c32328f40\'', function () {
      EventPlaceApiService.getPlacesList.and.callFake(function () {
        var deferred = $q.defer();
        deferred.resolve(fakeResponse.fakePlaceList);
        deferred.$promise = deferred.promise;
        return deferred;
      });
      $stateParams.userId = '546aff58e4b0370c32328f40';
      UserDetails.getUserPlacesList();
      $rootScope.$digest();
      expect(UserDetails.userPlaceList.length).toBeGreaterThan(0);
    });
    it('Should not fetch places when EventPlaceApiService.getPlacesList rejected', function () {
      EventPlaceApiService.getPlacesList.and.callFake(function () {
        var deferred = $q.defer();
        deferred.reject();
        deferred.$promise = deferred.promise;
        return deferred;
      });
      $stateParams.userId = '546aff58e4b0370c32328f40';
      UserDetails.getUserPlacesList();
      $rootScope.$digest();
      expect(UserDetails.userPlaceList).toEqual(null);
    });
  });

  describe('Function Unit: UserDetails.getBusinessIndividualsList', function () {
    it('Should be defined and be a function', function () {
      expect(UserDetails.getBusinessIndividualsList).toBeDefined();
      expect(typeof UserDetails.getBusinessIndividualsList).toEqual('function');
    });
    it('Should fetch business individuals of business user', function () {
      UserDetails.userProfileData = {id: '56497837e4b0290cecbedfca'};
      UsersApiService.getUsers.and.callFake(function () {
        var deferred = $q.defer();
        deferred.resolve({data: fakeResponse.fakeBusinessIndividuals});
        deferred.$promise = deferred.promise;
        return deferred;
      });
      UserDetails.getBusinessIndividualsList();
      $rootScope.$digest();
      expect(UserDetails.businessIndividuals.length).toBeGreaterThan(0);
    });
    it('Should not fetch places when  UsersApiService.getUsers rejected', function () {
      UserDetails.userProfileData = {id: '56497837e4b0290cecbedfca'};
      UsersApiService.getUsers.and.callFake(function () {
        var deferred = $q.defer();
        deferred.reject();
        deferred.$promise = deferred.promise;
        return deferred;
      });
      UserDetails.getBusinessIndividualsList();
      $rootScope.$digest();
      expect(UserDetails.businessIndividuals.length).toEqual(0);
    });
  });

  describe('Function Unit: UserDetails.addBusinessIndividual', function () {
    it('Should be defined and be a function', function () {
      expect(UserDetails.addBusinessIndividual).toBeDefined();
      expect(typeof UserDetails.addBusinessIndividual).toEqual('function');
    });
    it('Should add business individual', function () {
      UserDetails.USER_ROLES_OBJ = fakeResponse.fakeRolesObj;
      UserDetails.userProfileData = {role: 'BUSINESS', businessType: 'PROFIT'};
      UserDetails.businessIndividualUserInfo = {
        firstName: 'user',
        lastName: 'test2',
        email: 'usertest2@gmail.com',
        role: 'BUSINESS_INDIVIDUAL'
      };
      AddEditUserAPIService.addEditUser.and.callFake(function () {
        var deferred = $q.defer();
        deferred.resolve({status: 'SUCCESS', data: fakeResponse.fakeBusinessIndividuals[0]});
        deferred.$promise = deferred.promise;
        return deferred;
      });
      UserDetails.addBusinessIndividual();
      $rootScope.$digest();
      expect(UserDetails.businessIndividuals.length).toBeGreaterThan(0);
    });
    describe('Should not add business individual', function () {
      it('when AddEditUserAPIService.addEditUser rejected', function () {
        UserDetails.USER_ROLES_OBJ = fakeResponse.fakeRolesObj;
        UserDetails.userProfileData = {role: 'BUSINESS', businessType: 'PROFIT'};
        UserDetails.businessIndividualUserInfo = {
          firstName: 'user',
          lastName: 'test2',
          email: 'usertest2@gmail.com',
          role: 'BUSINESS_INDIVIDUAL'
        };
        AddEditUserAPIService.addEditUser.and.callFake(function () {
          var deferred = $q.defer();
          deferred.reject();
          deferred.$promise = deferred.promise;
          return deferred;
        });
        UserDetails.addBusinessIndividual();
        $rootScope.$digest();
        expect(UserDetails.businessIndividuals.length).toEqual(0);
      });
      it('when email is blank', function () {
        UserDetails.USER_ROLES_OBJ = fakeResponse.fakeRolesObj;
        UserDetails.userProfileData = {role: 'BUSINESS', businessType: 'PROFIT'};
        UserDetails.businessIndividualUserInfo = {
          firstName: 'user',
          lastName: 'test2',
          email: '',
          role: 'BUSINESS_INDIVIDUAL'
        };
        UserDetails.addBusinessIndividual();
        $rootScope.$digest();
        expect(UserDetails.businessIndividuals.length).toEqual(0);
      });
      it('when AddEditUserAPIService.addEditUser returns 200 with ERROR status', function () {
        UserDetails.USER_ROLES_OBJ = fakeResponse.fakeRolesObj;
        UserDetails.userProfileData = {role: 'BUSINESS', businessType: 'PROFIT'};
        UserDetails.businessIndividualUserInfo = {
          firstName: 'user',
          lastName: 'test2',
          email: 'usertest2@gmail.com',
          role: 'BUSINESS_INDIVIDUAL'
        };
        AddEditUserAPIService.addEditUser.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve({status: 'INSUFFICIENT_RIGHT'});
          deferred.$promise = deferred.promise;
          return deferred;
        });
        UserDetails.addBusinessIndividual();
        $rootScope.$digest();
        expect(UserDetails.businessIndividuals.length).toEqual(0);
      });
    });
  });

  describe('Function Unit: UserDetails.saveUserDetails ', function () {
    it('Should be defined and be a function', function () {
      expect(UserDetails.saveUserDetails).toBeDefined();
      expect(typeof UserDetails.saveUserDetails).toEqual('function');
    });
    it('Should update regular user', function () {
      UserDetails.USER_ROLES_OBJ = fakeResponse.fakeRolesObj;
      UserDetails.userProfileData = fakeResponse.fakeRegularUser;
      AddEditUserAPIService.addEditUser.and.callFake(function () {
        var deferred = $q.defer();
        deferred.resolve({status: 'SUCCESS'});
        deferred.$promise = deferred.promise;
        return deferred;
      });
      UserDetails.saveUserDetails();
      $rootScope.$digest();
      expect(MessageService.showSuccessfulMessage).toHaveBeenCalledWith(ToastrMessage.USER.UPDATE.SUCCESS);
      expect(LoaderService.Loader.hideSpinner).toHaveBeenCalled();
    });
    it('Should update business user', function () {
      UserDetails.USER_ROLES_OBJ = fakeResponse.fakeRolesObj;
      UserDetails.userProfileData = fakeResponse.fakeBusinessUser;
      UserDetails.contactName = 'test user';
      AddEditUserAPIService.addEditUser.and.callFake(function () {
        var deferred = $q.defer();
        deferred.resolve({status: 'SUCCESS'});
        deferred.$promise = deferred.promise;
        return deferred;
      });
      UserDetails.saveUserDetails();
      $rootScope.$digest();
      expect(MessageService.showSuccessfulMessage).toHaveBeenCalledWith(ToastrMessage.USER.UPDATE.SUCCESS);
      expect(LoaderService.Loader.hideSpinner).toHaveBeenCalled();
    });
    it('Should update user when AddEditUserAPIService.addEditUser rejected', function () {
      UserDetails.USER_ROLES_OBJ = fakeResponse.fakeRolesObj;
      UserDetails.userProfileData = fakeResponse.fakeBusinessUser;
      UserDetails.contactName = 'test user';
      AddEditUserAPIService.addEditUser.and.callFake(function () {
        var deferred = $q.defer();
        deferred.reject();
        deferred.$promise = deferred.promise;
        return deferred;
      });
      UserDetails.saveUserDetails();
      $rootScope.$digest();
      expect(MessageService.showFailedMessage).toHaveBeenCalled();
    });
  });

  describe('Function Unit: UserDetails.isActiveTab', function () {
    it('Should be defined and be a function', function () {
      expect(UserDetails.isActiveTab).toBeDefined();
      expect(typeof UserDetails.isActiveTab).toEqual('function');
    });
    it('Should return true', function () {
      var tabUrl = 'app/userDetails/userEvents.html';
      UserDetails.currentTab = 'app/userDetails/userEvents.html';
      var result = UserDetails.isActiveTab(tabUrl);
      $rootScope.$digest();
      expect(result).toBeTruthy();
    });
    it('Should return false', function () {
      var tabUrl = 'app/userDetails/userEvents.html';
      UserDetails.currentTab = 'app/userDetails/userPlaces.html';
      var result = UserDetails.isActiveTab(tabUrl);
      $rootScope.$digest();
      expect(result).toBeFalsy();
    });
  });

  describe('Function Unit: UserDetails.onClickTab ', function () {
    it('Should be defined and be a function', function () {
      expect(UserDetails.onClickTab).toBeDefined();
      expect(typeof UserDetails.onClickTab).toEqual('function');
    });
    it('Should change value of UserDetails.currentTab', function () {
      var tab = {
        title: 'Events',
        url: 'app/userDetails/userEvents.html'
      };
      UserDetails.currentTab = 'app/userDetails/userPlaces.html';
      UserDetails.onClickTab(tab);
      $rootScope.$digest();
      expect(UserDetails.currentTab).toEqual('app/userDetails/userEvents.html');
    });
  });

  describe('Function Unit: UserDetails.addBusinessContactName ', function () {
    it('Should be defined and be a function', function () {
      expect(UserDetails.addBusinessContactName).toBeDefined();
      expect(typeof UserDetails.addBusinessContactName).toEqual('function');
    });
    it('Should add a business contact name', function () {
      var index = 0;
      UserDetails.contactName = 'Sandy Chhapola';
      UserDetails.userProfileData = {role: 'BUSINESS', contactNames: []};
      UserDetails.USER_ROLES_OBJ = fakeResponse.fakeRolesObj;
      UserDetails.addBusinessContactName();
      $rootScope.$digest();
      expect(UserDetails.userProfileData.contactNames.length).toBeGreaterThan(0);
    });
  });

  describe('Function Unit: UserDetails.removeBusinessContactName ', function () {
    it('Should be defined and be a function', function () {
      expect(UserDetails.removeBusinessContactName).toBeDefined();
      expect(typeof UserDetails.removeBusinessContactName).toEqual('function');
    });
    it('Should remove a business contact name', function () {
      var index = 0;
      UserDetails.userProfileData = {role: 'BUSINESS', contactNames: ['Sandy Chhapola']};
      UserDetails.USER_ROLES_OBJ = fakeResponse.fakeRolesObj;
      UserDetails.removeBusinessContactName(index);
      $rootScope.$digest();
      expect(UserDetails.userProfileData.contactNames.length).toEqual(0);
    });
  });

  describe('Function Unit: UserDetails.init', function () {
    it('Should be defined and be a function', function () {
      expect(UserDetails.init).toBeDefined();
      expect(typeof UserDetails.init).toEqual('function');
    });
    describe('When Success Callbacks called', function () {
      beforeEach(function () {
        EnumsService.getUserRoleEnums.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve(fakeResponse.fakeRoles);
          return deferred.promise;
        });
        EnumsService.getBusinessTypeEnums.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve(fakeResponse.fakeBusinessTypes);
          return deferred.promise;
        });
        EnumsService.getAuthorizationStateEnums.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve(fakeResponse.fakeAuthorizedStates);
          return deferred.promise;
        });
        EventPlaceApiService.getEventsList.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve([]);
          deferred.$promise = deferred.promise;
          return deferred;
        });
      });
      it('Should fetch user data when current user role ADMIN ', function () {
        $stateParams.userId = '5677b0c2e4b02b658d41cb37';
        EventPlaceApiService.getPlacesList.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve([]);
          deferred.$promise = deferred.promise;
          return deferred;
        });
        AuthService.getCurrentUser.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve(fakeResponse.fakeAdminUser);
          return deferred.promise;
        });
        UserDetails.init();
        $rootScope.$digest();
        expect(LoaderService.Loader.hideSpinner).toHaveBeenCalled();
      });
      it('Should fetch user data when current user role BUSINESS having type PROFIT ', function () {
        $stateParams.userId = '56497837e4b0290cecbedfca';
        UserDetails.userProfileData = {id: '56497837e4b0290cecbedfca'};
        UserDetails.BUSINESS_TYPES = null;
        UsersApiService.getUsers.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve([]);
          deferred.$promise = deferred.promise;
          return deferred;
        });
        AuthService.getCurrentUser.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve(fakeResponse.fakeBusinessUser);
          return deferred.promise;
        });
        UserDetails.init();
        $rootScope.$digest();
        expect(UserDetails.BUSINESS_TYPES.length).toBeGreaterThan(0);
        expect(LoaderService.Loader.hideSpinner).toHaveBeenCalled();
      });
      it('Should fetch user data when current user role BUSINESS_INDIVIDUAL ', function () {
        $stateParams.userId = '5677b0c2e4b02b658d41cb37';
        AuthService.getCurrentUser.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve(fakeResponse.fakeBusinessIndividuals[0]);
          return deferred.promise;
        });
        UserDetails.init();
        $rootScope.$digest();
        expect($state.go).toHaveBeenCalledWith('domains');
      });
    });
    describe('When Failure Callbacks called', function () {
      it('UserDetails.USER_ROLES and UserDetails.AUTHORIZATION_STATE should be null', function () {
        EnumsService.getAuthorizationStateEnums.and.callFake(function () {
          var deferred = $q.defer();
          deferred.reject();
          return deferred.promise;
        });
        EnumsService.getUserRoleEnums.and.callFake(function () {
          var deferred = $q.defer();
          deferred.reject();
          return deferred.promise;
        });
        UserDetails.init();
        $rootScope.$digest();
        expect(UserDetails.AUTHORIZATION_STATE).toEqual(null);
        expect(UserDetails.USER_ROLES).toEqual(null);
      });
      it('EnumsService.getBusinessTypeEnums get rejected', function () {
        EnumsService.getBusinessTypeEnums.and.callFake(function () {
          var deferred = $q.defer();
          deferred.reject();
          return deferred.promise;
        });
        EnumsService.getUserRoleEnums.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve(fakeResponse.fakeRoles);
          return deferred.promise;
        });
        EnumsService.getAuthorizationStateEnums.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve(fakeResponse.fakeAuthorizedStates);
          return deferred.promise;
        });
        EventPlaceApiService.getEventsList.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve([]);
          deferred.$promise = deferred.promise;
          return deferred;
        });
        UsersApiService.getUsers.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve([]);
          deferred.$promise = deferred.promise;
          return deferred;
        });
        AuthService.getCurrentUser.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve(fakeResponse.fakeBusinessUser);
          return deferred.promise;
        });
        $stateParams.userId = '56497837e4b0290cecbedfca';
        UserDetails.userProfileData = {id: '56497837e4b0290cecbedfca'};
        UserDetails.BUSINESS_TYPES = null;
        UserDetails.init();
        $rootScope.$digest();
        expect(UserDetails.BUSINESS_TYPES).toEqual(null);
      });
    });
    describe('When UserProfileData is null', function () {
      beforeEach(function () {
        UserDetails = $controller('UserDetailsCtrl', {
          $scope: $scope,
          $state: $state,
          $stateParams: $stateParams,
          $modal: $modal,
          AuthService: AuthService,
          EventPlaceApiService: EventPlaceApiService,
          UsersApiService: UsersApiService,
          UserDetailsService: UserDetailsService,
          AddEditUserAPIService: AddEditUserAPIService,
          DTOptionsBuilder: DTOptionsBuilder,
          DTColumnDefBuilder: DTColumnDefBuilder,
          ToastrMessage: ToastrMessage,
          MessageService: MessageService,
          LoaderService: LoaderService,
          EnumsService: EnumsService,
          UserProfileData: null
        });
      });

      it('Should be redirected to domains state', function () {
        UserDetails.init();
        $rootScope.$digest();
        expect($state.go).toHaveBeenCalledWith('domains');
      });
    });
  });
});