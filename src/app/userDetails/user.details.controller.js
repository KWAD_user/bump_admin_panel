'use strict';

(function (angular) {
  angular.module('kwadWeb')
    .controller('UserDetailsCtrl', ['$q', '$scope', '$state', '$stateParams', '$modal', '$log', 'AuthService', 'DTOptionsBuilder', 'DTColumnDefBuilder', 'EnumsService', 'UserDetailsService', 'UsersApiService', 'AddEditUserAPIService', 'MessageService', 'ToastrMessage', 'UserProfileData', 'EventPlaceApiService', 'LoaderService',
      function ($q, $scope, $state, $stateParams, $modal, $log, AuthService, DTOptionsBuilder, DTColumnDefBuilder, EnumsService, UserDetailsService, UsersApiService, AddEditUserAPIService, MessageService, ToastrMessage, UserProfileData, EventPlaceApiService, LoaderService) {
        // Define variables
        var UserDetails = this;
        UserDetails.userProfileData = null;
        UserDetails.USER_ROLES = null;
        UserDetails.USER_ROLES_OBJ = null;
        UserDetails.BUSINESS_TYPES = null;
        UserDetails.AUTHORIZATION_STATE = null;
        UserDetails.AUTHORIZATION_STATE_OBJ = null;
        UserDetails.dtOptions = DTOptionsBuilder.newOptions().withPaginationType('full_numbers');
        UserDetails.dtColumnDefs = [
          //DTColumnDefBuilder.newColumnDef(0)
          //DTColumnDefBuilder.newColumnDef(1).notVisible(),
          //DTColumnDefBuilder.newColumnDef(2).notSortable()
        ];

        UserDetails.tabs = [{
          title: 'Events',
          url: 'app/userDetails/userEvents.html'
        }, {
          title: 'Places',
          url: 'app/userDetails/userPlaces.html'
        }];
        UserDetails.btabs = [{
          title: 'Events',
          url: 'app/userDetails/userEvents.html'
        }, {
          title: 'Business Individuals',
          url: 'app/userDetails/business-individuals.html'
        }];
        UserDetails.userPlaceList = null;
        UserDetails.userEventList = null;
        UserDetails.businessIndividuals = [];
        UserDetails.currentTab = UserDetails.tabs[0].url;
        UserDetails.contactName = '';

        UserDetails.businessIndividualUserInfo = {
          firstName: '',
          lastName: '',
          email: '',
          role: 'BUSINESS_INDIVIDUAL'
        };

        var userRoleEnumsSuccess = function (userRoles) {
            UserDetails.USER_ROLES = angular.copy(userRoles);
            UserDetails.USER_ROLES_OBJ = userRoles.toObject();
            AuthService.getCurrentUser().then(function (user) {
              LoaderService.Loader.hideSpinner();
              if ((user.id === $stateParams.userId && user.role === UserDetails.USER_ROLES_OBJ.LAUNCHSQUAD) || (user.role === UserDetails.USER_ROLES_OBJ.ADMIN)) {
                UserDetails.getUserPlacesList();
                UserDetails.getUserEventsList();
                if (UserDetails.userProfileData.role === UserDetails.USER_ROLES_OBJ.BUSINESS) {
                  UserDetails.getBusinessIndividualsList();
                }
              } else if (user.role === UserDetails.USER_ROLES_OBJ.EDITOR || (user.id === $stateParams.userId && user.role === UserDetails.USER_ROLES_OBJ.BUSINESS && user.businessType === 'PROFIT')) {
                EnumsService.getBusinessTypeEnums().then(function (result) {
                  UserDetails.BUSINESS_TYPES = result;
                }, function (err) {
                  $log.info('Error: ', err);
                });
                UserDetails.getBusinessIndividualsList();
                UserDetails.getUserEventsList();
              } else {
                $state.go('domains');
              }
            });
          },
          userRoleEnumsFailure = function (error) {
            LoaderService.Loader.hideSpinner();
            $log.info('Error: ', error);
          };

        var authorizationStateEnumsSuccess = function (authorizationStates) {
            UserDetails.AUTHORIZATION_STATE = angular.copy(authorizationStates);
            UserDetails.AUTHORIZATION_STATE_OBJ = authorizationStates.toObject();
            LoaderService.Loader.hideSpinner();
          },
          authorizationStateEnumsFailure = function () {
            LoaderService.Loader.hideSpinner();
          };

        function resetDefaults() {
          UserDetails.businessIndividualUserInfo = {
            firstName: '',
            lastName: '',
            email: '',
            role: UserDetails.USER_ROLES_OBJ.BUSINESS_INDIVIDUAL
          };
        }

        function getPayload(role) {
          var payload = null;
          switch (role) {
            case UserDetails.USER_ROLES_OBJ.REGULAR:
            case UserDetails.USER_ROLES_OBJ.LAUNCHSQUAD:
            case UserDetails.USER_ROLES_OBJ.ADMIN:
            case UserDetails.USER_ROLES_OBJ.EDITOR:
              payload = {
                userId: UserDetails.userProfileData.id,
                email: UserDetails.userProfileData.email ? UserDetails.userProfileData.email : '',
                role: UserDetails.userProfileData.role ? UserDetails.userProfileData.role : '',
                state: UserDetails.userProfileData.state ? UserDetails.userProfileData.state : '',
                firstName: UserDetails.userProfileData.first_name ? UserDetails.userProfileData.first_name : UserDetails.userProfileData.firstName ? UserDetails.userProfileData.firstName : '',
                lastName: UserDetails.userProfileData.last_name ? UserDetails.userProfileData.last_name : UserDetails.userProfileData.lastName ? UserDetails.userProfileData.lastName : '',
                college: UserDetails.userProfileData.college ? UserDetails.userProfileData.college : ''
              };
              return payload;
            case UserDetails.USER_ROLES_OBJ.BUSINESS_INDIVIDUAL:
              payload = {
                role: UserDetails.businessIndividualUserInfo.role,
                associatedBusinessId: UserDetails.userProfileData.id,
                email: UserDetails.businessIndividualUserInfo.email,
                firstName: UserDetails.businessIndividualUserInfo.firstName ? UserDetails.businessIndividualUserInfo.firstName : '',
                lastName: UserDetails.businessIndividualUserInfo.lastName ? UserDetails.businessIndividualUserInfo.lastName : ''
              };
              return payload;
            case UserDetails.USER_ROLES_OBJ.BUSINESS:
              payload = {
                userId: UserDetails.userProfileData.id,
                email: UserDetails.userProfileData.email,
                role: UserDetails.userProfileData.role,
                businessType: UserDetails.userProfileData.type,
                state: UserDetails.userProfileData.state,
                businessName: UserDetails.userProfileData.businessName,
                contactNumber: UserDetails.userProfileData.contactNumber,
                website: UserDetails.userProfileData.website,
                address: UserDetails.userProfileData.address,
                contactNames: UserDetails.userProfileData.contactNames
              };
              if (UserDetails.contactName) {
                payload.contactNames.push(UserDetails.contactName);
                UserDetails.contactName = '';
              }
              return payload;
          }
        }

        function updateUserDetails(payload) {
          var deferred = $q.defer();
          AddEditUserAPIService.addEditUser(payload).$promise.then(function (result) {
            if (result.status === 'SUCCESS') {
              MessageService.showSuccessfulMessage(ToastrMessage.USER.UPDATE[result.status]);
              deferred.resolve(result);
            } else {
              MessageService.showFailedMessage(ToastrMessage.USER.UPDATE[result.status]);
              deferred.reject(result);
            }
          }, function (err) {
            MessageService.showFailedMessage();
            deferred.reject(err);
          });
          return deferred.promise;
        }

        UserDetails.init = function () {
          LoaderService.Loader.showSpinner();
          UserDetails.userProfileData = UserProfileData ? UserProfileData : UserDetailsService.getUserDetails();
          if (!UserDetails.userProfileData) {
            LoaderService.Loader.hideSpinner();
            $state.go('domains');
            return;
          }

          EnumsService.getUserRoleEnums().then(userRoleEnumsSuccess, userRoleEnumsFailure);

          EnumsService.getAuthorizationStateEnums().then(authorizationStateEnumsSuccess, authorizationStateEnumsFailure);
          UserDetails.userProfileData.state = UserDetails.userProfileData.state ? UserDetails.userProfileData.state : UserDetails.userProfileData.status;
        };

        UserDetails.onClickTab = function (tab) {
          UserDetails.currentTab = tab.url;
        };

        UserDetails.addBusinessContactName = function () {
          if (UserDetails.contactName && UserDetails.userProfileData.role === UserDetails.USER_ROLES_OBJ.BUSINESS) {
            UserDetails.userProfileData.contactNames.push(UserDetails.contactName);
            UserDetails.contactName = '';
          }
        };

        UserDetails.removeBusinessContactName = function (index) {
          if (UserDetails.userProfileData.role === UserDetails.USER_ROLES_OBJ.BUSINESS) {
            UserDetails.userProfileData.contactNames.splice(index, 1);
          }
        };

        UserDetails.isActiveTab = function (tabUrl) {
          return tabUrl === UserDetails.currentTab;
        };

        UserDetails.saveUserDetails = function () {
          LoaderService.Loader.showSpinner();
          var payload = getPayload(UserDetails.userProfileData.role),
            success = function () {
              UserDetailsService.updateUsersList(payload);
              LoaderService.Loader.hideSpinner();
            },
            failure = function (err) {
              $log.info('Error: ', err);
              LoaderService.Loader.hideSpinner();
            };
          updateUserDetails(payload).then(success, failure);
        };

        UserDetails.addBusinessIndividual = function () {
          LoaderService.Loader.showSpinner();
          if (!UserDetails.businessIndividualUserInfo.email) {
            MessageService.showFailedMessage(ToastrMessage.USER.ADD.BLANK_EMAIL_FIELD);
            LoaderService.Loader.hideSpinner();
            return;
          }
          var payload = getPayload(UserDetails.businessIndividualUserInfo.role),
            success = function (res) {
              UserDetails.businessIndividuals.push(res.data);
              resetDefaults();
              LoaderService.Loader.hideSpinner();
            },
            failure = function (err) {
              $log.info('Error: ', err);
              LoaderService.Loader.hideSpinner();
            };
          updateUserDetails(payload).then(success, failure);
        };

        UserDetails.getBusinessIndividualsList = function () {
          var payload = {businessId: UserDetails.userProfileData.id},
            success = function (res) {
              UserDetails.businessIndividuals = res.data;
              LoaderService.Loader.hideSpinner();
            },
            failure = function (err) {
              $log.info('Error: ', err);
              LoaderService.Loader.hideSpinner();
            };
          LoaderService.Loader.showSpinner();
          UsersApiService.getUsers(payload).$promise.then(success, failure);
        };

        UserDetails.getUserPlacesList = function () {
          LoaderService.Loader.showSpinner();
          var payload = {
              userId: UserDetails.userProfileData ? UserDetails.userProfileData.id : $stateParams.userId,
              type: 'PLACE',
              query: ''
            },
            success = function (result) {
				result = result.data;
              if (Array.isArray(result) && result.length > 0) {
                UserDetails.userPlaceList = result;
              }
              LoaderService.Loader.hideSpinner();
            },
            failure = function () {
              LoaderService.Loader.hideSpinner();
              MessageService.showFailedMessage();
            };

          EventPlaceApiService.getPlacesList(payload).$promise.then(success, failure);
        };

        UserDetails.getUserEventsList = function () {
          LoaderService.Loader.showSpinner();
          var payload = {
              userId: UserDetails.userProfileData ? UserDetails.userProfileData.id : $stateParams.userId,
              type: 'EVENT',
              query: ''
            },
            success = function (result) {
				result = result.data;
              if (Array.isArray(result) && result.length > 0) {
                UserDetails.userEventList = result;
              }
              LoaderService.Loader.hideSpinner();
            },
            failure = function () {
              MessageService.showFailedMessage();
              LoaderService.Loader.hideSpinner();
            };

          EventPlaceApiService.getEventsList(payload).$promise.then(success, failure);
        };

        UserDetails.backToUserList = function () {
          switch (UserDetails.userProfileData.role) {
            case UserDetails.USER_ROLES_OBJ.REGULAR:
              $state.go('users.regular');
              break;
            case UserDetails.USER_ROLES_OBJ.LAUNCHSQUAD:
              $state.go('users.bump');
              break;
            case UserDetails.USER_ROLES_OBJ.ADMIN:
              $state.go('users.admin');
              break;
            case UserDetails.USER_ROLES_OBJ.EDITOR:
              $state.go('users.editor');
              break;
            case UserDetails.USER_ROLES_OBJ.BUSINESS:
              $state.go('users.business');
              break;
            default :
              $state.go('users.regular');
              break;
          }
        };

        UserDetails.resetPassword = function (individual) {

          var modalInstance = $modal.open({
            templateUrl: 'components/modals/resetPassword/resetPassword.html',
            controllerAs: 'ResetPasswordModal',
            controller: 'ResetPasswordModalCtrl',
            size: 'md',
            resolve: {
              UserInfo: function () {
                return {id: individual ? individual.id : UserDetails.userProfileData.id};
              }
            }
          });

          modalInstance.result.then(function (data) {
            $log.info(data);
          }, function () {
            $log.info('Modal dismissed at: ' + new Date());
          });
        };

        UserDetails.editEventDetails = function (data) {
          $state.go('eventDetails', {eventId: data.epId});
        };

        UserDetails.editPlaceDetails = function (data) {
          $state.go('placeDetails', {placeId: data.epId});
        };

        $scope.$emit('HIDE_ACTION_BAR');
      }
    ]);
})(angular);
