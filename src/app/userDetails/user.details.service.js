'use strict';

(function (angular) {
  angular.module('kwadWeb')
    .factory('UserDetailsService', [function () {
      var _userList = null, _currentUserFilter = '', _user = '';

      var addNewUserToUsersList = function (payload) {
        if (_userList && payload.role === _currentUserFilter) {
          _userList.push(payload);
        }
      };
      var removeUserFromUsersList = function (index) {
        if (_userList) {
          _userList.splice(index, 1);
        }
      };
      var updateUsersList = function (payload) {
        if (_user && _user.index && _userList && _user.data.id === payload.userId) {
          if (payload.role === _currentUserFilter) {
            _user.data.email = payload.email;
            _user.data.role = payload.role;
            _user.data.status = payload.state;
            _user.data.firstName = payload.firstName;
            _user.data.lastName = payload.lastName;

            _userList.splice(_user.index, 1, _user.data);
          }
          else {
            _userList.splice(_user.index, 1);
            _user = '';
          }
        }
      };
      var getUserDetails = function () {
        return _user.data;
      };
      var setUserDetails = function (user) {
        _user = user;
      };
      var getUsersList = function () {
        return _userList;
      };
      var getCurrentUserFilter = function () {
        return _currentUserFilter;
      };
      var setUsersList = function (userList) {
        _userList = userList;
      };
      var setCurrentUserFilter = function (currentUserFilter) {
        _currentUserFilter = currentUserFilter;
      };
      return {
        addNewUserToUsersList: addNewUserToUsersList,
        removeUserFromUsersList: removeUserFromUsersList,
        updateUsersList: updateUsersList,
        getUsersList: getUsersList,
        getUserDetails: getUserDetails,
        getCurrentUserFilter: getCurrentUserFilter,
        setUsersList: setUsersList,
        setUserDetails: setUserDetails,
        setCurrentUserFilter: setCurrentUserFilter
      };
    }]);
})(angular);