describe('Unit: index.js', function () {
  describe('Unit: app routes', function () {
    beforeEach(module('kwadWeb'));
    var location, $state, rootScope, $httpBackend;
    beforeEach(inject(function (_$location_, _$state_, _$rootScope_, _$httpBackend_) {
      location = _$location_;
      $state = _$state_;
      $httpBackend = _$httpBackend_;
      rootScope = _$rootScope_;
    }));

    describe('Home route', function () {
      beforeEach(function () {
        $httpBackend.expectGET('app/login/login.html')
          .respond(200);
      });

      it('should load the login page on successful load of location path /', function () {
        location.path('/');
        $httpBackend.flush();
        rootScope.$digest();
        expect($state.current.controller).toBe('LoginCtrl')
      });
    });
  });
});