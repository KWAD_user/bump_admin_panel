'use strict';
(function (angular) {
  angular.module('kwadWeb')
    .directive('datePickerStartDate', [function () {
      return {
        scope: {dateDefault: '=', 'onDateChange': '='},
        restrict: 'EA',
        link: function (scope, element) {
          var _minDate = new Date(scope.dateDefault);
          _minDate.setMilliseconds(0);
          _minDate.setSeconds(0);
          _minDate.setMinutes(0);
          _minDate.setHours(0);

          element.datetimepicker({
            defaultDate: scope.dateDefault,
            ignoreReadonly: true,
            format: 'MM/DD/YYYY'
          });
          element.data('DateTimePicker').minDate(_minDate);
          element.bind('dp.change', function (e) {
            scope.onDateChange(e.date.toString());
          });
        }
      };
    }])
    .directive('datePickerStartTime', [function () {
      return {
        scope: {dateDefault: '=', 'onTimeChange': '='},
        restrict: 'EA',
        link: function (scope, element) {
          element.datetimepicker({
            defaultDate: scope.dateDefault,
            ignoreReadonly: true,
            format: 'LT'
          });
          element.bind('dp.change', function (e) {
            scope.onTimeChange(e.date.toString());
          });
        }
      };
    }])
    .directive('datePickerEndDate', [function () {
      return {
        scope: {dateDefault: '=', onDateChange: '='},
        restrict: 'EA',
        link: function (scope, element) {
          var _minDate = new Date(scope.dateDefault);
          _minDate.setMilliseconds(0);
          _minDate.setSeconds(0);
          _minDate.setMinutes(0);
          _minDate.setHours(0);

          element.datetimepicker({
            defaultDate: scope.dateDefault,
            ignoreReadonly: true,
            format: 'MM/DD/YYYY'
          });
          element.data('DateTimePicker').minDate(_minDate);
          element.bind('dp.change', function (e) {
            scope.onDateChange(e.date.toString());
          });
        }
      };
    }])
    .directive('datePickerEndTime', [function () {
      return {
        scope: {dateDefault: '=', onTimeChange: '='},
        restrict: 'EA',
        link: function (scope, element) {
          element.datetimepicker({
            defaultDate: scope.dateDefault,
            ignoreReadonly: true,
            format: 'LT'
          });
          element.bind('dp.change', function (e) {
            scope.onTimeChange(e.date.toString());
          });
        }
      };
    }])
    .directive('datePickerRecurrenceDate', [function () {
      return {
        scope: {dateDefault: '=', onDateChange: '='},
        restrict: 'EA',
        link: function (scope, element) {
          var _minDate = new Date(scope.dateDefault);
          _minDate.setMilliseconds(0);
          _minDate.setSeconds(0);
          _minDate.setMinutes(0);
          _minDate.setHours(0);
          element.datetimepicker({
            defaultDate: scope.dateDefault,
            ignoreReadonly: true,
            format: 'MM/DD/YYYY'
          });
          element.data('DateTimePicker').minDate(_minDate);
          element.bind('dp.change', function (e) {
            scope.onDateChange(e.date.toString());
          });
        }
      };
    }]);
})(angular);