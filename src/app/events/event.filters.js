'use strict';

/**
 * @ngdoc function
 * @name kwadWeb.Filter:filterEventList filter
 * @description
 * # used to filter events by current status
 */
(function (angular) {
  angular.module('kwadWeb')
    .filter('filterEventList', [function () {
      return function (EventList, type) {
        if (type === 'ALL') {
          return EventList;
        }

        var output = [];
        if (Array.isArray(EventList) && EventList.length) {
          output = EventList.filter(function (obj) {
            return (obj.eventTimeLine === type);
          });
        }
        return output;
      };
    }]);
})(angular);
