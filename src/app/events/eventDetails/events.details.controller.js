'use strict';

(function (angular) {
    angular.module('kwadWeb')
            .controller('EventDetailsCtrl', ['$filter', '$modal', '$log', 'AuthService', 'UsersApiService', '$state', 'EventData', 'EventAddEditApiService', 'AddAssetsService', 'FetchAssetsService', 'DeleteAssetsService', 'AWSUploadService', 'EnumsService', 'EventPlaceApiService', 'LoaderService', 'MessageService', 'ToastrMessage', 'AWS_UPLOAD',
                function ($filter, $modal, $log, AuthService, UsersApiService, $state, EventData, EventAddEditApiService, AddAssetsService, FetchAssetsService, DeleteAssetsService, AWSUploadService, EnumsService, EventPlaceApiService, LoaderService, MessageService, ToastrMessage, AWS_UPLOAD) {
                    // Define variables
                    var EventDetails = this;
                    EventDetails.currentObject = {};
                    EventDetails.event = EventData;
                    EventDetails.EP_STATES = null;
                    EventDetails.EP_TYPES = null;
                    EventDetails.EVENT_CATEGORIES = null;
                    EventDetails.ASSET_TYPES = EnumsService.getAssetTypeEnums();
                    EventDetails.selectedFiles = '';
                    EventDetails.photos = [];
                    EventDetails.USER_ROLES = null;
                    EventDetails.USER_ROLES_OBJ = null;
                    EventDetails.EVENT_CATEGORY_TYPES = null;
                    EventDetails.currentObject.status = EventDetails.event.disabled ? 'DISABLED' : 'ENABLED';
                    EventDetails.currentObject.categoryType = EventDetails.event.private ? 'PRIVATE' : 'PUBLIC';
                    EventDetails.currentObject.editAll = false;
                    EventDetails.event.keywords = EventDetails.event.keywords ? EventDetails.event.keywords : [];

                    var isStartDateChanged = false;
                    var isEndDateChanged = false;
                    var creatorName = EventDetails.event.creator_name;
                    var eventName = EventDetails.event.place_name;

                    EventDetails.currentObject.startDate = new Date(EventDetails.event.start_time.split('+')[0]);
                    EventDetails.currentObject.endDate = new Date(EventDetails.event.end_time.split('+')[0]);

                    var _successAddEvent = function (response) {
                        LoaderService.Loader.hideSpinner();
                        if (response.status === 'SUCCESS') {
                            if (EventDetails.event.banner_asset !== response.data.banner_asset) {
                                EventDetails.event.banner_asset = response.data.banner_asset;
                            }
                            MessageService.showSuccessfulMessage(ToastrMessage.EVENT.UPDATE[response.status]);
                        } else {
                            MessageService.showFailedMessage(ToastrMessage.EVENT.UPDATE[response.status]);
                        }
                    };
                    var _failAddEvent = function (err) {
                        LoaderService.Loader.hideSpinner();
                        MessageService.showFailedMessage();
                    };

                    var userRoleEnumsSuccess = function (userRoles) {
                        EventDetails.USER_ROLES = angular.copy(userRoles);
                        EventDetails.USER_ROLES_OBJ = userRoles.toObject();
                        AuthService.getCurrentUser().then(function (user) {
                            EventDetails.currentUser = user;
                            switch (EventDetails.currentUser.role) {
                                case EventDetails.USER_ROLES_OBJ.ADMIN:
                                case EventDetails.USER_ROLES_OBJ.LAUNCHSQUAD:
                                    EventDetails.EVENT_CATEGORY_TYPES = ['PRIVATE', 'PUBLIC'];
                                    break;
                                case EventDetails.USER_ROLES_OBJ.REGULAR:
                                    EventDetails.EVENT_CATEGORY_TYPES = null;
                                    break;
                                default:
                                    EventDetails.EVENT_CATEGORY_TYPES = ['PUBLIC'];
                                    break;
                            }
                        }, function (err) {
                            EventDetails.currentUser = null;
                            MessageService.showFailedMessage();
                        });
                    };
                    var userRoleEnumsFailure = function () {
                        MessageService.showFailedMessage();
                        LoaderService.Loader.hideSpinner();
                    };

                    function getAssetType(type) {
                        switch (type) {
                            case'image/jpg':
                                return EventDetails.ASSET_TYPES.IMAGE_JPG;
                            case'image/jpeg':
                                return EventDetails.ASSET_TYPES.IMAGE_JPEG;
                            case'video/*':
                                return EventDetails.ASSET_TYPES.VIDEO;
                            default :
                                return false;
                        }
                    }

                    function uploadFile(file, AWSObj) {
                        LoaderService.Loader.showSpinner();
                        var options = {quality: 72};
                        AWSUploadService.uploadFile(file, AWSObj, options).then(function (response) {
                            var asset = {url: response.Location, id: AWSObj.asset_id};
                            LoaderService.Loader.hideSpinner();
                            if (EventDetails.photos.length === 0) {
                                EventDetails.event.banner_asset = asset.id;
                                EventDetails.addBannerImage(asset);
                            }
                            EventDetails.photos.push(asset);
                            if (!EventDetails.event.photos) {
                                EventDetails.event.photos = [];
                            }
                            EventDetails.event.photos.push(AWSObj.asset_id);
                            MessageService.showSuccessfulMessage(ToastrMessage.UPLOAD.SUCCESS);
                        }, function (err) {
                            LoaderService.Loader.hideSpinner();
                            MessageService.showFailedMessage();
                        });
                    }

                    function fetchAsset(AWSObj) {                        
                        if (Array.isArray(AWSObj) && AWSObj.length) {
                            AWSObj.forEach(function (obj) {
                                var bucketName = obj.s3_bucket;//'dev-web.whatsbumpin.net'
                                var IMAGE_URL = '';
                                
                                IMAGE_URL = AWS_UPLOAD.URL + bucketName + '/' + obj.s3_object;
                                EventDetails.photos.push({url: IMAGE_URL, id: obj.prefix.split('/')[2]});
                            });
                        } else {
                            MessageService.showFailedMessage();
                        }
                    }

                    function getPhotosUrls(photos) {
                        if (!Array.isArray(photos) || !photos.length) {
                            return false;
                        }
                        var chunks = photos.chunks(10);
                        chunks.forEach(function (chunk) {
                            EventDetails.fetchAssets(chunk);
                        });
                    }

                    EventDetails.init = function () {
                        EnumsService.getUserRoleEnums().then(userRoleEnumsSuccess, userRoleEnumsFailure);
                        EnumsService.getEpStateEnums().then(function (result) {
                            EventDetails.EP_STATES = result;
                        }, function (error) {
                            console.log('EP_STATES ERROR: ', error);
                        });
                        EnumsService.getEpTypeEnums().then(function (result) {
                            EventDetails.EP_TYPES = result;
                        }, function (error) {
                            console.log('EP_TYPES ERROR: ', error);
                        });
                        EnumsService.getEventCategoryEnums().then(function (result) {
                            EventDetails.EVENT_CATEGORIES = [];
                            Object.keys(result).forEach(function (key) {
                                EventDetails.EVENT_CATEGORIES.push({
                                    id: key, value: result[key]
                                })
                            });
                            EventDetails.currentObject.category = {
                                id: EventDetails.event.cat_id, value: result[EventDetails.event.cat_id]
                            };
                        }, function (error) {
                            EventDetails.EVENT_CATEGORIES = null;
                            console.log('Event Category ERROR: ', error);
                        });
                        getPhotosUrls(EventDetails.event.photos);
                    };

                    EventDetails.addNewKeyword = function () {
                        if (EventDetails.newTag) {
                            EventDetails.event.keywords.push(EventDetails.newTag);
                            EventDetails.newTag = '';
                        }
                    };

                    EventDetails.removeKeyword = function (index) {
                        EventDetails.event.keywords.splice(index, 1);
                    };

                    EventDetails.searchPlaces = function () {
                        LoaderService.Loader.showSpinner();
                        var _successGetPlaces = function (response) {
                            LoaderService.Loader.hideSpinner();
                            var modalInstance = $modal.open({
                                templateUrl: 'components/modals/selectPlace/selectPlaceContent.html',
                                controllerAs: 'SelectPlaceModal',
                                controller: 'SelectPlaceModalCtrl',
                                size: 'md',
                                resolve: {
                                    PlaceResult: function () {
                                        return response.data;
                                    }
                                }
                            });
                            modalInstance.result.then(function (place) {
                                $log.info('saved place info: ', place);
                                EventDetails.event.place_name = place.name;
                                EventDetails.newPlaceID = place.epId;
                            }, function () {
                                EventDetails.event.place_name = eventName;
                                $log.info('Modal dismissed at: ' + new Date());
                            });
                        };
                        var _failGetPlaces = function (err) {
                            LoaderService.Loader.hideSpinner();
                            console.log(err);
                        };
                        var _payloadPlace = {
                            'type': 'PLACE',
                            'query': EventDetails.event.place_name
                        };
                        EventPlaceApiService.getPlacesList(_payloadPlace).$promise.then(_successGetPlaces, _failGetPlaces);
                    };

                    EventDetails.updateEvent = function () {
                        LoaderService.Loader.showSpinner();
                        var start_date = '';
                        var end_date = '';

                        var _payload = {
                            category_id: EventDetails.currentObject.category.id,
                            creatorId: EventDetails.event.creator_id,
                            e_id: EventDetails.event.ep_id,
                            title: EventDetails.event.title,
                            description: EventDetails.event.description,
                            keywords: EventDetails.event.keywords,
                            private: EventDetails.currentObject.categoryType === 'PRIVATE',
                            disabled: EventDetails.currentObject.status === 'DISABLED',
                            edit_all: EventDetails.currentObject.editAll
                        };

                        if (EventDetails.newPlaceID) {
                            _payload.p_id = EventDetails.newPlaceID;
                        }

                        if (isStartDateChanged) {
                            start_date = $filter('date')(EventDetails.currentObject.startDate, 'yyyy-MM-ddTHH:mm:ss.sssZ');
                            if (new Date(start_date.split('+')[0]) == 'Invalid Date') {
                                MessageService.showFailedMessage(ToastrMessage.DATE_VALIDATE.INVALID_START_DATE);
                                LoaderService.Loader.hideSpinner();
                                return;
                            }
                            _payload.start_time = start_date;
                        }

                        if (isEndDateChanged) {
                            end_date = $filter('date')(EventDetails.currentObject.endDate, 'yyyy-MM-ddTHH:mm:ss.sssZ');
                            if (new Date(end_date.split('+')[0]) == 'Invalid Date') {
                                MessageService.showFailedMessage(ToastrMessage.DATE_VALIDATE.INVALID_END_DATE);
                                LoaderService.Loader.hideSpinner();
                                return;
                            }
                            _payload.end_time = end_date;
                        }

                        if (start_date && end_date && start_date > end_date) {
                            MessageService.showFailedMessage(ToastrMessage.DATE_VALIDATE.START_DATE_ERROR);
                            LoaderService.Loader.hideSpinner();
                            return;
                        }

                        EventAddEditApiService.addEditEvent(_payload).$promise.then(_successAddEvent, _failAddEvent);
                    };

                    EventDetails.CreateNewEventBasedOnCurrent = function () {
                        $state.go('addEventsFromCurrent', {eventId: EventDetails.event.ep_id});
                    };

                    EventDetails.uploadFile = function () {
                        LoaderService.Loader.showSpinner();

                        if (!EventDetails.selectedFiles || !EventDetails.selectedFiles.length) {
                            LoaderService.Loader.hideSpinner();
                            MessageService.showFailedMessage(ToastrMessage.UPLOAD.FILE_NOT_FOUND);
                            return false;
                        }

                        if (EventDetails.selectedFiles.length > 10) {
                            LoaderService.Loader.hideSpinner();
                            MessageService.showFailedMessage(ToastrMessage.UPLOAD.MAX_FILES);
                            return false;
                        }

                        var _payload = [],
                                success = function (response) {
                                    LoaderService.Loader.hideSpinner();
                                    var AWSObjs = response.data && response.data.length ? response.data : false;
                                    for (var j = 0; j < AWSObjs.length; j++) {
                                        uploadFile(EventDetails.selectedFiles[j], AWSObjs[j]);
                                    }
                                },
                                failure = function (err) {
                                    LoaderService.Loader.hideSpinner();
                                    MessageService.showFailedMessage();
                                };

                        for (var i = 0; i < EventDetails.selectedFiles.length; i++) {
                            var file = EventDetails.selectedFiles[i];
                            _payload.push({
                                file: file.name,
                                type: getAssetType(file.type),
                                ep_id: EventDetails.event.ep_id,
                                add_all: false
                            });
                        }

                        AddAssetsService.addAsset(_payload).$promise.then(success, failure);
                    };

                    EventDetails.fetchAssets = function (asset_ids) {
                        LoaderService.Loader.showSpinner();
                        var _payload = '',
                                success = function (response) {
                                    LoaderService.Loader.hideSpinner();
                                    var AWSObj = response.data ? response.data : response;
                                    fetchAsset(AWSObj);
                                },
                                failure = function (err) {
                                    LoaderService.Loader.hideSpinner();
                                    MessageService.showFailedMessage();
                                };
                        if (typeof asset_ids === 'string') {
                            _payload = {"asset_ids":[asset_ids]};
                        } else if (Array.isArray(asset_ids) && asset_ids.length) {                            
                            _payload = {"asset_ids":asset_ids};                            
                        } else {
                            LoaderService.Loader.hideSpinner();
                            MessageService.showFailedMessage(ToastrMessage.ASSET.ASSET_ID_NOT_FOUND);
                            return false;
                        }
                        FetchAssetsService.fetchAsset(_payload).$promise.then(success, failure);
                    };

                    EventDetails.addBannerImage = function (asset) {
                        LoaderService.Loader.showSpinner();
                        if (!asset || !asset.id) {
                            LoaderService.Loader.hideSpinner();
                            MessageService.showFailedMessage(ToastrMessage.ASSET.ASSET_ID_NOT_FOUND);
                            return false;
                        }
                        var _payload = {
                            e_id: EventDetails.event.ep_id,
                            banner_asset: asset.id
                        };
                        EventAddEditApiService.addEditEvent(_payload).$promise.then(_successAddEvent, _failAddEvent);
                    };

                    EventDetails.removeBannerImage = function (e, asset) {
                        e.stopPropagation();
                        LoaderService.Loader.showSpinner();
                        var _payload = [],
                                success = function (response) {
                                    LoaderService.Loader.hideSpinner();
                                    if (response.status === 'SUCCESS') {
                                        EventDetails.photos = EventDetails.photos.filter(function (el) {
                                            return el.id !== response.data[0].asset_id;
                                        });
                                        MessageService.showSuccessfulMessage(ToastrMessage.ASSET.REMOVED_SUCCESSFULLY);
                                    } else {
                                        MessageService.showFailedMessage();
                                    }
                                },
                                failure = function (err) {
                                    LoaderService.Loader.hideSpinner();
                                    MessageService.showFailedMessage();
                                };

                        if (asset.id && typeof asset.id === 'string') {                            
                            _payload.push({
                                asset_id: asset.id,
                                associated_event_place_id: EventDetails.event.ep_id,
                                delete_all: true
                            });                            
                        } else {
                            LoaderService.Loader.hideSpinner();
                            MessageService.showFailedMessage(ToastrMessage.ASSET.ASSET_ID_NOT_FOUND);
                            return false;
                        }
                        DeleteAssetsService.removeAsset(_payload).$promise.then(success, failure);
                    };

                    EventDetails.isBannerImage = function (image) {
                        return image.id === EventDetails.event.banner_asset;
                    };

                    EventDetails.changeStartDate = function (dateString) {
                        var newDate = new Date(dateString);
                        EventDetails.currentObject.startDate.setDate(newDate.getDate());
                        EventDetails.currentObject.startDate.setMonth(newDate.getMonth());
                        EventDetails.currentObject.startDate.setFullYear(newDate.getFullYear());
                        isStartDateChanged = true;
                    };

                    EventDetails.changeStartTime = function (dateString) {
                        var newDate = new Date(dateString);
                        EventDetails.currentObject.startDate.setHours(newDate.getHours());
                        EventDetails.currentObject.startDate.setMinutes(newDate.getMinutes());
                        isStartDateChanged = true
                    };

                    EventDetails.changeEndDate = function (dateString) {
                        var newDate = new Date(dateString);
                        EventDetails.currentObject.endDate.setDate(newDate.getDate());
                        EventDetails.currentObject.endDate.setMonth(newDate.getMonth());
                        EventDetails.currentObject.endDate.setFullYear(newDate.getFullYear());
                        isEndDateChanged = true;
                    };

                    EventDetails.changeEndTime = function (dateString) {
                        var newDate = new Date(dateString);
                        EventDetails.currentObject.endDate.setHours(newDate.getHours());
                        EventDetails.currentObject.endDate.setMinutes(newDate.getMinutes());
                        isEndDateChanged = true;
                    };

                    EventDetails.searchCreators = function () {
                        LoaderService.Loader.showSpinner();
                        var _successUsers = function (response) {
                            LoaderService.Loader.hideSpinner();
                            var modalInstance = $modal.open({
                                templateUrl: 'components/modals/selectCreator/selectCreatorContent.html',
                                controllerAs: 'SelectCreatorModal',
                                controller: 'SelectCreatorModalCtrl',
                                size: 'md',
                                resolve: {
                                    CreatorList: function () {
                                        return response.data;
                                    }
                                }
                            });
                            modalInstance.result.then(function (user) {
                                EventDetails.event.creator_name = user.fullName;
                                EventDetails.event.creator_id = user.id;
                            }, function () {
                                EventDetails.event.creator_name = creatorName;
                                $log.info('Modal dismissed at: ' + new Date());
                            });
                        };
                        var _failUsers = function (err) {
                            LoaderService.Loader.hideSpinner();
                            console.log(err);
                        };
                        var _payload = {
                            query: EventDetails.event.creator_name
                        };
                        UsersApiService.getUsers(_payload).$promise.then(_successUsers, _failUsers);
                    }
                }]);
})(angular);
