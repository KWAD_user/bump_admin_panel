'use strict';

describe('Unit: EventDetailsCtrl Controller', function () {
  var EventDetails,
    $rootScope,
    $scope,
    $controller,
    $state,
    $filter,
    $modal,
    $q,
    $httpBackend,
    AuthService,
    FetchAssetsService,
    AddAssetsService,
    DeleteAssetsService,
    EventAddEditApiService,
    EventPlaceApiService,
    AWSUploadService,
    ToastrMessage,
    UsersApiService,
    MessageService,
    LoaderService = {Loader: {}},
    EnumsService,
    fakeResponse,
    EventData;

  beforeAll(function () {
    fakeResponse = {
      fakeRoles: ['ANONYMOUS', 'REGULAR', 'LAUNCHSQUAD', 'ADMIN', 'BUSINESS', 'BUSINESS_INDIVIDUAL', 'EDITOR'],
      fakeRolesObj: {
        ANONYMOUS: 'ANONYMOUS',
        REGULAR: 'REGULAR',
        LAUNCHSQUAD: 'LAUNCHSQUAD',
        ADMIN: 'ADMIN',
        BUSINESS: 'BUSINESS',
        BUSINESS_INDIVIDUAL: 'BUSINESS_INDIVIDUAL',
        EDITOR: 'EDITOR'
      },
      fakeRecurrenceType: ['DAILY', 'WEEKLY', 'TWO_WEEKLY', 'MONTHLY', 'YEARLY', 'NEVER'],
      fakeEpTypes: ['BUMP', 'NON_BUMP', 'SPONSORED'],
      fakeEpStateEnums: ['ENABLED', 'DISABLED'],
      fakeEventCategories: {
        '54b83a2be4b06b211e892748': 'Music',
        '54b83a2be4b06b211e892750': 'Group',
        '54b83a2be4b06b211e89274a': 'Sports',
        '54b83a2be4b06b211e892746': 'Bar/Club',
        '54b83a2be4b06b211e892752': 'Outdoors',
        '54b83a2be4b06b211e892744': 'Coffee',
        '54b83a2be4b06b211e892754': 'General',
        '54b83a2be4b06b211e89274e': 'Party',
        '54b83a2be4b06b211e892742': 'Food',
        '54b83a2be4b06b211e89274c': 'School'
      },
      fakeFetchedAsset: {
        aws_key: 'ASIAIG63HOWFIRIEQCJQ',
        aws_secret: 'z3ZnvoIHEBVFe21GuiqxOLuJ+CSIBlguUbMu5c2h',
        aws_token: 'AQoDYXdzEEYasAODTH4AOcylcz+EkZt4QEaYlmOiUe/e7HfuIrVhdnLET3djkip1VXCM571/ipl1qe8gt7Kek2JCWNJFCPlAIn7gkWFIb3qNpHip/yhCXZedyFiiaQGhfcWPhhbNFL+yL+vX3BpLO3Vu4+a2p/4V+egycRQp60QPmpgH0KQAqHDCnBf4bVYBf+bSJIQGM09WpwaSOAaiZ2UZdeqAC39CLdwLlqe85NJ0kMT4B3SJCLKmxbeUaZQtV8P9LXQygNoAlqmNA6S38HFtByRrU32wiSTVeGXDiUTRxJmdcYKgUNZQMsaRTuj2llwN5kZLL1SoBpO8Sw6XFBIDOgFWIf0VkAQJOH3RUg7T/udeyvOqNVNhPelUBraz8+IxeYoxjEThEXDfTzMkelrhqpT2DNCLWmCtMlJst505QH5pkQgq6CxVQTA7504nTbQ9iKphtttgDgHcS6zwsTsa0F6rJOV3SH/rR857NT83ImXw3kGw+0edJL1IAFnEIc+H7WBjvZhx6pokP+jEgakL6n/o3fCm6/bNmsxozNonPQeUG6zrgkdTK+MfJ1TD2oYXg3fghDpXDMsgjqDAswU=',
        prefixes: ['/5592497703cef8958d68d1a5/56559821e4b0da91f9e72234'],
        s3_bucket: 'kwad-bump-assets'
      },
      fakeAddedAsset: {
        asset_id: '5672a991e4b02b658c9278dd',
        aws_key: 'ASIAJ64GQOFRALJOBWQA',
        aws_secret: 'fqN3PS7ao5e2aj38g3CYhVTXCn/R7VLXwTtXCthD',
        aws_token: 'AQoDYXdzEHYakAO+mVAbXL6gqWKmhtyx4IBROjvkzp3Zv2l1BcCVMI5BJ3ONeWhCzcaFp4gHv+72s4/UHUnMZjunMCvACoy/UZt90RnFdAUr6ly2UZofxNLOxcZLHb7kXikIsFocOAF72/KBRfEv+ntNuhaF0yJPVb4nWV1kdOJ0+7ZCLm1Ulz6AvdXrZIenhNxMl4jMRn5MctzXyq1mFNHlM1nyqsOrIiYstIiDv/Mdy4mKTsgqUBIxe2dKKXxvfiNx60FZfUNYr2M70j97+6TbJ9sfHD4Xh9vY9OiMZ+3zDpeX1NdL2BUjHHsyJlSnQVN2Q7U7DFatFOUqDnKOeChHybj8tRe4l2/po8mdGO4b3kQQvJ75VX2pPakIQ2Khn0x4tPRDBxsOzL9vlFwrzX1ehb5XAI4lWKnhaOXLvuFAAYkqzs2evFYShjnyVUnV/sr7cQ3w/nNrjbb2HZVueJzMzuwjnj06+h0QLthi8b/uNyat0A3Xl9IQEEEI/16dy5zNV+dZtDOD+w5Ynjt20Eu81GodKsAq0MxcIJHTyrMF',
        s3_bucket: 'kwad-bump-assets',
        s3_object: '/5592497703cef8958d68d1a5/5672a991e4b02b658c9278dd/raw_upload.jpeg'
      },
      fakeImage: {
        id: '56559821e4b0da91f9e72234',
        url: '/5592497703cef8958d68d1a5/56559821e4b0da91f9e72234/raw_upload'
      },
      fakeRemovedAsset: {
        asset_ids: ['56559821e4b0da91f9e72234']
      },
      fackEvent: {
        stopPropagation: function () {
        }
      },
      fakeAdminUser: {
        attributes: [],
        college: '',
        first_name: 'admin',
        full_name: 'admin',
        last_name: 'admin',
        role: 'ADMIN',
        state: 'ENABLED'
      },
      fakeLaunchsquadUser: {
        attributes: [],
        college: '',
        first_name: 'bump',
        full_name: 'bump user',
        last_name: 'user',
        role: 'LAUNCHSQUAD',
        state: 'ENABLED'
      },
      fakeRegularUser: {
        attributes: [],
        college: '',
        first_name: 'regular',
        full_name: 'regular user',
        last_name: 'user',
        role: 'REGULAR',
        state: 'ENABLED'
      },
      fakeEditorUser: {
        attributes: [],
        college: '',
        first_name: 'editor',
        full_name: 'editor user',
        last_name: 'user',
        role: 'EDITOR',
        state: 'ENABLED'
      },
      fakeAddBannerImage: {
        status: 'SUCCESS',
        data: {ep_id: '55e78700e4b016c1bcd784ab', banner_asset: '56559821e4b0da91f9e72234'}
      },
      fakePlaceList: [{
        'epId': '546aff5be4b0370c32328fbb',
        'name': 'Phoenix Rock Gym',
        'place': '1353 E University Drive, Tempe AZ',
        'creatorName': 'Demo  User ',
        'creatorId': '546aff58e4b0370c32328f40',
        'feedImpression': 0,
        'detailImpression': 0,
        'appCheckinCount': 26,
        'geoCheckinCount': 8,
        'shares': 26,
        'status': 'ENABLED',
        'associatedEvents': 2,
        'creatorRole': 'ADMIN',
        'eventDate': null,
        'eventTimeLine': 'UNDETERMINED'
      }, {
        'epId': '546aff5be4b0370c32328fbf',
        'name': 'The Vue on Apache',
        'place': '922 E Apache Blvd, Tempe AZ',
        'creatorName': 'Brandy Smith',
        'creatorId': '546aff59e4b0370c32328f43',
        'feedImpression': 6,
        'detailImpression': 0,
        'appCheckinCount': 24,
        'geoCheckinCount': 283,
        'shares': 24,
        'status': 'ENABLED',
        'associatedEvents': 0,
        'creatorRole': 'BUMP',
        'eventDate': null,
        'eventTimeLine': 'UNDETERMINED'
      }],
      fakeJpegFile: [{
        lastModified: 1434780236336,
        lastModifiedDate: 'Sat Jun 18 2015 18:38:16 GMT+0530 (IST)',
        name: 'image.jpeg',
        size: 2852093,
        type: 'image/jpeg',
        webkitRelativePath: ''
      }],
      fakeJpgFile: [{
        lastModified: 1434780236336,
        lastModifiedDate: 'Sat Jun 20 2015 11:33:56 GMT+0530 (IST)',
        name: 'nainital.jpg',
        size: 2852093,
        type: 'image/jpg',
        webkitRelativePath: ''
      }],
      fakeVideoFile: [{
        lastModified: 1422600046658,
        lastModifiedDate: 'Fri Jan 30 2015 12:10:46 GMT+0530 (IST)',
        name: 'crockford-tjpl-1.m4v',
        size: 2591999,
        type: 'video/*',
        webkitRelativePath: ''
      }],
      fakeAudioFile: [{
        lastModified: 1422600046658,
        lastModifiedDate: 'Fri Jan 3 2015 15:30:36 GMT+0530 (IST)',
        name: 'song.mp3',
        size: 2591999,
        type: 'audio/mp3',
        webkitRelativePath: ''
      }],
      fakeUserList: [{
        'id': '5600d67ce4b037ce4d25269c',
        'email': 'vkvang@ucdavis.edu',
        'role': 'REGULAR',
        'status': 'ENABLED',
        'registrationDate': '2015-09-22T04:18:04.277+0000',
        'contactNumber': '2092440733',
        'businessName': null,
        'bPlaces': 0,
        'type': null,
        'contactNames': [],
        'address': null,
        'website': null,
        'notes': null,
        'associatedBusinessId': null,
        'associatedBusinessName': null,
        'firstName': 'Kazoua',
        'lastName': 'Vang',
        'friends': 0,
        'sessions': 0,
        'events': 0,
        'places': 0
      }, {
        'id': '5605f79be4b0a9be00b29c0a',
        'email': 'katrinao@uw.edu',
        'role': 'REGULAR',
        'status': 'DISABLED',
        'registrationDate': '2015-09-26T01:40:43.858+0000',
        'contactNumber': '4088354776',
        'businessName': null,
        'bPlaces': 0,
        'type': null,
        'contactNames': [],
        'address': null,
        'website': null,
        'notes': null,
        'associatedBusinessId': null,
        'associatedBusinessName': null,
        'firstName': 'Katrina',
        'lastName': 'Odfina',
        'friends': 0,
        'sessions': 0,
        'events': 0,
        'places': 0
      }]
    };
    EventData = {
      'ep_id': '55e78700e4b016c1bcd784ab',
      'cat_id': '54b83a2be4b06b211e89274e',
      'type': 'EVENT',
      'title': 'All White Affair',
      'description': 'One of the greatest 18+ parties of the year! Located at School of Rock on Mill. Guaranteed good time. updated',
      'p_id': '54f7d82ce4b0964212462037',
      'place_name': 'University of Washington',
      'location': {'type': 'Point', 'coordinates': [47.65394169259868, -122.3078935593367]},
      'banner_asset': '56559821e4b0da91f9e72234',
      'start_time': '2015-09-04T05:00:20.000+0000',
      'end_time': '2015-09-04T09:00:00.000+0000',
      'affiliation': 'NONE',
      'creator_id': '54df925ae4b072bb08b7733e',
      'creator_name': 'Elizabeth Cheney',
      'address': 'University of Washington, 1707 NE Grant Ln, Seattle, WA 98195',
      'creator_pic_asset': '5605b90ce4b0a9be00a15632',
      'attending': 2,
      'positive_ratings': 0,
      'negative_ratings': 0,
      'ongoing': -1,
      'connections_data': [{
        'id': '55e8f457e4b016c1bcda3de8',
        'first_name': 'Max',
        'last_name': 'Mao',
        'college': 'Macalester College',
        'year': 2019,
        'role': 'REGULAR',
        'email': 'mmao@macalester.edu',
        'friend': false,
        'state': 'ENABLED',
        'registration_date': '2015-09-04T01:34:04.488+0000',
        'full_name': 'Max Mao'
      }, {
        'id': '54df925ae4b072bb08b7733e',
        'first_name': 'Elizabeth',
        'last_name': 'Cheney',
        'college': 'Arizona State University',
        'year': 2015,
        'photo_asset': '5605b90ce4b0a9be00a15632',
        'role': 'LAUNCHSQUAD',
        'email': 'echeney1@asu.edu',
        'friend': false,
        'state': 'ENABLED',
        'registration_date': '2015-07-09T10:25:15.277+0000',
        'full_name': 'Elizabeth Cheney'
      }],
      'photos': ['56559821e4b0da91f9e72234', '56559819e4b0da91f9e71b29', '55e78700e4b016c1bcd784ae'],
      'bump_type': 'NON_BUMP',
      'keywords': ['sxsxsx'],
      'positive_rating_count': 0,
      'negative_rating_count': 0,
      'invite_status': 'NONE',
      'event_timeline': 'PAST',
      'recurrence_type': 'NEVER',
      'private': false,
      'disabled': false
    };
  });
  beforeEach(module('kwadWeb'));
  beforeEach(inject(function (_$rootScope_, _$controller_, _$q_, _$filter_, _ToastrMessage_, _$httpBackend_) {
    $rootScope = _$rootScope_;
    $scope = _$rootScope_.$new();
    $controller = _$controller_;
    $state = jasmine.createSpyObj('$state', ['go']);
    $filter = _$filter_;
    $modal = jasmine.createSpyObj('$modal', ['open']);
    $q = _$q_;
    $httpBackend = _$httpBackend_;
    AuthService = jasmine.createSpyObj('AuthService', ['getCurrentUser']);
    FetchAssetsService = jasmine.createSpyObj('FetchAssetsService', ['fetchAsset']);
    AddAssetsService = jasmine.createSpyObj('AddAssetsService', ['addAsset']);
    DeleteAssetsService = jasmine.createSpyObj('DeleteAssetsService', ['removeAsset']);
    EventAddEditApiService = jasmine.createSpyObj('EventAddEditApiService', ['addEditEvent']);
    EventPlaceApiService = jasmine.createSpyObj('EventPlaceApiService', ['getEventsList', 'getPlacesList']);
    AWSUploadService = jasmine.createSpyObj('AWSUploadService', ['uploadFile']);
    UsersApiService = jasmine.createSpyObj('UsersApiService', ['getUsers']);
    ToastrMessage = _ToastrMessage_;
    MessageService = jasmine.createSpyObj('MessageService', ['showSuccessfulMessage', 'showFailedMessage']);
    LoaderService.Loader = jasmine.createSpyObj('LoaderService.Loader', ['hideSpinner', 'showSpinner']);
    EnumsService = jasmine.createSpyObj('EnumsService', ['getUserRoleEnums', 'getEpStateEnums', 'getEpTypeEnums', 'getEventCategoryEnums', 'getAssetTypeEnums']);
    MessageService.showSuccessfulMessage.and.callFake(function () {
    });
    MessageService.showFailedMessage.and.callFake(function () {
    });
    LoaderService.Loader.hideSpinner.and.callFake(function () {
    });
    LoaderService.Loader.showSpinner.and.callFake(function () {
    });
    $state.go.and.callFake(function () {
    });
    EnumsService.getAssetTypeEnums.and.callFake(function () {
      return {
        IMAGE_JPG: 'IMAGE_JPG',
        IMAGE_JPEG: 'IMAGE_JPEG',
        VIDEO: 'VIDEO'
      };
    });
  }));
  beforeEach(function () {
    EventDetails = $controller('EventDetailsCtrl', {
      $scope: $scope,
      $state: $state,
      $filter: $filter,
      $modal: $modal,
      AuthService: AuthService,
      FetchAssetsService: FetchAssetsService,
      AddAssetsService: AddAssetsService,
      DeleteAssetsService: DeleteAssetsService,
      EventAddEditApiService: EventAddEditApiService,
      EventPlaceApiService: EventPlaceApiService,
      AWSUploadService: AWSUploadService,
      UsersApiService: UsersApiService,
      ToastrMessage: ToastrMessage,
      MessageService: MessageService,
      LoaderService: LoaderService,
      EnumsService: EnumsService,
      EventData: EventData
    });
    $httpBackend.expectGET('app/login/login.html').respond(200);
  });

  describe('Units Should be defined', function () {
    it('EventDetails should be defined', function () {
      expect(EventDetails).toBeDefined();
    });
    it('EventPlaceApiService should be defined', function () {
      expect(EventPlaceApiService).toBeDefined();
    });
    it('ToastrMessage should be defined', function () {
      expect(ToastrMessage).toBeDefined();
    });
  });

  describe('Function Unit: EventDetails.init', function () {
    it('Should be defined and be a function', function () {
      expect(EventDetails.init).toBeDefined();
      expect(typeof EventDetails.init).toEqual('function');
    });
    describe('When Enums Api call returns success', function () {
      beforeEach(function () {
        EnumsService.getUserRoleEnums.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve(fakeResponse.fakeRoles);
          return deferred.promise;
        });
        EnumsService.getEpTypeEnums.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve(fakeResponse.fakeEpTypes);
          return deferred.promise;
        });
        EnumsService.getEpStateEnums.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve(fakeResponse.fakeEpStateEnums);
          return deferred.promise;
        });
        EnumsService.getEventCategoryEnums.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve(fakeResponse.fakeEventCategories);
          return deferred.promise;
        });
        FetchAssetsService.fetchAsset.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve(fakeResponse.fakeFetchedAsset);
          deferred.$promise = deferred.promise;
          return deferred;
        });
      });
      it('Should assign values when User is admin/bump.', function () {
        AuthService.getCurrentUser.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve(fakeResponse.fakeAdminUser);
          return deferred.promise;
        });
        EventDetails.init();
        $rootScope.$digest();
        expect(EventDetails.EP_STATES.length).toBeGreaterThan(0);
        expect(EventDetails.EP_TYPES.length).toBeGreaterThan(0);
        expect(EventDetails.EVENT_CATEGORY_TYPES.length).toEqual(2);
        expect(EventDetails.EVENT_CATEGORIES.length).toBeGreaterThan(0);
      });
      it('Should assign values when User is regular', function () {
        AuthService.getCurrentUser.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve(fakeResponse.fakeRegularUser);
          return deferred.promise;
        });
        EventDetails.init();
        $rootScope.$digest();
        expect(EventDetails.EP_STATES.length).toBeGreaterThan(0);
        expect(EventDetails.EP_TYPES.length).toBeGreaterThan(0);
        expect(EventDetails.EVENT_CATEGORY_TYPES).toEqual(null);
        expect(EventDetails.EVENT_CATEGORIES.length).toBeGreaterThan(0);
      });
      it('Should assign values when User is editor', function () {
        AuthService.getCurrentUser.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve(fakeResponse.fakeEditorUser);
          return deferred.promise;
        });
        EventDetails.init();
        $rootScope.$digest();
        expect(EventDetails.EP_STATES.length).toBeGreaterThan(0);
        expect(EventDetails.EP_TYPES.length).toBeGreaterThan(0);
        expect(EventDetails.EVENT_CATEGORY_TYPES.length).toEqual(1);
        expect(EventDetails.EVENT_CATEGORIES.length).toBeGreaterThan(0);
      });
      it('Should not assign values on AuthService failure', function () {
        AuthService.getCurrentUser.and.callFake(function () {
          var deferred = $q.defer();
          deferred.reject();
          return deferred.promise;
        });
        EventDetails.init();
        $rootScope.$digest();
        expect(EventDetails.currentUser).toEqual(null);
      });
    });
    describe('When Enums Api call returns error', function () {
      beforeEach(function () {
        EnumsService.getEpStateEnums.and.callFake(function () {
          var deferred = $q.defer();
          deferred.reject();
          return deferred.promise;
        });
        EnumsService.getUserRoleEnums.and.callFake(function () {
          var deferred = $q.defer();
          deferred.reject();
          return deferred.promise;
        });
        EnumsService.getEpTypeEnums.and.callFake(function () {
          var deferred = $q.defer();
          deferred.reject();
          return deferred.promise;
        });
        EnumsService.getEventCategoryEnums.and.callFake(function () {
          var deferred = $q.defer();
          deferred.reject();
          return deferred.promise;
        });
        FetchAssetsService.fetchAsset.and.callFake(function () {
          var deferred = $q.defer();
          deferred.reject();
          deferred.$promise = deferred.promise;
          return deferred;
        });
      });
      it('Should not assign values.', function () {
        EventDetails.init();
        $rootScope.$digest();
        expect(EventDetails.USER_ROLES).toEqual(null);
        expect(EventDetails.USER_ROLES_OBJ).toEqual(null);
        expect(EventDetails.EP_TYPES).toEqual(null);
        expect(EventDetails.EP_STATES).toEqual(null);
        expect(EventDetails.EVENT_CATEGORIES).toEqual(null);
      });
      it('Should be EventDetails.event.photos.length equals to 0.', function () {
        EventDetails.event.photos = [];
        EventDetails.init();
        $rootScope.$digest();
        expect(EventDetails.EP_STATES).toEqual(null);
        expect(EventDetails.USER_ROLES).toEqual(null);
        expect(EventDetails.USER_ROLES_OBJ).toEqual(null);
        expect(EventDetails.EP_TYPES).toEqual(null);
        expect(EventDetails.EVENT_CATEGORIES).toEqual(null);
      });
    });
  });

  describe('Function Unit: EventDetails.addNewKeyword', function () {
    it('EventDetails.addNewKeyword should be defined and be a function', function () {
      expect(EventDetails.addNewKeyword).toBeDefined();
      expect(typeof EventDetails.addNewKeyword).toEqual('function');
    });
    it('EventDetails.addNewKeyword Should add 1 keyword', function () {
      EventDetails.event.keywords = [];
      EventDetails.newTag = 'new';
      EventDetails.addNewKeyword();
      $rootScope.$digest();
      expect(EventDetails.event.keywords.length).toEqual(1);
    });
  });

  describe('Function Unit: EventDetails.removeKeyword', function () {
    it('EventDetails.removeKeyword should be defined and be a function', function () {
      expect(EventDetails.removeKeyword).toBeDefined();
      expect(typeof EventDetails.removeKeyword).toEqual('function');
    });
    it('EventDetails.removeKeyword Should remove 1 keyword', function () {
      EventDetails.event.keywords = ['keyword1', 'keyword2'];
      EventDetails.removeKeyword();
      $rootScope.$digest();
      expect(EventDetails.event.keywords.length).toEqual(1);
    });
  });

  describe('Function Unit: EventDetails.changeEndDate', function () {
    it(' EventDetails.changeEndDate should be defined and be a function', function () {
      expect(EventDetails.changeEndDate).toBeDefined();
      expect(typeof EventDetails.changeEndDate).toEqual('function');
    });
    it('EventDetails.changeEndDate should change event end date', function () {
      var dateString = 'Thu Dec 17 2015 15:36:23 GMT+0530 (IST)';
      var expectedDate = '';
      var resultDate = '';
      EventDetails.changeEndDate(dateString);
      expectedDate = $filter('date')(new Date(dateString), 'MM/dd/yyyy');
      resultDate = $filter('date')(EventDetails.currentObject.endDate, 'MM/dd/yyyy');
      $rootScope.$digest();
      expect(resultDate).toEqual(expectedDate);
    });
  });

  describe('Function Unit: EventDetails.changeEndTime', function () {
    it(' EventDetails.changeEndTime should be defined and be a function', function () {
      expect(EventDetails.changeEndTime).toBeDefined();
      expect(typeof EventDetails.changeEndTime).toEqual('function');
    });
    it('EventDetails.changeEndTime should change event end time', function () {
      var dateString = 'Thu Dec 17 2015 15:36:23 GMT+0530 (IST)';
      var expectedEndTime = '';
      var resultEndTime = '';
      EventDetails.changeEndTime(dateString);
      expectedEndTime = $filter('date')(new Date(dateString), 'hh:mm a');
      resultEndTime = $filter('date')(EventDetails.currentObject.endDate, 'hh:mm a');
      $rootScope.$digest();
      expect(resultEndTime).toEqual(expectedEndTime);
    });
  });

  describe('Function Unit: EventDetails.changeStartDate', function () {
    it(' EventDetails.changeStartDate should be defined and be a function', function () {
      expect(EventDetails.changeStartDate).toBeDefined();
      expect(typeof EventDetails.changeStartDate).toEqual('function');
    });
    it('EventDetails.changeStartDate should change event start date', function () {
      var dateString = 'Thu Dec 15 2015 15:36:23 GMT+0530 (IST)';
      var expectedDate = '';
      var resultDate = '';
      EventDetails.changeStartDate(dateString);
      expectedDate = $filter('date')(new Date(dateString), 'MM/dd/yyyy');
      resultDate = $filter('date')(EventDetails.currentObject.startDate, 'MM/dd/yyyy');
      $rootScope.$digest();
      expect(resultDate).toEqual(expectedDate);
    });
  });

  describe('Function Unit: EventDetails.changeStartTime', function () {
    it(' EventDetails.changeStartTime should be defined and be a function', function () {
      expect(EventDetails.changeStartTime).toBeDefined();
      expect(typeof EventDetails.changeStartTime).toEqual('function');
    });
    it('EventDetails.changeEndTime should change event start time', function () {
      var dateString = 'Thu Dec 15 2015 18:45:53 GMT+0530 (IST)';
      var expectedEndTime = '';
      var resultEndTime = '';
      EventDetails.changeStartTime(dateString);
      expectedEndTime = $filter('date')(new Date(dateString), 'hh:mm a');
      resultEndTime = $filter('date')(EventDetails.currentObject.startDate, 'hh:mm a');
      $rootScope.$digest();
      expect(resultEndTime).toEqual(expectedEndTime);
    });
  });

  describe('Function Unit: EventDetails.isBannerImage', function () {
    it('EventDetails.isBannerImage should be defined and be a function', function () {
      expect(EventDetails.isBannerImage).toBeDefined();
      expect(typeof EventDetails.isBannerImage).toEqual('function');
    });
    it('EventDetails.isBannerImage should return true', function () {
      var result = EventDetails.isBannerImage(fakeResponse.fakeImage);
      EventDetails.event.banner_asset = '56559821e4b0da91f9e72234';
      $rootScope.$digest();
      expect(result).toEqual(true);
    });
  });

  describe('Function Unit: EventDetails.addBannerImage', function () {
    it('EventDetails.addBannerImage should be defined and be a function', function () {
      expect(EventDetails.addBannerImage).toBeDefined();
      expect(typeof EventDetails.addBannerImage).toEqual('function');
    });
    it('EventDetails.addBannerImage should add an image as bannerImage for the event', function () {
      EventAddEditApiService.addEditEvent.and.callFake(function () {
        var deferred = $q.defer();
        deferred.resolve(fakeResponse.fakeAddBannerImage);
        deferred.$promise = deferred.promise;
        return deferred;
      });
      EventDetails.event.ep_id = '55e78700e4b016c1bcd784ab';
      EventDetails.event.banner_asset = '';
      EventDetails.addBannerImage(fakeResponse.fakeImage);
      $rootScope.$digest();
      expect(EventDetails.event.banner_asset).toEqual('56559821e4b0da91f9e72234');
    });
    it('EventDetails.addBannerImage should not add an image as bannerImage if asset id not found', function () {
      EventDetails.event.ep_id = '55e78700e4b016c1bcd784ab';
      EventDetails.event.banner_asset = '';
      EventDetails.addBannerImage({id: '', url: ''});
      $rootScope.$digest();
      expect(EventDetails.event.banner_asset).toEqual('');
    });
    it('EventDetails.addBannerImage should not add an image if EventAddEditApiService.addEditEvent respond an error status', function () {
      EventAddEditApiService.addEditEvent.and.callFake(function () {
        var deferred = $q.defer();
        deferred.resolve({status: 'ALREADY_EXISTS'});
        deferred.$promise = deferred.promise;
        return deferred;
      });
      EventDetails.event.ep_id = '55e78700e4b016c1bcd784ab';
      EventDetails.event.banner_asset = '';
      EventDetails.addBannerImage(fakeResponse.fakeImage);
      $rootScope.$digest();
      expect(MessageService.showFailedMessage).toHaveBeenCalled();
    });
    it('EventDetails.addBannerImage should not add an image if EventAddEditApiService.addEditEvent respond to failure', function () {
      EventAddEditApiService.addEditEvent.and.callFake(function () {
        var deferred = $q.defer();
        deferred.reject();
        deferred.$promise = deferred.promise;
        return deferred;
      });
      EventDetails.event.ep_id = '55e78700e4b016c1bcd784ab';
      EventDetails.event.banner_asset = '';
      EventDetails.addBannerImage(fakeResponse.fakeImage);
      $rootScope.$digest();
      expect(EventDetails.event.banner_asset).toEqual('');
    });
  });

  describe('Function Unit: EventDetails.removeBannerImage', function () {
    it('EventDetails.removeBannerImage should be defined and be a function', function () {
      expect(EventDetails.removeBannerImage).toBeDefined();
      expect(typeof EventDetails.removeBannerImage).toEqual('function');
    });
    it('EventDetails.removeBannerImage should remove an image', function () {
      DeleteAssetsService.removeAsset.and.callFake(function () {
        var deferred = $q.defer();
        deferred.resolve(fakeResponse.fakeRemovedAsset);
        deferred.$promise = deferred.promise;
        return deferred;
      });
      EventDetails.photos = [fakeResponse.fakeImage];
      EventDetails.removeBannerImage(fakeResponse.fackEvent, fakeResponse.fakeImage);
      $rootScope.$digest();
      expect(EventDetails.photos.length).toEqual(0);
    });
    it('EventDetails.removeBannerImage should not remove an image if DeleteAssetsService.removeAsset respond an error status', function () {
      DeleteAssetsService.removeAsset.and.callFake(function () {
        var deferred = $q.defer();
        deferred.resolve({status: 'ERROR'});
        deferred.$promise = deferred.promise;
        return deferred;
      });
      EventDetails.photos = [fakeResponse.fakeImage];
      EventDetails.removeBannerImage(fakeResponse.fackEvent, fakeResponse.fakeImage);
      $rootScope.$digest();
      expect(EventDetails.photos.length).not.toEqual(0);
    });
    it('EventDetails.removeBannerImage should not remove an image if asset id not found', function () {
      EventDetails.photos = [fakeResponse.fakeImage];
      EventDetails.removeBannerImage(fakeResponse.fackEvent, {id: '', url: ''});
      $rootScope.$digest();
      expect(EventDetails.photos.length).not.toEqual(0);
    });
    it('EventDetails.removeBannerImage should not remove an image if DeleteAssetsService.removeAsset respond to failure', function () {
      DeleteAssetsService.removeAsset.and.callFake(function () {
        var deferred = $q.defer();
        deferred.reject();
        deferred.$promise = deferred.promise;
        return deferred;
      });
      EventDetails.photos = [fakeResponse.fakeImage];
      EventDetails.removeBannerImage(fakeResponse.fackEvent, fakeResponse.fakeImage);
      $rootScope.$digest();
      expect(EventDetails.photos.length).not.toEqual(0);
    });
  });

  describe('Function Unit: EventDetails.fetchAssets', function () {
    it('EventDetails.fetchAssets should be defined and be a function', function () {
      expect(EventDetails.fetchAssets).toBeDefined();
      expect(typeof EventDetails.fetchAssets).toEqual('function');
    });
    it('EventDetails.fetchAssets should fetch images', function () {
      EventDetails.photos = [];
      FetchAssetsService.fetchAsset.and.callFake(function () {
        var deferred = $q.defer();
        deferred.resolve(fakeResponse.fakeFetchedAsset);
        deferred.$promise = deferred.promise;
        return deferred;
      });
      EventDetails.fetchAssets('56559821e4b0da91f9e72234');
      $rootScope.$digest();
      expect(EventDetails.photos.length).toEqual(1);
    });
    it('EventDetails.fetchAssets should fetch data with no image prefixes', function () {
      EventDetails.photos = [];
      FetchAssetsService.fetchAsset.and.callFake(function () {
        var deferred = $q.defer();
        deferred.resolve({
          aws_key: 'ASIAIG63HOWFIRIEQCJQ',
          aws_secret: 'z3ZnvoIHEBVFe21GuiqxOLuJ+CSIBlguUbMu5c2h',
          aws_token: 'AQoDYXdzEEYasAODTH4AOcylcz+EkZt4QEaYlmOiUe/e7HfuIrVhdnLET3djkip1VXCM571/ipl1qe8gt7Kek2JCWNJFCPlAIn7gkWFIb3qNpHip/yhCXZedyFiiaQGhfcWPhhbNFL+yL+vX3BpLO3Vu4+a2p/4V+egycRQp60QPmpgH0KQAqHDCnBf4bVYBf+bSJIQGM09WpwaSOAaiZ2UZdeqAC39CLdwLlqe85NJ0kMT4B3SJCLKmxbeUaZQtV8P9LXQygNoAlqmNA6S38HFtByRrU32wiSTVeGXDiUTRxJmdcYKgUNZQMsaRTuj2llwN5kZLL1SoBpO8Sw6XFBIDOgFWIf0VkAQJOH3RUg7T/udeyvOqNVNhPelUBraz8+IxeYoxjEThEXDfTzMkelrhqpT2DNCLWmCtMlJst505QH5pkQgq6CxVQTA7504nTbQ9iKphtttgDgHcS6zwsTsa0F6rJOV3SH/rR857NT83ImXw3kGw+0edJL1IAFnEIc+H7WBjvZhx6pokP+jEgakL6n/o3fCm6/bNmsxozNonPQeUG6zrgkdTK+MfJ1TD2oYXg3fghDpXDMsgjqDAswU=',
          prefixes: [],
          s3_bucket: 'kwad-bump-assets'
        });
        deferred.$promise = deferred.promise;
        return deferred;
      });
      EventDetails.fetchAssets('56559821e4b0da91f9e72234');
      $rootScope.$digest();
      expect(EventDetails.photos.length).toEqual(0);
    });
    it('EventDetails.fetchAssets should not fetch data when asset id is undefined', function () {
      EventDetails.photos = [];
      EventDetails.fetchAssets();
      $rootScope.$digest();
      expect(EventDetails.photos.length).toEqual(0);
    });
    it('EventDetails.fetchAssets should not fetch data when FetchAssetsService.fetchAsset rejected', function () {
      EventDetails.photos = [];
      FetchAssetsService.fetchAsset.and.callFake(function () {
        var deferred = $q.defer();
        deferred.reject();
        deferred.$promise = deferred.promise;
        return deferred;
      });
      EventDetails.fetchAssets('56559821e4b0da91f9e72234');
      $rootScope.$digest();
      expect(EventDetails.photos.length).toEqual(0);
    });
  });

  describe('Function Unit: EventDetails.uploadFile', function () {
    it(' EventDetails.uploadFile should be defined and be a function', function () {
      expect(EventDetails.uploadFile).toBeDefined();
      expect(typeof EventDetails.uploadFile).toEqual('function');
    });
    it(' EventDetails.uploadFile should upload file on KWAD and aws s3 server', function () {
      EventDetails.photos = [];
      EventDetails.event.photos = null;
      EventDetails.selectedFiles = fakeResponse.fakeJpegFile;
      AddAssetsService.addAsset.and.callFake(function () {
        var deferred = $q.defer();
        deferred.resolve({data: [fakeResponse.fakeAddedAsset]});
        deferred.$promise = deferred.promise;
        return deferred;
      });
      AWSUploadService.uploadFile.and.callFake(function (file, AWSObj, options) {
        var deferred = $q.defer();
        deferred.resolve({Location: 'https://s3.amazonaws.com/kwad-bump-assets//5592497703cef8958d68d1a5/56559821e4b0da91f9e72234/raw_upload'});
        return deferred.promise;
      });
      EventAddEditApiService.addEditEvent.and.callFake(function () {
        var deferred = $q.defer();
        deferred.resolve(fakeResponse.fakeAddBannerImage);
        deferred.$promise = deferred.promise;
        return deferred;
      });
      EventDetails.uploadFile();
      $rootScope.$digest();
      expect(EventDetails.event.photos.length).toEqual(1);
      expect(EventDetails.event.photos[0]).toEqual('5672a991e4b02b658c9278dd');
    });
    describe('EventDetails.uploadFile should not upload file', function () {
      it('When length of selected files is zero', function () {
        EventDetails.photos = [];
        EventDetails.event.photos = null;
        EventDetails.selectedFiles = [];
        EventDetails.uploadFile();
        $rootScope.$digest();
        expect(EventDetails.event.photos).toEqual(null);
      });
      it('When length of selected files greater then ten', function () {
        EventDetails.photos = [];
        EventDetails.event.photos = null;
        EventDetails.selectedFiles = {length: 12};
        EventDetails.uploadFile();
        $rootScope.$digest();
        expect(EventDetails.event.photos).toEqual(null);
      });
      it('when AWSUploadService.uploadFile rejected', function () {
        EventDetails.photos = [];
        EventDetails.event.photos = null;
        EventDetails.selectedFiles = fakeResponse.fakeJpgFile;
        AddAssetsService.addAsset.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve({data: [fakeResponse.fakeAddedAsset]});
          deferred.$promise = deferred.promise;
          return deferred;
        });
        EventAddEditApiService.addEditEvent.and.callFake(function () {
          var deferred = $q.defer();
          deferred.reject();
          deferred.$promise = deferred.promise;
          return deferred;
        });
        AWSUploadService.uploadFile.and.callFake(function (file, AWSObj, options) {
          var deferred = $q.defer();
          deferred.reject();
          return deferred.promise;
        });
        EventDetails.uploadFile();
        $rootScope.$digest();
        expect(EventDetails.event.photos).toEqual(null);
      });
      it('when AWSUploadService.uploadFile rejected when file type is video', function () {
        EventDetails.photos = [];
        EventDetails.event.photos = null;
        EventDetails.selectedFiles = fakeResponse.fakeVideoFile;
        AddAssetsService.addAsset.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve({data: [fakeResponse.fakeAddedAsset]});
          deferred.$promise = deferred.promise;
          return deferred;
        });
        EventAddEditApiService.addEditEvent.and.callFake(function () {
          var deferred = $q.defer();
          deferred.reject();
          deferred.$promise = deferred.promise;
          return deferred;
        });
        AWSUploadService.uploadFile.and.callFake(function (file, AWSObj, options) {
          var deferred = $q.defer();
          deferred.reject();
          return deferred.promise;
        });
        EventDetails.uploadFile();
        $rootScope.$digest();
        expect(EventDetails.event.photos).toEqual(null);
      });
      it('when AWSUploadService.uploadFile rejected when file type is audio', function () {
        EventDetails.photos = [];
        EventDetails.event.photos = null;
        EventDetails.selectedFiles = fakeResponse.fakeAudioFile;
        AddAssetsService.addAsset.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve({data: [fakeResponse.fakeAddedAsset]});
          deferred.$promise = deferred.promise;
          return deferred;
        });
        EventAddEditApiService.addEditEvent.and.callFake(function () {
          var deferred = $q.defer();
          deferred.reject();
          deferred.$promise = deferred.promise;
          return deferred;
        });
        AWSUploadService.uploadFile.and.callFake(function (file, AWSObj, options) {
          var deferred = $q.defer();
          deferred.reject();
          return deferred.promise;
        });
        EventDetails.uploadFile();
        $rootScope.$digest();
        expect(EventDetails.event.photos).toEqual(null);
      });
      it('when AddAssetsService.addAsset rejected', function () {
        EventDetails.photos = [];
        EventDetails.event.photos = null;
        EventDetails.selectedFiles = fakeResponse.fakeJpegFile;
        AddAssetsService.addAsset.and.callFake(function () {
          var deferred = $q.defer();
          deferred.reject();
          deferred.$promise = deferred.promise;
          return deferred;
        });
        EventDetails.uploadFile();
        $rootScope.$digest();
        expect(EventDetails.event.photos).toEqual(null);
      });
    });
  });

  describe('Function Unit: EventDetails.searchPlaces', function () {
    it('EventDetails.searchPlaces should be defined and be a function', function () {
      expect(EventDetails.searchPlaces).toBeDefined();
      expect(typeof EventDetails.searchPlaces).toEqual('function');
    });
    it('EventDetails.searchPlaces should reject places list and $modal.open should not invoked', function () {
      EventPlaceApiService.getPlacesList.and.callFake(function () {
        var deferred = $q.defer();
        deferred.reject();
        deferred.$promise = deferred.promise;
        return deferred;
      });
      EventDetails.searchPlaces();
      $rootScope.$digest();
      expect(LoaderService.Loader.hideSpinner).toHaveBeenCalled();
    });
    describe('EventDetails.searchPlaces should resolve places list', function () {
      beforeEach(function () {
        EventPlaceApiService.getPlacesList.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve(fakeResponse.fakePlaceList);
          deferred.$promise = deferred.promise;
          return deferred;
        });
      });
      it('when $modal.open invoked and clicked Ok', function () {
        $modal.open.and.callFake(function (obj) {
          var deferred = $q.defer();
          obj.resolve.PlaceResult();
          deferred.resolve(fakeResponse.fakePlaceList[0]);
          return {
            result: deferred.promise
          };
        });
        EventDetails.searchPlaces();
        $rootScope.$digest();
        expect(EventDetails.newPlaceID).toEqual('546aff5be4b0370c32328fbb');
        expect(EventDetails.event.place_name).toEqual('Phoenix Rock Gym');
      });
      it('when $modal.open invoked and clicked Cancel', function () {
        $modal.open.and.callFake(function (obj) {
          var deferred = $q.defer();
          obj.resolve.PlaceResult();
          deferred.reject();
          return {
            result: deferred.promise
          };
        });
        EventDetails.searchPlaces();
        $rootScope.$digest();
        expect(EventDetails.event.p_id).toEqual('54f7d82ce4b0964212462037');
      });
    });
  });

  describe('Function Unit: EventDetails.searchCreators', function () {
    it('Should be defined and be a function', function () {
      expect(EventDetails.searchCreators).toBeDefined();
      expect(typeof EventDetails.searchCreators).toEqual('function');
    });
    it('Should reject places list and $modal.open should not invoked', function () {
      UsersApiService.getUsers.and.callFake(function () {
        var deferred = $q.defer();
        deferred.reject();
        deferred.$promise = deferred.promise;
        return deferred;
      });
      EventDetails.event.creator_name = 'Ka';
      EventDetails.event.creator_id = null;
      EventDetails.searchCreators();
      $rootScope.$digest();
      expect(LoaderService.Loader.hideSpinner).toHaveBeenCalled();
      expect(EventDetails.event.creator_id).toEqual(null);
    });
    describe('Should resolve user list', function () {
      beforeEach(function () {
        UsersApiService.getUsers.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve({data: fakeResponse.fakeUserList});
          deferred.$promise = deferred.promise;
          return deferred;
        });
      });
      it('when $modal.open invoked and clicked Ok', function () {
        $modal.open.and.callFake(function (obj) {
          var deferred = $q.defer();
          fakeResponse.fakeUserList[0].fullName = fakeResponse.fakeUserList[0].firstName + ' ' + fakeResponse.fakeUserList[0].lastName;
          obj.resolve.CreatorList();
          deferred.resolve(fakeResponse.fakeUserList[0]);
          return {
            result: deferred.promise
          };
        });
        EventDetails.event.creator_id = null;
        EventDetails.event.creator_name = 'Ka';
        EventDetails.searchCreators();
        $rootScope.$digest();
        expect(EventDetails.event.creator_id).toEqual('5600d67ce4b037ce4d25269c');
        expect(EventDetails.event.creator_name).toEqual('Kazoua Vang');
      });
      it('when $modal.open invoked and clicked Cancel', function () {
        $modal.open.and.callFake(function (obj) {
          var deferred = $q.defer();
          obj.resolve.CreatorList();
          deferred.reject();
          return {
            result: deferred.promise
          };
        });
        EventDetails.event.creator_id = null;
        EventDetails.event.creator_name = 'Ka';
        EventDetails.searchCreators();
        $rootScope.$digest();
        expect(EventDetails.event.creator_id).toEqual(null);
      });
    });
  });

  describe('Function Unit: EventDetails.CreateNewEventBasedOnCurrent', function () {
    it('Should be defined and be a function', function () {
      expect(EventDetails.CreateNewEventBasedOnCurrent).toBeDefined();
      expect(typeof EventDetails.CreateNewEventBasedOnCurrent).toEqual('function');
    });
    it('Should pass and call $state.go', function () {
      EventDetails.CreateNewEventBasedOnCurrent();
      $rootScope.$digest();
      expect($state.go).toHaveBeenCalledWith('addEventsFromCurrent', {eventId: EventDetails.event.ep_id});
    });
  });

  describe('Function Unit: EventDetails.updateEvent', function () {
    it('Should be defined and be a function', function () {
      expect(EventDetails.updateEvent).toBeDefined();
      expect(typeof EventDetails.updateEvent).toEqual('function');
    });
    it('Should be update an event', function () {
      var startDateString = 'Thu Dec 17 2015 15:36:23 GMT+0530 (IST)';
      var endDateString = 'Thu Dec 18 2015 15:36:23 GMT+0530 (IST)';
      EventAddEditApiService.addEditEvent.and.callFake(function () {
        var deferred = $q.defer();
        deferred.resolve({status: 'SUCCESS', data: EventData});
        deferred.$promise = deferred.promise;
        return deferred;
      });
      EventDetails.currentObject.categoryType = 'PRIVATE';
      EventDetails.currentObject.status = 'DISABLED';
      EventDetails.newPlaceID = '546aff5be4b0370c32328fbb';
      EventDetails.currentObject.category = {id: '54b83a2be4b06b211e892748'};
      EventDetails.changeStartDate(startDateString);
      EventDetails.changeEndDate(endDateString);
      EventDetails.updateEvent();
      $rootScope.$digest();
      expect(MessageService.showSuccessfulMessage).toHaveBeenCalled();
    });
    describe('should not be update an event', function () {
      it('when invalid EventDetails.currentObject.startDate', function () {
        EventDetails.currentObject.category = {id: '54b83a2be4b06b211e892748'};
        EventDetails.changeStartDate('12/12/2015');
        EventDetails.currentObject.startDate = '32/32/2015+456';
        EventDetails.updateEvent();
        $rootScope.$digest();
        expect(MessageService.showFailedMessage).toHaveBeenCalled();
      });
      it('when invalid EventDetails.currentObject.endDate', function () {
        EventDetails.currentObject.category = {id: '54b83a2be4b06b211e892748'};
        EventDetails.changeEndDate('12/12/2015');
        EventDetails.currentObject.endDate = '32/32/2015+456';
        EventDetails.updateEvent();
        $rootScope.$digest();
        expect(MessageService.showFailedMessage).toHaveBeenCalled();
      });
      it('when start date greater then end date', function () {
        var startDateString = 'Thu Dec 18 2015 15:36:23 GMT+0530 (IST)';
        var endDateString = 'Thu Dec 17 2015 15:36:23 GMT+0530 (IST)';
        EventDetails.currentObject.category = {id: '54b83a2be4b06b211e892748'};
        EventDetails.changeStartDate(startDateString);
        EventDetails.changeEndDate(endDateString);
        EventDetails.updateEvent();
        $rootScope.$digest();
        expect(MessageService.showFailedMessage).toHaveBeenCalled();
      });
    });
  });
});
