'use strict';

describe('Unit: filterEventList Filter', function () {
  var $filter, EventList;

  beforeEach(function () {
    module('kwadWeb');
    EventList = [{
      'epId': '5594cf91e4b044fb28d25c1a',
      'name': 'New Songs',
      'place': '1353 E University Drive, Tempe AZ',
      'creatorName': 'Cherry Lindz',
      'creatorId': '546aff5ae4b0370c32328f5a',
      'feedImpression': 0,
      'detailImpression': 0,
      'appCheckinCount': 1,
      'geoCheckinCount': 0,
      'shares': 1,
      'status': 'ENABLED',
      'associatedEvents': 0,
      'creatorRole': 'REGULAR',
      'eventDate': '2015-07-02T18:42:00.637+0000',
      'eventTimeLine': 'PAST'
    }, {
      'epId': '55fa4d3ee4b04e131361c62b',
      'name': 'Rock Climbing Student Day',
      'place': '1353 E University Drive, Tempe AZ',
      'creatorName': 'Brooke Mitchell',
      'creatorId': '54cf9f72e4b0e8031d9017a5',
      'feedImpression': 0,
      'detailImpression': 0,
      'appCheckinCount': 1,
      'geoCheckinCount': 0,
      'shares': 1,
      'status': 'ENABLED',
      'associatedEvents': 0,
      'creatorRole': 'LAUNCHSQUAD',
      'eventDate': '2015-09-20T21:00:12.000+0000',
      'eventTimeLine': 'ONGOING'
    }];

    inject(function (_$filter_) {
      $filter = _$filter_;
    });
  });

  it('should return array of all events when domain type is set as ALL', function () {
    // Arrange.
    var type = 'ALL', result;

    // Act.
    result = $filter('filterEventList')(EventList, type);

    // Assert.
    expect(result).toEqual(EventList);
    expect(result.length).toEqual(2);
  });

  it('should return array of PAST events when type is set as PAST', function () {
    // Arrange.
    var type = 'PAST', result;

    // Act.
    result = $filter('filterEventList')(EventList, type);

    // Assert.
    expect(result[0]).toEqual(EventList[0]);
    expect(result.length).toEqual(1);
  });

});
