'use strict';
(function (angular) {
	angular.module('kwadWeb')
			.controller('EventsCtrl', ['$rootScope', '$scope', '$state', '$filter', '$compile', 'DTOptionsBuilder', 'DTColumnBuilder', 'EventPlaceApiService', 'DomainAddEditApiService', 'MessageService', 'ToastrMessage', 'LoaderService', 'HOST', 'CookieService',
				function ($rootScope, $scope, $state, $filter, $compile, DTOptionsBuilder, DTColumnBuilder, EventPlaceApiService, DomainAddEditApiService, MessageService, ToastrMessage, LoaderService, HOST, CookieService) {
					// Define variables
					var Events = this;

					Events.viewName = 'Events';
					Events.timeFilter = 'ALL';
					Events.allEvents = [];
					Events.dtInstance = {};


					var userToken = CookieService.userSession.get();

					// Methods					
					var _failGetEvents = function () {
						Events.allEvents = [];
						MessageService.showFailedMessage();
						LoaderService.Loader.hideSpinner();
					};

					// Events
					Events.init = function () {
						Events.getEvents();
					};

					/**
					 * fetch events from the API
					 * uses DataTables ajax for server side processing					 
					 */
					Events.getEvents = function () {
						LoaderService.Loader.showSpinner();

						Events.dtOptions = DTOptionsBuilder.newOptions()
								.withOption('ajax', {
									url: HOST.HOST_URL + '/api/ext/s/search',
									type: 'POST',
									headers: {'Authorization': 'Bearer ' + btoa(userToken)},
									data: function (data) {
										var _payload = JSON.stringify({
											'type': 'EVENT',
											'query': data.search.value,
											'limit': data.length,
											'offSet': data.start,
											'timeFilter': Events.timeFilter
										});
										return _payload;
									},
									dataFilter: function (data) {
										var json = jQuery.parseJSON(data);
										json.recordsTotal = json.count;
										json.recordsFiltered = json.count;

										return JSON.stringify(json);
									},
									dataType: 'json',
									contentType: 'application/json'
								})
								.withDataProp('data')
								.withOption('processing', true)
								.withOption('serverSide', true)
								.withOption('createdRow', createdRow)
								.withPaginationType('full_numbers');

						Events.dtColumns = [
							DTColumnBuilder.newColumn('name').withTitle('Event Name').renderWith(function (data, type, full, meta) {
								return '<p>' + data + '</p><a class="btn btn-primary btn-more" ng-click="Events.editEventDetails(\'' + full.epId + '\')">More</a>';
							}),
							DTColumnBuilder.newColumn('eventDate').withTitle('Date').renderWith(function (data, type, full, meta) {
								return $filter('date')(data, "medium");
							}),
							DTColumnBuilder.newColumn('place').withTitle('Location'),
							DTColumnBuilder.newColumn('creatorName').withTitle('Creator'),
							DTColumnBuilder.newColumn('feedImpression').withTitle('Fd. Impr'),
							DTColumnBuilder.newColumn('detailImpression').withTitle('Dt. Impr'),
							DTColumnBuilder.newColumn('appCheckinCount').withTitle('App CI'),
							DTColumnBuilder.newColumn('geoCheckinCount').withTitle('Geo CI'),
							DTColumnBuilder.newColumn('shares').withTitle('Shares'),
							DTColumnBuilder.newColumn('status').withTitle('Status')
						];

						LoaderService.Loader.hideSpinner();
					};

					function createdRow(row, data, dataIndex) {
						// Recompiling so we can bind Angular directive to the DT
						$compile(angular.element(row).contents())($scope);
					}

					Events.editEventDetails = function (epId) {
						$state.go('eventDetails', {eventId: epId});
					};

					Events.addEvents = function () {
						$state.go('addEvents');
					};

					Events.filterBy = function (filterName) {
						Events.timeFilter = filterName;
						Events.dtInstance.rerender();
					};
				}]);
})(angular);