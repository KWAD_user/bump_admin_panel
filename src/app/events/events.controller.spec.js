'use strict';

describe('Unit: Events Controller', function () {
  var Events,
    $scope,
    $rootScope,
    $q,
    $controller,
    $state,
    EventList,
    EventPlaceApiService,
    ToastrMessage,
    MessageService,
    $httpBackend;

  beforeAll(function () {
    EventList = [{
      'epId': '5594cf91e4b044fb28d25c1a',
      'name': 'New Songs',
      'place': '1353 E University Drive, Tempe AZ',
      'creatorName': 'Cherry Lindz',
      'creatorId': '546aff5ae4b0370c32328f5a',
      'feedImpression': 0,
      'detailImpression': 0,
      'appCheckinCount': 1,
      'geoCheckinCount': 0,
      'shares': 1,
      'status': 'ENABLED',
      'associatedEvents': 0,
      'creatorRole': 'REGULAR',
      'eventDate': '2015-07-02T18:42:00.637+0000',
      'eventTimeLine': 'PAST'
    }, {
      'epId': '55fa4d3ee4b04e131361c62b',
      'name': 'Rock Climbing Student Day',
      'place': '1353 E University Drive, Tempe AZ',
      'creatorName': 'Brooke Mitchell',
      'creatorId': '54cf9f72e4b0e8031d9017a5',
      'feedImpression': 0,
      'detailImpression': 0,
      'appCheckinCount': 1,
      'geoCheckinCount': 0,
      'shares': 1,
      'status': 'ENABLED',
      'associatedEvents': 0,
      'creatorRole': 'LAUNCHSQUAD',
      'eventDate': '2015-09-20T21:00:12.000+0000',
      'eventTimeLine': 'UPCOMING'
    }, {
      'epId': '553ebc2ae4b02e14c9cdf6b4',
      'name': 'Bowling night',
      'place': '4407 S Rural Road, Tempe AZ',
      'creatorName': 'Marcus Jones',
      'creatorId': '54e61572e4b03a194c28f908',
      'feedImpression': 0,
      'detailImpression': 0,
      'appCheckinCount': 0,
      'geoCheckinCount': 0,
      'shares': 0,
      'status': 'ENABLED',
      'associatedEvents': 0,
      'creatorRole': 'REGULAR',
      'eventDate': '2015-04-28T00:45:53.000+0000',
      'eventTimeLine': 'ONGOING'
    }];
  });
  beforeEach(module('kwadWeb'));
  beforeEach(inject(function (_$rootScope_, _$controller_, _$q_, _ToastrMessage_, _$httpBackend_) {
    $q = _$q_;
    $rootScope = _$rootScope_;
    $scope = _$rootScope_.$new();
    $httpBackend = _$httpBackend_;
    $controller = _$controller_;
    EventPlaceApiService = jasmine.createSpyObj('EventPlaceApiService', ['getEventsList']);
    $state = jasmine.createSpyObj('$state', ['go']);
    MessageService = jasmine.createSpyObj('MessageService', ['showSuccessfulMessage', 'showFailedMessage']);
    ToastrMessage = _ToastrMessage_;
    MessageService.showSuccessfulMessage.and.callFake(function () {
    });
    MessageService.showFailedMessage.and.callFake(function () {
    });
    $state.go.and.callFake(function () {
    });
  }));
  beforeEach(function () {
    Events = $controller('EventsCtrl', {
      $scope: $scope,
      $state: $state,
      EventPlaceApiService: EventPlaceApiService,
      ToastrMessage: ToastrMessage,
      MessageService: MessageService
    });
    $httpBackend.expectGET('app/login/login.html').respond(200);
  });

  describe('Units Should be defined', function () {
    it('Events should be defined', function () {
      expect(Events).toBeDefined();
    });
    it('EventPlaceApiService should be defined', function () {
      expect(EventPlaceApiService).toBeDefined();
    });
    it('ToastrMessage should be defined', function () {
      expect(ToastrMessage).toBeDefined();
    });
  });

  describe('Unit: Events.init function', function () {
    it('Events.init should be defined and be a function', function () {
      expect(Events.init).toBeDefined();
      expect(typeof Events.init).toEqual('function');
    });
    it('Events.init Should assign value to Events.allEvents when EventPlaceApiService.getPlacesList get resolved.', function () {

      EventPlaceApiService.getEventsList.and.callFake(function () {
        var deferred = $q.defer();
        deferred.resolve(EventList);
        deferred.$promise = deferred.promise;
        return deferred;
      });
      Events.init();
      $rootScope.$digest();
      $httpBackend.flush();
      expect(Events.allEvents.length).toBeGreaterThan(0);
    });
    it('Events.init Should not assign value to Events.allEvents when EventPlaceApiService.getPlacesList get rejected.', function () {
      EventPlaceApiService.getEventsList.and.callFake(function () {
        var deferred = $q.defer();
        deferred.reject();
        deferred.$promise = deferred.promise;
        return deferred;
      });
      Events.init();
      $rootScope.$digest();
      $httpBackend.flush();
      expect(Events.allEvents.length).toEqual(0);
    });
  });

  describe('Unit: Events.editEventDetails function', function () {
    it('Events.editEventDetails should be defined and be a function', function () {
      expect(Events.editEventDetails).toBeDefined();
      expect(typeof Events.editEventDetails).toEqual('function');
    });
    it('Events.editEventDetails Should change state to event details', function () {
      var eventData = {
        'epId': '5594cf91e4b044fb28d25c1a',
        'name': 'New Songs',
        'place': '1353 E University Drive, Tempe AZ',
        'creatorName': 'Cherry Lindz',
        'creatorId': '546aff5ae4b0370c32328f5a',
        'feedImpression': 0,
        'detailImpression': 0,
        'appCheckinCount': 1,
        'geoCheckinCount': 0,
        'shares': 1,
        'status': 'ENABLED',
        'associatedEvents': 0,
        'creatorRole': 'REGULAR',
        'eventDate': '2015-07-02T18:42:00.637+0000',
        'eventTimeLine': 'PAST'
      };

      Events.editEventDetails(eventData);
      $rootScope.$digest();
      $httpBackend.flush();
      expect($state.go).toHaveBeenCalled();
    });
  });

  describe('Unit: Events.addEvents function', function () {
    it('Events.addEvents should be defined and be a function', function () {
      expect(Events.addEvents).toBeDefined();
      expect(typeof Events.addEvents).toEqual('function');
    });
    it('Events.addEvents Should change state to event add', function () {
      Events.addEvents();
      $rootScope.$digest();
      $httpBackend.flush();
      expect($state.go).toHaveBeenCalled();
    });
  });

  describe('Unit: Events.filterBy function', function () {
    it('Events.filterBy should be defined and be a function', function () {
      expect(Events.filterBy).toBeDefined();
      expect(typeof Events.filterBy).toEqual('function');
    });
    it('Events.filterBy Should filter events based on type ongoing', function () {
      Events.allEvents = EventList;
      Events.filterBy('ongoing');
      $rootScope.$digest();
      $httpBackend.flush();
      expect(Events.filteredEvents.length).toBeGreaterThan(0);
    });
    it('Events.filterBy Should filter events based on type past', function () {
      Events.allEvents = EventList;
      Events.filterBy('past');
      $rootScope.$digest();
      $httpBackend.flush();
      expect(Events.filteredEvents.length).toBeGreaterThan(0);
    });
    it('Events.filterBy Should filter events based on type upcoming', function () {
      Events.allEvents = EventList;
      Events.filterBy('upcoming');
      $rootScope.$digest();
      $httpBackend.flush();
      expect(Events.filteredEvents.length).toBeGreaterThan(0);
    });
    it('Events.filterBy Should filter events by default type all', function () {
      Events.allEvents = EventList;
      Events.filterBy();
      $rootScope.$digest();
      $httpBackend.flush();
      expect(Events.filteredEvents.length).toBeGreaterThan(0);
    });
    it('Events.filterBy Should filter events based on type all', function () {
      Events.allEvents = EventList;
      Events.filterBy('all');
      $rootScope.$digest();
      $httpBackend.flush();
      expect(Events.filteredEvents.length).toBeGreaterThan(0);
    });
  });
});
