'use strict';

describe('Unit: AddEventsCtrl Controller', function () {
  var AddEvents,
    $rootScope,
    $scope,
    $controller,
    $state,
    $filter,
    $modal,
    $q,
    $httpBackend,
    AuthService,
    FetchAssetsService,
    AddAssetsService,
    DeleteAssetsService,
    EventAddEditApiService,
    EventPlaceApiService,
    AWSUploadService,
    ToastrMessage,
    MessageService,
    LoaderService = {Loader: {}},
    EnumsService,
    fakeResponse,
    EventData;

  beforeAll(function () {
    fakeResponse = {
      fakeRoles: ['ANONYMOUS', 'REGULAR', 'LAUNCHSQUAD', 'ADMIN', 'BUSINESS', 'BUSINESS_INDIVIDUAL', 'EDITOR'],
      fakeRolesObj: {
        ANONYMOUS: 'ANONYMOUS',
        REGULAR: 'REGULAR',
        LAUNCHSQUAD: 'LAUNCHSQUAD',
        ADMIN: 'ADMIN',
        BUSINESS: 'BUSINESS',
        BUSINESS_INDIVIDUAL: 'BUSINESS_INDIVIDUAL',
        EDITOR: 'EDITOR'
      },
      fakeRecurrenceType: ['DAILY', 'WEEKLY', 'TWO_WEEKLY', 'MONTHLY', 'YEARLY', 'NEVER'],
      fakeEpTypes: ['BUMP', 'NON_BUMP', 'SPONSORED'],
      fakeEventCategories: {
        '54b83a2be4b06b211e892748': 'Music',
        '54b83a2be4b06b211e892750': 'Group',
        '54b83a2be4b06b211e89274a': 'Sports',
        '54b83a2be4b06b211e892746': 'Bar/Club',
        '54b83a2be4b06b211e892752': 'Outdoors',
        '54b83a2be4b06b211e892744': 'Coffee',
        '54b83a2be4b06b211e892754': 'General',
        '54b83a2be4b06b211e89274e': 'Party',
        '54b83a2be4b06b211e892742': 'Food',
        '54b83a2be4b06b211e89274c': 'School'
      },
      fakeFetchedAsset: {
        aws_key: 'ASIAIG63HOWFIRIEQCJQ',
        aws_secret: 'z3ZnvoIHEBVFe21GuiqxOLuJ+CSIBlguUbMu5c2h',
        aws_token: 'AQoDYXdzEEYasAODTH4AOcylcz+EkZt4QEaYlmOiUe/e7HfuIrVhdnLET3djkip1VXCM571/ipl1qe8gt7Kek2JCWNJFCPlAIn7gkWFIb3qNpHip/yhCXZedyFiiaQGhfcWPhhbNFL+yL+vX3BpLO3Vu4+a2p/4V+egycRQp60QPmpgH0KQAqHDCnBf4bVYBf+bSJIQGM09WpwaSOAaiZ2UZdeqAC39CLdwLlqe85NJ0kMT4B3SJCLKmxbeUaZQtV8P9LXQygNoAlqmNA6S38HFtByRrU32wiSTVeGXDiUTRxJmdcYKgUNZQMsaRTuj2llwN5kZLL1SoBpO8Sw6XFBIDOgFWIf0VkAQJOH3RUg7T/udeyvOqNVNhPelUBraz8+IxeYoxjEThEXDfTzMkelrhqpT2DNCLWmCtMlJst505QH5pkQgq6CxVQTA7504nTbQ9iKphtttgDgHcS6zwsTsa0F6rJOV3SH/rR857NT83ImXw3kGw+0edJL1IAFnEIc+H7WBjvZhx6pokP+jEgakL6n/o3fCm6/bNmsxozNonPQeUG6zrgkdTK+MfJ1TD2oYXg3fghDpXDMsgjqDAswU=',
        prefixes: ['/5592497703cef8958d68d1a5/56559821e4b0da91f9e72234'],
        s3_bucket: 'kwad-bump-assets'
      },
      fakeAddedAsset: {
        asset_id: '5672a991e4b02b658c9278dd',
        aws_key: 'ASIAJ64GQOFRALJOBWQA',
        aws_secret: 'fqN3PS7ao5e2aj38g3CYhVTXCn/R7VLXwTtXCthD',
        aws_token: 'AQoDYXdzEHYakAO+mVAbXL6gqWKmhtyx4IBROjvkzp3Zv2l1BcCVMI5BJ3ONeWhCzcaFp4gHv+72s4/UHUnMZjunMCvACoy/UZt90RnFdAUr6ly2UZofxNLOxcZLHb7kXikIsFocOAF72/KBRfEv+ntNuhaF0yJPVb4nWV1kdOJ0+7ZCLm1Ulz6AvdXrZIenhNxMl4jMRn5MctzXyq1mFNHlM1nyqsOrIiYstIiDv/Mdy4mKTsgqUBIxe2dKKXxvfiNx60FZfUNYr2M70j97+6TbJ9sfHD4Xh9vY9OiMZ+3zDpeX1NdL2BUjHHsyJlSnQVN2Q7U7DFatFOUqDnKOeChHybj8tRe4l2/po8mdGO4b3kQQvJ75VX2pPakIQ2Khn0x4tPRDBxsOzL9vlFwrzX1ehb5XAI4lWKnhaOXLvuFAAYkqzs2evFYShjnyVUnV/sr7cQ3w/nNrjbb2HZVueJzMzuwjnj06+h0QLthi8b/uNyat0A3Xl9IQEEEI/16dy5zNV+dZtDOD+w5Ynjt20Eu81GodKsAq0MxcIJHTyrMF',
        s3_bucket: 'kwad-bump-assets',
        s3_object: '/5592497703cef8958d68d1a5/5672a991e4b02b658c9278dd/raw_upload.jpeg'
      },
      fakeImage: {
        id: '56559821e4b0da91f9e72234',
        url: '/5592497703cef8958d68d1a5/56559821e4b0da91f9e72234/raw_upload'
      },
      fakeRemovedAsset: {
        asset_ids: ['56559821e4b0da91f9e72234']
      },
      fackTriggeredEvent: {
        stopPropagation: function () {
        }
      },
      fakeAdminUser: {
        attributes: [],
        college: '',
        first_name: 'admin',
        full_name: 'admin',
        last_name: 'admin',
        role: 'ADMIN',
        state: 'ENABLED'
      },
      fakeLaunchsquadUser: {
        attributes: [],
        college: '',
        first_name: 'bump',
        full_name: 'bump user',
        last_name: 'user',
        role: 'LAUNCHSQUAD',
        state: 'ENABLED'
      },
      fakeRegularUser: {
        attributes: [],
        college: '',
        first_name: 'regular',
        full_name: 'regular user',
        last_name: 'user',
        role: 'REGULAR',
        state: 'ENABLED'
      },
      fakeEditorUser: {
        attributes: [],
        college: '',
        first_name: 'editor',
        full_name: 'editor user',
        last_name: 'user',
        role: 'EDITOR',
        state: 'ENABLED'
      },
      fakeAddBannerImage: {
        status: 'SUCCESS',
        data: {ep_id: '55e78700e4b016c1bcd784ab', banner_asset: '56559821e4b0da91f9e72234'}
      },
      fakePlaceList: [{
        'epId': '546aff5be4b0370c32328fbb',
        'name': 'Phoenix Rock Gym',
        'place': '1353 E University Drive, Tempe AZ',
        'creatorName': 'Demo  User ',
        'creatorId': '546aff58e4b0370c32328f40',
        'feedImpression': 0,
        'detailImpression': 0,
        'appCheckinCount': 26,
        'geoCheckinCount': 8,
        'shares': 26,
        'status': 'ENABLED',
        'associatedEvents': 2,
        'creatorRole': 'ADMIN',
        'eventDate': null,
        'eventTimeLine': 'UNDETERMINED'
      }, {
        'epId': '546aff5be4b0370c32328fbf',
        'name': 'The Vue on Apache',
        'place': '922 E Apache Blvd, Tempe AZ',
        'creatorName': 'Brandy Smith',
        'creatorId': '546aff59e4b0370c32328f43',
        'feedImpression': 6,
        'detailImpression': 0,
        'appCheckinCount': 24,
        'geoCheckinCount': 283,
        'shares': 24,
        'status': 'ENABLED',
        'associatedEvents': 0,
        'creatorRole': 'BUMP',
        'eventDate': null,
        'eventTimeLine': 'UNDETERMINED'
      }],
      fakeJpegFile: [{
        lastModified: 1434780236336,
        lastModifiedDate: 'Sat Jun 18 2015 18:38:16 GMT+0530 (IST)',
        name: 'image.jpeg',
        size: 2852093,
        type: 'image/jpeg',
        webkitRelativePath: ''
      }],
      fakeJpgFile: [{
        lastModified: 1434780236336,
        lastModifiedDate: 'Sat Jun 20 2015 11:33:56 GMT+0530 (IST)',
        name: 'nainital.jpg',
        size: 2852093,
        type: 'image/jpg',
        webkitRelativePath: ''
      }],
      fakeVideoFile: [{
        lastModified: 1422600046658,
        lastModifiedDate: 'Fri Jan 30 2015 12:10:46 GMT+0530 (IST)',
        name: 'crockford-tjpl-1.m4v',
        size: 2591999,
        type: 'video/*',
        webkitRelativePath: ''
      }],
      fakeAudioFile: [{
        lastModified: 1422600046658,
        lastModifiedDate: 'Fri Jan 3 2015 15:30:36 GMT+0530 (IST)',
        name: 'song.mp3',
        size: 2591999,
        type: 'audio/mp3',
        webkitRelativePath: ''
      }]
    };
    EventData = {
      'ep_id': '55e78700e4b016c1bcd784ab',
      'cat_id': '54b83a2be4b06b211e89274e',
      'type': 'EVENT',
      'title': 'All Black Affair',
      'description': 'One of the greatest 18+ parties of the year! Located at School of Rock on Mill. Guaranteed good time. updated',
      'p_id': '54f7d82ce4b0964212462037',
      'place_name': 'University of Washington',
      'location': {'type': 'Point', 'coordinates': [47.65394169259868, -122.3078935593367]},
      'banner_asset': '56559821e4b0da91f9e72234',
      'start_time': '2015-09-04T05:00:20.000+0000',
      'end_time': '2015-09-04T09:00:00.000+0000',
      'affiliation': 'NONE',
      'creator_id': '54df925ae4b072bb08b7733e',
      'creator_name': 'Elizabeth Cheney',
      'address': 'University of Washington, 1707 NE Grant Ln, Seattle, WA 98195',
      'creator_pic_asset': '5605b90ce4b0a9be00a15632',
      'attending': 2,
      'positive_ratings': 0,
      'negative_ratings': 0,
      'ongoing': -1,
      'connections_data': [{
        'id': '55e8f457e4b016c1bcda3de8',
        'first_name': 'Max',
        'last_name': 'Mao',
        'college': 'Macalester College',
        'year': 2019,
        'role': 'REGULAR',
        'email': 'mmao@macalester.edu',
        'friend': false,
        'state': 'ENABLED',
        'registration_date': '2015-09-04T01:34:04.488+0000',
        'full_name': 'Max Mao'
      }, {
        'id': '54df925ae4b072bb08b7733e',
        'first_name': 'Elizabeth',
        'last_name': 'Cheney',
        'college': 'Arizona State University',
        'year': 2015,
        'photo_asset': '5605b90ce4b0a9be00a15632',
        'role': 'LAUNCHSQUAD',
        'email': 'echeney1@asu.edu',
        'friend': false,
        'state': 'ENABLED',
        'registration_date': '2015-07-09T10:25:15.277+0000',
        'full_name': 'Elizabeth Cheney'
      }],
      'photos': ['56559821e4b0da91f9e72234'],
      'bump_type': 'NON_BUMP',
      'keywords': ['sxsxsx'],
      'positive_rating_count': 0,
      'negative_rating_count': 0,
      'invite_status': 'NONE',
      'event_timeline': 'PAST',
      'recurrence_type': 'NEVER',
      'private': false,
      'disabled': false
    };
  });
  beforeEach(module('kwadWeb'));
  beforeEach(inject(function (_$rootScope_, _$controller_, _$filter_, _$q_, _$httpBackend_, _ToastrMessage_) {
    $rootScope = _$rootScope_;
    $scope = _$rootScope_.$new();
    $controller = _$controller_;
    $state = jasmine.createSpyObj('$state', ['go']);
    $filter = _$filter_;
    $modal = jasmine.createSpyObj('$modal', ['open']);
    $q = _$q_;
    $httpBackend = _$httpBackend_;
    AuthService = jasmine.createSpyObj('AuthService', ['getCurrentUser']);
    FetchAssetsService = jasmine.createSpyObj('FetchAssetsService', ['fetchAsset']);
    AddAssetsService = jasmine.createSpyObj('AddAssetsService', ['addAsset']);
    DeleteAssetsService = jasmine.createSpyObj('DeleteAssetsService', ['removeAsset']);
    EventAddEditApiService = jasmine.createSpyObj('EventAddEditApiService', ['addEditEvent']);
    EventPlaceApiService = jasmine.createSpyObj('EventPlaceApiService', ['getEventsList', 'getPlacesList']);
    AWSUploadService = jasmine.createSpyObj('AWSUploadService', ['uploadFile']);
    ToastrMessage = _ToastrMessage_;
    MessageService = jasmine.createSpyObj('MessageService', ['showSuccessfulMessage', 'showFailedMessage']);
    LoaderService.Loader = jasmine.createSpyObj('LoaderService.Loader', ['hideSpinner', 'showSpinner']);
    EnumsService = jasmine.createSpyObj('EnumsService', ['getEpTypeEnums', 'getEventCategoryEnums', 'getAssetTypeEnums', 'getRecurrenceTypeEnums', 'getUserRoleEnums']);
    MessageService.showSuccessfulMessage.and.callFake(function () {
    });
    MessageService.showFailedMessage.and.callFake(function () {
    });
    LoaderService.Loader.hideSpinner.and.callFake(function () {
    });
    LoaderService.Loader.showSpinner.and.callFake(function () {
    });
    EnumsService.getAssetTypeEnums.and.callFake(function () {
      return {
        IMAGE_JPG: 'IMAGE_JPG',
        IMAGE_JPEG: 'IMAGE_JPEG',
        VIDEO: 'VIDEO'
      };
    });
    $state.go.and.callFake(function () {
    });
  }));
  beforeEach(function () {
    AddEvents = $controller('AddEventsCtrl', {
      $scope: $scope,
      $state: $state,
      $filter: $filter,
      $modal: $modal,
      AuthService: AuthService,
      FetchAssetsService: FetchAssetsService,
      AddAssetsService: AddAssetsService,
      DeleteAssetsService: DeleteAssetsService,
      EventAddEditApiService: EventAddEditApiService,
      EventPlaceApiService: EventPlaceApiService,
      AWSUploadService: AWSUploadService,
      ToastrMessage: ToastrMessage,
      MessageService: MessageService,
      LoaderService: LoaderService,
      EnumsService: EnumsService,
      EventData: angular.copy(EventData)
    });
    $httpBackend.expectGET('app/login/login.html').respond(200);
  });

  describe('Units Should be defined', function () {
    it('AddEvents should be defined', function () {
      expect(AddEvents).toBeDefined();
    });
    it('EventPlaceApiService should be defined', function () {
      expect(EventPlaceApiService).toBeDefined();
    });
    it('ToastrMessage should be defined', function () {
      expect(ToastrMessage).toBeDefined();
    });
  });

  describe('Function Unit: AddEvents.init', function () {
    it('Should be defined and be a function', function () {
      expect(AddEvents.init).toBeDefined();
      expect(typeof AddEvents.init).toEqual('function');
    });
    describe('When Enums Api call returns success', function () {
      beforeEach(function () {
        EnumsService.getUserRoleEnums.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve(fakeResponse.fakeRoles);
          return deferred.promise;
        });
        EnumsService.getRecurrenceTypeEnums.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve(fakeResponse.fakeRecurrenceType);
          return deferred.promise;
        });
        EnumsService.getEpTypeEnums.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve(fakeResponse.fakeEpTypes);
          return deferred.promise;
        });
        EnumsService.getEventCategoryEnums.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve(fakeResponse.fakeEventCategories);
          return deferred.promise;
        });
        FetchAssetsService.fetchAsset.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve(fakeResponse.fakeFetchedAsset);
          deferred.$promise = deferred.promise;
          return deferred;
        });
      });
      it('Should assign values when User is admin/bump.', function () {
        AuthService.getCurrentUser.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve(fakeResponse.fakeAdminUser);
          return deferred.promise;
        });
        AddEvents.init();
        $rootScope.$digest();
        expect(AddEvents.EP_TYPES.length).toBeGreaterThan(0);
        expect(AddEvents.EVENT_CATEGORY_TYPES.length).toEqual(2);
        expect(AddEvents.EVENT_CATEGORIES.length).toBeGreaterThan(0);
      });
      it('Should assign values when User is regular', function () {
        AuthService.getCurrentUser.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve(fakeResponse.fakeRegularUser);
          return deferred.promise;
        });
        AddEvents.init();
        $rootScope.$digest();
        expect(AddEvents.EP_TYPES.length).toBeGreaterThan(0);
        expect(AddEvents.EVENT_CATEGORY_TYPES).toEqual(null);
        expect(AddEvents.EVENT_CATEGORIES.length).toBeGreaterThan(0);
      });
      it('Should assign values when User is editor', function () {
        AuthService.getCurrentUser.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve(fakeResponse.fakeEditorUser);
          return deferred.promise;
        });
        AddEvents.init();
        $rootScope.$digest();
        expect(AddEvents.EP_TYPES.length).toBeGreaterThan(0);
        expect(AddEvents.EVENT_CATEGORY_TYPES.length).toEqual(1);
        expect(AddEvents.EVENT_CATEGORIES.length).toBeGreaterThan(0);
      });
      it('Should not assign values on AuthService failure', function () {
        AuthService.getCurrentUser.and.callFake(function () {
          var deferred = $q.defer();
          deferred.reject();
          return deferred.promise;
        });
        AddEvents.init();
        $rootScope.$digest();
        expect(AddEvents.currentUser).toEqual(null);
      });
    });
    describe('When Enums Api call returns error', function () {
      beforeEach(function () {
        EnumsService.getUserRoleEnums.and.callFake(function () {
          var deferred = $q.defer();
          deferred.reject();
          return deferred.promise;
        });
        EnumsService.getRecurrenceTypeEnums.and.callFake(function () {
          var deferred = $q.defer();
          deferred.reject();
          return deferred.promise;
        });
        EnumsService.getEpTypeEnums.and.callFake(function () {
          var deferred = $q.defer();
          deferred.reject();
          return deferred.promise;
        });
        EnumsService.getEventCategoryEnums.and.callFake(function () {
          var deferred = $q.defer();
          deferred.reject();
          return deferred.promise;
        });
        FetchAssetsService.fetchAsset.and.callFake(function () {
          var deferred = $q.defer();
          deferred.reject();
          deferred.$promise = deferred.promise;
          return deferred;
        });
      });
      it('Should not assign values.', function () {
        AddEvents.init();
        $rootScope.$digest();
        expect(AddEvents.USER_ROLES).toEqual(null);
        expect(AddEvents.USER_ROLES_OBJ).toEqual(null);
        expect(AddEvents.EVENT_RECURRENCE_TYPE).toEqual(null);
        expect(AddEvents.EP_TYPES).toEqual(null);
        expect(AddEvents.EVENT_CATEGORIES).toEqual(null);
      });
      it('When AddEvents.event.photos.length equals to 0.', function () {
        AddEvents.event.photos = [];
        AddEvents.init();
        $rootScope.$digest();
        expect(AddEvents.USER_ROLES).toEqual(null);
        expect(AddEvents.USER_ROLES_OBJ).toEqual(null);
        expect(AddEvents.EVENT_RECURRENCE_TYPE).toEqual(null);
        expect(AddEvents.EP_TYPES).toEqual(null);
        expect(AddEvents.EVENT_CATEGORIES).toEqual(null);
      });
    });
  });

  describe('Function Unit: AddEvents.addNewKeyword', function () {
    it('Should be defined and be a function', function () {
      expect(AddEvents.addNewKeyword).toBeDefined();
      expect(typeof AddEvents.addNewKeyword).toEqual('function');
    });
    it('Should add a keyword', function () {
      AddEvents.event.keywords = [];
      AddEvents.newTag = 'new';
      AddEvents.addNewKeyword();
      $rootScope.$digest();
      expect(AddEvents.event.keywords.length).toEqual(1);
    });
  });

  describe('Function Unit: AddEvents.removeKeyword', function () {
    it('Should be defined and be a function', function () {
      expect(AddEvents.removeKeyword).toBeDefined();
      expect(typeof AddEvents.removeKeyword).toEqual('function');
    });
    it('Should remove a keyword', function () {
      AddEvents.event.keywords = ['keyword1', 'keyword2'];
      AddEvents.removeKeyword();
      $rootScope.$digest();
      expect(AddEvents.event.keywords.length).toEqual(1);
    });
  });

  describe('Function Unit: AddEvents.addBannerImage', function () {
    it('Should be defined and be a function', function () {
      expect(AddEvents.addBannerImage).toBeDefined();
      expect(typeof AddEvents.addBannerImage).toEqual('function');
    });
    describe('Should add bannerImage', function () {
      beforeEach(function () {
        EventAddEditApiService.addEditEvent.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve(fakeResponse.fakeAddBannerImage);
          deferred.$promise = deferred.promise;
          return deferred;
        });
      });
      it('When event exist', function () {
        AddEvents.event.ep_id = '55e78700e4b016c1bcd784ab';
        AddEvents.event.title = 'All White Affair';
        AddEvents.event.banner_asset = '';
        AddEvents.addBannerImage(fakeResponse.fakeImage);
        $rootScope.$digest();
        expect(AddEvents.event.banner_asset).toEqual('56559821e4b0da91f9e72234');
      });
      it('By posting event first', function () {
        AddEvents.event.ep_id = null;
        AddEvents.event.title = 'All White Affair';
        AddEvents.event.banner_asset = '';
        AddEvents.currentObject.category = {id: '54b83a2be4b06b211e892748'};
        AddEvents.addBannerImage(fakeResponse.fakeImage);
        $rootScope.$digest();
        expect(AddEvents.event.banner_asset).toEqual('56559821e4b0da91f9e72234');
      });
    });
    describe('Should not add bannerImage', function () {
      beforeEach(function () {

      });
      it('when asset id not found', function () {
        AddEvents.event.ep_id = '55e78700e4b016c1bcd784ab';
        AddEvents.event.title = 'All White Affair';
        AddEvents.event.banner_asset = '';
        AddEvents.addBannerImage({id: '', url: ''});
        $rootScope.$digest();
        expect(AddEvents.event.banner_asset).toEqual('');
      });
      it('when AddEvents.event.title is empty', function () {
        AddEvents.event.ep_id = '55e78700e4b016c1bcd784ab';
        AddEvents.event.title = '';
        AddEvents.event.banner_asset = '';
        AddEvents.addBannerImage(fakeResponse.fakeImage);
        $rootScope.$digest();
        expect(AddEvents.event.banner_asset).toEqual('');
      });
      it('when EventAddEditApiService.addEditEvent respond an error status when posting event', function () {
        EventAddEditApiService.addEditEvent.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve({status: 'ALREADY_EXISTS'});
          deferred.$promise = deferred.promise;
          return deferred;
        });
        AddEvents.event.ep_id = '';
        AddEvents.event.title = 'All White Affair';
        AddEvents.event.banner_asset = '';
        AddEvents.currentObject.category = {id: '54b83a2be4b06b211e892748'};
        AddEvents.addBannerImage(fakeResponse.fakeImage);
        $rootScope.$digest();
        expect(MessageService.showFailedMessage).toHaveBeenCalled();
      });
      it('when EventAddEditApiService.addEditEvent respond an error status', function () {
        EventAddEditApiService.addEditEvent.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve({status: 'ALREADY_EXISTS'});
          deferred.$promise = deferred.promise;
          return deferred;
        });
        AddEvents.event.ep_id = '55e78700e4b016c1bcd784ab';
        AddEvents.event.title = 'All White Affair';
        AddEvents.event.banner_asset = '';
        AddEvents.currentObject.category = {id: '54b83a2be4b06b211e892748'};
        AddEvents.addBannerImage(fakeResponse.fakeImage);
        $rootScope.$digest();
        expect(MessageService.showFailedMessage).toHaveBeenCalled();
      });
      it('when EventAddEditApiService.addEditEvent respond to failure', function () {
        EventAddEditApiService.addEditEvent.and.callFake(function () {
          var deferred = $q.defer();
          deferred.reject();
          deferred.$promise = deferred.promise;
          return deferred;
        });
        AddEvents.event.ep_id = '55e78700e4b016c1bcd784ab';
        AddEvents.event.title = 'All White Affair';
        AddEvents.event.banner_asset = '';
        AddEvents.currentObject.category = {id: '54b83a2be4b06b211e892748'};
        AddEvents.addBannerImage(fakeResponse.fakeImage);
        $rootScope.$digest();
        expect(AddEvents.event.banner_asset).toEqual('');
      });
    });
  });

  describe('Function Unit: AddEvents.removeBannerImage', function () {
    it('Should be defined and be a function', function () {
      expect(AddEvents.removeBannerImage).toBeDefined();
      expect(typeof AddEvents.removeBannerImage).toEqual('function');
    });
    it('Should remove bannerImage', function () {
      DeleteAssetsService.removeAsset.and.callFake(function () {
        var deferred = $q.defer();
        deferred.resolve(fakeResponse.fakeRemovedAsset);
        deferred.$promise = deferred.promise;
        return deferred;
      });
      AddEvents.photos = [fakeResponse.fakeImage];
      AddEvents.removeBannerImage(fakeResponse.fackTriggeredEvent, fakeResponse.fakeImage);
      $rootScope.$digest();
      expect(AddEvents.photos.length).toEqual(0);
    });
    describe('Should not remove bannerImage', function () {
      it('when DeleteAssetsService.removeAsset respond an error status', function () {
        DeleteAssetsService.removeAsset.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve({status: 'ERROR'});
          deferred.$promise = deferred.promise;
          return deferred;
        });
        AddEvents.photos = [fakeResponse.fakeImage];
        AddEvents.removeBannerImage(fakeResponse.fackTriggeredEvent, fakeResponse.fakeImage);
        $rootScope.$digest();
        expect(AddEvents.photos.length).not.toEqual(0);
      });
      it('when asset id not found', function () {
        AddEvents.photos = [fakeResponse.fakeImage];
        AddEvents.removeBannerImage(fakeResponse.fackTriggeredEvent, {id: '', url: ''});
        $rootScope.$digest();
        expect(AddEvents.photos.length).not.toEqual(0);
      });
      it('when DeleteAssetsService.removeAsset respond to failure', function () {
        DeleteAssetsService.removeAsset.and.callFake(function () {
          var deferred = $q.defer();
          deferred.reject();
          deferred.$promise = deferred.promise;
          return deferred;
        });
        AddEvents.photos = [fakeResponse.fakeImage];
        AddEvents.removeBannerImage(fakeResponse.fackTriggeredEvent, fakeResponse.fakeImage);
        $rootScope.$digest();
        expect(AddEvents.photos.length).not.toEqual(0);
      });

    });
  });

  describe('Function Unit: AddEvents.isBannerImage', function () {
    it('Should be defined and be a function', function () {
      expect(AddEvents.isBannerImage).toBeDefined();
      expect(typeof AddEvents.isBannerImage).toEqual('function');
    });
    it('Should return true', function () {
      AddEvents.event.banner_asset = '56559821e4b0da91f9e72234';
      var result = AddEvents.isBannerImage(fakeResponse.fakeImage);
      $rootScope.$digest();
      expect(result).toEqual(true);
    });
  });

  describe('Function Unit: AddEvents.addEvent', function () {
    it('Should be defined and be a function', function () {
      expect(AddEvents.addEvent).toBeDefined();
      expect(typeof AddEvents.addEvent).toEqual('function');
    });
    it('Should be add an event', function () {
      EventAddEditApiService.addEditEvent.and.callFake(function () {
        var deferred = $q.defer();
        deferred.resolve({status: 'SUCCESS', data: EventData});
        deferred.$promise = deferred.promise;
        return deferred;
      });
      AddEvents.event.ep_id = null;
      AddEvents.event.title = 'All White Affair';
      AddEvents.currentObject.category = {id: '54b83a2be4b06b211e892748'};
      AddEvents.addEvent();
      $rootScope.$digest();
      expect($state.go).toHaveBeenCalledWith('eventDetails', {eventId: '55e78700e4b016c1bcd784ab'});
    });
    describe('should not be update an event', function () {
      it('when title does not exist', function () {
        AddEvents.event.ep_id = '55e78700e4b016c1bcd784ab';
        AddEvents.event.title = '';
        AddEvents.currentObject.category = {id: '54b83a2be4b06b211e892748'};
        AddEvents.addEvent();
        $rootScope.$digest();
        expect(MessageService.showFailedMessage).toHaveBeenCalled();
      });
      it('when title already exist', function () {
        EventAddEditApiService.addEditEvent.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve({status: 'ALREADY_EXISTS'});
          deferred.$promise = deferred.promise;
          return deferred;
        });
        AddEvents.event.ep_id = '55e78700e4b016c1bcd784ab';
        AddEvents.event.title = 'All Black Affair';
        AddEvents.currentObject.category = {id: '54b83a2be4b06b211e892748'};
        AddEvents.addEvent();
        $rootScope.$digest();
        expect(MessageService.showFailedMessage).toHaveBeenCalled();
      });
      it('when event\'s place_name does not exist', function () {
        AddEvents.event.ep_id = '55e78700e4b016c1bcd784ab';
        AddEvents.event.title = 'All Black Affair';
        AddEvents.event.place_name = '';
        AddEvents.currentObject.category = {id: '54b83a2be4b06b211e892748'};
        AddEvents.addEvent();
        $rootScope.$digest();
        expect(MessageService.showFailedMessage).toHaveBeenCalled();
      });
      it('when invalid AddEvents.currentObject.startDate', function () {
        EventAddEditApiService.addEditEvent.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve({status: 'ALREADY_EXISTS'});
          deferred.$promise = deferred.promise;
          return deferred;
        });
        AddEvents.event.ep_id = '55e78700e4b016c1bcd784ab';
        AddEvents.event.title = 'All Black Affair';
        AddEvents.currentObject.category = {id: '54b83a2be4b06b211e892748'};
        AddEvents.currentObject.startDate = '32/32/2015+456';
        AddEvents.addEvent();
        $rootScope.$digest();
        expect(MessageService.showFailedMessage).toHaveBeenCalled();
      });
      it('when invalid AddEvents.currentObject.endDate', function () {
        EventAddEditApiService.addEditEvent.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve({status: 'ALREADY_EXISTS'});
          deferred.$promise = deferred.promise;
          return deferred;
        });
        AddEvents.event.ep_id = '55e78700e4b016c1bcd784ab';
        AddEvents.event.title = 'All Black Affair';
        AddEvents.currentObject.category = {id: '54b83a2be4b06b211e892748'};
        AddEvents.currentObject.endDate = '32/32/2015+456';
        AddEvents.addEvent();
        $rootScope.$digest();
        expect(MessageService.showFailedMessage).toHaveBeenCalled();
      });
      it('when invalid AddEvents.currentObject.recurrenceEndDate', function () {
        EventAddEditApiService.addEditEvent.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve({status: 'ALREADY_EXISTS'});
          deferred.$promise = deferred.promise;
          return deferred;
        });
        AddEvents.event.ep_id = '55e78700e4b016c1bcd784ab';
        AddEvents.event.title = 'All Black Affair';
        AddEvents.currentObject.category = {id: '54b83a2be4b06b211e892748'};
        AddEvents.currentObject.recurrenceEndDate = '32/32/2015+456';
        AddEvents.addEvent();
        $rootScope.$digest();
        expect(MessageService.showFailedMessage).toHaveBeenCalled();
      });
      it('when start date greater then end date', function () {
        EventAddEditApiService.addEditEvent.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve({status: 'ALREADY_EXISTS'});
          deferred.$promise = deferred.promise;
          return deferred;
        });
        AddEvents.event.ep_id = '55e78700e4b016c1bcd784ab';
        AddEvents.event.title = 'All Black Affair';
        AddEvents.currentObject.category = {id: '54b83a2be4b06b211e892748'};
        AddEvents.currentObject.startDate = 'Thu Dec 18 2015 15:36:23 GMT+0530 (IST)';
        AddEvents.currentObject.endDate = 'Thu Dec 17 2015 15:36:23 GMT+0530 (IST)';
        AddEvents.addEvent();
        $rootScope.$digest();
        expect(MessageService.showFailedMessage).toHaveBeenCalled();
      });
      it('when recurrence type is not NEVER greater and end date greater then recurrence date', function () {
        EventAddEditApiService.addEditEvent.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve({status: 'ALREADY_EXISTS'});
          deferred.$promise = deferred.promise;
          return deferred;
        });
        AddEvents.event.ep_id = '55e78700e4b016c1bcd784ab';
        AddEvents.event.title = 'All Black Affair';
        AddEvents.currentObject.category = {id: '54b83a2be4b06b211e892748'};
        AddEvents.currentObject.startDate = 'Thu Dec 15 2015 15:36:23 GMT+0530 (IST)';
        AddEvents.currentObject.endDate = 'Thu Dec 16 2015 15:36:23 GMT+0530 (IST)';
        AddEvents.currentObject.recurrenceDate = 'Thu Dec 15 2015 15:36:23 GMT+0530 (IST)';
        AddEvents.currentObject.recurrenceType = 'WEEKLY';
        AddEvents.addEvent();
        $rootScope.$digest();
        expect(MessageService.showFailedMessage).toHaveBeenCalled();
      });
    });
  });

  describe('Function Unit: AddEvents.changeRecurrenceEndDate', function () {
    it('Should be defined and be a function', function () {
      expect(AddEvents.changeRecurrenceEndDate).toBeDefined();
      expect(typeof AddEvents.changeRecurrenceEndDate).toEqual('function');
    });
    it('Should be change recurrence end date', function () {
      var dateString = 'Thu Dec 25 2015 15:36:23 GMT+0530 (IST)';
      var expectedDate = '';
      var resultDate = '';
      AddEvents.changeRecurrenceEndDate(dateString);
      expectedDate = $filter('date')(new Date(dateString), 'MM/dd/yyyy');
      resultDate = $filter('date')(AddEvents.currentObject.recurrenceEndDate, 'MM/dd/yyyy');
      $rootScope.$digest();
      expect(resultDate).toEqual(expectedDate);
    });
  });

  describe('Function Unit: AddEvents.changeEndDate', function () {
    it('Should be defined and be a function', function () {
      expect(AddEvents.changeEndDate).toBeDefined();
      expect(typeof AddEvents.changeEndDate).toEqual('function');
    });
    it('Should change event end date', function () {
      var dateString = 'Thu Dec 17 2015 15:36:23 GMT+0530 (IST)';
      var expectedDate = '';
      var resultDate = '';
      AddEvents.changeEndDate(dateString);
      expectedDate = $filter('date')(new Date(dateString), 'MM/dd/yyyy');
      resultDate = $filter('date')(AddEvents.currentObject.endDate, 'MM/dd/yyyy');
      $rootScope.$digest();
      expect(resultDate).toEqual(expectedDate);
    });
  });

  describe('Function Unit: AddEvents.changeEndTime', function () {
    it('Should be defined and be a function', function () {
      expect(AddEvents.changeEndTime).toBeDefined();
      expect(typeof AddEvents.changeEndTime).toEqual('function');
    });
    it('Should change event end time', function () {
      var dateString = 'Thu Dec 17 2015 15:36:23 GMT+0530 (IST)';
      var expectedEndTime = '';
      var resultEndTime = '';
      AddEvents.changeEndTime(dateString);
      expectedEndTime = $filter('date')(new Date(dateString), 'hh:mm a');
      resultEndTime = $filter('date')(AddEvents.currentObject.endDate, 'hh:mm a');
      $rootScope.$digest();
      expect(resultEndTime).toEqual(expectedEndTime);
    });
  });

  describe('Function Unit: AddEvents.changeStartDate', function () {
    it('Should be defined and be a function', function () {
      expect(AddEvents.changeStartDate).toBeDefined();
      expect(typeof AddEvents.changeStartDate).toEqual('function');
    });
    it('Should change event start date', function () {
      var dateString = 'Thu Dec 15 2015 15:36:23 GMT+0530 (IST)';
      var expectedDate = '';
      var resultDate = '';
      AddEvents.changeStartDate(dateString);
      expectedDate = $filter('date')(new Date(dateString), 'MM/dd/yyyy');
      resultDate = $filter('date')(AddEvents.currentObject.startDate, 'MM/dd/yyyy');
      $rootScope.$digest();
      expect(resultDate).toEqual(expectedDate);
    });
  });

  describe('Function Unit: AddEvents.changeStartTime', function () {
    it('Should be defined and be a function', function () {
      expect(AddEvents.changeStartTime).toBeDefined();
      expect(typeof AddEvents.changeStartTime).toEqual('function');
    });
    it('Should change event start time', function () {
      var dateString = 'Thu Dec 15 2015 18:45:53 GMT+0530 (IST)';
      var expectedEndTime = '';
      var resultEndTime = '';
      AddEvents.changeStartTime(dateString);
      expectedEndTime = $filter('date')(new Date(dateString), 'hh:mm a');
      resultEndTime = $filter('date')(AddEvents.currentObject.startDate, 'hh:mm a');
      $rootScope.$digest();
      expect(resultEndTime).toEqual(expectedEndTime);
    });
  });

  describe('Function Unit: AddEvents.searchPlaces', function () {
    it('Should be defined and be a function', function () {
      expect(AddEvents.searchPlaces).toBeDefined();
      expect(typeof AddEvents.searchPlaces).toEqual('function');
    });
    it('Should reject places list and $modal.open should not invoked', function () {
      EventPlaceApiService.getPlacesList.and.callFake(function () {
        var deferred = $q.defer();
        deferred.reject();
        deferred.$promise = deferred.promise;
        return deferred;
      });
      AddEvents.searchPlaces();
      $rootScope.$digest();
      expect(LoaderService.Loader.hideSpinner).toHaveBeenCalled();
    });
    describe('Should resolve places list', function () {
      beforeEach(function () {
        EventPlaceApiService.getPlacesList.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve(fakeResponse.fakePlaceList);
          deferred.$promise = deferred.promise;
          return deferred;
        });
      });
      it('when $modal.open invoked and clicked Ok', function () {
        $modal.open.and.callFake(function (obj) {
          var deferred = $q.defer();
          obj.resolve.PlaceResult();
          deferred.resolve(fakeResponse.fakePlaceList[0]);
          return {
            result: deferred.promise
          };
        });
        AddEvents.searchPlaces();
        $rootScope.$digest();
        expect(AddEvents.event.p_id).toEqual('546aff5be4b0370c32328fbb');
        expect(AddEvents.event.place_name).toEqual('Phoenix Rock Gym');
      });

      it('when $modal.open invoked and clicked Cancel', function () {
        $modal.open.and.callFake(function (obj) {
          var deferred = $q.defer();
          obj.resolve.PlaceResult();
          deferred.reject();
          return {
            result: deferred.promise
          };
        });
        AddEvents.searchPlaces();
        $rootScope.$digest();
        expect(AddEvents.event.p_id).not.toEqual('546aff5be4b0370c32328fbb');
      });
    });
  });

  describe('Function Unit: AddEvents.uploadFile', function () {
    it('Should be defined and be a function', function () {
      expect(AddEvents.uploadFile).toBeDefined();
      expect(typeof AddEvents.uploadFile).toEqual('function');
    });
    describe('Should upload file on KWAD and aws s3 server', function () {
      it('On existing event', function () {
        AddEvents.photos = [];
        AddEvents.event.photos = null;
        AddEvents.event.title = 'All White Affair';
        AddEvents.selectedFiles = fakeResponse.fakeJpegFile;
        AddEvents.event.ep_id = '55e78700e4b016c1bcd784ab';
        AddAssetsService.addAsset.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve({data: [fakeResponse.fakeAddedAsset]});
          deferred.$promise = deferred.promise;
          return deferred;
        });
        AWSUploadService.uploadFile.and.callFake(function (file, AWSObj, options) {
          var deferred = $q.defer();
          deferred.resolve({Location: 'https://s3.amazonaws.com/kwad-bump-assets//5592497703cef8958d68d1a5/56559821e4b0da91f9e72234/raw_upload'});
          return deferred.promise;
        });
        EventAddEditApiService.addEditEvent.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve(fakeResponse.fakeAddBannerImage);
          deferred.$promise = deferred.promise;
          return deferred;
        });
        AddEvents.uploadFile();
        $rootScope.$digest();
        expect(AddEvents.event.photos.length).toEqual(1);
        expect(AddEvents.event.photos[0]).toEqual('5672a991e4b02b658c9278dd');
      });
      it('By posting event first', function () {
        AddEvents.photos = [];
        AddEvents.event.photos = null;
        AddEvents.event.ep_id = null;
        AddEvents.event.title = 'All White Affair';
        AddEvents.selectedFiles = fakeResponse.fakeJpegFile;
        AddEvents.currentObject.category = {id: '54b83a2be4b06b211e892748'};
        AddAssetsService.addAsset.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve({data: [fakeResponse.fakeAddedAsset]});
          deferred.$promise = deferred.promise;
          return deferred;
        });
        EventAddEditApiService.addEditEvent.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve({status: 'SUCCESS', data: EventData});
          deferred.$promise = deferred.promise;
          return deferred;
        });
        AWSUploadService.uploadFile.and.callFake(function (file, AWSObj, options) {
          var deferred = $q.defer();
          deferred.resolve({Location: 'https://s3.amazonaws.com/kwad-bump-assets//5592497703cef8958d68d1a5/56559821e4b0da91f9e72234/raw_upload'});
          return deferred.promise;
        });
        AddEvents.uploadFile();
        $rootScope.$digest();
        expect(AddEvents.event.photos.length).toEqual(1);
        expect(AddEvents.event.photos[0]).toEqual('5672a991e4b02b658c9278dd');
      });
    });
    describe('Should not upload file', function () {
      it('When event title is empty', function () {
        AddEvents.photos = [];
        AddEvents.event.photos = null;
        AddEvents.event.title = '';
        AddEvents.uploadFile();
        $rootScope.$digest();
        expect(AddEvents.event.photos).toEqual(null);
      });
      it('When length of selected files is zero', function () {
        AddEvents.photos = [];
        AddEvents.event.photos = null;
        AddEvents.event.title = 'All White Affair';
        AddEvents.selectedFiles = [];
        AddEvents.uploadFile();
        $rootScope.$digest();
        expect(AddEvents.event.photos).toEqual(null);
      });
      it('When length of selected files greater then ten', function () {
        AddEvents.photos = [];
        AddEvents.event.photos = null;
        AddEvents.event.title = 'All White Affair';
        AddEvents.selectedFiles = {length: 12};
        AddEvents.uploadFile();
        $rootScope.$digest();
        expect(AddEvents.event.photos).toEqual(null);
      });
      it('when EventAddEditApiService.addEditEvent rejected', function () {
        AddEvents.photos = [];
        AddEvents.event.photos = null;
        AddEvents.event.ep_id = null;
        AddEvents.event.title = 'All White Affair';
        AddEvents.selectedFiles = fakeResponse.fakeJpegFile;
        AddEvents.currentObject.category = {id: '54b83a2be4b06b211e892748'};
        AddAssetsService.addAsset.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve({data: [fakeResponse.fakeAddedAsset]});
          deferred.$promise = deferred.promise;
          return deferred;
        });
        EventAddEditApiService.addEditEvent.and.callFake(function () {
          var deferred = $q.defer();
          deferred.reject();
          deferred.$promise = deferred.promise;
          return deferred;
        });
        AddEvents.uploadFile();
        $rootScope.$digest();
        expect(AddEvents.event.photos).toEqual(null);
      });
      it('when AWSUploadService.uploadFile rejected', function () {
        AddEvents.photos = [];
        AddEvents.event.photos = null;
        AddEvents.event.ep_id = '55e78700e4b016c1bcd784ab';
        AddEvents.event.title = 'All White Affair';
        AddEvents.selectedFiles = fakeResponse.fakeJpgFile;
        AddEvents.currentObject.category = {id: '54b83a2be4b06b211e892748'};
        AddAssetsService.addAsset.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve({data: [fakeResponse.fakeAddedAsset]});
          deferred.$promise = deferred.promise;
          return deferred;
        });
        AWSUploadService.uploadFile.and.callFake(function (file, AWSObj, options) {
          var deferred = $q.defer();
          deferred.reject();
          return deferred.promise;
        });
        AddEvents.uploadFile();
        $rootScope.$digest();
        expect(AddEvents.event.photos).toEqual(null);
      });
      it('when AWSUploadService.uploadFile rejected when file type is video', function () {
        AddEvents.photos = [];
        AddEvents.event.photos = null;
        AddEvents.event.ep_id = '55e78700e4b016c1bcd784ab';
        AddEvents.event.title = 'All White Affair';
        AddEvents.selectedFiles = fakeResponse.fakeVideoFile;
        AddEvents.currentObject.category = {id: '54b83a2be4b06b211e892748'};
        AddAssetsService.addAsset.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve({data: [fakeResponse.fakeAddedAsset]});
          deferred.$promise = deferred.promise;
          return deferred;
        });
        AWSUploadService.uploadFile.and.callFake(function (file, AWSObj, options) {
          var deferred = $q.defer();
          deferred.reject();
          return deferred.promise;
        });
        AddEvents.uploadFile();
        $rootScope.$digest();
        expect(AddEvents.event.photos).toEqual(null);
      });
      it('when AWSUploadService.uploadFile rejected when file type is audio', function () {
        AddEvents.photos = [];
        AddEvents.event.photos = null;
        AddEvents.event.ep_id = '55e78700e4b016c1bcd784ab';
        AddEvents.event.title = 'All White Affair';
        AddEvents.selectedFiles = fakeResponse.fakeAudioFile;
        AddEvents.currentObject.category = {id: '54b83a2be4b06b211e892748'};
        AddAssetsService.addAsset.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve({data: [fakeResponse.fakeAddedAsset]});
          deferred.$promise = deferred.promise;
          return deferred;
        });
        AWSUploadService.uploadFile.and.callFake(function (file, AWSObj, options) {
          var deferred = $q.defer();
          deferred.reject();
          return deferred.promise;
        });
        AddEvents.uploadFile();
        $rootScope.$digest();
        expect(AddEvents.event.photos).toEqual(null);
      });
      it('when AddAssetsService.addAsset rejected', function () {
        AddEvents.photos = [];
        AddEvents.event.photos = null;
        AddEvents.event.ep_id = '55e78700e4b016c1bcd784ab';
        AddEvents.event.title = 'All White Affair';
        AddEvents.selectedFiles = fakeResponse.fakeJpegFile;
        AddEvents.currentObject.category = {id: '54b83a2be4b06b211e892748'};
        AddAssetsService.addAsset.and.callFake(function () {
          var deferred = $q.defer();
          deferred.reject();
          deferred.$promise = deferred.promise;
          return deferred;
        });
        AddEvents.uploadFile();
        $rootScope.$digest();
        expect(AddEvents.event.photos).toEqual(null);
      });
      it('when posting event first if EventAddEditApiService.addEditEvent return 200 with error status', function () {
        AddEvents.photos = [];
        AddEvents.event.photos = null;
        AddEvents.event.ep_id = null;
        AddEvents.event.title = 'All White Affair';
        AddEvents.selectedFiles = fakeResponse.fakeJpegFile;
        AddEvents.currentObject.category = {id: '54b83a2be4b06b211e892748'};
        EventAddEditApiService.addEditEvent.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve({status: 'INSUFFICIENT_RIGHTS'});
          deferred.$promise = deferred.promise;
          return deferred;
        });
        AddEvents.uploadFile();
        $rootScope.$digest();
        expect(AddEvents.event.photos).toEqual(null);
      });
    });
  });

  describe('Function Unit: AddEvents.fetchAssets', function () {
    it('Should be defined and be a function', function () {
      expect(AddEvents.fetchAssets).toBeDefined();
      expect(typeof AddEvents.fetchAssets).toEqual('function');
    });
    it('Should fetch images', function () {
      AddEvents.photos = [];
      FetchAssetsService.fetchAsset.and.callFake(function () {
        var deferred = $q.defer();
        deferred.resolve(fakeResponse.fakeFetchedAsset);
        deferred.$promise = deferred.promise;
        return deferred;
      });
      AddEvents.fetchAssets('56559821e4b0da91f9e72234');
      $rootScope.$digest();
      expect(AddEvents.photos.length).toEqual(1);
    });
    it('Should fetch data with no image prefixes', function () {
      AddEvents.photos = [];
      FetchAssetsService.fetchAsset.and.callFake(function () {
        var deferred = $q.defer();
        deferred.resolve({
          aws_key: 'ASIAIG63HOWFIRIEQCJQ',
          aws_secret: 'z3ZnvoIHEBVFe21GuiqxOLuJ+CSIBlguUbMu5c2h',
          aws_token: 'AQoDYXdzEEYasAODTH4AOcylcz+EkZt4QEaYlmOiUe/e7HfuIrVhdnLET3djkip1VXCM571/ipl1qe8gt7Kek2JCWNJFCPlAIn7gkWFIb3qNpHip/yhCXZedyFiiaQGhfcWPhhbNFL+yL+vX3BpLO3Vu4+a2p/4V+egycRQp60QPmpgH0KQAqHDCnBf4bVYBf+bSJIQGM09WpwaSOAaiZ2UZdeqAC39CLdwLlqe85NJ0kMT4B3SJCLKmxbeUaZQtV8P9LXQygNoAlqmNA6S38HFtByRrU32wiSTVeGXDiUTRxJmdcYKgUNZQMsaRTuj2llwN5kZLL1SoBpO8Sw6XFBIDOgFWIf0VkAQJOH3RUg7T/udeyvOqNVNhPelUBraz8+IxeYoxjEThEXDfTzMkelrhqpT2DNCLWmCtMlJst505QH5pkQgq6CxVQTA7504nTbQ9iKphtttgDgHcS6zwsTsa0F6rJOV3SH/rR857NT83ImXw3kGw+0edJL1IAFnEIc+H7WBjvZhx6pokP+jEgakL6n/o3fCm6/bNmsxozNonPQeUG6zrgkdTK+MfJ1TD2oYXg3fghDpXDMsgjqDAswU=',
          prefixes: [],
          s3_bucket: 'kwad-bump-assets'
        });
        deferred.$promise = deferred.promise;
        return deferred;
      });
      AddEvents.fetchAssets('56559821e4b0da91f9e72234');
      $rootScope.$digest();
      expect(AddEvents.photos.length).toEqual(0);
    });
    it('Should not fetch data when asset id is undefined', function () {
      AddEvents.photos = [];
      AddEvents.fetchAssets();
      $rootScope.$digest();
      expect(AddEvents.photos.length).toEqual(0);
    });
    it('Should not fetch data when FetchAssetsService.fetchAsset rejected', function () {
      AddEvents.photos = [];
      FetchAssetsService.fetchAsset.and.callFake(function () {
        var deferred = $q.defer();
        deferred.reject();
        deferred.$promise = deferred.promise;
        return deferred;
      });
      AddEvents.fetchAssets('56559821e4b0da91f9e72234');
      $rootScope.$digest();
      expect(AddEvents.photos.length).toEqual(0);
    });
  });
});