'use strict';

(function (angular) {
  angular.module('kwadWeb')
    .controller('AddEventsCtrl', ['$rootScope', '$scope', '$q', 'AuthService', '$filter', '$modal', '$log', '$state', 'EventData', 'PlaceData', 'EventAddEditApiService', 'AddAssetsService', 'FetchAssetsService', 'DeleteAssetsService', 'AWSUploadService', 'EnumsService', 'EventPlaceApiService', 'LoaderService', 'MessageService', 'ToastrMessage', 'AWS_UPLOAD',
      function ($rootScope, $scope, $q, AuthService, $filter, $modal, $log, $state, EventData, PlaceData, EventAddEditApiService, AddAssetsService, FetchAssetsService, DeleteAssetsService, AWSUploadService, EnumsService, EventPlaceApiService, LoaderService, MessageService, ToastrMessage, AWS_UPLOAD) {
        // Define variables
        var newDate = new Date();
        var isNewEventSaved = false;
        var AddEvents = this;
        AddEvents.event = EventData ? EventData : {};

        if (PlaceData && PlaceData.hasOwnProperty('ep_id')) {
          AddEvents.event.place_name = PlaceData.title;
          AddEvents.event.p_id = PlaceData.ep_id;
        }

        AddEvents.event.title = '';
        AddEvents.event.ep_id = null;
        AddEvents.event.keywords = AddEvents.event.keywords || [];
        AddEvents.currentObject = {};
        AddEvents.currentObject.bump_type = AddEvents.event.bump_type === 'BUMP';
        AddEvents.EP_TYPES = null;
        AddEvents.EVENT_CATEGORIES = null;
        AddEvents.EVENT_RECURRENCE_TYPE = null;
        AddEvents.EVENT_CATEGORY_TYPES = null;
        AddEvents.currentObject.categoryType = AddEvents.event.private ? 'PRIVATE' : 'PUBLIC';
        AddEvents.currentObject.recurrenceType = AddEvents.event.recurrence_type ? AddEvents.event.recurrence_type : 'NEVER';
        AddEvents.currentObject.startDate = new Date();
        AddEvents.currentObject.endDate = new Date();
        AddEvents.currentObject.recurrenceEndDate = new Date(newDate.setDate(newDate.getDate() + 1));
        AddEvents.selectedFiles = '';
        AddEvents.photos = [];
        AddEvents.ASSET_TYPES = EnumsService.getAssetTypeEnums();
        AddEvents.USER_ROLES = null;
        AddEvents.USER_ROLES_OBJ = null;


        var _successAddEvent = function (response) {
          LoaderService.Loader.hideSpinner();
          if (response.status === 'SUCCESS') {
            isNewEventSaved = true;
            MessageService.showSuccessfulMessage(ToastrMessage.EVENT.ADD[response.status]);
            $state.go('eventDetails', {eventId: response.data.ep_id});
          } else {
            MessageService.showFailedMessage(ToastrMessage.EVENT.ADD[response.status]);
          }
        };
        var _failAddEvent = function (err) {
          LoaderService.Loader.hideSpinner();
          switch (err) {
            case 'INVALID_START_DATE' :
              MessageService.showFailedMessage(ToastrMessage.DATE_VALIDATE.INVALID_START_DATE);
              break;
            case 'INVALID_END_DATE' :
              MessageService.showFailedMessage(ToastrMessage.DATE_VALIDATE.INVALID_END_DATE);
              break;
            case 'INVALID_RECURRENCE_DATE' :
              MessageService.showFailedMessage(ToastrMessage.DATE_VALIDATE.INVALID_RECURRENCE_DATE);
              break;
            case 'START_DATE_ERROR' :
              MessageService.showFailedMessage(ToastrMessage.DATE_VALIDATE.START_DATE_ERROR);
              break;
            case 'RECURRENCE_DATE_ERROR' :
              MessageService.showFailedMessage(ToastrMessage.DATE_VALIDATE.RECURRENCE_DATE_ERROR);
              break;
            case 'EMPTY_EVENT' :
              MessageService.showFailedMessage(ToastrMessage.DATA_VALIDATE.EMPTY_EVENT);
              break;
            case 'EMPTY_PLACE' :
              MessageService.showFailedMessage(ToastrMessage.DATA_VALIDATE.EMPTY_PLACE);
              break;
            default :
              MessageService.showFailedMessage();
              break;
          }
        };

        var userRoleEnumsSuccess = function (userRoles) {
          AddEvents.USER_ROLES = angular.copy(userRoles);
          AddEvents.USER_ROLES_OBJ = userRoles.toObject();
          AuthService.getCurrentUser().then(function (user) {
            AddEvents.currentUser = user;
            switch (AddEvents.currentUser.role) {
              case AddEvents.USER_ROLES_OBJ.ADMIN:
              case AddEvents.USER_ROLES_OBJ.LAUNCHSQUAD:
                AddEvents.EVENT_CATEGORY_TYPES = ['PRIVATE', 'PUBLIC'];
                break;
              case AddEvents.USER_ROLES_OBJ.REGULAR:
                AddEvents.EVENT_CATEGORY_TYPES = null;
                break;
              default:
                AddEvents.EVENT_CATEGORY_TYPES = ['PUBLIC'];
                break;
            }
          }, function (err) {
            AddEvents.currentUser = null;
            MessageService.showFailedMessage();
          });
        };
        var userRoleEnumsFailure = function () {
          MessageService.showFailedMessage();
          LoaderService.Loader.hideSpinner();
        };

        function addEvent() {
          var deferred = $q.defer();
          var start_date = $filter('date')(AddEvents.currentObject.startDate, 'yyyy-MM-ddTHH:mm:ss.sssZ');
          var end_date = $filter('date')(AddEvents.currentObject.endDate, 'yyyy-MM-ddTHH:mm:ss.sssZ');
          var recurrence_end_date = $filter('date')(AddEvents.currentObject.recurrenceEndDate, 'yyyy-MM-ddTHH:mm:ss.sssZ');
          var _payload = null;

          if (new Date(start_date.split('+')[0]) == "Invalid Date") {
            deferred.reject('INVALID_START_DATE');
          } else if (new Date(end_date.split('+')[0]) == "Invalid Date") {
            deferred.reject('INVALID_END_DATE');
          } else if (new Date(recurrence_end_date.split('+')[0]) == "Invalid Date") {
            deferred.reject('INVALID_RECURRENCE_DATE');
          } else if (start_date > end_date) {
            deferred.reject('START_DATE_ERROR');
          } else if (AddEvents.currentObject.recurrenceType !== 'NEVER' && recurrence_end_date <= end_date) {
            deferred.reject('RECURRENCE_DATE_ERROR');
          } else if (!AddEvents.event.title) {
            deferred.reject('EMPTY_EVENT');
          } else if (!AddEvents.event.p_id || !AddEvents.event.place_name) {
            deferred.reject('EMPTY_PLACE');
          } else {
            _payload = {
              p_id: AddEvents.event.p_id,
              category_id: AddEvents.currentObject.category.id,
              title: AddEvents.event.title,
              description: AddEvents.event.description || '',
              keywords: AddEvents.event.keywords || [],
              private: AddEvents.currentObject.categoryType === 'PRIVATE',
              disabled: AddEvents.currentObject.status === 'DISABLED',
              start_time: start_date,
              end_time: end_date,
              recurrence_type: AddEvents.currentObject.recurrenceType,
              recurrence_end_date: recurrence_end_date,
              bump_type: AddEvents.currentObject.bump_type ? 'BUMP' : 'NON_BUMP'
            };

            if (AddEvents.event.ep_id) {
              _payload.e_id = AddEvents.event.ep_id;
              _payload.photos = AddEvents.event.photos;
            }

            EventAddEditApiService.addEditEvent(_payload).$promise.then(function (resp) {
              deferred.resolve(resp);
            }, function (err) {
              deferred.reject(err);
            });
          }
          return deferred.promise;
        }

        function getPhotosUrls(photos) {
          if (!Array.isArray(photos) || !photos.length) {
            return false;
          }
          var chunks = photos.chunks(10);
          chunks.forEach(function (chunk) {
            AddEvents.fetchAssets(chunk);
          });
        }

        function getAssetType(type) {
          switch (type) {
            case'image/jpg':
              return AddEvents.ASSET_TYPES.IMAGE_JPG;
            case'image/jpeg':
              return AddEvents.ASSET_TYPES.IMAGE_JPEG;
            case'video/*':
              return AddEvents.ASSET_TYPES.VIDEO;
            default :
              return false;
          }
        }

        function fetchAsset(AWSObj) {
          var bucketName = AWSObj.s3_bucket;//'dev-web.whatsbumpin.net'
          var IMAGE_URL = '';
          if (Array.isArray(AWSObj.prefixes) && AWSObj.prefixes.length) {
            AWSObj.prefixes.forEach(function (prefix) {
              IMAGE_URL = AWS_UPLOAD.URL + bucketName + '/' + prefix + '/raw_upload';
              AddEvents.photos.push({url: IMAGE_URL, id: prefix.split('/')[2]});
            });
          } else {
            MessageService.showFailedMessage();
          }
        }

        function uploadFile(file, AWSObj) {
          var options = {quality: 72};
          AWSUploadService.uploadFile(file, AWSObj, options).then(function (response) {
            var asset = {url: response.Location, id: AWSObj.asset_id};
            LoaderService.Loader.hideSpinner();
            if (AddEvents.photos.length === 0) {
              AddEvents.event.banner_asset = asset.id;
              AddEvents.addBannerImage(asset);
            }
            AddEvents.photos.push(asset);
            if (!AddEvents.event.photos) {
              AddEvents.event.photos = [];
            }
            AddEvents.event.photos.push(AWSObj.asset_id);
            MessageService.showSuccessfulMessage(ToastrMessage.UPLOAD.SUCCESS);
          }, function () {
            LoaderService.Loader.hideSpinner();
            MessageService.showFailedMessage();
          });
        }

        function addBannerImage(eventId, asset) {
          var _payload = {
            e_id: eventId,
            banner_asset: asset.id
          };

          var _successAddBannerImage = function (response) {
            LoaderService.Loader.hideSpinner();
            if (response.status === 'SUCCESS') {
              if (AddEvents.event.banner_asset !== response.data.banner_asset) {
                AddEvents.event.banner_asset = response.data.banner_asset;
              }
              MessageService.showSuccessfulMessage(ToastrMessage.EVENT.UPDATE[response.status]);
            } else {
              MessageService.showFailedMessage(ToastrMessage.EVENT.UPDATE[response.status]);
            }
          };
          var _failAddBannerImage = function (err) {
            LoaderService.Loader.hideSpinner();
            MessageService.showFailedMessage();
          };

          EventAddEditApiService.addEditEvent(_payload).$promise.then(_successAddBannerImage, _failAddBannerImage);
        }

        AddEvents.fetchAssets = function (asset_ids) {
          LoaderService.Loader.showSpinner();
          var _payload = [],
            success = function (response) {
              LoaderService.Loader.hideSpinner();
              var AWSObj = response.data ? response.data : response;
              fetchAsset(AWSObj);
            },
            failure = function (err) {
              LoaderService.Loader.hideSpinner();
              MessageService.showFailedMessage();
            };
          if (typeof asset_ids === 'string') {
            _payload.push({
              asset_id: asset_ids
            });
          }
          else if (Array.isArray(asset_ids) && asset_ids.length) {
            asset_ids.forEach(function (asset_id) {
              _payload.push({
                asset_id: asset_id
              });
            });
          } else {
            LoaderService.Loader.hideSpinner();
            MessageService.showFailedMessage(ToastrMessage.ASSET.ASSET_ID_NOT_FOUND);
            return false;
          }
          FetchAssetsService.fetchAsset(_payload).$promise.then(success, failure);
        };

        AddEvents.uploadFile = function () {
          LoaderService.Loader.showSpinner();

          if (!AddEvents.event.title) {
            MessageService.showFailedMessage(ToastrMessage.DATA_VALIDATE.EMPTY_EVENT);
            LoaderService.Loader.hideSpinner();
            return false;
          }

          if (!AddEvents.selectedFiles || !AddEvents.selectedFiles.length) {
            LoaderService.Loader.hideSpinner();
            MessageService.showFailedMessage(ToastrMessage.UPLOAD.FILE_NOT_FOUND);
            return false;
          }

          if (AddEvents.selectedFiles.length > 10) {
            LoaderService.Loader.hideSpinner();
            MessageService.showFailedMessage(ToastrMessage.UPLOAD.MAX_FILES);
            return false;
          }

          var _payload = [],
            success = function (response) {
              var AWSObjs = response.data && response.data.length ? response.data : false;
              for (var j = 0; j < AWSObjs.length; j++) {
                uploadFile(AddEvents.selectedFiles[j], AWSObjs[j]);
              }
            },
            failure = function (err) {
              LoaderService.Loader.hideSpinner();
              MessageService.showFailedMessage();
            },
            getAwsCredentialsForSelectedFiles = function () {
              for (var i = 0; i < AddEvents.selectedFiles.length; i++) {
                var file = AddEvents.selectedFiles[i];
                _payload.push({
                  file: file.name,
                  type: getAssetType(file.type),
                  ep_id: AddEvents.event.ep_id
                });
              }
              AddAssetsService.addAsset(_payload).$promise.then(success, failure);
            };

          if (AddEvents.event.ep_id) {
            getAwsCredentialsForSelectedFiles();
          } else {
            addEvent().then(function (response) {
              if (response.status === 'SUCCESS') {
                AddEvents.event.ep_id = response.data.ep_id;
                getAwsCredentialsForSelectedFiles();
              } else {
                MessageService.showFailedMessage(ToastrMessage.EVENT.ADD[response.status]);
              }
            }, _failAddEvent)
          }
        };

        AddEvents.init = function () {
          EnumsService.getUserRoleEnums().then(userRoleEnumsSuccess, userRoleEnumsFailure);
          EnumsService.getRecurrenceTypeEnums().then(function (result) {
            AddEvents.EVENT_RECURRENCE_TYPE = result;
          }, function (error) {
            console.log('EVENT_RECURRENCE_TYPE ERROR: ', error);
          });
          EnumsService.getEpTypeEnums().then(function (result) {
            AddEvents.EP_TYPES = result;
          }, function (error) {
            console.log('EP_TYPES ERROR: ', error);
          });
          EnumsService.getEventCategoryEnums().then(function (result) {
            AddEvents.EVENT_CATEGORIES = [];
            Object.keys(result).forEach(function (key) {
              AddEvents.EVENT_CATEGORIES.push({
                id: key, value: result[key]
              })
            });
            var index = AddEvents.EVENT_CATEGORIES.map(function (obj) {
                return obj.id;
              }).indexOf(AddEvents.event.cat_id) || 0;
            index = index !== -1 ? index : 0;
            AddEvents.currentObject.category = AddEvents.EVENT_CATEGORIES[index];
          }, function (error) {
            AddEvents.EVENT_CATEGORIES = null;
          });
          getPhotosUrls(AddEvents.event.photos);
        };

        AddEvents.addNewKeyword = function () {
          if (AddEvents.newTag) {
            AddEvents.event.keywords.push(AddEvents.newTag);
            AddEvents.newTag = '';
          }
        };

        AddEvents.removeKeyword = function (index) {
          AddEvents.event.keywords.splice(index, 1);
        };

        AddEvents.searchPlaces = function () {
          LoaderService.Loader.showSpinner();
          var _successGetPlaces = function (response) {
            LoaderService.Loader.hideSpinner();
            var modalInstance = $modal.open({
              templateUrl: 'components/modals/selectPlace/selectPlaceContent.html',
              controllerAs: 'SelectPlaceModal',
              controller: 'SelectPlaceModalCtrl',
              size: 'md',
              resolve: {
                PlaceResult: function () {
                  return response.data;
                }
              }
            });
            modalInstance.result.then(function (place) {
              $log.info('saved place info: ', place);
              AddEvents.event.place_name = place.name;
              AddEvents.event.p_id = place.epId;
            }, function () {
              $log.info('Modal dismissed at: ' + new Date());
            });
          };
          var _failGetPlaces = function (err) {
            LoaderService.Loader.hideSpinner();
            console.log(err);
          };

          var _payloadPlace = {
            'type': 'PLACE',
            'query': AddEvents.event.place_name            
          };
          EventPlaceApiService.getPlacesList(_payloadPlace).$promise.then(_successGetPlaces, _failGetPlaces);
        };

        AddEvents.changeStartDate = function (dateString) {
          var newDate = new Date(dateString);
          AddEvents.currentObject.startDate.setDate(newDate.getDate());
          AddEvents.currentObject.startDate.setMonth(newDate.getMonth());
          AddEvents.currentObject.startDate.setFullYear(newDate.getFullYear());
        };

        AddEvents.changeStartTime = function (dateString) {
          var newDate = new Date(dateString);
          AddEvents.currentObject.startDate.setHours(newDate.getHours());
          AddEvents.currentObject.startDate.setMinutes(newDate.getMinutes());
        };

        AddEvents.changeEndDate = function (dateString) {
          var newDate = new Date(dateString);
          AddEvents.currentObject.endDate.setDate(newDate.getDate());
          AddEvents.currentObject.endDate.setMonth(newDate.getMonth());
          AddEvents.currentObject.endDate.setFullYear(newDate.getFullYear());
        };

        AddEvents.changeEndTime = function (dateString) {
          var newDate = new Date(dateString);
          AddEvents.currentObject.endDate.setHours(newDate.getHours());
          AddEvents.currentObject.endDate.setMinutes(newDate.getMinutes());
        };

        AddEvents.changeRecurrenceEndDate = function (dateString) {
          var newDate = new Date(dateString);
          AddEvents.currentObject.recurrenceEndDate.setDate(newDate.getDate());
          AddEvents.currentObject.recurrenceEndDate.setMonth(newDate.getMonth());
          AddEvents.currentObject.recurrenceEndDate.setFullYear(newDate.getFullYear());
        };

        AddEvents.addEvent = function () {
          addEvent().then(_successAddEvent, _failAddEvent)
        };

        AddEvents.isBannerImage = function (image) {
          return image.id === AddEvents.event.banner_asset;
        };

        AddEvents.addBannerImage = function (asset) {
          LoaderService.Loader.showSpinner();
          if (!asset || !asset.id) {
            LoaderService.Loader.hideSpinner();
            MessageService.showFailedMessage(ToastrMessage.ASSET.ASSET_ID_NOT_FOUND);
            return false;
          }

          if (!AddEvents.event.title) {
            MessageService.showFailedMessage(ToastrMessage.DATA_VALIDATE.EMPTY_EVENT);
            LoaderService.Loader.hideSpinner();
            return false;
          }

          if (AddEvents.event.ep_id) {
            addBannerImage(AddEvents.event.ep_id, asset)
          } else {
            LoaderService.Loader.showSpinner();
            addEvent().then(function (response) {
              LoaderService.Loader.hideSpinner();
              if (response.status === 'SUCCESS') {
                AddEvents.event.ep_id = response.data.ep_id;
                addBannerImage(AddEvents.event.ep_id, asset);
              } else {
                MessageService.showFailedMessage(ToastrMessage.EVENT.ADD[response.status]);
              }
            }, _failAddEvent)
          }
        };

        AddEvents.removeBannerImage = function (e, asset) {
          e.stopPropagation();
          LoaderService.Loader.showSpinner();
          var _payload = [],
            success = function (response) {
              LoaderService.Loader.hideSpinner();
              if (response.asset_ids && response.asset_ids.length) {
                AddEvents.photos = AddEvents.photos.filter(function (el) {
                  return el.id !== response.asset_ids[0];
                });
                MessageService.showSuccessfulMessage(ToastrMessage.ASSET.REMOVED_SUCCESSFULLY);
              } else {
                MessageService.showFailedMessage();
              }
            },
            failure = function (err) {
              LoaderService.Loader.hideSpinner();
              MessageService.showFailedMessage();
            };

          if (asset.id && typeof asset.id === 'string') {
            _payload.push({
              asset_id: asset.id
            });
          } else {
            LoaderService.Loader.hideSpinner();
            MessageService.showFailedMessage(ToastrMessage.ASSET.ASSET_ID_NOT_FOUND);
            return false;
          }
          DeleteAssetsService.removeAsset(_payload).$promise.then(success, failure);
        };

        $scope.$on('$stateChangeStart', function (event) {
          var answer = '';
          if (!isNewEventSaved) {
            answer = confirm("Are you sure you want to leave this page?");
            if (!answer) {
              $rootScope.$broadcast('EVENT_CANCEL');
              event.preventDefault();
            }
          }
        });
      }]);
})(angular);
