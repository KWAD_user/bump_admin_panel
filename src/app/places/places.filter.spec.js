'use strict';

describe('Unit: filterPlaceList Filter', function () {
  var $filter, placeList;
  beforeEach(function () {
    module('kwadWeb');
    placeList = [{
      'epId': '546aff5be4b0370c32328fbb',
      'name': 'Phoenix Rock Gym',
      'place': '1353 E University Drive, Tempe AZ',
      'creatorName': 'Demo  User ',
      'creatorId': '546aff58e4b0370c32328f40',
      'feedImpression': 0,
      'detailImpression': 0,
      'appCheckinCount': 26,
      'geoCheckinCount': 8,
      'shares': 26,
      'status': 'ENABLED',
      'associatedEvents': 2,
      'creatorRole': 'ADMIN',
      'eventDate': null,
      'eventTimeLine': 'UNDETERMINED'
    }, {
      'epId': '546aff5be4b0370c32328fbf',
      'name': 'The Vue on Apache',
      'place': '922 E Apache Blvd, Tempe AZ',
      'creatorName': 'Brandy Smith',
      'creatorId': '546aff59e4b0370c32328f43',
      'feedImpression': 6,
      'detailImpression': 0,
      'appCheckinCount': 24,
      'geoCheckinCount': 283,
      'shares': 24,
      'status': 'ENABLED',
      'associatedEvents': 0,
      'creatorRole': 'ANONYMOUS',
      'eventDate': null,
      'eventTimeLine': 'UNDETERMINED'
    }];

    inject(function (_$filter_) {
      $filter = _$filter_;
    });
  });
  it('should return array of all events when domain role is set as ALL', function () {
    // Arrange.
    var role = 'ALL', result;

    // Act.
    result = $filter('filterPlaceList')(placeList, role);

    // Assert.
    expect(result).toEqual(placeList);
    expect(result.length).toEqual(2);
  });
  it('should return array of PAST events when role is set as ADMIN', function () {
    // Arrange.
    var role = 'ADMIN', result;

    // Act.
    result = $filter('filterPlaceList')(placeList, role);

    // Assert.
    expect(result[0]).toEqual(placeList[0]);
    expect(result.length).toEqual(1);
  });
});
