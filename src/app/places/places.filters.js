'use strict';

/**
 * @ngdoc function
 * @name kwadWeb.Filter:domainsStatus filter
 * @description
 * # used to filter domains by status
 */
(function (angular) {
  angular.module('kwadWeb')
    .filter('filterPlaceList', [function () {
      return function (placeList, role) {
        if (role === 'ALL') {
          return placeList;
        }

        var output = [];
        if (Array.isArray(placeList) && placeList.length) {
          output = placeList.filter(function (obj) {
            return (obj.creatorRole === role);
          });
        }
        return output;
      };
    }]);
})(angular);