'use strict';

(function (angular) {
    angular.module('kwadWeb')
            .controller('PlaceDetailsCtrl', ['$scope', '$state', '$modal', '$log', 'AuthService', 'UsersApiService', 'PlaceData', 'PlaceAddEditApiService', 'AddAssetsService', 'DeleteAssetsService', 'FetchAssetsService', 'AWSUploadService', 'EnumsService', 'LoaderService', 'MessageService', 'ToastrMessage', 'AWS_UPLOAD',
                function ($scope, $state, $modal, $log, AuthService, UsersApiService, PlaceData, PlaceAddEditApiService, AddAssetsService, DeleteAssetsService, FetchAssetsService, AWSUploadService, EnumsService, LoaderService, MessageService, ToastrMessage, AWS_UPLOAD) {
                    // Define variables
                    var PlaceDetails = this;
                    PlaceDetails.currentObject = {};
                    PlaceDetails.place = PlaceData;
                    PlaceDetails.USER_ROLES = null;
                    PlaceDetails.USER_ROLES_OBJ = null;
                    PlaceDetails.EP_STATES = null;
                    PlaceDetails.EP_TYPES = null;
                    PlaceDetails.PLACE_CATEGORIES = null;
                    PlaceDetails.PLACE_CATEGORY_TYPES = null;
                    PlaceDetails.ASSET_TYPES = EnumsService.getAssetTypeEnums();
                    PlaceDetails.selectedFiles = '';
                    PlaceDetails.photos = [];
                    PlaceDetails.currentObject.status = PlaceDetails.place.disabled ? 'DISABLED' : 'ENABLED';
                    PlaceDetails.currentObject.categoryType = PlaceDetails.place.private ? 'PRIVATE' : 'PUBLIC';
                    PlaceDetails.place.keywords = PlaceDetails.place.keywords ? PlaceDetails.place.keywords : [];

                    var creatorName = PlaceDetails.place.creator_name;

                    var _successUpdatePlace = function (response) {
                        LoaderService.Loader.hideSpinner();
                        if (response.status === 'SUCCESS') {
                            if (PlaceDetails.place.banner_asset !== response.data.banner_asset) {
                                PlaceDetails.place.banner_asset = response.data.banner_asset;
                            }
                            MessageService.showSuccessfulMessage(ToastrMessage.PLACE.UPDATE[response.status]);
                        } else {
                            MessageService.showFailedMessage(ToastrMessage.PLACE.UPDATE[response.status]);
                        }
                    };
                    var _failUpdatePlace = function () {
                        LoaderService.Loader.hideSpinner();
                        MessageService.showFailedMessage();
                    };

                    var userRoleEnumsSuccess = function (userRoles) {
                        PlaceDetails.USER_ROLES = angular.copy(userRoles);
                        PlaceDetails.USER_ROLES_OBJ = userRoles.toObject();
                        AuthService.getCurrentUser().then(function (user) {
                            PlaceDetails.currentUser = user;
                            switch (PlaceDetails.currentUser.role) {
                                case PlaceDetails.USER_ROLES_OBJ.ADMIN:
                                case PlaceDetails.USER_ROLES_OBJ.LAUNCHSQUAD:
                                    PlaceDetails.PLACE_CATEGORY_TYPES = ['PRIVATE', 'PUBLIC'];
                                    break;
                                case PlaceDetails.USER_ROLES_OBJ.REGULAR:
                                    PlaceDetails.PLACE_CATEGORY_TYPES = null;
                                    break;
                                default:
                                    PlaceDetails.PLACE_CATEGORY_TYPES = ['PUBLIC'];
                                    break;
                            }
                        }, function () {
                            PlaceDetails.currentUser = null;
                            MessageService.showFailedMessage();
                        });
                    };
                    var userRoleEnumsFailure = function () {
                        MessageService.showFailedMessage();
                        LoaderService.Loader.hideSpinner();
                    };

                    function getAssetType(type) {
                        switch (type) {
                            case'image/jpg':
                                return PlaceDetails.ASSET_TYPES.IMAGE_JPG;
                            case'image/jpeg':
                                return PlaceDetails.ASSET_TYPES.IMAGE_JPEG;
                            case'video/*':
                                return PlaceDetails.ASSET_TYPES.VIDEO;
                            default :
                                return false;
                        }
                    }

                    function uploadFile(file, AWSObj) {
                        LoaderService.Loader.showSpinner();
                        var options = {quality: 72};
                        AWSUploadService.uploadFile(file, AWSObj, options).then(function (response) {
                            var asset = {url: response.Location, id: AWSObj.asset_id};
                            LoaderService.Loader.hideSpinner();
                            if (PlaceDetails.photos.length === 0) {
                                PlaceDetails.place.banner_asset = asset.id;
                                PlaceDetails.addBannerImage(asset);
                            }
                            PlaceDetails.photos.push(asset);
                            if (!PlaceDetails.place.photos) {
                                PlaceDetails.place.photos = [];
                            }
                            PlaceDetails.place.photos.push(AWSObj.asset_id);
                            MessageService.showSuccessfulMessage(ToastrMessage.UPLOAD.SUCCESS);
                        }, function () {
                            LoaderService.Loader.hideSpinner();
                            MessageService.showFailedMessage();
                        });
                    }

                    function fetchAsset(AWSObj) {
                        if (Array.isArray(AWSObj) && AWSObj.length) {
                            AWSObj.forEach(function (obj) {
                                var bucketName = obj.s3_bucket;//'dev-web.whatsbumpin.net'
                                var IMAGE_URL = '';
                                
                                IMAGE_URL = AWS_UPLOAD.URL + bucketName + '/' + obj.s3_object;
                                PlaceDetails.photos.push({url: IMAGE_URL, id: obj.prefix.split('/')[2]});
                            });
                        } else {
                            MessageService.showFailedMessage();
                        }
                    }

                    function getPhotosUrls(photos) {
                        if (!Array.isArray(photos) || !photos.length) {
                            return false;
                        }
                        var chunks = photos.chunks(10);
                        chunks.forEach(function (chunk) {
                            PlaceDetails.fetchAssets(chunk);
                        });
                    }

                    PlaceDetails.init = function () {
                        EnumsService.getUserRoleEnums().then(userRoleEnumsSuccess, userRoleEnumsFailure);
                        EnumsService.getEpStateEnums().then(function (result) {
                            PlaceDetails.EP_STATES = result;
                        }, function (error) {
                            $log.info('EP_STATES ERROR: ', error);
                        });
                        EnumsService.getEpTypeEnums().then(function (result) {
                            PlaceDetails.EP_TYPES = result;
                        }, function (error) {
                            $log.info('EP_TYPES ERROR: ', error);
                        });
                        EnumsService.getPlaceCategoryEnums().then(function (result) {
                            PlaceDetails.PLACE_CATEGORIES = [];
                            Object.keys(result).forEach(function (key) {
                                PlaceDetails.PLACE_CATEGORIES.push({
                                    id: key, value: result[key]
                                });
                            });
                            PlaceDetails.currentObject.category = {
                                id: PlaceDetails.place.cat_id, value: result[PlaceDetails.place.cat_id]
                            };
                        }, function (error) {
                            PlaceDetails.PLACE_CATEGORIES = null;
                            $log.info('Place Category ERROR: ', error);
                        });
                        getPhotosUrls(PlaceDetails.place.photos);
                    };

                    PlaceDetails.addNewKeyword = function () {
                        if (PlaceDetails.newTag) {
                            PlaceDetails.place.keywords.push(PlaceDetails.newTag);
                            PlaceDetails.newTag = '';
                        }
                    };

                    PlaceDetails.removeKeyword = function (index) {
                        PlaceDetails.place.keywords.splice(index, 1);
                    };

                    PlaceDetails.updatePlace = function () {
                        var _payload = {
                            category_id: PlaceDetails.currentObject.category.id,
                            creatorId: PlaceDetails.place.creator_id,
                            p_id: PlaceDetails.place.ep_id,
                            title: PlaceDetails.place.title,
                            description: PlaceDetails.place.description,
                            address: PlaceDetails.place.address,
                            keywords: PlaceDetails.place.keywords,
                            website: PlaceDetails.place.website,
                            location: PlaceDetails.place.location,
                            private: PlaceDetails.currentObject.categoryType === 'PRIVATE',
                            disabled: PlaceDetails.currentObject.status === 'DISABLED'
                        };

                        if (!_payload.location) {
                            MessageService.showFailedMessage(ToastrMessage.PLACE.ADD.BLANK_LOCATION);
                            return;
                        }

                        LoaderService.Loader.showSpinner();
                        PlaceAddEditApiService.addEditPlace(_payload).$promise.then(_successUpdatePlace, _failUpdatePlace);
                    };

                    PlaceDetails.addEventAtCurrentPlace = function () {
                        $state.go('addEventsFromPlace', {placeId: PlaceDetails.place.ep_id});
                    };

                    PlaceDetails.CreateNewPlaceBasedOnCurrent = function () {
                        $state.go('addPlacesFromCurrent', {placeId: PlaceDetails.place.ep_id});
                    };

                    PlaceDetails.uploadFile = function () {
                        LoaderService.Loader.showSpinner();

                        if (!PlaceDetails.selectedFiles || !PlaceDetails.selectedFiles.length) {
                            LoaderService.Loader.hideSpinner();
                            MessageService.showFailedMessage(ToastrMessage.UPLOAD.FILE_NOT_FOUND);
                            return false;
                        }

                        if (PlaceDetails.selectedFiles.length > 10) {
                            LoaderService.Loader.hideSpinner();
                            MessageService.showFailedMessage(ToastrMessage.UPLOAD.MAX_FILES);
                            return false;
                        }

                        var _payload = [],
                                success = function (response) {
                                    LoaderService.Loader.hideSpinner();
                                    var AWSObjs = response.data && response.data.length ? response.data : false;
                                    for (var j = 0; j < AWSObjs.length; j++) {
                                        uploadFile(PlaceDetails.selectedFiles[j], AWSObjs[j]);
                                    }
                                },
                                failure = function () {
                                    LoaderService.Loader.hideSpinner();
                                    MessageService.showFailedMessage();
                                };

                        for (var i = 0; i < PlaceDetails.selectedFiles.length; i++) {
                            var file = PlaceDetails.selectedFiles[i];
                            _payload.push({
                                file: file.name,
                                type: getAssetType(file.type),
                                ep_id: PlaceDetails.place.ep_id
                            });
                        }

                        AddAssetsService.addAsset(_payload).$promise.then(success, failure);
                    };

                    PlaceDetails.fetchAssets = function (asset_ids) {
                        LoaderService.Loader.showSpinner();
                        var _payload = [],
                                success = function (response) {
                                    LoaderService.Loader.hideSpinner();
                                    var AWSObj = response.data ? response.data : response;
                                    fetchAsset(AWSObj);
                                },
                                failure = function () {
                                    LoaderService.Loader.hideSpinner();
                                    MessageService.showFailedMessage();
                                };
                        if (typeof asset_ids === 'string') {
                            _payload = {"asset_ids":[asset_ids]};
                        } else if (Array.isArray(asset_ids) && asset_ids.length) {
                            _payload = {"asset_ids":asset_ids};
                        } else {
                            LoaderService.Loader.hideSpinner();
                            MessageService.showFailedMessage(ToastrMessage.ASSET.ASSET_ID_NOT_FOUND);
                            return false;
                        }
                        FetchAssetsService.fetchAsset(_payload).$promise.then(success, failure);
                    };

                    PlaceDetails.addBannerImage = function (asset) {
                        LoaderService.Loader.showSpinner();
                        if (!asset || !asset.id) {
                            LoaderService.Loader.hideSpinner();
                            MessageService.showFailedMessage(ToastrMessage.ASSET.ASSET_ID_NOT_FOUND);
                            return false;
                        }
                        var _payload = {
                            p_id: PlaceDetails.place.ep_id,
                            banner_asset: asset.id,
                            location: PlaceDetails.place.location
                        };
                        PlaceAddEditApiService.addEditPlace(_payload).$promise.then(_successUpdatePlace, _failUpdatePlace);
                    };

                    PlaceDetails.removeBannerImage = function (e, asset) {
                        e.stopPropagation();
                        LoaderService.Loader.showSpinner();
                        var _payload = [],
                                success = function (response) {
                                    LoaderService.Loader.hideSpinner();
                                    if (response.status === 'SUCCESS') {
                                        PlaceDetails.photos = PlaceDetails.photos.filter(function (el) {
                                            return el.id !== response.data[0].asset_id;
                                        });
                                        MessageService.showSuccessfulMessage(ToastrMessage.ASSET.REMOVED_SUCCESSFULLY);
                                    } else {
                                        MessageService.showFailedMessage();
                                    }
                                },
                                failure = function () {
                                    LoaderService.Loader.hideSpinner();
                                    MessageService.showFailedMessage();
                                };

                        if (asset.id && typeof asset.id === 'string') {
                            _payload.push({
                                asset_id: asset.id,
                                associated_event_place_id: PlaceDetails.place.ep_id,
                                delete_all: true
                            });
                        } else {
                            LoaderService.Loader.hideSpinner();
                            MessageService.showFailedMessage(ToastrMessage.ASSET.ASSET_ID_NOT_FOUND);
                            return false;
                        }
                        DeleteAssetsService.removeAsset(_payload).$promise.then(success, failure);
                    };

                    PlaceDetails.isBannerImage = function (image) {
                        return image.id === PlaceDetails.place.banner_asset;
                    };

                    PlaceDetails.clearData = function () {
                        PlaceDetails.place.location = '';
                    };

                    PlaceDetails.setPlaceAddress = function (data) {
                        PlaceDetails.place.location = {
                            coordinates: data.coordinates,
                            type: 'Point'
                        };
                        PlaceDetails.place.address = data.location;
                    };

                    PlaceDetails.searchCreators = function () {
                        LoaderService.Loader.showSpinner();
                        var _successUsers = function (response) {
                            LoaderService.Loader.hideSpinner();
                            var modalInstance = $modal.open({
                                templateUrl: 'components/modals/selectCreator/selectCreatorContent.html',
                                controllerAs: 'SelectCreatorModal',
                                controller: 'SelectCreatorModalCtrl',
                                size: 'md',
                                resolve: {
                                    CreatorList: function () {
                                        return response.data;
                                    }
                                }
                            });
                            modalInstance.result.then(function (user) {
                                PlaceDetails.place.creator_name = user.fullName;
                                PlaceDetails.place.creator_id = user.id;
                            }, function () {
                                PlaceDetails.place.creator_name = creatorName;
                                $log.info('Modal dismissed at: ' + new Date());
                            });
                        };
                        var _failUsers = function (err) {
                            LoaderService.Loader.hideSpinner();
                            $log.info('ERROR:', err);
                        };
                        var _payload = {
                            query: PlaceDetails.place.creator_name
                        };
                        UsersApiService.getUsers(_payload).$promise.then(_successUsers, _failUsers);
                    };
                }]);
})(angular);
