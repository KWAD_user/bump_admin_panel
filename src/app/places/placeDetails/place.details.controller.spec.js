'use strict';

describe('Unit: PlaceDetails Controller', function () {
  var PlaceDetails,
    $rootScope,
    $scope,
    $controller,
    $state,
    $modal,
    $q,
    $httpBackend,
    AuthService,
    PlaceAddEditApiService,
    FetchAssetsService,
    AddAssetsService,
    DeleteAssetsService,
    AWSUploadService,
    UsersApiService,
    ToastrMessage,
    MessageService,
    LoaderService = {Loader: {}},
    EnumsService,
    AWS_UPLOAD,
    fakeResponse,
    PlaceData;


  beforeAll(function () {
    PlaceData = {
      'ep_id': '550f987fe4b09642124625a5',
      'cat_id': '54b83a2be4b06b211e892745',
      'type': 'PLACE',
      'title': '1865 Coffee',
      'description': 'xsxsxs',
      'phone': '4809678649',
      'website': '',
      'location': {'type': 'Point', 'coordinates': [33.42195744640333, -111.9392205029726]},
      'banner_asset': '5655d3dce4b0da91fa10f50b',
      'affiliation': 'NONE',
      'creator_id': '54e13604e4b072bb08b77438',
      'creator_name': 'Taylor Transtrum',
      'address': 'Arizona State University, Tempe Campus, 32-42 E University Dr, Tempe, AZ 85281',
      'creator_pic_asset': '54e13605e4b072bb08b7743c',
      'attending': 0,
      'positive_ratings': 0,
      'negative_ratings': 0,
      'ongoing': 0,
      'connections_data': [],
      'photos': ['5655d3dce4b0da91fa10f50b'],
      'bump_type': 'NON_BUMP',
      'positive_rating_count': 0,
      'negative_rating_count': 0,
      'invite_status': 'NONE',
      'event_timeline': 'UNDETERMINED',
      'place_schedule': {'periods': [], 'alwaysOpened': true},
      'private': false,
      'disabled': false
    };
    fakeResponse = {
      fakeRoles: ['ANONYMOUS', 'REGULAR', 'LAUNCHSQUAD', 'ADMIN', 'BUSINESS', 'BUSINESS_INDIVIDUAL', 'EDITOR'],
      fakeRolesObj: {
        ANONYMOUS: 'ANONYMOUS',
        REGULAR: 'REGULAR',
        LAUNCHSQUAD: 'LAUNCHSQUAD',
        ADMIN: 'ADMIN',
        BUSINESS: 'BUSINESS',
        BUSINESS_INDIVIDUAL: 'BUSINESS_INDIVIDUAL',
        EDITOR: 'EDITOR'
      },
      fakeEpTypes: ['BUMP', 'NON_BUMP', 'SPONSORED'],
      fakeEpStateEnums: ['ENABLED', 'DISABLED'],
      fakePlaceCategories: {
        '54b83a2be4b06b211e892749': 'Music',
        '54d56cceeca14131c5fb3d9c': 'House',
        '54b83a2be4b06b211e892747': 'Bar/Club',
        '54b83a2be4b06b211e89274b': 'Sports',
        '54b83a2be4b06b211e892745': 'Coffee',
        '54b83a2be4b06b211e892751': 'Group',
        '54b83a2be4b06b211e892743': 'Food',
        '54b83a2be4b06b211e892753': 'Outdoors',
        '54b83a2be4b06b211e89274f': 'Party',
        '54b83a2be4b06b211e892755': 'General',
        '54b83a2be4b06b211e89274d': 'School'
      },
      fakeFetchedAsset: {
        aws_key: 'ASIAIG63HOWFIRIEQCJQ',
        aws_secret: 'z3ZnvoIHEBVFe21GuiqxOLuJ+CSIBlguUbMu5c2h',
        aws_token: 'AQoDYXdzEEYasAODTH4AOcylcz+EkZt4QEaYlmOiUe/e7HfuIrVhdnLET3djkip1VXCM571/ipl1qe8gt7Kek2JCWNJFCPlAIn7gkWFIb3qNpHip/yhCXZedyFiiaQGhfcWPhhbNFL+yL+vX3BpLO3Vu4+a2p/4V+egycRQp60QPmpgH0KQAqHDCnBf4bVYBf+bSJIQGM09WpwaSOAaiZ2UZdeqAC39CLdwLlqe85NJ0kMT4B3SJCLKmxbeUaZQtV8P9LXQygNoAlqmNA6S38HFtByRrU32wiSTVeGXDiUTRxJmdcYKgUNZQMsaRTuj2llwN5kZLL1SoBpO8Sw6XFBIDOgFWIf0VkAQJOH3RUg7T/udeyvOqNVNhPelUBraz8+IxeYoxjEThEXDfTzMkelrhqpT2DNCLWmCtMlJst505QH5pkQgq6CxVQTA7504nTbQ9iKphtttgDgHcS6zwsTsa0F6rJOV3SH/rR857NT83ImXw3kGw+0edJL1IAFnEIc+H7WBjvZhx6pokP+jEgakL6n/o3fCm6/bNmsxozNonPQeUG6zrgkdTK+MfJ1TD2oYXg3fghDpXDMsgjqDAswU=',
        prefixes: ['/5592497703cef8958d68d1a5/56559821e4b0da91f9e72234'],
        s3_bucket: 'kwad-bump-assets'
      },
      fakeAddedAsset: {
        asset_id: '5672a991e4b02b658c9278dd',
        aws_key: 'ASIAJ64GQOFRALJOBWQA',
        aws_secret: 'fqN3PS7ao5e2aj38g3CYhVTXCn/R7VLXwTtXCthD',
        aws_token: 'AQoDYXdzEHYakAO+mVAbXL6gqWKmhtyx4IBROjvkzp3Zv2l1BcCVMI5BJ3ONeWhCzcaFp4gHv+72s4/UHUnMZjunMCvACoy/UZt90RnFdAUr6ly2UZofxNLOxcZLHb7kXikIsFocOAF72/KBRfEv+ntNuhaF0yJPVb4nWV1kdOJ0+7ZCLm1Ulz6AvdXrZIenhNxMl4jMRn5MctzXyq1mFNHlM1nyqsOrIiYstIiDv/Mdy4mKTsgqUBIxe2dKKXxvfiNx60FZfUNYr2M70j97+6TbJ9sfHD4Xh9vY9OiMZ+3zDpeX1NdL2BUjHHsyJlSnQVN2Q7U7DFatFOUqDnKOeChHybj8tRe4l2/po8mdGO4b3kQQvJ75VX2pPakIQ2Khn0x4tPRDBxsOzL9vlFwrzX1ehb5XAI4lWKnhaOXLvuFAAYkqzs2evFYShjnyVUnV/sr7cQ3w/nNrjbb2HZVueJzMzuwjnj06+h0QLthi8b/uNyat0A3Xl9IQEEEI/16dy5zNV+dZtDOD+w5Ynjt20Eu81GodKsAq0MxcIJHTyrMF',
        s3_bucket: 'kwad-bump-assets',
        s3_object: '/5592497703cef8958d68d1a5/5672a991e4b02b658c9278dd/raw_upload.jpeg'
      },
      fakeImage: {
        id: '56559821e4b0da91f9e72234',
        url: '/5592497703cef8958d68d1a5/56559821e4b0da91f9e72234/raw_upload'
      },
      fakeRemovedAsset: {
        asset_ids: ['56559821e4b0da91f9e72234']
      },
      fackTriggeredEvent: {
        stopPropagation: function () {
        }
      },
      fakeAdminUser: {
        attributes: [],
        college: '',
        first_name: 'admin',
        full_name: 'admin',
        last_name: 'admin',
        role: 'ADMIN',
        state: 'ENABLED'
      },
      fakeLaunchsquadUser: {
        attributes: [],
        college: '',
        first_name: 'bump',
        full_name: 'bump user',
        last_name: 'user',
        role: 'LAUNCHSQUAD',
        state: 'ENABLED'
      },
      fakeRegularUser: {
        attributes: [],
        college: '',
        first_name: 'regular',
        full_name: 'regular user',
        last_name: 'user',
        role: 'REGULAR',
        state: 'ENABLED'
      },
      fakeEditorUser: {
        attributes: [],
        college: '',
        first_name: 'editor',
        full_name: 'editor user',
        last_name: 'user',
        role: 'EDITOR',
        state: 'ENABLED'
      },
      fakeAddBannerImage: {
        status: 'SUCCESS',
        data: {ep_id: '55e78700e4b016c1bcd784ab', banner_asset: '56559821e4b0da91f9e72234'}
      },
      fakeUpdatedPlace: {
        status: 'SUCCESS',
        data: {
          'ep_id': '550f987fe4b09642124625a5',
          'banner_asset': '5655d3dce4b0da91fa10f50b'
        }
      },
      fakePlaceList: [{
        'epId': '546aff5be4b0370c32328fbb',
        'name': 'Phoenix Rock Gym',
        'place': '1353 E University Drive, Tempe AZ',
        'creatorName': 'Demo  User ',
        'creatorId': '546aff58e4b0370c32328f40',
        'feedImpression': 0,
        'detailImpression': 0,
        'appCheckinCount': 26,
        'geoCheckinCount': 8,
        'shares': 26,
        'status': 'ENABLED',
        'associatedEvents': 2,
        'creatorRole': 'ADMIN',
        'eventDate': null,
        'eventTimeLine': 'UNDETERMINED'
      }, {
        'epId': '546aff5be4b0370c32328fbf',
        'name': 'The Vue on Apache',
        'place': '922 E Apache Blvd, Tempe AZ',
        'creatorName': 'Brandy Smith',
        'creatorId': '546aff59e4b0370c32328f43',
        'feedImpression': 6,
        'detailImpression': 0,
        'appCheckinCount': 24,
        'geoCheckinCount': 283,
        'shares': 24,
        'status': 'ENABLED',
        'associatedEvents': 0,
        'creatorRole': 'BUMP',
        'eventDate': null,
        'eventTimeLine': 'UNDETERMINED'
      }],
      fakeJpegFile: [{
        lastModified: 1434780236336,
        lastModifiedDate: 'Sat Jun 18 2015 18:38:16 GMT+0530 (IST)',
        name: 'image.jpeg',
        size: 2852093,
        type: 'image/jpeg',
        webkitRelativePath: ''
      }],
      fakeJpgFile: [{
        lastModified: 1434780236336,
        lastModifiedDate: 'Sat Jun 20 2015 11:33:56 GMT+0530 (IST)',
        name: 'nainital.jpg',
        size: 2852093,
        type: 'image/jpg',
        webkitRelativePath: ''
      }],
      fakeVideoFile: [{
        lastModified: 1422600046658,
        lastModifiedDate: 'Fri Jan 30 2015 12:10:46 GMT+0530 (IST)',
        name: 'crockford-tjpl-1.m4v',
        size: 2591999,
        type: 'video/*',
        webkitRelativePath: ''
      }],
      fakeAudioFile: [{
        lastModified: 1422600046658,
        lastModifiedDate: 'Fri Jan 3 2015 15:30:36 GMT+0530 (IST)',
        name: 'song.mp3',
        size: 2591999,
        type: 'audio/mp3',
        webkitRelativePath: ''
      }],
      fakeLocationData: {
        'location': 'Arizona State University, Tempe Campus, 32-42 E University Dr, Tempe, AZ 85281',
        'coordinates': [33.42195744640333, -111.9392205029726]
      },
      fakeUserList: [{
        'id': '5600d67ce4b037ce4d25269c',
        'email': 'vkvang@ucdavis.edu',
        'role': 'REGULAR',
        'status': 'ENABLED',
        'registrationDate': '2015-09-22T04:18:04.277+0000',
        'contactNumber': '2092440733',
        'businessName': null,
        'bPlaces': 0,
        'type': null,
        'contactNames': [],
        'address': null,
        'website': null,
        'notes': null,
        'associatedBusinessId': null,
        'associatedBusinessName': null,
        'firstName': 'Kazoua',
        'lastName': 'Vang',
        'friends': 0,
        'sessions': 0,
        'events': 0,
        'places': 0
      }, {
        'id': '5605f79be4b0a9be00b29c0a',
        'email': 'katrinao@uw.edu',
        'role': 'REGULAR',
        'status': 'DISABLED',
        'registrationDate': '2015-09-26T01:40:43.858+0000',
        'contactNumber': '4088354776',
        'businessName': null,
        'bPlaces': 0,
        'type': null,
        'contactNames': [],
        'address': null,
        'website': null,
        'notes': null,
        'associatedBusinessId': null,
        'associatedBusinessName': null,
        'firstName': 'Katrina',
        'lastName': 'Odfina',
        'friends': 0,
        'sessions': 0,
        'events': 0,
        'places': 0
      }]
    };
  });
  beforeEach(module('kwadWeb'));
  beforeEach(inject(function (_$rootScope_, _$controller_, _$q_, _ToastrMessage_, _$httpBackend_, _AWS_UPLOAD_) {
    $rootScope = _$rootScope_;
    $scope = _$rootScope_.$new();
    $controller = _$controller_;
    $state = jasmine.createSpyObj('$state', ['go']);
    $modal = jasmine.createSpyObj('$modal', ['open']);
    $q = _$q_;
    $httpBackend = _$httpBackend_;
    AuthService = jasmine.createSpyObj('AuthService', ['getCurrentUser']);
    PlaceAddEditApiService = jasmine.createSpyObj('PlaceAddEditApiService', ['addEditPlace']);
    FetchAssetsService = jasmine.createSpyObj('FetchAssetsService', ['fetchAsset']);
    AddAssetsService = jasmine.createSpyObj('AddAssetsService', ['addAsset']);
    DeleteAssetsService = jasmine.createSpyObj('DeleteAssetsService', ['removeAsset']);
    AWSUploadService = jasmine.createSpyObj('AWSUploadService', ['uploadFile']);
    UsersApiService = jasmine.createSpyObj('UsersApiService', ['getUsers']);
    ToastrMessage = _ToastrMessage_;
    MessageService = jasmine.createSpyObj('MessageService', ['showSuccessfulMessage', 'showFailedMessage']);
    LoaderService.Loader = jasmine.createSpyObj('LoaderService.Loader', ['hideSpinner', 'showSpinner']);
    EnumsService = jasmine.createSpyObj('EnumsService', ['getEpTypeEnums', 'getEpStateEnums', 'getUserRoleEnums', 'getAssetTypeEnums', 'getPlaceCategoryEnums']);
    AWS_UPLOAD = _AWS_UPLOAD_;
    MessageService.showSuccessfulMessage.and.callFake(function () {
    });
    MessageService.showFailedMessage.and.callFake(function () {
    });
    LoaderService.Loader.hideSpinner.and.callFake(function () {
    });
    LoaderService.Loader.showSpinner.and.callFake(function () {
    });
    EnumsService.getAssetTypeEnums.and.callFake(function () {
      return {
        IMAGE_JPG: 'IMAGE_JPG',
        IMAGE_JPEG: 'IMAGE_JPEG',
        VIDEO: 'VIDEO'
      };
    });
    $state.go.and.callFake(function () {
    });
  }));
  beforeEach(function () {
    PlaceDetails = $controller('PlaceDetailsCtrl', {
      $scope: $scope,
      $state: $state,
      $modal: $modal,
      AuthService: AuthService,
      PlaceAddEditApiService: PlaceAddEditApiService,
      FetchAssetsService: FetchAssetsService,
      AddAssetsService: AddAssetsService,
      DeleteAssetsService: DeleteAssetsService,
      AWSUploadService: AWSUploadService,
      UsersApiService: UsersApiService,
      ToastrMessage: ToastrMessage,
      MessageService: MessageService,
      LoaderService: LoaderService,
      EnumsService: EnumsService,
      AWS_UPLOAD: AWS_UPLOAD,
      PlaceData: angular.copy(PlaceData)
    });
    $httpBackend.expectGET('app/login/login.html').respond(200);
  });

  describe('Function Units Should be defined', function () {
    it('PlaceDetails should be defined', function () {
      expect(PlaceDetails).toBeDefined();
    });
    it('PlaceAddEditApiService should be defined', function () {
      expect(PlaceAddEditApiService).toBeDefined();
    });
    it('ToastrMessage should be defined', function () {
      expect(ToastrMessage).toBeDefined();
    });
  });

  describe('Function Unit: PlaceDetails.init', function () {
    it('Should be defined and be a function', function () {
      expect(PlaceDetails.init).toBeDefined();
      expect(typeof PlaceDetails.init).toEqual('function');
    });
    describe('When Enums Api call returns success', function () {
      beforeEach(function () {
        EnumsService.getEpStateEnums.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve(fakeResponse.fakeEpStateEnums);
          return deferred.promise;
        });
        EnumsService.getUserRoleEnums.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve(fakeResponse.fakeRoles);
          return deferred.promise;
        });
        EnumsService.getEpTypeEnums.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve(fakeResponse.fakeEpTypes);
          return deferred.promise;
        });
        EnumsService.getPlaceCategoryEnums.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve(fakeResponse.fakePlaceCategories);
          return deferred.promise;
        });
        FetchAssetsService.fetchAsset.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve(fakeResponse.fakeFetchedAsset);
          deferred.$promise = deferred.promise;
          return deferred;
        });
      });
      it('Should assign values when User is admin/bump.', function () {
        AuthService.getCurrentUser.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve(fakeResponse.fakeAdminUser);
          return deferred.promise;
        });
        PlaceDetails.init();
        $rootScope.$digest();
        expect(PlaceDetails.EP_STATES.length).toBeGreaterThan(0);
        expect(PlaceDetails.EP_TYPES.length).toBeGreaterThan(0);
        expect(PlaceDetails.PLACE_CATEGORY_TYPES.length).toEqual(2);
        expect(PlaceDetails.PLACE_CATEGORIES.length).toBeGreaterThan(0);
      });
      it('Should assign values when User is regular', function () {
        AuthService.getCurrentUser.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve(fakeResponse.fakeRegularUser);
          return deferred.promise;
        });
        PlaceDetails.init();
        $rootScope.$digest();
        expect(PlaceDetails.EP_STATES.length).toBeGreaterThan(0);
        expect(PlaceDetails.EP_TYPES.length).toBeGreaterThan(0);
        expect(PlaceDetails.PLACE_CATEGORY_TYPES).toEqual(null);
        expect(PlaceDetails.PLACE_CATEGORIES.length).toBeGreaterThan(0);
      });
      it('Should assign values when User is editor', function () {
        AuthService.getCurrentUser.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve(fakeResponse.fakeEditorUser);
          return deferred.promise;
        });
        PlaceDetails.init();
        $rootScope.$digest();
        expect(PlaceDetails.EP_STATES.length).toBeGreaterThan(0);
        expect(PlaceDetails.EP_TYPES.length).toBeGreaterThan(0);
        expect(PlaceDetails.PLACE_CATEGORY_TYPES.length).toEqual(1);
        expect(PlaceDetails.PLACE_CATEGORIES.length).toBeGreaterThan(0);
      });
      it('Should not assign values on AuthService failure', function () {
        AuthService.getCurrentUser.and.callFake(function () {
          var deferred = $q.defer();
          deferred.reject();
          return deferred.promise;
        });
        PlaceDetails.init();
        $rootScope.$digest();
        expect(PlaceDetails.currentUser).toEqual(null);
      });
    });
    describe('When Enums Api call returns error', function () {
      beforeEach(function () {
        EnumsService.getEpStateEnums.and.callFake(function () {
          var deferred = $q.defer();
          deferred.reject();
          return deferred.promise;
        });
        EnumsService.getUserRoleEnums.and.callFake(function () {
          var deferred = $q.defer();
          deferred.reject();
          return deferred.promise;
        });
        EnumsService.getEpTypeEnums.and.callFake(function () {
          var deferred = $q.defer();
          deferred.reject();
          return deferred.promise;
        });
        EnumsService.getPlaceCategoryEnums.and.callFake(function () {
          var deferred = $q.defer();
          deferred.reject();
          return deferred.promise;
        });
        FetchAssetsService.fetchAsset.and.callFake(function () {
          var deferred = $q.defer();
          deferred.reject();
          deferred.$promise = deferred.promise;
          return deferred;
        });
      });
      it('Should not assign values.', function () {
        PlaceDetails.init();
        $rootScope.$digest();
        expect(PlaceDetails.USER_ROLES).toEqual(null);
        expect(PlaceDetails.USER_ROLES_OBJ).toEqual(null);
        expect(PlaceDetails.EP_TYPES).toEqual(null);
        expect(PlaceDetails.EP_STATES).toEqual(null);
        expect(PlaceDetails.PLACE_CATEGORIES).toEqual(null);
      });
      it('when PlaceDetails.place.photos.length equals to 0.', function () {
        PlaceDetails.place.photos = [];
        PlaceDetails.init();
        $rootScope.$digest();
        expect(PlaceDetails.USER_ROLES).toEqual(null);
        expect(PlaceDetails.USER_ROLES_OBJ).toEqual(null);
        expect(PlaceDetails.EP_TYPES).toEqual(null);
        expect(PlaceDetails.PLACE_CATEGORIES).toEqual(null);
      });
    });
  });

  describe('Function Unit: PlaceDetails.addNewKeyword', function () {
    it('Should be defined and be a function', function () {
      expect(PlaceDetails.addNewKeyword).toBeDefined();
      expect(typeof PlaceDetails.addNewKeyword).toEqual('function');
    });
    it('Should add a keyword', function () {
      PlaceDetails.place.keywords = [];
      PlaceDetails.newTag = 'new';
      PlaceDetails.addNewKeyword();
      $rootScope.$digest();
      expect(PlaceDetails.place.keywords.length).toEqual(1);
    });
  });

  describe('Function Unit: PlaceDetails.removeKeyword', function () {
    it('Should be defined and be a function', function () {
      expect(PlaceDetails.removeKeyword).toBeDefined();
      expect(typeof PlaceDetails.removeKeyword).toEqual('function');
    });
    it('Should remove a keyword', function () {
      PlaceDetails.place.keywords = ['keyword1', 'keyword2'];
      PlaceDetails.removeKeyword();
      $rootScope.$digest();
      expect(PlaceDetails.place.keywords.length).toEqual(1);
    });
  });

  describe('Function Unit: PlaceDetails.updatePlace', function () {
    it('Should be defined and be a function', function () {
      expect(PlaceDetails.updatePlace).toBeDefined();
      expect(typeof PlaceDetails.updatePlace).toEqual('function');
    });
    it('Should add a new place and respond with SUCCESS status', function () {
      PlaceAddEditApiService.addEditPlace.and.callFake(function () {
        var deferred = $q.defer();
        deferred.resolve(fakeResponse.fakeUpdatedPlace);
        deferred.$promise = deferred.promise;
        return deferred;
      });
      PlaceDetails.currentObject.category = {id: '54b83a2be4b06b211e892745'};
      PlaceDetails.updatePlace();
      $rootScope.$digest();
      expect(MessageService.showSuccessfulMessage).toHaveBeenCalledWith(ToastrMessage.PLACE.UPDATE.SUCCESS);
    });
    describe('Should not add a new place', function () {
      it('When place location is blank', function () {
        PlaceDetails.place.location = '';
        PlaceDetails.currentObject.category = {id: '54b83a2be4b06b211e892745'};
        PlaceDetails.updatePlace();
        $rootScope.$digest();
        expect(MessageService.showFailedMessage).toHaveBeenCalledWith(ToastrMessage.PLACE.ADD.BLANK_LOCATION);
      });
      it('When addplace api respond status 200 without SUCCESS status', function () {
        PlaceAddEditApiService.addEditPlace.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve({status: 'ALREADY_EXISTS'});
          deferred.$promise = deferred.promise;
          return deferred;
        });
        PlaceDetails.currentObject.category = {id: '54b83a2be4b06b211e892745'};
        PlaceDetails.updatePlace();
        $rootScope.$digest();
        expect(MessageService.showFailedMessage).toHaveBeenCalledWith(ToastrMessage.PLACE.ADD.ALREADY_EXISTS);
      });
      it('When addplace api respond an error', function () {
        PlaceAddEditApiService.addEditPlace.and.callFake(function () {
          var deferred = $q.defer();
          deferred.reject();
          deferred.$promise = deferred.promise;
          return deferred;
        });
        PlaceDetails.currentObject.category = {id: '54b83a2be4b06b211e892745'};
        PlaceDetails.updatePlace();
        $rootScope.$digest();
        expect(MessageService.showFailedMessage).toHaveBeenCalled();
      });
    });
  });

  describe('Function Unit: PlaceDetails.setPlaceAddress', function () {
    it('Should be defined and be a function', function () {
      expect(PlaceDetails.setPlaceAddress).toBeDefined();
      expect(typeof PlaceDetails.setPlaceAddress).toEqual('function');
    });
    it('Should assign new location data', function () {
      var expectedLocation = {
        'type': 'Point',
        'coordinates': [33.42195744640333, -111.9392205029726]
      };
      PlaceDetails.place.location = '';
      PlaceDetails.place.address = '';
      PlaceDetails.setPlaceAddress(fakeResponse.fakeLocationData);
      $rootScope.$digest();
      expect(PlaceDetails.place.location).toEqual(expectedLocation);
      expect(PlaceDetails.place.address).toEqual('Arizona State University, Tempe Campus, 32-42 E University Dr, Tempe, AZ 85281');
    });
  });

  describe('Function Unit: PlaceDetails.clearData', function () {
    it('Should be defined and be a function', function () {
      expect(PlaceDetails.clearData).toBeDefined();
      expect(typeof PlaceDetails.clearData).toEqual('function');
    });
    it('Should set location to blank', function () {
      PlaceDetails.clearData();
      $rootScope.$digest();
      expect(PlaceDetails.place.location).toEqual('');
    });
  });

  describe('Function Unit: PlaceDetails.addBannerImage', function () {
    it('Should be defined and be a function', function () {
      expect(PlaceDetails.addBannerImage).toBeDefined();
      expect(typeof PlaceDetails.addBannerImage).toEqual('function');
    });
    it('Should add bannerImage', function () {
      PlaceAddEditApiService.addEditPlace.and.callFake(function () {
        var deferred = $q.defer();
        deferred.resolve(fakeResponse.fakeAddBannerImage);
        deferred.$promise = deferred.promise;
        return deferred;
      });
      PlaceDetails.place.banner_asset = '';
      PlaceDetails.addBannerImage(fakeResponse.fakeImage);
      $rootScope.$digest();
      expect(PlaceDetails.place.banner_asset).toEqual('56559821e4b0da91f9e72234');
    });

    describe('Should not add bannerImage', function () {
      it('when asset id not found', function () {
        PlaceDetails.place.banner_asset = '';
        PlaceDetails.addBannerImage({id: '', url: ''});
        $rootScope.$digest();
        expect(PlaceDetails.place.banner_asset).toEqual('');
      });
      it('when PlaceAddEditApiService.addEditPlace respond an error status', function () {
        PlaceAddEditApiService.addEditPlace.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve({status: 'INSUFFICIENT_RIGHTS'});
          deferred.$promise = deferred.promise;
          return deferred;
        });
        PlaceDetails.place.banner_asset = '';
        PlaceDetails.addBannerImage(fakeResponse.fakeImage);
        $rootScope.$digest();
        expect(MessageService.showFailedMessage).toHaveBeenCalled();
      });
      it('when PlaceAddEditApiService.addEditPlace respond to failure', function () {
        PlaceAddEditApiService.addEditPlace.and.callFake(function () {
          var deferred = $q.defer();
          deferred.reject();
          deferred.$promise = deferred.promise;
          return deferred;
        });
        PlaceDetails.place.banner_asset = '';
        PlaceDetails.addBannerImage(fakeResponse.fakeImage);
        $rootScope.$digest();
        expect(PlaceDetails.place.banner_asset).toEqual('');
      });
    });
  });

  describe('Function Unit: PlaceDetails.removeBannerImage', function () {
    it('Should be defined and be a function', function () {
      expect(PlaceDetails.removeBannerImage).toBeDefined();
      expect(typeof PlaceDetails.removeBannerImage).toEqual('function');
    });
    it('Should remove bannerImage', function () {
      DeleteAssetsService.removeAsset.and.callFake(function () {
        var deferred = $q.defer();
        deferred.resolve(fakeResponse.fakeRemovedAsset);
        deferred.$promise = deferred.promise;
        return deferred;
      });
      PlaceDetails.photos = [fakeResponse.fakeImage];
      PlaceDetails.removeBannerImage(fakeResponse.fackTriggeredEvent, fakeResponse.fakeImage);
      $rootScope.$digest();
      expect(PlaceDetails.photos.length).toEqual(0);
    });
    describe('Should not remove bannerImage', function () {
      it('when DeleteAssetsService.removeAsset respond an error status', function () {
        DeleteAssetsService.removeAsset.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve({status: 'ERROR'});
          deferred.$promise = deferred.promise;
          return deferred;
        });
        PlaceDetails.photos = [fakeResponse.fakeImage];
        PlaceDetails.removeBannerImage(fakeResponse.fackTriggeredEvent, fakeResponse.fakeImage);
        $rootScope.$digest();
        expect(PlaceDetails.photos.length).not.toEqual(0);
      });
      it('when asset id not found', function () {
        PlaceDetails.photos = [fakeResponse.fakeImage];
        PlaceDetails.removeBannerImage(fakeResponse.fackTriggeredEvent, {id: '', url: ''});
        $rootScope.$digest();
        expect(PlaceDetails.photos.length).not.toEqual(0);
      });
      it('when DeleteAssetsService.removeAsset respond to failure', function () {
        DeleteAssetsService.removeAsset.and.callFake(function () {
          var deferred = $q.defer();
          deferred.reject();
          deferred.$promise = deferred.promise;
          return deferred;
        });
        PlaceDetails.photos = [fakeResponse.fakeImage];
        PlaceDetails.removeBannerImage(fakeResponse.fackTriggeredEvent, fakeResponse.fakeImage);
        $rootScope.$digest();
        expect(PlaceDetails.photos.length).not.toEqual(0);
      });
    });
  });

  describe('Function Unit: PlaceDetails.isBannerImage', function () {
    it('Should be defined and be a function', function () {
      expect(PlaceDetails.isBannerImage).toBeDefined();
      expect(typeof PlaceDetails.isBannerImage).toEqual('function');
    });
    it('Should return true', function () {
      PlaceDetails.place.banner_asset = '56559821e4b0da91f9e72234';
      var result = PlaceDetails.isBannerImage(fakeResponse.fakeImage);
      $rootScope.$digest();
      expect(result).toEqual(true);
    });
  });

  describe('Function Unit: PlaceDetails.uploadFile', function () {
    it('Should be defined and be a function', function () {
      expect(PlaceDetails.uploadFile).toBeDefined();
      expect(typeof PlaceDetails.uploadFile).toEqual('function');
    });
    it('Should upload file on KWAD and aws s3 server', function () {
      PlaceDetails.photos = [];
      PlaceDetails.place.photos = null;
      PlaceDetails.selectedFiles = fakeResponse.fakeJpegFile;
      AddAssetsService.addAsset.and.callFake(function () {
        var deferred = $q.defer();
        deferred.resolve({data: [fakeResponse.fakeAddedAsset]});
        deferred.$promise = deferred.promise;
        return deferred;
      });
      AWSUploadService.uploadFile.and.callFake(function (file, AWSObj, options) {
        var deferred = $q.defer();
        deferred.resolve({Location: 'https://s3.amazonaws.com/kwad-bump-assets//5592497703cef8958d68d1a5/56559821e4b0da91f9e72234/raw_upload'});
        return deferred.promise;
      });
      PlaceAddEditApiService.addEditPlace.and.callFake(function () {
        var deferred = $q.defer();
        deferred.resolve(fakeResponse.fakeAddBannerImage);
        deferred.$promise = deferred.promise;
        return deferred;
      });
      PlaceDetails.uploadFile();
      $rootScope.$digest();
      expect(PlaceDetails.place.photos.length).toEqual(1);
      expect(PlaceDetails.place.photos[0]).toEqual('5672a991e4b02b658c9278dd');
    });

    describe('Should not upload file', function () {
      it('When length of selected files is zero', function () {
        PlaceDetails.photos = [];
        PlaceDetails.place.photos = null;
        PlaceDetails.selectedFiles = [];
        PlaceDetails.uploadFile();
        $rootScope.$digest();
        expect(PlaceDetails.place.photos).toEqual(null);
      });
      it('When length of selected files greater then ten', function () {
        PlaceDetails.photos = [];
        PlaceDetails.place.photos = null;
        PlaceDetails.selectedFiles = {length: 12};
        PlaceDetails.uploadFile();
        $rootScope.$digest();
        expect(PlaceDetails.place.photos).toEqual(null);
      });
      it('when AWSUploadService.uploadFile rejected', function () {
        PlaceDetails.photos = [];
        PlaceDetails.place.photos = null;
        PlaceDetails.selectedFiles = fakeResponse.fakeJpgFile;
        AddAssetsService.addAsset.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve({data: [fakeResponse.fakeAddedAsset]});
          deferred.$promise = deferred.promise;
          return deferred;
        });
        AWSUploadService.uploadFile.and.callFake(function (file, AWSObj, options) {
          var deferred = $q.defer();
          deferred.reject();
          return deferred.promise;
        });
        PlaceDetails.uploadFile();
        $rootScope.$digest();
        expect(PlaceDetails.place.photos).toEqual(null);
      });
      it('when AWSUploadService.uploadFile rejected when file type is video', function () {
        PlaceDetails.photos = [];
        PlaceDetails.place.photos = null;
        PlaceDetails.selectedFiles = fakeResponse.fakeVideoFile;
        AddAssetsService.addAsset.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve({data: [fakeResponse.fakeAddedAsset]});
          deferred.$promise = deferred.promise;
          return deferred;
        });
        AWSUploadService.uploadFile.and.callFake(function (file, AWSObj, options) {
          var deferred = $q.defer();
          deferred.reject();
          return deferred.promise;
        });
        PlaceDetails.uploadFile();
        $rootScope.$digest();
        expect(PlaceDetails.place.photos).toEqual(null);
      });
      it('when AWSUploadService.uploadFile rejected when file type is audio', function () {
        PlaceDetails.photos = [];
        PlaceDetails.place.photos = null;
        PlaceDetails.selectedFiles = fakeResponse.fakeAudioFile;
        AddAssetsService.addAsset.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve({data: [fakeResponse.fakeAddedAsset]});
          deferred.$promise = deferred.promise;
          return deferred;
        });
        AWSUploadService.uploadFile.and.callFake(function (file, AWSObj, options) {
          var deferred = $q.defer();
          deferred.reject();
          return deferred.promise;
        });
        PlaceDetails.uploadFile();
        $rootScope.$digest();
        expect(PlaceDetails.place.photos).toEqual(null);
      });
      it('when AddAssetsService.addAsset rejected', function () {
        PlaceDetails.photos = [];
        PlaceDetails.place.photos = null;
        PlaceDetails.selectedFiles = fakeResponse.fakeJpegFile;
        AddAssetsService.addAsset.and.callFake(function () {
          var deferred = $q.defer();
          deferred.reject();
          deferred.$promise = deferred.promise;
          return deferred;
        });
        PlaceDetails.uploadFile();
        $rootScope.$digest();
        expect(PlaceDetails.place.photos).toEqual(null);
      });
    });
  });

  describe('Function Unit: PlaceDetails.fetchAssets', function () {
    it('Should be defined and be a function', function () {
      expect(PlaceDetails.fetchAssets).toBeDefined();
      expect(typeof PlaceDetails.fetchAssets).toEqual('function');
    });
    it('Should fetch images', function () {
      PlaceDetails.photos = [];
      FetchAssetsService.fetchAsset.and.callFake(function () {
        var deferred = $q.defer();
        deferred.resolve(fakeResponse.fakeFetchedAsset);
        deferred.$promise = deferred.promise;
        return deferred;
      });
      PlaceDetails.fetchAssets('56559821e4b0da91f9e72234');
      $rootScope.$digest();
      expect(PlaceDetails.photos.length).toEqual(1);
    });
    it('Should fetch data with no image prefixes', function () {
      PlaceDetails.photos = [];
      FetchAssetsService.fetchAsset.and.callFake(function () {
        var deferred = $q.defer();
        deferred.resolve({
          aws_key: 'ASIAIG63HOWFIRIEQCJQ',
          aws_secret: 'z3ZnvoIHEBVFe21GuiqxOLuJ+CSIBlguUbMu5c2h',
          aws_token: 'AQoDYXdzEEYasAODTH4AOcylcz+EkZt4QEaYlmOiUe/e7HfuIrVhdnLET3djkip1VXCM571/ipl1qe8gt7Kek2JCWNJFCPlAIn7gkWFIb3qNpHip/yhCXZedyFiiaQGhfcWPhhbNFL+yL+vX3BpLO3Vu4+a2p/4V+egycRQp60QPmpgH0KQAqHDCnBf4bVYBf+bSJIQGM09WpwaSOAaiZ2UZdeqAC39CLdwLlqe85NJ0kMT4B3SJCLKmxbeUaZQtV8P9LXQygNoAlqmNA6S38HFtByRrU32wiSTVeGXDiUTRxJmdcYKgUNZQMsaRTuj2llwN5kZLL1SoBpO8Sw6XFBIDOgFWIf0VkAQJOH3RUg7T/udeyvOqNVNhPelUBraz8+IxeYoxjEThEXDfTzMkelrhqpT2DNCLWmCtMlJst505QH5pkQgq6CxVQTA7504nTbQ9iKphtttgDgHcS6zwsTsa0F6rJOV3SH/rR857NT83ImXw3kGw+0edJL1IAFnEIc+H7WBjvZhx6pokP+jEgakL6n/o3fCm6/bNmsxozNonPQeUG6zrgkdTK+MfJ1TD2oYXg3fghDpXDMsgjqDAswU=',
          prefixes: [],
          s3_bucket: 'kwad-bump-assets'
        });
        deferred.$promise = deferred.promise;
        return deferred;
      });
      PlaceDetails.fetchAssets('56559821e4b0da91f9e72234');
      $rootScope.$digest();
      expect(PlaceDetails.photos.length).toEqual(0);
    });
    it('Should not fetch data when asset id is undefined', function () {
      PlaceDetails.photos = [];
      PlaceDetails.fetchAssets();
      $rootScope.$digest();
      expect(PlaceDetails.photos.length).toEqual(0);
    });
    it('Should not fetch data when FetchAssetsService.fetchAsset rejected', function () {
      PlaceDetails.photos = [];
      FetchAssetsService.fetchAsset.and.callFake(function () {
        var deferred = $q.defer();
        deferred.reject();
        deferred.$promise = deferred.promise;
        return deferred;
      });
      PlaceDetails.fetchAssets('56559821e4b0da91f9e72234');
      $rootScope.$digest();
      expect(PlaceDetails.photos.length).toEqual(0);
    });
  });

  describe('Function Unit: PlaceDetails.searchCreators', function () {
    it('Should be defined and be a function', function () {
      expect(PlaceDetails.searchCreators).toBeDefined();
      expect(typeof PlaceDetails.searchCreators).toEqual('function');
    });
    it('Should reject places list and $modal.open should not invoked', function () {
      UsersApiService.getUsers.and.callFake(function () {
        var deferred = $q.defer();
        deferred.reject();
        deferred.$promise = deferred.promise;
        return deferred;
      });
      PlaceDetails.place.creator_name = 'Ka';
      PlaceDetails.place.creator_id = null;
      PlaceDetails.searchCreators();
      $rootScope.$digest();
      expect(LoaderService.Loader.hideSpinner).toHaveBeenCalled();
      expect(PlaceDetails.place.creator_id).toEqual(null);
    });
    describe('Should resolve user list', function () {
      beforeEach(function () {
        UsersApiService.getUsers.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve({data: fakeResponse.fakeUserList});
          deferred.$promise = deferred.promise;
          return deferred;
        });
      });
      it('when $modal.open invoked and clicked Ok', function () {
        $modal.open.and.callFake(function (obj) {
          var deferred = $q.defer();
          fakeResponse.fakeUserList[0].fullName = fakeResponse.fakeUserList[0].firstName + ' ' + fakeResponse.fakeUserList[0].lastName;
          obj.resolve.CreatorList();
          deferred.resolve(fakeResponse.fakeUserList[0]);
          return {
            result: deferred.promise
          };
        });
        PlaceDetails.place.creator_id = null;
        PlaceDetails.place.creator_name = 'Ka';
        PlaceDetails.searchCreators();
        $rootScope.$digest();
        expect(PlaceDetails.place.creator_id).toEqual('5600d67ce4b037ce4d25269c');
        expect(PlaceDetails.place.creator_name).toEqual('Kazoua Vang');
      });
      it('when $modal.open invoked and clicked Cancel', function () {
        $modal.open.and.callFake(function (obj) {
          var deferred = $q.defer();
          obj.resolve.CreatorList();
          deferred.reject();
          return {
            result: deferred.promise
          };
        });
        PlaceDetails.place.creator_id = null;
        PlaceDetails.place.creator_name = 'Ka';
        PlaceDetails.searchCreators();
        $rootScope.$digest();
        expect(PlaceDetails.place.creator_id).toEqual(null);
      });
    });
  });

  describe('Function Unit: PlaceDetails.addEventAtCurrentPlace', function () {
    it('Should be defined and be a function', function () {
      expect(PlaceDetails.addEventAtCurrentPlace).toBeDefined();
      expect(typeof PlaceDetails.addEventAtCurrentPlace).toEqual('function');
    });
    it('Should pass and call $state.go', function () {
      PlaceDetails.addEventAtCurrentPlace();
      $rootScope.$digest();
      expect($state.go).toHaveBeenCalledWith('addEventsFromPlace', {placeId: PlaceDetails.place.ep_id});
    });
  });

  describe('Function Unit: PlaceDetails.CreateNewPlaceBasedOnCurrent', function () {
    it('Should be defined and be a function', function () {
      expect(PlaceDetails.CreateNewPlaceBasedOnCurrent).toBeDefined();
      expect(typeof PlaceDetails.CreateNewPlaceBasedOnCurrent).toEqual('function');
    });
    it('Should pass and call $state.go', function () {
      PlaceDetails.CreateNewPlaceBasedOnCurrent();
      $rootScope.$digest();
      expect($state.go).toHaveBeenCalledWith('addPlacesFromCurrent', {placeId: PlaceDetails.place.ep_id});
    });
  });
});
