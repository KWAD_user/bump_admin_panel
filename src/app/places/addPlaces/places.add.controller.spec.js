'use strict';

describe('Unit: AddPlacesCtrl Controller', function () {
  var AddPlaces,
    $rootScope,
    $scope,
    $controller,
    $state,
    $q,
    $httpBackend,
    AuthService,
    PlaceAddEditApiService,
    FetchAssetsService,
    AddAssetsService,
    DeleteAssetsService,
    AWSUploadService,
    ToastrMessage,
    MessageService,
    LoaderService = {Loader: {}},
    EnumsService,
    AWS_UPLOAD,
    fakeResponse,
    PlaceData;


  beforeAll(function () {
    PlaceData = {
      'ep_id': '550f987fe4b09642124625a5',
      'cat_id': '54b83a2be4b06b211e892745',
      'type': 'PLACE',
      'title': '1865 Coffee',
      'description': 'xsxsxs',
      'phone': '4809678649',
      'website': '',
      'location': {'type': 'Point', 'coordinates': [33.42195744640333, -111.9392205029726]},
      'banner_asset': '5655d3dce4b0da91fa10f50b',
      'affiliation': 'NONE',
      'creator_id': '54e13604e4b072bb08b77438',
      'creator_name': 'Taylor Transtrum',
      'address': 'Arizona State University, Tempe Campus, 32-42 E University Dr, Tempe, AZ 85281',
      'creator_pic_asset': '54e13605e4b072bb08b7743c',
      'attending': 0,
      'positive_ratings': 0,
      'negative_ratings': 0,
      'ongoing': 0,
      'connections_data': [],
      'photos': ['5655d3dce4b0da91fa10f50b'],
      'bump_type': 'NON_BUMP',
      'positive_rating_count': 0,
      'negative_rating_count': 0,
      'invite_status': 'NONE',
      'event_timeline': 'UNDETERMINED',
      'place_schedule': {'periods': [], 'alwaysOpened': true},
      'private': false,
      'disabled': false
    };
    fakeResponse = {
      fakeRoles: ['ANONYMOUS', 'REGULAR', 'LAUNCHSQUAD', 'ADMIN', 'BUSINESS', 'BUSINESS_INDIVIDUAL', 'EDITOR'],
      fakeRolesObj: {
        ANONYMOUS: 'ANONYMOUS',
        REGULAR: 'REGULAR',
        LAUNCHSQUAD: 'LAUNCHSQUAD',
        ADMIN: 'ADMIN',
        BUSINESS: 'BUSINESS',
        BUSINESS_INDIVIDUAL: 'BUSINESS_INDIVIDUAL',
        EDITOR: 'EDITOR'
      },
      fakeEpTypes: ['BUMP', 'NON_BUMP', 'SPONSORED'],
      fakePlaceCategories: {
        '54b83a2be4b06b211e892749': 'Music',
        '54d56cceeca14131c5fb3d9c': 'House',
        '54b83a2be4b06b211e892747': 'Bar/Club',
        '54b83a2be4b06b211e89274b': 'Sports',
        '54b83a2be4b06b211e892745': 'Coffee',
        '54b83a2be4b06b211e892751': 'Group',
        '54b83a2be4b06b211e892743': 'Food',
        '54b83a2be4b06b211e892753': 'Outdoors',
        '54b83a2be4b06b211e89274f': 'Party',
        '54b83a2be4b06b211e892755': 'General',
        '54b83a2be4b06b211e89274d': 'School'
      },
      fakeFetchedAsset: {
        aws_key: 'ASIAIG63HOWFIRIEQCJQ',
        aws_secret: 'z3ZnvoIHEBVFe21GuiqxOLuJ+CSIBlguUbMu5c2h',
        aws_token: 'AQoDYXdzEEYasAODTH4AOcylcz+EkZt4QEaYlmOiUe/e7HfuIrVhdnLET3djkip1VXCM571/ipl1qe8gt7Kek2JCWNJFCPlAIn7gkWFIb3qNpHip/yhCXZedyFiiaQGhfcWPhhbNFL+yL+vX3BpLO3Vu4+a2p/4V+egycRQp60QPmpgH0KQAqHDCnBf4bVYBf+bSJIQGM09WpwaSOAaiZ2UZdeqAC39CLdwLlqe85NJ0kMT4B3SJCLKmxbeUaZQtV8P9LXQygNoAlqmNA6S38HFtByRrU32wiSTVeGXDiUTRxJmdcYKgUNZQMsaRTuj2llwN5kZLL1SoBpO8Sw6XFBIDOgFWIf0VkAQJOH3RUg7T/udeyvOqNVNhPelUBraz8+IxeYoxjEThEXDfTzMkelrhqpT2DNCLWmCtMlJst505QH5pkQgq6CxVQTA7504nTbQ9iKphtttgDgHcS6zwsTsa0F6rJOV3SH/rR857NT83ImXw3kGw+0edJL1IAFnEIc+H7WBjvZhx6pokP+jEgakL6n/o3fCm6/bNmsxozNonPQeUG6zrgkdTK+MfJ1TD2oYXg3fghDpXDMsgjqDAswU=',
        prefixes: ['/5592497703cef8958d68d1a5/56559821e4b0da91f9e72234'],
        s3_bucket: 'kwad-bump-assets'
      },
      fakeAddedAsset: {
        asset_id: '5672a991e4b02b658c9278dd',
        aws_key: 'ASIAJ64GQOFRALJOBWQA',
        aws_secret: 'fqN3PS7ao5e2aj38g3CYhVTXCn/R7VLXwTtXCthD',
        aws_token: 'AQoDYXdzEHYakAO+mVAbXL6gqWKmhtyx4IBROjvkzp3Zv2l1BcCVMI5BJ3ONeWhCzcaFp4gHv+72s4/UHUnMZjunMCvACoy/UZt90RnFdAUr6ly2UZofxNLOxcZLHb7kXikIsFocOAF72/KBRfEv+ntNuhaF0yJPVb4nWV1kdOJ0+7ZCLm1Ulz6AvdXrZIenhNxMl4jMRn5MctzXyq1mFNHlM1nyqsOrIiYstIiDv/Mdy4mKTsgqUBIxe2dKKXxvfiNx60FZfUNYr2M70j97+6TbJ9sfHD4Xh9vY9OiMZ+3zDpeX1NdL2BUjHHsyJlSnQVN2Q7U7DFatFOUqDnKOeChHybj8tRe4l2/po8mdGO4b3kQQvJ75VX2pPakIQ2Khn0x4tPRDBxsOzL9vlFwrzX1ehb5XAI4lWKnhaOXLvuFAAYkqzs2evFYShjnyVUnV/sr7cQ3w/nNrjbb2HZVueJzMzuwjnj06+h0QLthi8b/uNyat0A3Xl9IQEEEI/16dy5zNV+dZtDOD+w5Ynjt20Eu81GodKsAq0MxcIJHTyrMF',
        s3_bucket: 'kwad-bump-assets',
        s3_object: '/5592497703cef8958d68d1a5/5672a991e4b02b658c9278dd/raw_upload.jpeg'
      },
      fakeImage: {
        id: '56559821e4b0da91f9e72234',
        url: '/5592497703cef8958d68d1a5/56559821e4b0da91f9e72234/raw_upload'
      },
      fakeRemovedAsset: {
        asset_ids: ['56559821e4b0da91f9e72234']
      },
      fackTriggeredEvent: {
        stopPropagation: function () {
        }
      },
      fakeAdminUser: {
        attributes: [],
        college: '',
        first_name: 'admin',
        full_name: 'admin',
        last_name: 'admin',
        role: 'ADMIN',
        state: 'ENABLED'
      },
      fakeLaunchsquadUser: {
        attributes: [],
        college: '',
        first_name: 'bump',
        full_name: 'bump user',
        last_name: 'user',
        role: 'LAUNCHSQUAD',
        state: 'ENABLED'
      },
      fakeRegularUser: {
        attributes: [],
        college: '',
        first_name: 'regular',
        full_name: 'regular user',
        last_name: 'user',
        role: 'REGULAR',
        state: 'ENABLED'
      },
      fakeEditorUser: {
        attributes: [],
        college: '',
        first_name: 'editor',
        full_name: 'editor user',
        last_name: 'user',
        role: 'EDITOR',
        state: 'ENABLED'
      },
      fakeAddBannerImage: {
        status: 'SUCCESS',
        data: {ep_id: '55e78700e4b016c1bcd784ab', banner_asset: '56559821e4b0da91f9e72234'}
      },
      fakeAddedPlace: {
        status: 'SUCCESS',
        data: {
          'ep_id': '546aff5be4b0370c32328fbf'
        }
      },
      fakePlaceList: [{
        'epId': '546aff5be4b0370c32328fbb',
        'name': 'Phoenix Rock Gym',
        'place': '1353 E University Drive, Tempe AZ',
        'creatorName': 'Demo  User ',
        'creatorId': '546aff58e4b0370c32328f40',
        'feedImpression': 0,
        'detailImpression': 0,
        'appCheckinCount': 26,
        'geoCheckinCount': 8,
        'shares': 26,
        'status': 'ENABLED',
        'associatedEvents': 2,
        'creatorRole': 'ADMIN',
        'eventDate': null,
        'eventTimeLine': 'UNDETERMINED'
      }, {
        'epId': '546aff5be4b0370c32328fbf',
        'name': 'The Vue on Apache',
        'place': '922 E Apache Blvd, Tempe AZ',
        'creatorName': 'Brandy Smith',
        'creatorId': '546aff59e4b0370c32328f43',
        'feedImpression': 6,
        'detailImpression': 0,
        'appCheckinCount': 24,
        'geoCheckinCount': 283,
        'shares': 24,
        'status': 'ENABLED',
        'associatedEvents': 0,
        'creatorRole': 'BUMP',
        'eventDate': null,
        'eventTimeLine': 'UNDETERMINED'
      }],
      fakeJpegFile: [{
        lastModified: 1434780236336,
        lastModifiedDate: 'Sat Jun 18 2015 18:38:16 GMT+0530 (IST)',
        name: 'image.jpeg',
        size: 2852093,
        type: 'image/jpeg',
        webkitRelativePath: ''
      }],
      fakeJpgFile: [{
        lastModified: 1434780236336,
        lastModifiedDate: 'Sat Jun 20 2015 11:33:56 GMT+0530 (IST)',
        name: 'nainital.jpg',
        size: 2852093,
        type: 'image/jpg',
        webkitRelativePath: ''
      }],
      fakeVideoFile: [{
        lastModified: 1422600046658,
        lastModifiedDate: 'Fri Jan 30 2015 12:10:46 GMT+0530 (IST)',
        name: 'crockford-tjpl-1.m4v',
        size: 2591999,
        type: 'video/*',
        webkitRelativePath: ''
      }],
      fakeAudioFile: [{
        lastModified: 1422600046658,
        lastModifiedDate: 'Fri Jan 3 2015 15:30:36 GMT+0530 (IST)',
        name: 'song.mp3',
        size: 2591999,
        type: 'audio/mp3',
        webkitRelativePath: ''
      }],
      fakeLocationData: {
        'location': 'Arizona State University, Tempe Campus, 32-42 E University Dr, Tempe, AZ 85281',
        'coordinates': [33.42195744640333, -111.9392205029726]
      }
    };
  });
  beforeEach(module('kwadWeb'));
  beforeEach(inject(function (_$rootScope_, _$controller_, _$q_, _ToastrMessage_, _$httpBackend_, _AWS_UPLOAD_) {
    $rootScope = _$rootScope_;
    $scope = _$rootScope_.$new();
    $controller = _$controller_;
    $state = jasmine.createSpyObj('$state', ['go']);
    $q = _$q_;
    $httpBackend = _$httpBackend_;
    AuthService = jasmine.createSpyObj('AuthService', ['getCurrentUser']);
    PlaceAddEditApiService = jasmine.createSpyObj('PlaceAddEditApiService', ['addEditPlace']);
    FetchAssetsService = jasmine.createSpyObj('FetchAssetsService', ['fetchAsset']);
    AddAssetsService = jasmine.createSpyObj('AddAssetsService', ['addAsset']);
    DeleteAssetsService = jasmine.createSpyObj('DeleteAssetsService', ['removeAsset']);
    AWSUploadService = jasmine.createSpyObj('AWSUploadService', ['uploadFile']);
    ToastrMessage = _ToastrMessage_;
    MessageService = jasmine.createSpyObj('MessageService', ['showSuccessfulMessage', 'showFailedMessage']);
    LoaderService.Loader = jasmine.createSpyObj('LoaderService.Loader', ['hideSpinner', 'showSpinner']);
    EnumsService = jasmine.createSpyObj('EnumsService', ['getEpTypeEnums', 'getUserRoleEnums', 'getAssetTypeEnums', 'getPlaceCategoryEnums']);
    AWS_UPLOAD = _AWS_UPLOAD_;
    MessageService.showSuccessfulMessage.and.callFake(function () {
    });
    MessageService.showFailedMessage.and.callFake(function () {
    });
    LoaderService.Loader.hideSpinner.and.callFake(function () {
    });
    LoaderService.Loader.showSpinner.and.callFake(function () {
    });
    EnumsService.getAssetTypeEnums.and.callFake(function () {
      return {
        IMAGE_JPG: 'IMAGE_JPG',
        IMAGE_JPEG: 'IMAGE_JPEG',
        VIDEO: 'VIDEO'
      };
    });
    $state.go.and.callFake(function () {
    });
  }));
  beforeEach(function () {
    AddPlaces = $controller('AddPlacesCtrl', {
      $scope: $scope,
      $state: $state,
      AuthService: AuthService,
      PlaceAddEditApiService: PlaceAddEditApiService,
      FetchAssetsService: FetchAssetsService,
      AddAssetsService: AddAssetsService,
      DeleteAssetsService: DeleteAssetsService,
      AWSUploadService: AWSUploadService,
      ToastrMessage: ToastrMessage,
      MessageService: MessageService,
      LoaderService: LoaderService,
      EnumsService: EnumsService,
      AWS_UPLOAD: AWS_UPLOAD,
      PlaceData: angular.copy(PlaceData)
    });
    $httpBackend.expectGET('app/login/login.html').respond(200);
  });

  describe('Function Units Should be defined', function () {
    it('AddPlaces should be defined', function () {
      expect(AddPlaces).toBeDefined();
    });
    it('PlaceAddEditApiService should be defined', function () {
      expect(PlaceAddEditApiService).toBeDefined();
    });
    it('ToastrMessage should be defined', function () {
      expect(ToastrMessage).toBeDefined();
    });
  });

  describe('Function Unit: AddPlaces.init', function () {
    it('Should be defined and be a function', function () {
      expect(AddPlaces.init).toBeDefined();
      expect(typeof AddPlaces.init).toEqual('function');
    });
    describe('When Enums Api call returns success', function () {
      beforeEach(function () {
        EnumsService.getUserRoleEnums.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve(fakeResponse.fakeRoles);
          return deferred.promise;
        });
        EnumsService.getEpTypeEnums.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve(fakeResponse.fakeEpTypes);
          return deferred.promise;
        });
        EnumsService.getPlaceCategoryEnums.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve(fakeResponse.fakePlaceCategories);
          return deferred.promise;
        });
        FetchAssetsService.fetchAsset.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve(fakeResponse.fakeFetchedAsset);
          deferred.$promise = deferred.promise;
          return deferred;
        });
      });
      it('Should assign values when User is admin/bump.', function () {
        AuthService.getCurrentUser.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve(fakeResponse.fakeAdminUser);
          return deferred.promise;
        });
        AddPlaces.place.cat_id = null;
        AddPlaces.init();
        $rootScope.$digest();
        expect(AddPlaces.place.cat_id).not.toEqual(null);
        expect(AddPlaces.EP_TYPES.length).toBeGreaterThan(0);
        expect(AddPlaces.PLACE_CATEGORY_TYPES.length).toEqual(2);
        expect(AddPlaces.PLACE_CATEGORIES.length).toBeGreaterThan(0);
      });
      it('Should assign values when User is regular', function () {
        AuthService.getCurrentUser.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve(fakeResponse.fakeRegularUser);
          return deferred.promise;
        });
        AddPlaces.init();
        $rootScope.$digest();
        expect(AddPlaces.EP_TYPES.length).toBeGreaterThan(0);
        expect(AddPlaces.PLACE_CATEGORY_TYPES).toEqual(null);
        expect(AddPlaces.PLACE_CATEGORIES.length).toBeGreaterThan(0);
      });
      it('Should assign values when User is editor', function () {
        AuthService.getCurrentUser.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve(fakeResponse.fakeEditorUser);
          return deferred.promise;
        });
        AddPlaces.init();
        $rootScope.$digest();
        expect(AddPlaces.EP_TYPES.length).toBeGreaterThan(0);
        expect(AddPlaces.PLACE_CATEGORY_TYPES.length).toEqual(1);
        expect(AddPlaces.PLACE_CATEGORIES.length).toBeGreaterThan(0);
      });
      it('Should not assign values on AuthService failure', function () {
        AuthService.getCurrentUser.and.callFake(function () {
          var deferred = $q.defer();
          deferred.reject();
          return deferred.promise;
        });
        AddPlaces.init();
        $rootScope.$digest();
        expect(AddPlaces.currentUser).toEqual(null);
      });
    });
    describe('When Enums Api call returns error', function () {
      beforeEach(function () {
        EnumsService.getUserRoleEnums.and.callFake(function () {
          var deferred = $q.defer();
          deferred.reject();
          return deferred.promise;
        });
        EnumsService.getEpTypeEnums.and.callFake(function () {
          var deferred = $q.defer();
          deferred.reject();
          return deferred.promise;
        });
        EnumsService.getPlaceCategoryEnums.and.callFake(function () {
          var deferred = $q.defer();
          deferred.reject();
          return deferred.promise;
        });
        FetchAssetsService.fetchAsset.and.callFake(function () {
          var deferred = $q.defer();
          deferred.reject();
          deferred.$promise = deferred.promise;
          return deferred;
        });
      });
      it('Should not assign values.', function () {
        AddPlaces.init();
        $rootScope.$digest();
        expect(AddPlaces.USER_ROLES).toEqual(null);
        expect(AddPlaces.USER_ROLES_OBJ).toEqual(null);
        expect(AddPlaces.EP_TYPES).toEqual(null);
        expect(AddPlaces.PLACE_CATEGORIES).toEqual(null);
      });
      it('when AddPlaces.place.photos.length equals to 0.', function () {
        AddPlaces.place.photos = [];
        AddPlaces.init();
        $rootScope.$digest();
        expect(AddPlaces.USER_ROLES).toEqual(null);
        expect(AddPlaces.USER_ROLES_OBJ).toEqual(null);
        expect(AddPlaces.EP_TYPES).toEqual(null);
        expect(AddPlaces.PLACE_CATEGORIES).toEqual(null);
      });
    });
  });

  describe('Function Unit: AddPlaces.addNewKeyword', function () {
    it('Should be defined and be a function', function () {
      expect(AddPlaces.addNewKeyword).toBeDefined();
      expect(typeof AddPlaces.addNewKeyword).toEqual('function');
    });
    it('Should add a keyword', function () {
      AddPlaces.place.keywords = [];
      AddPlaces.newTag = 'new';
      AddPlaces.addNewKeyword();
      $rootScope.$digest();
      expect(AddPlaces.place.keywords.length).toEqual(1);
    });
  });

  describe('Function Unit: AddPlaces.removeKeyword', function () {
    it('Should be defined and be a function', function () {
      expect(AddPlaces.removeKeyword).toBeDefined();
      expect(typeof AddPlaces.removeKeyword).toEqual('function');
    });
    it('Should remove a keyword', function () {
      AddPlaces.place.keywords = ['keyword1', 'keyword2'];
      AddPlaces.removeKeyword();
      $rootScope.$digest();
      expect(AddPlaces.place.keywords.length).toEqual(1);
    });
  });

  describe('Function Unit: AddPlaces.onCategoryChange', function () {
    it('Should be defined and be a function', function () {
      expect(AddPlaces.onCategoryChange).toBeDefined();
      expect(typeof AddPlaces.onCategoryChange).toEqual('function');
    });
    it('Should change cat_id', function () {
      AddPlaces.category = {
        id: '54b83a2be4b06b211e892742'
      };
      AddPlaces.onCategoryChange();
      $rootScope.$digest();
      expect(AddPlaces.place.cat_id).toEqual(AddPlaces.category.id);
    });
  });

  describe('Function Unit: AddPlaces.onCategoryTypeChange', function () {
    it('Should be defined and be a function', function () {
      expect(AddPlaces.onCategoryTypeChange).toBeDefined();
      expect(typeof AddPlaces.onCategoryTypeChange).toEqual('function');
    });
    it('Should assign value true when categoryType set as  PRIVATE ', function () {
      AddPlaces.categoryType = 'PRIVATE';
      AddPlaces.onCategoryTypeChange();
      $rootScope.$digest();
      expect(AddPlaces.place.private).toEqual(true);
    });
    it('Should assign value false when categoryType not set as  PRIVATE ', function () {
      AddPlaces.categoryType = 'PUBLIC';
      AddPlaces.onCategoryTypeChange();
      $rootScope.$digest();
      expect(AddPlaces.place.private).toEqual(false);
    });
  });

  describe('Function Unit: AddPlaces.onBumpTypeChange', function () {
    it('Should be defined and be a function', function () {
      expect(AddPlaces.onBumpTypeChange).toBeDefined();
      expect(typeof AddPlaces.onBumpTypeChange).toEqual('function');
    });
    it('Should assign value BUMP when bump_type set as true ', function () {
      AddPlaces.bump_type = true;
      AddPlaces.onBumpTypeChange();
      $rootScope.$digest();
      expect(AddPlaces.place.bump_type).toEqual('BUMP');
    });
    it('Should assign value NON_BUMP when bump_type not set as false ', function () {
      AddPlaces.bump_type = false;
      AddPlaces.onBumpTypeChange();
      $rootScope.$digest();
      expect(AddPlaces.place.bump_type).toEqual('NON_BUMP');
    });
  });

  describe('Function Unit: AddPlaces.addPlace', function () {
    it('Should be defined and be a function', function () {
      expect(AddPlaces.addPlace).toBeDefined();
      expect(typeof AddPlaces.addPlace).toEqual('function');
    });
    it('Should add a new place and respond with SUCCESS status', function () {
      PlaceAddEditApiService.addEditPlace.and.callFake(function () {
        var deferred = $q.defer();
        deferred.resolve(fakeResponse.fakeAddedPlace);
        deferred.$promise = deferred.promise;
        return deferred;
      });
      AddPlaces.place.ep_id = '550f987fe4b09642124625a5';
      AddPlaces.place.title = 'new place test';
      AddPlaces.addPlace();
      $rootScope.$digest();
      expect(MessageService.showSuccessfulMessage).toHaveBeenCalled();
    });
    describe('Should not add a new place', function () {
      it('When place name is blank', function () {
        AddPlaces.place.title = '';
        AddPlaces.addPlace();
        $rootScope.$digest();
        expect(MessageService.showFailedMessage).toHaveBeenCalledWith(ToastrMessage.PLACE.ADD.BLANK_TITLE);
      });
      it('When place location is blank', function () {
        AddPlaces.place.title = 'new place test';
        AddPlaces.place.location = '';
        AddPlaces.addPlace();
        $rootScope.$digest();
        expect(MessageService.showFailedMessage).toHaveBeenCalledWith(ToastrMessage.PLACE.ADD.BLANK_LOCATION);
      });
      it('When addplace api respond status 200 without SUCCESS status', function () {
        PlaceAddEditApiService.addEditPlace.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve({status: 'ALREADY_EXISTS'});
          deferred.$promise = deferred.promise;
          return deferred;
        });
        AddPlaces.place.title = '1865 Coffee';
        AddPlaces.addPlace();
        $rootScope.$digest();
        expect(MessageService.showFailedMessage).toHaveBeenCalledWith(ToastrMessage.PLACE.ADD.ALREADY_EXISTS);
      });
      it('When addplace api respond an error', function () {
        PlaceAddEditApiService.addEditPlace.and.callFake(function () {
          var deferred = $q.defer();
          deferred.reject();
          deferred.$promise = deferred.promise;
          return deferred;
        });
        AddPlaces.place.title = 'new place test';
        AddPlaces.addPlace();
        $rootScope.$digest();
        expect(MessageService.showFailedMessage).toHaveBeenCalled();
      });
    });
  });

  describe('Function Unit: AddPlaces.setPlaceAddress', function () {
    it('Should be defined and be a function', function () {
      expect(AddPlaces.setPlaceAddress).toBeDefined();
      expect(typeof AddPlaces.setPlaceAddress).toEqual('function');
    });
    it('Should assign new location data', function () {
      var expectedLocation = {
        'type': 'Point',
        'coordinates': [33.42195744640333, -111.9392205029726]
      };
      AddPlaces.place.location = '';
      AddPlaces.place.address = '';
      AddPlaces.setPlaceAddress(fakeResponse.fakeLocationData);
      $rootScope.$digest();
      expect(AddPlaces.place.location).toEqual(expectedLocation);
      expect(AddPlaces.place.address).toEqual('Arizona State University, Tempe Campus, 32-42 E University Dr, Tempe, AZ 85281');
    });
  });

  describe('Function Unit: AddPlaces.clearData', function () {
    it('Should be defined and be a function', function () {
      expect(AddPlaces.clearData).toBeDefined();
      expect(typeof AddPlaces.clearData).toEqual('function');
    });
    it('Should set location to blank', function () {
      AddPlaces.clearData();
      $rootScope.$digest();
      expect(AddPlaces.place.location).toEqual('');
    });
  });

  describe('Function Unit: AddPlaces.addBannerImage', function () {
    it('Should be defined and be a function', function () {
      expect(AddPlaces.addBannerImage).toBeDefined();
      expect(typeof AddPlaces.addBannerImage).toEqual('function');
    });
    describe('Should add bannerImage', function () {
      beforeEach(function () {
        PlaceAddEditApiService.addEditPlace.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve(fakeResponse.fakeAddBannerImage);
          deferred.$promise = deferred.promise;
          return deferred;
        });
      });
      it('When place exist', function () {
        AddPlaces.place.ep_id = '550f987fe4b09642124625a5';
        AddPlaces.place.title = 'new place test';
        AddPlaces.place.banner_asset = '';
        AddPlaces.addBannerImage(fakeResponse.fakeImage);
        $rootScope.$digest();
        expect(AddPlaces.place.banner_asset).toEqual('56559821e4b0da91f9e72234');
      });
      it('By posting place first', function () {
        AddPlaces.place.ep_id = null;
        AddPlaces.place.title = 'new place test';
        AddPlaces.place.banner_asset = '';
        AddPlaces.addBannerImage(fakeResponse.fakeImage);
        $rootScope.$digest();
        expect(AddPlaces.place.banner_asset).toEqual('56559821e4b0da91f9e72234');
      });
    });
    describe('Should not add bannerImage', function () {
      it('when asset id not found', function () {
        AddPlaces.place.ep_id = '550f987fe4b09642124625a5';
        AddPlaces.place.title = 'new place test';
        AddPlaces.place.banner_asset = '';
        AddPlaces.addBannerImage({id: '', url: ''});
        $rootScope.$digest();
        expect(AddPlaces.place.banner_asset).toEqual('');
      });
      it('when AddPlaces.place.title is empty', function () {
        AddPlaces.place.ep_id = '550f987fe4b09642124625a5';
        AddPlaces.place.title = '';
        AddPlaces.place.banner_asset = '';
        AddPlaces.addBannerImage(fakeResponse.fakeImage);
        $rootScope.$digest();
        expect(AddPlaces.place.banner_asset).toEqual('');
      });
      it('when PlaceAddEditApiService.addEditPlace respond an error status when posting place', function () {
        PlaceAddEditApiService.addEditPlace.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve({status: 'ALREADY_EXISTS'});
          deferred.$promise = deferred.promise;
          return deferred;
        });
        AddPlaces.place.ep_id = '550f987fe4b09642124625a5';
        AddPlaces.place.title = 'new place test';
        AddPlaces.place.banner_asset = '';
        AddPlaces.addBannerImage(fakeResponse.fakeImage);
        $rootScope.$digest();
        expect(MessageService.showFailedMessage).toHaveBeenCalled();
      });
      it('when PlaceAddEditApiService.addEditPlace respond an error status', function () {
        PlaceAddEditApiService.addEditPlace.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve({status: 'ALREADY_EXISTS'});
          deferred.$promise = deferred.promise;
          return deferred;
        });
        AddPlaces.place.ep_id = '';
        AddPlaces.place.title = 'new place test';
        AddPlaces.place.banner_asset = '';
        AddPlaces.addBannerImage(fakeResponse.fakeImage);
        $rootScope.$digest();
        expect(MessageService.showFailedMessage).toHaveBeenCalled();
      });
      it('when PlaceAddEditApiService.addEditPlace respond to failure', function () {
        PlaceAddEditApiService.addEditPlace.and.callFake(function () {
          var deferred = $q.defer();
          deferred.reject();
          deferred.$promise = deferred.promise;
          return deferred;
        });
        AddPlaces.place.ep_id = '550f987fe4b09642124625a5';
        AddPlaces.place.title = 'new place test';
        AddPlaces.place.banner_asset = '';
        AddPlaces.addBannerImage(fakeResponse.fakeImage);
        $rootScope.$digest();
        expect(AddPlaces.place.banner_asset).toEqual('');
      });
    });
  });

  describe('Function Unit: AddPlaces.removeBannerImage', function () {
    it('Should be defined and be a function', function () {
      expect(AddPlaces.removeBannerImage).toBeDefined();
      expect(typeof AddPlaces.removeBannerImage).toEqual('function');
    });
    it('Should remove bannerImage', function () {
      DeleteAssetsService.removeAsset.and.callFake(function () {
        var deferred = $q.defer();
        deferred.resolve(fakeResponse.fakeRemovedAsset);
        deferred.$promise = deferred.promise;
        return deferred;
      });
      AddPlaces.photos = [fakeResponse.fakeImage];
      AddPlaces.removeBannerImage(fakeResponse.fackTriggeredEvent, fakeResponse.fakeImage);
      $rootScope.$digest();
      expect(AddPlaces.photos.length).toEqual(0);
    });
    describe('Should not remove bannerImage', function () {
      it('when DeleteAssetsService.removeAsset respond an error status', function () {
        DeleteAssetsService.removeAsset.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve({status: 'ERROR'});
          deferred.$promise = deferred.promise;
          return deferred;
        });
        AddPlaces.photos = [fakeResponse.fakeImage];
        AddPlaces.removeBannerImage(fakeResponse.fackTriggeredEvent, fakeResponse.fakeImage);
        $rootScope.$digest();
        expect(AddPlaces.photos.length).not.toEqual(0);
      });
      it('when asset id not found', function () {
        AddPlaces.photos = [fakeResponse.fakeImage];
        AddPlaces.removeBannerImage(fakeResponse.fackTriggeredEvent, {id: '', url: ''});
        $rootScope.$digest();
        expect(AddPlaces.photos.length).not.toEqual(0);
      });
      it('when DeleteAssetsService.removeAsset respond to failure', function () {
        DeleteAssetsService.removeAsset.and.callFake(function () {
          var deferred = $q.defer();
          deferred.reject();
          deferred.$promise = deferred.promise;
          return deferred;
        });
        AddPlaces.photos = [fakeResponse.fakeImage];
        AddPlaces.removeBannerImage(fakeResponse.fackTriggeredEvent, fakeResponse.fakeImage);
        $rootScope.$digest();
        expect(AddPlaces.photos.length).not.toEqual(0);
      });
    });
  });

  describe('Function Unit: AddPlaces.isBannerImage', function () {
    it('Should be defined and be a function', function () {
      expect(AddPlaces.isBannerImage).toBeDefined();
      expect(typeof AddPlaces.isBannerImage).toEqual('function');
    });
    it('Should return true', function () {
      AddPlaces.place.banner_asset = '56559821e4b0da91f9e72234';
      var result = AddPlaces.isBannerImage(fakeResponse.fakeImage);
      $rootScope.$digest();
      expect(result).toEqual(true);
    });
  });

  describe('Function Unit: AddPlaces.uploadFile', function () {
    it('Should be defined and be a function', function () {
      expect(AddPlaces.uploadFile).toBeDefined();
      expect(typeof AddPlaces.uploadFile).toEqual('function');
    });
    describe('Should upload file on KWAD and aws s3 server', function () {
      it('On existing place', function () {
        AddPlaces.photos = [];
        AddPlaces.place.photos = null;
        AddPlaces.selectedFiles = fakeResponse.fakeJpegFile;
        AddPlaces.place.ep_id = '550f987fe4b09642124625a5';
        AddPlaces.place.title = '1865 Coffee';
        AddAssetsService.addAsset.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve({data: [fakeResponse.fakeAddedAsset]});
          deferred.$promise = deferred.promise;
          return deferred;
        });
        AWSUploadService.uploadFile.and.callFake(function (file, AWSObj, options) {
          var deferred = $q.defer();
          deferred.resolve({Location: 'https://s3.amazonaws.com/kwad-bump-assets//5592497703cef8958d68d1a5/56559821e4b0da91f9e72234/raw_upload'});
          return deferred.promise;
        });
        PlaceAddEditApiService.addEditPlace.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve(fakeResponse.fakeAddBannerImage);
          deferred.$promise = deferred.promise;
          return deferred;
        });
        AddPlaces.uploadFile();
        $rootScope.$digest();
        expect(AddPlaces.place.photos.length).toEqual(1);
        expect(AddPlaces.place.photos[0]).toEqual('5672a991e4b02b658c9278dd');
      });
      it('By posting place first', function () {
        AddPlaces.photos = [];
        AddPlaces.place.photos = null;
        AddPlaces.place.ep_id = null;
        AddPlaces.place.title = 'new place test';
        AddPlaces.selectedFiles = fakeResponse.fakeJpegFile;
        AddAssetsService.addAsset.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve({data: [fakeResponse.fakeAddedAsset]});
          deferred.$promise = deferred.promise;
          return deferred;
        });
        PlaceAddEditApiService.addEditPlace.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve({status: 'SUCCESS', data: PlaceData});
          deferred.$promise = deferred.promise;
          return deferred;
        });
        AWSUploadService.uploadFile.and.callFake(function (file, AWSObj, options) {
          var deferred = $q.defer();
          deferred.resolve({Location: 'https://s3.amazonaws.com/kwad-bump-assets//5592497703cef8958d68d1a5/56559821e4b0da91f9e72234/raw_upload'});
          return deferred.promise;
        });
        AddPlaces.uploadFile();
        $rootScope.$digest();
        expect(AddPlaces.place.photos.length).toEqual(1);
        expect(AddPlaces.place.photos[0]).toEqual('5672a991e4b02b658c9278dd');
      });
    });
    describe('Should not upload file', function () {
      it('When place title is empty', function () {
        AddPlaces.photos = [];
        AddPlaces.place.photos = null;
        AddPlaces.place.title = '';
        AddPlaces.uploadFile();
        $rootScope.$digest();
        expect(AddPlaces.place.photos).toEqual(null);
      });
      it('When length of selected files is zero', function () {
        AddPlaces.photos = [];
        AddPlaces.place.photos = null;
        AddPlaces.place.title = 'new place test';
        AddPlaces.selectedFiles = [];
        AddPlaces.uploadFile();
        $rootScope.$digest();
        expect(AddPlaces.place.photos).toEqual(null);
      });
      it('When length of selected files greater then ten', function () {
        AddPlaces.photos = [];
        AddPlaces.place.photos = null;
        AddPlaces.place.title = 'new place test';
        AddPlaces.selectedFiles = {length: 12};
        AddPlaces.uploadFile();
        $rootScope.$digest();
        expect(AddPlaces.place.photos).toEqual(null);
      });
      it('when PlaceAddEditApiService.addEditPlace rejected', function () {
        AddPlaces.photos = [];
        AddPlaces.place.photos = null;
        AddPlaces.place.ep_id = null;
        AddPlaces.place.title = 'new place test';
        AddPlaces.selectedFiles = fakeResponse.fakeJpegFile;
        AddAssetsService.addAsset.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve({data: [fakeResponse.fakeAddedAsset]});
          deferred.$promise = deferred.promise;
          return deferred;
        });
        PlaceAddEditApiService.addEditPlace.and.callFake(function () {
          var deferred = $q.defer();
          deferred.reject();
          deferred.$promise = deferred.promise;
          return deferred;
        });
        AddPlaces.uploadFile();
        $rootScope.$digest();
        expect(AddPlaces.place.photos).toEqual(null);
      });
      it('when AWSUploadService.uploadFile rejected', function () {
        AddPlaces.photos = [];
        AddPlaces.place.photos = null;
        AddPlaces.place.ep_id = '550f987fe4b09642124625a5';
        AddPlaces.place.title = 'new place test';
        AddPlaces.selectedFiles = fakeResponse.fakeJpgFile;
        AddAssetsService.addAsset.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve({data: [fakeResponse.fakeAddedAsset]});
          deferred.$promise = deferred.promise;
          return deferred;
        });
        AWSUploadService.uploadFile.and.callFake(function (file, AWSObj, options) {
          var deferred = $q.defer();
          deferred.reject();
          return deferred.promise;
        });
        AddPlaces.uploadFile();
        $rootScope.$digest();
        expect(AddPlaces.place.photos).toEqual(null);
      });
      it('when AWSUploadService.uploadFile rejected when file type is video', function () {
        AddPlaces.photos = [];
        AddPlaces.place.photos = null;
        AddPlaces.place.ep_id = '550f987fe4b09642124625a5';
        AddPlaces.place.title = 'new place test';
        AddPlaces.selectedFiles = fakeResponse.fakeVideoFile;
        AddAssetsService.addAsset.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve({data: [fakeResponse.fakeAddedAsset]});
          deferred.$promise = deferred.promise;
          return deferred;
        });
        AWSUploadService.uploadFile.and.callFake(function (file, AWSObj, options) {
          var deferred = $q.defer();
          deferred.reject();
          return deferred.promise;
        });
        AddPlaces.uploadFile();
        $rootScope.$digest();
        expect(AddPlaces.place.photos).toEqual(null);
      });
      it('when AWSUploadService.uploadFile rejected when file type is audio', function () {
        AddPlaces.photos = [];
        AddPlaces.place.photos = null;
        AddPlaces.place.ep_id = '550f987fe4b09642124625a5';
        AddPlaces.place.title = 'new place test';
        AddPlaces.selectedFiles = fakeResponse.fakeAudioFile;
        AddAssetsService.addAsset.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve({data: [fakeResponse.fakeAddedAsset]});
          deferred.$promise = deferred.promise;
          return deferred;
        });
        AWSUploadService.uploadFile.and.callFake(function (file, AWSObj, options) {
          var deferred = $q.defer();
          deferred.reject();
          return deferred.promise;
        });
        AddPlaces.uploadFile();
        $rootScope.$digest();
        expect(AddPlaces.place.photos).toEqual(null);
      });
      it('when AddAssetsService.addAsset rejected', function () {
        AddPlaces.photos = [];
        AddPlaces.place.photos = null;
        AddPlaces.place.ep_id = '550f987fe4b09642124625a5';
        AddPlaces.place.title = 'new place test';
        AddPlaces.selectedFiles = fakeResponse.fakeJpegFile;
        AddAssetsService.addAsset.and.callFake(function () {
          var deferred = $q.defer();
          deferred.reject();
          deferred.$promise = deferred.promise;
          return deferred;
        });
        AddPlaces.uploadFile();
        $rootScope.$digest();
        expect(AddPlaces.place.photos).toEqual(null);
      });
      it('when posting place first if PlaceAddEditApiService.addEditPlace return 200 with error status', function () {
        AddPlaces.photos = [];
        AddPlaces.place.photos = null;
        AddPlaces.place.ep_id = null;
        AddPlaces.place.title = 'new place test';
        AddPlaces.selectedFiles = fakeResponse.fakeJpegFile;
        PlaceAddEditApiService.addEditPlace.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve({status: 'INSUFFICIENT_RIGHTS'});
          deferred.$promise = deferred.promise;
          return deferred;
        });
        AddPlaces.uploadFile();
        $rootScope.$digest();
        expect(AddPlaces.place.photos).toEqual(null);
      });
    });
  });

  describe('Function Unit: AddPlaces.fetchAssets', function () {
    it('Should be defined and be a function', function () {
      expect(AddPlaces.fetchAssets).toBeDefined();
      expect(typeof AddPlaces.fetchAssets).toEqual('function');
    });
    it('Should fetch images', function () {
      AddPlaces.photos = [];
      FetchAssetsService.fetchAsset.and.callFake(function () {
        var deferred = $q.defer();
        deferred.resolve(fakeResponse.fakeFetchedAsset);
        deferred.$promise = deferred.promise;
        return deferred;
      });
      AddPlaces.fetchAssets('56559821e4b0da91f9e72234');
      $rootScope.$digest();
      expect(AddPlaces.photos.length).toEqual(1);
    });
    it('Should fetch data with no image prefixes', function () {
      AddPlaces.photos = [];
      FetchAssetsService.fetchAsset.and.callFake(function () {
        var deferred = $q.defer();
        deferred.resolve({
          aws_key: 'ASIAIG63HOWFIRIEQCJQ',
          aws_secret: 'z3ZnvoIHEBVFe21GuiqxOLuJ+CSIBlguUbMu5c2h',
          aws_token: 'AQoDYXdzEEYasAODTH4AOcylcz+EkZt4QEaYlmOiUe/e7HfuIrVhdnLET3djkip1VXCM571/ipl1qe8gt7Kek2JCWNJFCPlAIn7gkWFIb3qNpHip/yhCXZedyFiiaQGhfcWPhhbNFL+yL+vX3BpLO3Vu4+a2p/4V+egycRQp60QPmpgH0KQAqHDCnBf4bVYBf+bSJIQGM09WpwaSOAaiZ2UZdeqAC39CLdwLlqe85NJ0kMT4B3SJCLKmxbeUaZQtV8P9LXQygNoAlqmNA6S38HFtByRrU32wiSTVeGXDiUTRxJmdcYKgUNZQMsaRTuj2llwN5kZLL1SoBpO8Sw6XFBIDOgFWIf0VkAQJOH3RUg7T/udeyvOqNVNhPelUBraz8+IxeYoxjEThEXDfTzMkelrhqpT2DNCLWmCtMlJst505QH5pkQgq6CxVQTA7504nTbQ9iKphtttgDgHcS6zwsTsa0F6rJOV3SH/rR857NT83ImXw3kGw+0edJL1IAFnEIc+H7WBjvZhx6pokP+jEgakL6n/o3fCm6/bNmsxozNonPQeUG6zrgkdTK+MfJ1TD2oYXg3fghDpXDMsgjqDAswU=',
          prefixes: [],
          s3_bucket: 'kwad-bump-assets'
        });
        deferred.$promise = deferred.promise;
        return deferred;
      });
      AddPlaces.fetchAssets('56559821e4b0da91f9e72234');
      $rootScope.$digest();
      expect(AddPlaces.photos.length).toEqual(0);
    });
    it('Should not fetch data when asset id is undefined', function () {
      AddPlaces.photos = [];
      AddPlaces.fetchAssets();
      $rootScope.$digest();
      expect(AddPlaces.photos.length).toEqual(0);
    });
    it('Should not fetch data when FetchAssetsService.fetchAsset rejected', function () {
      AddPlaces.photos = [];
      FetchAssetsService.fetchAsset.and.callFake(function () {
        var deferred = $q.defer();
        deferred.reject();
        deferred.$promise = deferred.promise;
        return deferred;
      });
      AddPlaces.fetchAssets('56559821e4b0da91f9e72234');
      $rootScope.$digest();
      expect(AddPlaces.photos.length).toEqual(0);
    });
  });
});
