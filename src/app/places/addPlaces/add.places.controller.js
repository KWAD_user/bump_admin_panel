'use strict';

(function (angular) {
  angular.module('kwadWeb')
    .controller('AddPlacesCtrl', ['$rootScope', '$scope', '$state', '$q', 'AuthService', 'PlaceData', 'PlaceAddEditApiService', 'AddAssetsService', 'FetchAssetsService', 'DeleteAssetsService', 'AWSUploadService', 'EnumsService', 'LoaderService', 'MessageService', 'ToastrMessage', 'AWS_UPLOAD',
      function ($rootScope, $scope, $state, $q, AuthService, PlaceData, PlaceAddEditApiService, AddAssetsService, FetchAssetsService, DeleteAssetsService, AWSUploadService, EnumsService, LoaderService, MessageService, ToastrMessage, AWS_UPLOAD) {
        // Define variables
        var isNewPlaceSaved = false;
        var AddPlaces = this;
        AddPlaces.place = PlaceData ? PlaceData : {};
        AddPlaces.USER_ROLES = null;
        AddPlaces.USER_ROLES_OBJ = null;
        AddPlaces.EP_TYPES = null;
        AddPlaces.PLACE_CATEGORIES = null;
        AddPlaces.PLACE_CATEGORY_TYPES = null;
        AddPlaces.status = AddPlaces.place.disabled ? 'DISABLED' : 'ENABLED';
        AddPlaces.category = null;
        AddPlaces.categoryType = AddPlaces.place.private ? 'PRIVATE' : 'PUBLIC';
        AddPlaces.place.keywords = AddPlaces.place.keywords ? AddPlaces.place.keywords : [];
        AddPlaces.bump_type = AddPlaces.place.bump_type === 'BUMP';
        AddPlaces.place.title = '';
        AddPlaces.place.ep_id = null;
        AddPlaces.selectedFiles = '';
        AddPlaces.photos = [];
        AddPlaces.ASSET_TYPES = EnumsService.getAssetTypeEnums();

        var _successAddPlace = function (response) {
          LoaderService.Loader.hideSpinner();
          if (response.status === 'SUCCESS') {
            isNewPlaceSaved = true;
            MessageService.showSuccessfulMessage(ToastrMessage.PLACE.ADD[response.status]);
            $state.go('placeDetails', {placeId: response.data.ep_id});
          } else {
            MessageService.showFailedMessage(ToastrMessage.PLACE.ADD[response.status]);
          }
        };
        var _failAddPlace = function (err) {
          LoaderService.Loader.hideSpinner();
          if (err === 'BLANK_TITLE') {
            MessageService.showFailedMessage(ToastrMessage.PLACE.ADD.BLANK_TITLE);
          } else if (err === 'BLANK_LOCATION') {
            MessageService.showFailedMessage(ToastrMessage.PLACE.ADD.BLANK_LOCATION);
          } else {
            MessageService.showFailedMessage();
          }
        };

        var userRoleEnumsSuccess = function (userRoles) {
          AddPlaces.USER_ROLES = angular.copy(userRoles);
          AddPlaces.USER_ROLES_OBJ = userRoles.toObject();
          AuthService.getCurrentUser().then(function (user) {
            AddPlaces.currentUser = user;
            switch (AddPlaces.currentUser.role) {
              case AddPlaces.USER_ROLES_OBJ.ADMIN:
              case AddPlaces.USER_ROLES_OBJ.LAUNCHSQUAD:
                AddPlaces.PLACE_CATEGORY_TYPES = ['PRIVATE', 'PUBLIC'];
                break;
              case AddPlaces.USER_ROLES_OBJ.REGULAR:
                AddPlaces.PLACE_CATEGORY_TYPES = null;
                break;
              default:
                AddPlaces.PLACE_CATEGORY_TYPES = ['PUBLIC'];
                break;
            }
          }, function (err) {
            AddPlaces.currentUser = null;
            MessageService.showFailedMessage();
          });
        };
        var userRoleEnumsFailure = function () {
          MessageService.showFailedMessage();
          LoaderService.Loader.hideSpinner();
        };

        function addPlace() {
          var deferred = $q.defer();
          var _payload = {
            category_id: AddPlaces.place.cat_id,
            title: AddPlaces.place.title,
            description: AddPlaces.place.description || '',
            address: AddPlaces.place.address,
            keywords: AddPlaces.place.keywords || [],
            website: AddPlaces.place.website || '',
            private: AddPlaces.place.private || false,
            disabled: AddPlaces.place.disabled || false,
            bump_type: AddPlaces.place.bump_type,
            location: AddPlaces.place.location,
            phone: AddPlaces.place.phone || ''
          };

          if (AddPlaces.place.ep_id) {
            _payload.p_id = AddPlaces.place.ep_id;
          }

          if (!_payload.title) {
            deferred.reject('BLANK_TITLE');
          } else if (!_payload.location) {
            deferred.reject('BLANK_LOCATION');
          } else {
            PlaceAddEditApiService.addEditPlace(_payload).$promise.then(function (resp) {
              deferred.resolve(resp);
            }, function (err) {
              deferred.reject(err);
            });
          }
          return deferred.promise;
        }

        function getPhotosUrls(photos) {
          if (!Array.isArray(photos) || !photos.length) {
            return false;
          }
          var chunks = photos.chunks(10);
          chunks.forEach(function (chunk) {
            AddPlaces.fetchAssets(chunk);
          });
        }

        function getAssetType(type) {
          switch (type) {
            case'image/jpg':
              return AddPlaces.ASSET_TYPES.IMAGE_JPG;
            case'image/jpeg':
              return AddPlaces.ASSET_TYPES.IMAGE_JPEG;
            case'video/*':
              return AddPlaces.ASSET_TYPES.VIDEO;
            default :
              return false;
          }
        }

        function fetchAsset(AWSObj) {
          var bucketName = AWSObj.s3_bucket;//'dev-web.whatsbumpin.net'
          var IMAGE_URL = '';
          if (Array.isArray(AWSObj.prefixes) && AWSObj.prefixes.length) {
            AWSObj.prefixes.forEach(function (prefix) {
              IMAGE_URL = AWS_UPLOAD.URL + bucketName + '/' + prefix + '/raw_upload';
              AddPlaces.photos.push({url: IMAGE_URL, id: prefix.split('/')[2]});
            });
          } else {
            MessageService.showFailedMessage();
          }
        }

        function uploadFile(file, AWSObj) {
          var options = {quality: 72};
          AWSUploadService.uploadFile(file, AWSObj, options).then(function (response) {
            var asset = {url: response.Location, id: AWSObj.asset_id};
            LoaderService.Loader.hideSpinner();
            if (AddPlaces.photos.length === 0) {
              AddPlaces.place.banner_asset = asset.id;
              AddPlaces.addBannerImage(asset);
            }
            AddPlaces.photos.push(asset);
            if (!AddPlaces.place.photos) {
              AddPlaces.place.photos = [];
            }
            AddPlaces.place.photos.push(AWSObj.asset_id);
            MessageService.showSuccessfulMessage(ToastrMessage.UPLOAD.SUCCESS);
          }, function (err) {
            LoaderService.Loader.hideSpinner();
            MessageService.showFailedMessage();
          });
        }

        function addBannerImage(placeId, asset) {
          var _payload = {
            p_id: placeId,
            banner_asset: asset.id,
            location: AddPlaces.place.location
          };

          var _successAddEvent = function (response) {
            LoaderService.Loader.hideSpinner();
            if (response.status === 'SUCCESS') {
              if (AddPlaces.place.banner_asset !== response.data.banner_asset) {
                AddPlaces.place.banner_asset = response.data.banner_asset;
              }
              MessageService.showSuccessfulMessage(ToastrMessage.PLACE.UPDATE[response.status]);
            } else {
              MessageService.showFailedMessage(ToastrMessage.PLACE.UPDATE[response.status]);
            }
          };
          var _failAddPlace = function (err) {
            LoaderService.Loader.hideSpinner();
            MessageService.showFailedMessage();
          };

          PlaceAddEditApiService.addEditPlace(_payload).$promise.then(_successAddEvent, _failAddPlace);
        }

        AddPlaces.fetchAssets = function (asset_ids) {
          LoaderService.Loader.showSpinner();
          var _payload = [],
            success = function (response) {
              LoaderService.Loader.hideSpinner();
              var AWSObj = response.data ? response.data : response;
              fetchAsset(AWSObj);
            },
            failure = function (err) {
              LoaderService.Loader.hideSpinner();
              MessageService.showFailedMessage();
            };
          if (typeof asset_ids === 'string') {
            _payload.push({
              asset_id: asset_ids
            });
          }
          else if (Array.isArray(asset_ids) && asset_ids.length) {
            asset_ids.forEach(function (asset_id) {
              _payload.push({
                asset_id: asset_id
              });
            });
          } else {
            LoaderService.Loader.hideSpinner();
            MessageService.showFailedMessage(ToastrMessage.ASSET.ASSET_ID_NOT_FOUND);
            return false;
          }
          FetchAssetsService.fetchAsset(_payload).$promise.then(success, failure);
        };

        AddPlaces.uploadFile = function () {
          LoaderService.Loader.showSpinner();

          if (!AddPlaces.place.title) {
            MessageService.showFailedMessage(ToastrMessage.PLACE.ADD.BLANK_TITLE);
            LoaderService.Loader.hideSpinner();
            return false;
          }

          if (!AddPlaces.selectedFiles || !AddPlaces.selectedFiles.length) {
            LoaderService.Loader.hideSpinner();
            MessageService.showFailedMessage(ToastrMessage.UPLOAD.FILE_NOT_FOUND);
            return false;
          }

          if (AddPlaces.selectedFiles.length > 10) {
            LoaderService.Loader.hideSpinner();
            MessageService.showFailedMessage(ToastrMessage.UPLOAD.MAX_FILES);
            return false;
          }

          var _payload = [],
            success = function (response) {
              var AWSObjs = response.data && response.data.length ? response.data : false;
              for (var j = 0; j < AWSObjs.length; j++) {
                uploadFile(AddPlaces.selectedFiles[j], AWSObjs[j]);
              }
            },
            failure = function (err) {
              LoaderService.Loader.hideSpinner();
              MessageService.showFailedMessage();
            },
            getAwsCredentialsForSelectedFiles = function () {
              for (var i = 0; i < AddPlaces.selectedFiles.length; i++) {
                var file = AddPlaces.selectedFiles[i];
                _payload.push({
                  file: file.name,
                  type: getAssetType(file.type),
                  ep_id: AddPlaces.place.ep_id
                });
              }
              AddAssetsService.addAsset(_payload).$promise.then(success, failure);
            };

          if (AddPlaces.place.ep_id) {
            getAwsCredentialsForSelectedFiles();
          } else {
            addPlace().then(function (response) {
              if (response.status === 'SUCCESS') {
                AddPlaces.place.ep_id = response.data.ep_id;
                getAwsCredentialsForSelectedFiles();
              } else {
                MessageService.showFailedMessage(ToastrMessage.PLACE.ADD[response.status]);
              }
            }, _failAddPlace)
          }
        };

        AddPlaces.init = function () {
          EnumsService.getUserRoleEnums().then(userRoleEnumsSuccess, userRoleEnumsFailure);
          EnumsService.getEpTypeEnums().then(function (result) {
            AddPlaces.EP_TYPES = result;
          }, function (error) {
            console.log('EP_TYPES ERROR: ', error);
          });
          EnumsService.getPlaceCategoryEnums().then(function (result) {
            AddPlaces.PLACE_CATEGORIES = [];
            Object.keys(result).forEach(function (key) {
              AddPlaces.PLACE_CATEGORIES.push({
                id: key, value: result[key]
              })
            });
            if (AddPlaces.place.cat_id) {
              AddPlaces.category = {
                id: AddPlaces.place.cat_id, value: result[AddPlaces.place.cat_id]
              };
            } else {
              AddPlaces.category = AddPlaces.PLACE_CATEGORIES[0];
              AddPlaces.place.cat_id = AddPlaces.category.id;
            }
          }, function (error) {
            AddPlaces.PLACE_CATEGORIES = null;
            console.log('Place Category ERROR: ', error);
          });
          getPhotosUrls(AddPlaces.place.photos);
        };

        AddPlaces.addNewKeyword = function () {
          if (AddPlaces.newTag) {
            AddPlaces.place.keywords.push(AddPlaces.newTag);
            AddPlaces.newTag = '';
          }
        };

        AddPlaces.removeKeyword = function (index) {
          AddPlaces.place.keywords.splice(index, 1);
        };

        AddPlaces.onCategoryChange = function () {
          AddPlaces.place.cat_id = AddPlaces.category.id;
        };

        AddPlaces.onCategoryTypeChange = function () {
          AddPlaces.place.private = (AddPlaces.categoryType === 'PRIVATE');
        };

        AddPlaces.onBumpTypeChange = function () {
          AddPlaces.place.bump_type = AddPlaces.bump_type ? 'BUMP' : 'NON_BUMP';
        };

        AddPlaces.addPlace = function () {
          addPlace().then(_successAddPlace, _failAddPlace);
        };

        AddPlaces.isBannerImage = function (image) {
          return image.id === AddPlaces.place.banner_asset;
        };

        AddPlaces.addBannerImage = function (asset) {
          LoaderService.Loader.showSpinner();
          if (!asset || !asset.id) {
            LoaderService.Loader.hideSpinner();
            MessageService.showFailedMessage(ToastrMessage.ASSET.ASSET_ID_NOT_FOUND);
            return false;
          }

          if (!AddPlaces.place.title) {
            MessageService.showFailedMessage(ToastrMessage.PLACE.ADD.BLANK_TITLE);
            LoaderService.Loader.hideSpinner();
            return false;
          }

          if (AddPlaces.place.ep_id) {
            addBannerImage(AddPlaces.place.ep_id, asset)
          } else {
            LoaderService.Loader.showSpinner();
            addPlace().then(function (response) {
              LoaderService.Loader.hideSpinner();
              if (response.status === 'SUCCESS') {
                AddPlaces.place.ep_id = response.data.ep_id;
                addBannerImage(AddPlaces.place.ep_id, asset);
              } else {
                MessageService.showFailedMessage(ToastrMessage.PLACE.ADD[response.status]);
              }
            }, _failAddPlace)
          }
        };

        AddPlaces.removeBannerImage = function (e, asset) {
          e.stopPropagation();
          LoaderService.Loader.showSpinner();
          var _payload = [],
            success = function (response) {
              LoaderService.Loader.hideSpinner();
              if (response.asset_ids && response.asset_ids.length) {
                AddPlaces.photos = AddPlaces.photos.filter(function (el) {
                  return el.id !== response.asset_ids[0];
                });
                MessageService.showSuccessfulMessage(ToastrMessage.ASSET.REMOVED_SUCCESSFULLY);
              } else {
                MessageService.showFailedMessage();
              }
            },
            failure = function (err) {
              LoaderService.Loader.hideSpinner();
              MessageService.showFailedMessage();
            };

          if (asset.id && typeof asset.id === 'string') {
            _payload.push({
              asset_id: asset.id
            });
          } else {
            LoaderService.Loader.hideSpinner();
            MessageService.showFailedMessage(ToastrMessage.ASSET.ASSET_ID_NOT_FOUND);
            return false;
          }
          DeleteAssetsService.removeAsset(_payload).$promise.then(success, failure);
        };

        AddPlaces.clearData = function () {
          AddPlaces.place.location = '';
        };

        AddPlaces.setPlaceAddress = function (data) {
          AddPlaces.place.location = {
            coordinates: data.coordinates,
            type: 'Point'
          };
          AddPlaces.place.address = data.location;
        };

        $scope.$on('$stateChangeStart', function (event) {
          var answer = '';
          if (!isNewPlaceSaved) {
            answer = confirm("Are you sure you want to leave this page?");
            if (!answer) {
              $rootScope.$broadcast('EVENT_CANCEL');
              event.preventDefault();
            }
          }
        });
      }]);
})(angular);