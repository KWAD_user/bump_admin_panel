'use strict';

describe('Unit: Places Controller', function () {
  var Places, $scope, $rootScope, $q, $controller, $state, PlaceList, EventPlaceApiService, EnumsService, ToastrMessage, MessageService, $httpBackend;

  beforeEach(module('kwadWeb'));
  beforeEach(inject(function (_$rootScope_, _$controller_, _$q_, _ToastrMessage_, _$httpBackend_) {
    $q = _$q_;
    $rootScope = _$rootScope_;
    $scope = _$rootScope_.$new();
    $httpBackend = _$httpBackend_;
    $controller = _$controller_;
    EventPlaceApiService = jasmine.createSpyObj('EventPlaceApiService', ['getPlacesList']);
    EnumsService = jasmine.createSpyObj('EnumsService', ['getUserRoleEnums']);
    $state = jasmine.createSpyObj('$state', ['go']);
    MessageService = jasmine.createSpyObj('MessageService', ['showSuccessfulMessage', 'showFailedMessage']);
    ToastrMessage = _ToastrMessage_;
    MessageService.showSuccessfulMessage.and.callFake(function () {
    });
    MessageService.showFailedMessage.and.callFake(function () {
    });
    $state.go.and.callFake(function () {
    });
  }));
  beforeEach(function () {
    Places = $controller('PlacesCtrl', {
      $scope: $scope,
      $state: $state,
      EventPlaceApiService: EventPlaceApiService,
      EnumsService: EnumsService,
      ToastrMessage: ToastrMessage,
      MessageService: MessageService
    });
    $httpBackend.expectGET('app/login/login.html')
      .respond(200);
    PlaceList = [{
      'epId': '546aff5be4b0370c32328fbb',
      'name': 'Phoenix Rock Gym',
      'place': '1353 E University Drive, Tempe AZ',
      'creatorName': 'Demo  User ',
      'creatorId': '546aff58e4b0370c32328f40',
      'feedImpression': 0,
      'detailImpression': 0,
      'appCheckinCount': 26,
      'geoCheckinCount': 8,
      'shares': 26,
      'status': 'ENABLED',
      'associatedEvents': 2,
      'creatorRole': 'ADMIN',
      'eventDate': null,
      'eventTimeLine': 'UNDETERMINED'
    }, {
      'epId': '546aff5be4b0370c32328fbf',
      'name': 'The Vue on Apache',
      'place': '922 E Apache Blvd, Tempe AZ',
      'creatorName': 'Brandy Smith',
      'creatorId': '546aff59e4b0370c32328f43',
      'feedImpression': 6,
      'detailImpression': 0,
      'appCheckinCount': 24,
      'geoCheckinCount': 283,
      'shares': 24,
      'status': 'ENABLED',
      'associatedEvents': 0,
      'creatorRole': 'BUMP',
      'eventDate': null,
      'eventTimeLine': 'UNDETERMINED'
    }, {
      'epId': '546aff5ce4b0370c32328fc4',
      'name': 'AMF Tempe Village Lanes',
      'place': '4407 S Rural Road, Tempe AZ',
      'creatorName': 'John  Joseph Aguado',
      'creatorId': '546aff59e4b0370c32328f46',
      'feedImpression': 0,
      'detailImpression': 0,
      'appCheckinCount': 24,
      'geoCheckinCount': 0,
      'shares': 24,
      'status': 'ENABLED',
      'associatedEvents': 2,
      'creatorRole': 'REGULAR',
      'eventDate': null,
      'eventTimeLine': 'UNDETERMINED'
    }, {
      'epId': '546aff5ce4b0370c32328fc8',
      'name': 'Club Afterlife',
      'place': '4282 N Drinkwater Blvd, Tempe AZ',
      'creatorName': 'Dianne Smith',
      'creatorId': '546aff59e4b0370c32328f49',
      'feedImpression': 6,
      'detailImpression': 0,
      'appCheckinCount': 25,
      'geoCheckinCount': 283,
      'shares': 25,
      'status': 'ENABLED',
      'associatedEvents': 0,
      'creatorRole': 'business',
      'eventDate': null,
      'eventTimeLine': 'UNDETERMINED'
    },{
      'epId': '546aff5ce4b0370c32328fc8',
      'name': 'Club Afterlife',
      'place': '4282 N Drinkwater Blvd, Tempe AZ',
      'creatorName': 'Dianne Smith',
      'creatorId': '546aff59e4b0370c32328f49',
      'feedImpression': 6,
      'detailImpression': 0,
      'appCheckinCount': 25,
      'geoCheckinCount': 283,
      'shares': 25,
      'status': 'ENABLED',
      'associatedEvents': 0,
      'creatorRole': 'editor',
      'eventDate': null,
      'eventTimeLine': 'UNDETERMINED'
    }];
  });

  describe('Units Should be defined', function () {
    it('Places should be defined', function () {
      expect(Places).toBeDefined();
    });
    it('EventPlaceApiService should be defined', function () {
      expect(EventPlaceApiService).toBeDefined();
    });
    it('ToastrMessage should be defined', function () {
      expect(ToastrMessage).toBeDefined();
    });
  });

  describe('Unit: Places.init function', function () {
    it('Places.init should be defined and be a function', function () {
      expect(Places.init).toBeDefined();
      expect(typeof Places.init).toEqual('function');
    });

    it('Places.init Should assign value to Places.allPlaces when EventPlaceApiService.getPlacesList get resolved.', function () {

      EventPlaceApiService.getPlacesList.and.callFake(function () {
        var deferred = $q.defer();
        deferred.resolve(PlaceList);
        deferred.$promise = deferred.promise;
        return deferred;
      });
      EnumsService.getUserRoleEnums.and.callFake(function () {
        var deferred = $q.defer();
        deferred.resolve(['ANONYMOUS', 'REGULAR', 'LAUNCHSQUAD', 'ADMIN', 'BUSINESS', 'BUSINESS_INDIVIDUAL', 'EDITOR']);
        return deferred.promise;
      });
      Places.init();
      $rootScope.$digest();
      $httpBackend.flush();
      expect(Places.allPlaces.length).toBeGreaterThan(0);
    });

    it('Places.init Should not assign value to Places.allPlaces when EventPlaceApiService.getPlacesList get rejected.', function () {
      EventPlaceApiService.getPlacesList.and.callFake(function () {
        var deferred = $q.defer();
        deferred.reject();
        deferred.$promise = deferred.promise;
        return deferred;
      });
      EnumsService.getUserRoleEnums.and.callFake(function () {
        var deferred = $q.defer();
        deferred.reject();
        return deferred.promise;
      });
      Places.init();
      $rootScope.$digest();
      $httpBackend.flush();
      expect(Places.allPlaces.length).toEqual(0);
    });
  });

  describe('Unit: Places.editPlaceDetails function', function () {
    it('Places.editPlaceDetails should be defined and be a function', function () {
      expect(Places.editPlaceDetails).toBeDefined();
      expect(typeof Places.editPlaceDetails).toEqual('function');
    });

    it('Places.editPlaceDetails Should change state to place details', function () {
      var placeData = {
        'epId': '546aff5ce4b0370c32328fc8',
        'name': 'Club Afterlife',
        'place': '4282 N Drinkwater Blvd, Tempe AZ',
        'creatorName': 'Dianne Smith',
        'creatorId': '546aff59e4b0370c32328f49',
        'feedImpression': 6,
        'detailImpression': 0,
        'appCheckinCount': 25,
        'geoCheckinCount': 283,
        'shares': 25,
        'status': 'ENABLED',
        'associatedEvents': 0,
        'creatorRole': 'REGULAR',
        'eventDate': null,
        'eventTimeLine': 'UNDETERMINED'
      };

      Places.editPlaceDetails(placeData);
      $rootScope.$digest();
      $httpBackend.flush();
      expect($state.go).toHaveBeenCalled();
    });
  });

  describe('Unit: Places.addPlaces function', function () {
    it('Places.addPlaces should be defined and be a function', function () {
      expect(Places.addPlaces).toBeDefined();
      expect(typeof Places.addPlaces).toEqual('function');
    });

    it('Places.addPlaces Should change state to place add', function () {
      Places.addPlaces();
      $rootScope.$digest();
      $httpBackend.flush();
      expect($state.go).toHaveBeenCalled();
    });
  });

  describe('Unit: Places.filterBy function', function () {

    it('Places.filterBy Should filter Places based on their creatorRole', function () {
      EventPlaceApiService.getPlacesList.and.callFake(function () {
        var deferred = $q.defer();
        deferred.resolve(PlaceList);
        deferred.$promise = deferred.promise;
        return deferred;
      });
      EnumsService.getUserRoleEnums.and.callFake(function () {
        var deferred = $q.defer();
        deferred.resolve(['ANONYMOUS', 'REGULAR', 'LAUNCHSQUAD', 'ADMIN', 'BUSINESS', 'BUSINESS_INDIVIDUAL', 'EDITOR']);
        return deferred.promise;
      });
      Places.init();
      $rootScope.$digest();
      Places.allPlaces = PlaceList;
      Places.filterBy();
      $rootScope.$digest();
      $httpBackend.flush();
      Places.filterBy('regular');
      $rootScope.$digest();
      Places.filterBy('bump');
      $rootScope.$digest();
      Places.filterBy('business');
      $rootScope.$digest();
      Places.filterBy('editor');
      $rootScope.$digest();
      Places.filterBy('admin');
      $rootScope.$digest();
      expect(Places.filteredPlaces.length).toBeGreaterThan(0);
    });
  });
});
