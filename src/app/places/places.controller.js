'use strict';

(function (angular) {
	angular.module('kwadWeb')
			.controller('PlacesCtrl', ['$rootScope', '$scope', '$state', '$filter', '$compile', '$log', 'EnumsService', 'CookieService', 'DTOptionsBuilder', 'DTColumnDefBuilder', 'DTColumnBuilder', 'EventPlaceApiService', 'DomainAddEditApiService', 'MessageService', 'ToastrMessage', 'LoaderService', 'HOST',
				function ($rootScope, $scope, $state, $filter, $compile, $log, EnumsService, CookieService, DTOptionsBuilder, DTColumnDefBuilder, DTColumnBuilder, EventPlaceApiService, DomainAddEditApiService, MessageService, ToastrMessage, LoaderService, HOST) {
					// Define variables
					var USER_ROLES_OBJ = null,
							Places = this;

					Places.viewName = 'Places';
					Places.serchForStatus = 'ALL';
					Places.allPlaces = [];										
					Places.filteredPlaces = null;
					Places.dtInstance = {};
					
					var userToken = CookieService.userSession.get();

					var _successGetPlaces = function (data) {
						Places.allPlaces = data;
						Places.filteredPlaces = angular.copy(Places.allPlaces);
						LoaderService.Loader.hideSpinner();
					};
					var _failGetPlaces = function () {
						Places.allPlaces = [];
						MessageService.showFailedMessage();
						LoaderService.Loader.hideSpinner();
					};

					Places.init = function () {
						Places.getPlaces();												

						EnumsService.getUserRoleEnums().then(function (userRoles) {
							USER_ROLES_OBJ = userRoles.toObject();
						}, function (err) {
							$log.info('Error:', err);
						});

					};
					
					Places.getPlaces = function (){
						LoaderService.Loader.showSpinner();

						Places.dtOptions = DTOptionsBuilder.newOptions()
								.withOption('ajax', {
									url: HOST.HOST_URL + '/api/ext/s/search',
									type: 'POST',
									headers: {'Authorization': 'Bearer ' + btoa(userToken)},
									data: function (data) {
										var _payload = JSON.stringify({
											'type': 'PLACE',
											'query': data.search.value,
											'limit': data.length,
											'offSet': data.start,
											'role': Places.filteredPlaces
										});
										return _payload;
									},
									dataFilter: function (data) {
										var json = jQuery.parseJSON(data);
										json.recordsTotal = json.count;
										json.recordsFiltered = json.count;

										return JSON.stringify(json);
									},
									dataType: 'json',
									contentType: 'application/json'
								})
								.withDataProp('data')
								.withOption('processing', true)
								.withOption('serverSide', true)
								.withOption('createdRow', createdRow)
								.withPaginationType('full_numbers');

						Places.dtColumns = [
							DTColumnBuilder.newColumn('name').withTitle('Place Name'),																						
							DTColumnBuilder.newColumn('place').withTitle('Location'),
							DTColumnBuilder.newColumn('creatorName').withTitle('Creator'),
							DTColumnBuilder.newColumn('associatedEvents').withTitle('Assoc. Events'),
							DTColumnBuilder.newColumn('feedImpression').withTitle('Fd. Events'),
							DTColumnBuilder.newColumn('detailImpression').withTitle('Dt. Impr'),
							DTColumnBuilder.newColumn('appCheckinCount').withTitle('App CI'),
							DTColumnBuilder.newColumn('geoCheckinCount').withTitle('Geo CI'),
							DTColumnBuilder.newColumn('shares').withTitle('Shares'),
							DTColumnBuilder.newColumn('status').withTitle('Status')
						];

						LoaderService.Loader.hideSpinner();	
					};
					
					function createdRow(row, data, dataIndex) {
						$(row).addClass('edit table-row cursor-pointer');
						$(row).unbind('click');						
						$(row).bind('click', function(){
							$scope.$apply(function(){
								Places.editPlaceDetails(data);
							});
						});
						
						// Recompiling so we can bind Angular directive to the DT
						$compile(angular.element(row).contents())($scope);
					}

					Places.editPlaceDetails = function (data) {
						$state.go('placeDetails', {placeId: data.epId});
					};

					Places.addPlaces = function () {
						$state.go('addPlaces');
					};

					Places.filterBy = function (filterName) {
						Places.filteredPlaces = filterName;
						
						//rerender the table
						Places.dtInstance.rerender();
					};

				}]);
})(angular);
