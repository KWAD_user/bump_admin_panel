'use strict';
(function (angular) {
  angular.module('kwadWeb', ['ngAnimate', 'ngCookies', 'ngTouch', 'ngSanitize', 'ngResource', 'ui.router', 'ui.bootstrap', 'datatables'])
    .run(['$rootScope', '$state', 'CookieService', function ($rootScope, $state, CookieService) {
      $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState) {
        var isLive = CookieService.userSession.isLive();
        if (toState.authentication && !isLive) {
          // User isn’t authenticated
          $state.transitionTo('login');
          event.preventDefault();
        }
        if (toState.name === 'login' && isLive) {
          var newState = (fromState.name !== '' ) ? fromState.name : 'domains';
          $state.transitionTo(newState);
          event.preventDefault();
        }
        if ($state.current.name !== 'users' && toState.defaultRoute) {
          event.preventDefault();
          $state.go(toState.name + toState.defaultRoute);
        }
      });
    }])
    .config(['$stateProvider', '$urlRouterProvider', '$httpProvider', '$locationProvider', function ($stateProvider, $urlRouterProvider, $httpProvider, $locationProvider) {
      $httpProvider.interceptors.push('SessionInterceptorsService');
      $locationProvider.html5Mode(true);
      $stateProvider
        .state('login', {
          url: '/',
          templateUrl: 'app/login/login.html',
          controllerAs: 'Login',
          controller: 'LoginCtrl'
        })
        .state('forgetpassword', {
          url: '/forgetpassword',
          templateUrl: 'app/forgetPassword/forgetpassword.html',
          controllerAs: 'ForgetPassword',
          controller: 'ForgetPasswordCtrl'
        })
        .state('domains', {
          url: '/domains',
          templateUrl: 'app/domains/domains.html',
          controller: 'DomainsCtrl as Domains',
          authentication: true
        })
        .state('users', {
          url: '/users',
          defaultRoute: '.regular',
          templateUrl: 'app/users/users.html',
          controller: 'UsersCtrl as Users'
        })
        .state('users.admin', {
          url: '/admin',
          templateUrl: 'app/users/users.html',
          controller: 'UsersCtrl as Users',
          authentication: true
        })
        .state('users.bump', {
          url: '/bump',
          templateUrl: 'app/users/users.html',
          controller: 'UsersCtrl as Users',
          authentication: true
        })
        .state('users.business', {
          url: '/business',
          templateUrl: 'app/users/users.html',
          controller: 'UsersCtrl as Users',
          authentication: true
        })
        .state('users.editor', {
          url: '/editor',
          templateUrl: 'app/users/users.html',
          controller: 'UsersCtrl as Users',
          authentication: true
        })
        .state('users.regular', {
          url: '/regular',
          templateUrl: 'app/users/users.html',
          controller: 'UsersCtrl as Users',
          authentication: true
        })
        .state('userdetails', {
          url: '/users/userdetails/:userId',
          templateUrl: 'app/userDetails/user-details.html',
          controller: 'UserDetailsCtrl as UserDetails',
          authentication: true,
          resolve: {
            UserProfileData: ['$q', '$state', '$stateParams', 'UserDetailsApiService', function ($q, $state, $stateParams, UserDetailsApiService) {
              var deferred = $q.defer();
              if (!$stateParams.userId) {
                $state.go('users.regular');
                deferred.reject();
              }
              var _payload = {user_id: $stateParams.userId};
              UserDetailsApiService.getUserProfileData(_payload).$promise.then(function (result) {
                deferred.resolve(result);
              }, function (err) {
                $state.go('users.regular');
                deferred.reject(err);
              });
              return deferred.promise;
            }]
          }
        })
        .state('businessdetails', {
          url: '/users/businessdetails/:userId',
          templateUrl: 'app/userDetails/user-business-details.html',
          controller: 'UserDetailsCtrl as UserDetails',
          authentication: true,
          resolve: {
            UserProfileData: [function () {
              return null;
            }]
          }
        })
        .state('events', {
          url: '/events',
          templateUrl: 'app/events/events.html',
          controller: 'EventsCtrl as Events',
          authentication: true
        })
        .state('eventDetails', {
          url: '/events/details/:eventId',
          templateUrl: 'app/events/eventDetails/event.details.html',
          controller: 'EventDetailsCtrl as EventDetails',
          authentication: true,
          resolve: {
            EventData: ['$q', '$state', '$stateParams', 'EventPlaceDetailsApiService', 'MessageService', 'LoaderService', function ($q, $state, $stateParams, EventPlaceDetailsApiService, MessageService, LoaderService) {
              LoaderService.Loader.showSpinner();
              var deferred = $q.defer();
              if (!$stateParams.eventId) {
                LoaderService.Loader.hideSpinner();
                $state.go('events');
                deferred.reject();
              }
              var _payload = {ep_id: $stateParams.eventId};
              EventPlaceDetailsApiService.getEventsDetails(_payload).$promise.then(function (result) {
                LoaderService.Loader.hideSpinner();
                if (result.status === 'INSUFFICIENT_RIGHTS') {
                  $state.go('events');
                  deferred.reject(result);
                } else {
                  deferred.resolve(result);
                }
              }, function (err) {
                LoaderService.Loader.hideSpinner();
                $state.go('events');
                MessageService.showFailedMessage();
                deferred.reject(err);
              });
              return deferred.promise;
            }]
          }

        })
        .state('addEvents', {
          url: '/events/add',
          templateUrl: 'app/events/addEvents/addEvents.html',
          controller: 'AddEventsCtrl as AddEvents',
          authentication: true,
          resolve: {
            EventData: [function () {
              return false;
            }],
            PlaceData: [function () {
              return false;
            }]
          }
        })
        .state('addEventsFromCurrent', {
          url: '/events/add/:eventId',
          templateUrl: 'app/events/addEvents/addEvents.html',
          controller: 'AddEventsCtrl as AddEvents',
          authentication: true,
          resolve: {
            EventData: ['$q', '$state', '$stateParams', 'EventPlaceDetailsApiService', 'MessageService', 'LoaderService', function ($q, $state, $stateParams, EventPlaceDetailsApiService, MessageService, LoaderService) {
              LoaderService.Loader.showSpinner();
              var deferred = $q.defer();
              if (!$stateParams.eventId) {
                LoaderService.Loader.hideSpinner();
                $state.go('events');
                deferred.reject();
              }
              var _payload = {ep_id: $stateParams.eventId};
              EventPlaceDetailsApiService.getEventsDetails(_payload).$promise.then(function (result) {
                LoaderService.Loader.hideSpinner();
                if (result.status === 'INSUFFICIENT_RIGHTS') {
                  $state.go('events');
                  deferred.reject(result);
                } else {
                  deferred.resolve(result);
                }
              }, function (err) {
                LoaderService.Loader.hideSpinner();
                $state.go('events');
                MessageService.showFailedMessage();
                deferred.reject(err);
              });
              return deferred.promise;
            }],
            PlaceData: [function () {
              return false;
            }]
          }
        })
        .state('places', {
          url: '/places',
          templateUrl: 'app/places/places.html',
          controller: 'PlacesCtrl as Places',
          authentication: true
        })
        .state('addPlaces', {
          url: '/places/add',
          templateUrl: 'app/places/addPlaces/addPlaces.html',
          controller: 'AddPlacesCtrl as AddPlaces',
          authentication: true,
          resolve: {
            PlaceData: [function () {
              return false;
            }]
          }
        })
        .state('addPlacesFromCurrent', {
          url: '/places/add/:placeId',
          templateUrl: 'app/places/addPlaces/addPlaces.html',
          controllerAs: 'AddPlaces',
          controller: 'AddPlacesCtrl',
          authentication: true,
          resolve: {
            PlaceData: ['$q', '$state', '$stateParams', 'EventPlaceDetailsApiService', 'MessageService', 'LoaderService', function ($q, $state, $stateParams, EventPlaceDetailsApiService, MessageService, LoaderService) {
              LoaderService.Loader.showSpinner();
              var deferred = $q.defer();
              if (!$stateParams.placeId) {
                LoaderService.Loader.hideSpinner();
                $state.go('places');
                deferred.reject();
              }
              var _payload = {ep_id: $stateParams.placeId};
              EventPlaceDetailsApiService.getPlacesDetails(_payload).$promise.then(function (result) {
                LoaderService.Loader.hideSpinner();
                if (result.status === 'INSUFFICIENT_RIGHTS') {
                  $state.go('placeDetails', {placeId: $stateParams.placeId});
                  deferred.reject(result);
                } else {
                  deferred.resolve(result);
                }
              }, function (err) {
                LoaderService.Loader.hideSpinner();
                $state.go('placeDetails', {placeId: $stateParams.placeId});
                MessageService.showFailedMessage();
                deferred.reject(err);
              });
              return deferred.promise;
            }]
          }
        })
        .state('addEventsFromPlace', {
          url: '/places/addevent/:placeId',
          templateUrl: 'app/events/addEvents/addEvents.html',
          controller: 'AddEventsCtrl',
          controllerAs: 'AddEvents',
          authentication: true,
          resolve: {
            EventData: [function () {
              return false;
            }],
            PlaceData: ['$q', '$state', '$stateParams', 'EventPlaceDetailsApiService', 'MessageService', 'LoaderService', function ($q, $state, $stateParams, EventPlaceDetailsApiService, MessageService, LoaderService) {
              LoaderService.Loader.showSpinner();
              var deferred = $q.defer();
              if (!$stateParams.placeId) {
                LoaderService.Loader.hideSpinner();
                $state.go('places');
                deferred.reject();
              }
              var _payload = {ep_id: $stateParams.placeId};
              EventPlaceDetailsApiService.getPlacesDetails(_payload).$promise.then(function (result) {
                LoaderService.Loader.hideSpinner();
                if (result.status === 'INSUFFICIENT_RIGHTS') {
                  $state.go('placeDetails', {placeId: $stateParams.placeId});
                  deferred.reject(result);
                } else {
                  deferred.resolve(result);
                }
              }, function (err) {
                LoaderService.Loader.hideSpinner();
                $state.go('placeDetails', {placeId: $stateParams.placeId});
                MessageService.showFailedMessage();
                deferred.reject(err);
              });
              return deferred.promise;
            }]
          }
        })
        .state('placeDetails', {
          url: '/places/details/:placeId',
          templateUrl: 'app/places/placeDetails/place.details.html',
          controller: 'PlaceDetailsCtrl as PlaceDetails',
          authentication: true,
          resolve: {
            PlaceData: ['$q', '$state', '$stateParams', 'EventPlaceDetailsApiService', 'MessageService', 'LoaderService', function ($q, $state, $stateParams, EventPlaceDetailsApiService, MessageService, LoaderService) {
              LoaderService.Loader.showSpinner();
              var deferred = $q.defer();
              if (!$stateParams.placeId) {
                LoaderService.Loader.hideSpinner();
                $state.go('places');
                deferred.reject();
              }
              var _payload = {ep_id: $stateParams.placeId};
              EventPlaceDetailsApiService.getPlacesDetails(_payload).$promise.then(function (result) {
                LoaderService.Loader.hideSpinner();
                if (result.status === 'INSUFFICIENT_RIGHTS') {
                  $state.go('places');
                  deferred.reject(result);
                } else {
                  deferred.resolve(result);
                }
              }, function (err) {
                LoaderService.Loader.hideSpinner();
                $state.go('places');
                MessageService.showFailedMessage();
                deferred.reject(err);
              });
              return deferred.promise;
            }]
          }
        })
      ;
      $urlRouterProvider.otherwise('/');
    }]);
})(angular);