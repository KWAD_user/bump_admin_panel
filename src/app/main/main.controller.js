'use strict';

(function (angular) {
  angular.module('kwadWeb')
    .controller('MainCtrl', ['$rootScope', 'CookieService', function ($rootScope, CookieService) {
      var Main = this;

      Main.init = function () {
        if (CookieService.userSession.get()) {
          $rootScope.isLoggedInUser = true;
        }
      };
    }]);
})(angular);
