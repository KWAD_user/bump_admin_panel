'use strict';

describe('controllers', function () {
  var scope, Main, $rootScope, $controller, $httpBackend, CookieService = {};

  beforeEach(module('kwadWeb'));
  beforeEach(inject(function (_$rootScope_, _$controller_, _$httpBackend_) {
    scope = _$rootScope_.$new();
    $rootScope = _$rootScope_;
    $controller = _$controller_;
    $httpBackend = _$httpBackend_;
    CookieService.userSession = jasmine.createSpyObj('userSession', ['get']);
  }));
  beforeEach(function () {
    Main = $controller('MainCtrl', {
      $scope: scope,
      CookieService: CookieService
    });
  });

  describe('Units Should be defined', function () {
    it('Main should be defined', function () {
      expect(Main).toBeDefined();
    });
    it('$rootScope should be defined', function () {
      expect($rootScope).toBeDefined();
    });
    it('CookieService should be defined', function () {
      expect(CookieService).toBeDefined();
    });
  });

  describe('Unit: Main.init function', function () {
    it('Main.init should be defined and be a function', function () {
      expect(Main.init).toBeDefined();
      expect(typeof Main.init).toEqual('function');
    });

    it('Main.init Should not assign value true to isLoggedInUser when CookieService.userSession.get() return false ', function () {
      $httpBackend.expectGET('app/login/login.html')
        .respond(200);
      CookieService.userSession.get.and.callFake(function () {
        return false;
      });
      Main.init();
      $rootScope.$digest();
      $httpBackend.flush();
      expect($rootScope.isLoggedInUser).not.toBeTruthy();
    });

    it('Main.init Should assign value true to isLoggedInUser when CookieService.userSession.get() return true ', function () {
      $httpBackend.expectGET('app/login/login.html')
        .respond(200);
      CookieService.userSession.get.and.callFake(function () {
        return true;
      });
      Main.init();
      $rootScope.$digest();
      $httpBackend.flush();
      expect($rootScope.isLoggedInUser).toBeTruthy();
    });
  });
});
