'use strict';

/*
 *  userToken - Is use for Manage User session
 * appearance - Is user appearance first time or not ?
 * */
(function (angular) {
  angular.module('kwadWeb')
    .factory('CookieService', ['$cookieStore',
      function ($cookieStore) {
        var cookieName = {
          userToken: 'userToken'
        };

        //User Session Token Manager
        var userSession = {
          isLive: function () {
            var token = $cookieStore.get(cookieName.userToken);
            return token ? true : false;
          },
          create: function (token) {
            $cookieStore.put(cookieName.userToken, token);
          },
          clear: function () {
            $cookieStore.remove(cookieName.userToken);
          },
          get: function () {
            return $cookieStore.get(cookieName.userToken);
          }
        };

        return {
          userSession: userSession
        };
      }]);
})(angular);
