'use strict';

/**
 * @ngdoc function
 * @name kwadWeb.constant:Constant
 * @description
 * # all Kwad global constants
 */
(function (angular) {
  angular.module('kwadWeb')
    .constant('ToastrMessage', {
      LOGIN: {
        'BLANK_EMAIL_FIELD': 'Email should not be blank',
        'ACCOUNT_CLOSED': 'Your account has been Closed.',
        'ACCOUNT_DISABLED': 'Your account has been Disabled.',
        'ACCOUNT_UNVERIFIED': 'Your account is not Verified.',
        'EXCEPTION_OCCURED': 'An Exception has been occurred.',
        'INCORRECT_PASSWORD': 'Your Password is Incorrect.',
        'INVALID_LOGIN': 'Invalid Login.',
        'SUCCESS': '',
        'UNKNOWN_USER': 'You are unknown user.'
      },
      FORGET_PASSWORD: {
        'SUCCESS': 'An Email has sent successfully on your email id.'
      },
      UPLOAD: {
        SUCCESS: 'file uploaded successfully.',
        ERROR: 'Error occurred during uploading file.',
        FILE_NOT_FOUND: 'File name is missing. Try Again',
        TYPE_NOT_FOUND: 'File type is missing. Try Again',
        EP_NOT_FOUND: 'Associated Event or Place is missing. Try Again',
        MAX_FILES: 'Maximum 10 images can be uploaded at once.'
      },
      ASSET: {
        SUCCESS: 'asset fetched successfully.',
        REMOVED_SUCCESSFULLY: 'asset removed successfully.',
        ASSET_ID_NOT_FOUND: 'Asset Id is missing. Try Again'
      },
      USER: {
        UPDATE: {
          SUCCESS: 'User updated successfully.',
          INVALID_ROLE: 'Invalid role provided.',
          INSUFFICIENT_RIGHTS: 'You have insufficient rights to update user details.',
          ERROR: 'Error occurred during updating data'
        },
        ADD: {
          BLANK_ROLE_FIELD: 'Role should not be blank.',
          BLANK_EMAIL_FIELD: 'Email should not be blank',
          BLANK_TITLE: 'Name should not be blank.',
          PASSWORD_REQUIRED: 'Password should not be blank',
          INSUFFICIENT_RIGHTS: 'You don\'t have rights to add a new user.',
          ALREADY_REGISTERED: 'You have already registered.',
          SUCCESS: 'User added successfully.',
          ERROR: 'Something went wrong. Please try again later.'
        }
      },
      DOMAIN: {
        'DOMAIN_ALREADY_EXIST': 'This domain is already exist.',
        'INSUFFICIENT_RIGHTS': 'You have insufficient rights to perform this operation.',
        'SUCCESS': 'Domain updated successfully.',
        'UNIVERSITY_ALREADY_EXISTS': 'This university is already exist.',
        'UNIVERSITY_NOT_FOUND': 'This university is does not exist.'
      },
      PASSWORD: {
        RESET: {
          'SUCCESS': 'New Password has been sent to your registered email id.',
          'INSUFFICIENT_RIGHTS': 'Requesting user is not ADMIN.',
          'FAILURE': 'UserId is incorrect.'
        }
      },
      PLACE: {
        ADD: {
          'SUCCESS': 'Place added successfully.',
          'UNKNOWN_EP': 'An invalid place ID was provided.',
          'BLANK_LOCATION': 'Invalid place address provided.',
          'BLANK_TITLE': 'Place name must not be blank.',
          'INSUFFICIENT_RIGHTS': 'The user does\'t have rights to add this place,',
          'ALREADY_EXISTS': 'Duplicate places are not allowed.'
        },
        UPDATE: {
          'SUCCESS': 'Place updated successfully.',
          'UNKNOWN_EP': 'An invalid Place ID was provided.',
          'INSUFFICIENT_RIGHTS': 'The user does\'t have rights to edit this place,',
          'ALREADY_EXISTS': 'Duplicate places are not allowed.'
        }
      },
      EVENT: {
        ADD: {
          'SUCCESS': 'Event added successfully, Add photos with event.',
          'UNKNOWN_EP': 'An invalid Event ID was provided.',
          'INSUFFICIENT_RIGHTS': 'The user does\'t have rights to add this Event,',
          'ALREADY_EXISTS': 'Duplicate Event are not allowed.'
        },
        UPDATE: {
          'SUCCESS': 'Event updated successfully.',
          'UNKNOWN_EP': 'An invalid Event ID was provided.',
          'INSUFFICIENT_RIGHTS': 'The user does\'t have rights to edit this Event,',
          'ALREADY_EXISTS': 'Duplicate Event are not allowed.'
        }
      },
      DATA_VALIDATE: {
        'EMPTY_UNIVERSITY': 'Please enter a name for your university.',
        'EMPTY_EVENT': 'Please enter a name for your event',
        'EMPTY_PLACE': 'Please select a place from existing one.',
        'EMPTY_USERNAME': 'Please enter a username.',
        'EMPTY_PASSWORD': 'Please enter a password.'
      },
      DATE_VALIDATE: {
        'INVALID_START_DATE': 'Start Date or Time is not in proper format.',
        'INVALID_END_DATE': 'End Date or Time is not in proper format.',
        'INVALID_RECURRENCE_DATE': 'Recurrence Date is not in proper format.',
        'START_DATE_ERROR': 'Start date must be before end date.',
        'RECURRENCE_DATE_ERROR': 'Recurrence end date must be after end date.'
      },
      toastrFailedMessage: 'Something went wrong. Please try again later.'
    });
})(angular);
