'use strict';

describe('Unit: AuthService factory', function () {
  var AuthService,
    UserDetailsApiService,
    $q,
    $rootScope,
    $httpBackend,
    HOST,
    fakeResponse;

  beforeAll(function () {
    fakeResponse = {
      user: {
        id: '54bed6e2e4b026f0ebf5ab6b',
        attributes: [],
        college: '',
        first_name: 'Cherry',
        full_name: 'Cherry Lindz',
        last_name: 'Lindz',
        role: 'REGULAR',
        state: 'ENABLED'
      }
    }
  });
  beforeEach(module('kwadWeb'));
  describe('when UserDetailsApiService resolved user', function () {
    beforeEach(module(function ($provide) {
      $provide.factory('UserDetailsApiService', function () {
        var UserDetailsApiService = jasmine.createSpyObj('UserDetailsApiService', ['getUserProfileData']);
        UserDetailsApiService.getUserProfileData.and.callFake(function (obj) {
          var deferred = $q.defer();
          deferred.resolve(fakeResponse.user);
          deferred.$promise = deferred.promise;
          return deferred;
        });
        return UserDetailsApiService;
      });
    }));
    beforeEach(inject(function (_$rootScope_, _$q_, _$httpBackend_, _AuthService_, _HOST_) {
      $rootScope = _$rootScope_;
      $q = _$q_;
      $httpBackend = _$httpBackend_;
      AuthService = _AuthService_;
      HOST = _HOST_;
      $httpBackend.expectGET('app/login/login.html').respond(200);
      $httpBackend.flush();
    }));
    it('AuthService should be defined', function () {
      expect(AuthService).toBeDefined();
    });
    it('AuthService.getCurrentUser should be resolve current user by calling UserDetailsApiService', function () {
      var currentUser = null,
        success = function (data) {
          currentUser = data;
        }, error = function (err) {
          currentUser = err;
        };
      AuthService.getCurrentUser().then(success, error);
      $rootScope.$digest();
      expect(currentUser).toEqual(fakeResponse.user);
    });
    it('AuthService.getCurrentUser should be return current user without calling UserDetailsApiService', function () {
      var currentUser = null,
        success = function (data) {
          currentUser = data;
        }, error = function (err) {
          currentUser = err;
        };
      AuthService.setCurrentUser(fakeResponse.user);
      $rootScope.$digest();
      AuthService.getCurrentUser().then(success, error);
      $rootScope.$digest();
      expect(currentUser).toEqual(fakeResponse.user);
    });
  });
  describe('when UserDetailsApiService rejected user', function () {
    beforeEach(module(function ($provide) {
      $provide.factory('UserDetailsApiService', function () {
        var UserDetailsApiService = jasmine.createSpyObj('UserDetailsApiService', ['getUserProfileData']);
        UserDetailsApiService.getUserProfileData.and.callFake(function (obj) {
          var deferred = $q.defer();
          deferred.reject({status:'INSUFFICIENT_RIGHTS'});
          deferred.$promise = deferred.promise;
          return deferred;
        });
        return UserDetailsApiService;
      });
    }));
    beforeEach(inject(function (_$rootScope_, _$q_, _$httpBackend_, _AuthService_, _HOST_) {
      $rootScope = _$rootScope_;
      $q = _$q_;
      $httpBackend = _$httpBackend_;
      AuthService = _AuthService_;
      HOST = _HOST_;
      $httpBackend.expectGET('app/login/login.html').respond(200);
      $httpBackend.flush();
    }));
    it('UserDetailsApiService.getUserProfileData should be reject user', function () {
      var response = null,
        success = function (data) {
          response = data;
        }, error = function (err) {
          response = err;
        };
      AuthService.getCurrentUser().then(success, error);
      $rootScope.$digest();
      expect(response).toEqual({status:'INSUFFICIENT_RIGHTS'});
    });
   });
});