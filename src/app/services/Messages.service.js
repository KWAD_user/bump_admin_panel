'use strict';

/*
 *  userToken - Is use for Manage User session
 * appearance - Is user appearance first time or not ?
 * */
(function (angular, toastr) {
  angular.module('kwadWeb')
    .factory('MessageService', ['ToastrMessage',
      function (ToastrMessage) {
       var MessageService = {};

        MessageService = {
          /*
           * show toastr successful message
           */
          showSuccessfulMessage: function(messages) {
            toastr.options = {
              'closeButton': true,
              'debug': false,
              'newestOnTop': false,
              'progressBar': true,
              'iconClasses': {
                success: 'toast-success'
              },
              'positionClass': 'toast-top-right',
              'preventDuplicates': true,
              'onclick': null,
              'showDuration': '300',
              'hideDuration': '1000',
              'extendedTimeOut': '1000',
              'showEasing': 'swing',
              'hideEasing': 'linear',
              'showMethod': 'fadeIn',
              'hideMethod': 'fadeOut'
            };
            toastr.success(messages);
          },
          /*
           * show toastr failed message
           */
          showFailedMessage: function(messages) {
            toastr.options = {
              'closeButton': true,
              'debug': false,
              'newestOnTop': false,
              'progressBar': true,
              'positionClass': 'toast-top-right',
              'preventDuplicates': true,
              'onclick': null,
              'showDuration': '300',
              'hideDuration': '1000',
              'extendedTimeOut': '1000',
              'showEasing': 'swing',
              'hideEasing': 'linear',
              'showMethod': 'fadeIn',
              'hideMethod': 'fadeOut',
              iconClasses: {
                error: 'toast-error'
              }
            };
            if (!messages) {
              messages = ToastrMessage.toastrFailedMessage;
            }
            toastr.error(messages);
          }
        };

        return MessageService ;
      }]);
})(angular, toastr);
