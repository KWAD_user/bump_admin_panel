'use strict';

describe('Unit : Users.api.service.js ', function () {
  var UsersApiService, AddEditUserAPIService, $rootScope, $q, $httpBackend, HOST, ToastrMessage, fakeResponse;

  beforeAll(function () {
    fakeResponse = {
      fakeAdminUsers: [{
        id: '54bed6e2e4b026f0ebf5ab6b',
        attributes: [],
        college: '',
        first_name: 'Cherry',
        full_name: 'Cherry Lindz',
        last_name: 'Lindz',
        role: 'REGULAR',
        state: 'ENABLED'
      }],
      fakeUpdatedAdminUser: {
        id: '54bed6e2e4b026f0ebf5ab6b',
        attributes: [],
        college: '',
        first_name: 'Cherry',
        full_name: 'Cherry ADMIN',
        last_name: 'ADMIN',
        role: 'ADMIN',
        state: 'ENABLED'
      }
    }
  });
  beforeEach(module('kwadWeb'));
  beforeEach(inject(function (_UsersApiService_, _AddEditUserAPIService_, _$rootScope_, _$q_, _$httpBackend_, _HOST_, _ToastrMessage_) {
    $rootScope = _$rootScope_;
    $q = _$q_;
    $httpBackend = _$httpBackend_;
    UsersApiService = _UsersApiService_;
    AddEditUserAPIService = _AddEditUserAPIService_;
    HOST = _HOST_;
    ToastrMessage = _ToastrMessage_;
  }));
  beforeEach(function () {
    $httpBackend.expectGET('app/login/login.html').respond(200);
    $httpBackend.flush();
  });

  describe('Unit : UsersApiService Factory', function () {
    it('UsersApiService should be defined', function () {
      expect(UsersApiService).toBeDefined();
    });
    describe('Unit : UsersApiService.getUsers', function () {
      it('Should exist and be a function', function () {
        expect(UsersApiService.getUsers).toBeDefined();
        expect(typeof UsersApiService.getUsers).toEqual('function');
      });
      it('Should return success', function () {
        $httpBackend.expectPOST(HOST.HOST_URL + '/api/ext/s/usersearch').respond({
          data: fakeResponse.fakeAdminUsers,
          'status': 'SUCCESS'
        });
        var result = '',
          success = function (response) {
            result = response.data;
          },
          error = function (err) {
            result = err;
          };
        var _payload = {role: 'ADMIN', query: ''};
        UsersApiService.getUsers(_payload).$promise.then(success, error);
        $httpBackend.flush();
        $rootScope.$digest();
        expect(result.length).toBeGreaterThan(0);
      });
      it('Should return error', function () {
        $httpBackend.expectPOST(HOST.HOST_URL + '/api/ext/s/usersearch').respond(500, {errors: ['Internal server error']});
        var result = '',
          success = function (response) {
            result = response;
          },
          error = function (err) {
            result = err.data.errors[0];
          };
        var _payload = {role: 'ADMIN', query: ''};
        UsersApiService.getUsers(_payload).$promise.then(success, error);
        $httpBackend.flush();
        $rootScope.$digest();
        expect(result).toEqual('Internal server error');
      });
    });
  });

  describe('Unit : AddEditUserAPIService Factory', function () {
    it('AddEditUserAPIService should be defined', function () {
      expect(AddEditUserAPIService).toBeDefined();
    });
    describe('Unit : AddEditUserAPIService.addEditUser', function () {
      it('Should exist and be a function', function () {
        expect(AddEditUserAPIService.addEditUser).toBeDefined();
        expect(typeof AddEditUserAPIService.addEditUser).toEqual('function');
      });
      it('Should return success', function () {
        $httpBackend.expectPOST(HOST.HOST_URL + '/api/ext/auth/user').respond({
          data: fakeResponse.fakeUpdatedAdminUser,
          'status': 'SUCCESS'
        });
        var result = '',
          success = function (response) {
            result = response.data;
          },
          error = function (err) {
            result = err;
          };
        var _payload = {
          userId: '54bed6e2e4b026f0ebf5ab6b',
          email: 'cherry_lindz@gmail.com',
          role: 'REGULAR',
          state: 'ENABLED',
          firstName: 'Cherry',
          lastName: 'ADMIN',
          college: ''
        };
        AddEditUserAPIService.addEditUser(_payload).$promise.then(success, error);
        $httpBackend.flush();
        $rootScope.$digest();
        expect(result.role).not.toBe('REGULAR');
      });
      it('Should return error', function () {
        $httpBackend.expectPOST(HOST.HOST_URL + '/api/ext/auth/user').respond(500, {errors: ['Internal server error']});
        var result = '',
          success = function (response) {
            result = response;
          },
          error = function (err) {
            result = err.data.errors[0];
          };
        var _payload = {
          userId: '54bed6e2e4b026f0ebf5ab6b',
          email: 'cherry_lindz@gmail.com',
          role: 'REGULAR',
          state: 'ENABLED',
          firstName: 'Cherry',
          lastName: 'ADMIN',
          college: ''
        };
        AddEditUserAPIService.addEditUser(_payload).$promise.then(success, error);
        $httpBackend.flush();
        $rootScope.$digest();
        expect(result).toEqual('Internal server error');
      });
    });
  });
});