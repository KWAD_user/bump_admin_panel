'use strict';

describe('Unit : Logout.api.service.js ', function () {
  var LogoutApiService, $rootScope, $q, $httpBackend, HOST, ToastrMessage;

  beforeEach(module('kwadWeb'));
  beforeEach(inject(function (_LogoutApiService_, _$rootScope_, _$q_, _$httpBackend_, _HOST_, _ToastrMessage_) {
    $rootScope = _$rootScope_;
    $q = _$q_;
    $httpBackend = _$httpBackend_;
    LogoutApiService = _LogoutApiService_;
    HOST = _HOST_;
    ToastrMessage = _ToastrMessage_;
  }));
  beforeEach(function () {
    $httpBackend.expectGET('app/login/login.html').respond(200);
    $httpBackend.flush();
  });

  describe('Unit : LogoutApiService Factory', function () {
    it('LogoutApiService should be defined', function () {
      expect(LogoutApiService).toBeDefined();
    });
    describe('Unit : LogoutApiService.logout Success', function () {
      it('Should exist and be a function', function () {
        expect(LogoutApiService.logout).toBeDefined();
        expect(typeof LogoutApiService.logout).toEqual('function');
      });
      it('Should return success', function () {
        $httpBackend.expectPOST(HOST.URL + '/auth/logout').respond({
          status: 'SUCCESS'
        });
        var result = '',
          success = function (response) {
            result = response.status;
          },
          error = function (err) {
            result = err;
          };
        LogoutApiService.logout({}).$promise.then(success, error);
        $httpBackend.flush();
        $rootScope.$digest();
        expect(result).toEqual('SUCCESS');
      });
      it('Should return error', function () {
        $httpBackend.expectPOST(HOST.URL + '/auth/logout').respond(401, {errors: ['Credentials are required to access this resource']});
        var result = '',
          success = function (response) {
            result = response;
          },
          error = function (err) {
            result = err.data.errors[0];
          };
        LogoutApiService.logout({}).$promise.then(success, error);
        $httpBackend.flush();
        $rootScope.$digest();
        expect(result).toEqual('Credentials are required to access this resource');
      });
    });
  });
});