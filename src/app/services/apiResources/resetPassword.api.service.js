'use strict';

(function (angular) {
  angular.module('kwadWeb')
    .factory('ResetPasswordApiService', ['$resource', 'HOST',
      function ($resource, HOST) {
        return $resource(HOST.HOST_URL + '/api/ext/auth/resetpass', {}, {
          resetPassword: {method: 'POST'}
        });
      }]);
})(angular);