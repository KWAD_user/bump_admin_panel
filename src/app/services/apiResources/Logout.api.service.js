'use strict';

(function (angular) {
  angular.module('kwadWeb')
    .factory('LogoutApiService', ['$resource', 'HOST',
      function ($resource, HOST) {
        return $resource(HOST.URL + '/auth/logout', {}, {
          logout: {method: 'POST'}
        });
      }]);
})(angular);
