'use strict';

(function (angular) {
  angular.module('kwadWeb')
    .factory('EnumsApiService', ['$resource', 'HOST',
      function ($resource, HOST) {
        return $resource(HOST.HOST_URL + '/api/ext/fetchenum', {}, {
          getEnums: {method: 'POST'}
        });
      }]);
})(angular);
