'use strict';

describe('Unit : Domains.api.service.js ', function () {
  var DomainAddEditApiService, DomainApiService, $rootScope, $q, $httpBackend, HOST, ToastrMessage, fakeResponse;
  beforeAll(function () {
    fakeResponse = {
      fakeDomains: [{
        'name': 'Albion College',
        'universityId': '54c3d0a8e4b0d6dec07e5398',
        'domains': [{'name': 'albion.edu', 'status': 'INACTIVE'}]
      }],
      fakeDomain: {
        status: 'SUCCESS',
        data: {
          'name': 'Albion College',
          'universityId': '54c3d0a8e4b0d6dec07e5398',
          'domains': [{'name': 'albion.edu', 'status': 'INACTIVE'}]
        }
      }
    }
  });
  beforeEach(module('kwadWeb'));
  beforeEach(inject(function (_DomainAddEditApiService_, _DomainApiService_, _$rootScope_, _$q_, _$httpBackend_, _HOST_, _ToastrMessage_) {
    $rootScope = _$rootScope_;
    $q = _$q_;
    $httpBackend = _$httpBackend_;
    DomainAddEditApiService = _DomainAddEditApiService_;
    DomainApiService = _DomainApiService_;
    HOST = _HOST_;
    ToastrMessage = _ToastrMessage_;
  }));
  beforeEach(function () {
    $httpBackend.expectGET('app/login/login.html').respond(200);
    $httpBackend.flush();
  });

  describe('Unit : DomainApiService Factory', function () {
    it('DomainApiService should be defined', function () {
      expect(DomainApiService).toBeDefined();
    });
    describe('Unit : DomainApiService.getDomains', function () {
      it('Should exist and be a function', function () {
        expect(DomainApiService.getDomains).toBeDefined();
        expect(typeof DomainApiService.getDomains).toEqual('function');
      });
      it('Should return success', function () {
        $httpBackend.expectPOST(HOST.HOST_URL + '/api/ext/s/domainsearch').respond(fakeResponse.fakeDomains);
        var result = '',
          success = function (response) {
            result = response;
          },
          error = function (err) {
            result = err;
          };
        DomainApiService.getDomains().$promise.then(success, error);
        $httpBackend.flush();
        $rootScope.$digest();
        expect(result.length).toBeGreaterThan(0);
      });
      it('Should return error', function () {
        $httpBackend.expectPOST(HOST.HOST_URL + '/api/ext/s/domainsearch').respond(500, {errors: ['Internal server error']});
        var result = '',
          success = function (response) {
            result = response;
          },
          error = function (err) {
            result = err.data.errors[0];
          };
        DomainApiService.getDomains().$promise.then(success, error);
        $httpBackend.flush();
        $rootScope.$digest();
        expect(result).toEqual('Internal server error');
      });
    });
  });

  describe('Unit : DomainAddEditApiService Factory', function () {
    it('DomainAddEditApiService should be defined', function () {
      expect(DomainAddEditApiService).toBeDefined();
    });
    describe('Unit : DomainAddEditApiService.addEditDomain', function () {
      it('Should exist and be a function', function () {
        expect(DomainAddEditApiService.addEditDomain).toBeDefined();
        expect(typeof DomainAddEditApiService.addEditDomain).toEqual('function');
      });
      it('Should return success', function () {
        $httpBackend.expectPOST(HOST.HOST_URL + '/api/ext/ics/university').respond(fakeResponse.fakeDomain);
        var result = '',
          success = function (response) {
            result = response.data;
          },
          error = function (err) {
            result = err;
          };
        DomainAddEditApiService.addEditDomain().$promise.then(success, error);
        $httpBackend.flush();
        $rootScope.$digest();
        expect(result).toEqual(fakeResponse.fakeDomain.data);
      });
      it('Should return error', function () {
        $httpBackend.expectPOST(HOST.HOST_URL + '/api/ext/ics/university').respond(500, {errors: ['Internal server error']});
        var result = '',
          success = function (response) {
            result = response;
          },
          error = function (err) {
            result = err.data.errors[0];
          };
        DomainAddEditApiService.addEditDomain().$promise.then(success, error);
        $httpBackend.flush();
        $rootScope.$digest();
        expect(result).toEqual('Internal server error');
      });
    });
  });
});