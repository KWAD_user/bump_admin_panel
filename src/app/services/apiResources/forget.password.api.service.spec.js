'use strict';

describe('Unit : forget.password.api.service.js ', function () {
  var ForgetPasswordApiService, $rootScope, $q, $httpBackend, HOST, ToastrMessage;

  beforeEach(module('kwadWeb'));
  beforeEach(inject(function (_ForgetPasswordApiService_, _$rootScope_, _$q_, _$httpBackend_, _HOST_, _ToastrMessage_) {
    $rootScope = _$rootScope_;
    $q = _$q_;
    $httpBackend = _$httpBackend_;
    ForgetPasswordApiService = _ForgetPasswordApiService_;
    HOST = _HOST_;
    ToastrMessage = _ToastrMessage_;
  }));
  beforeEach(function () {
    $httpBackend.expectGET('app/login/login.html').respond(200);
    $httpBackend.flush();
  });

  describe('Unit : ForgetPasswordApiService Factory', function () {
    it('ForgetPasswordApiService should be defined', function () {
      expect(ForgetPasswordApiService).toBeDefined();
    });
    describe('Unit : ForgetPasswordApiService.forgetPassword Success', function () {
      it('Should exist and be a function', function () {
        expect(ForgetPasswordApiService.forgetPassword).toBeDefined();
        expect(typeof ForgetPasswordApiService.forgetPassword).toEqual('function');
      });
      it('Should return success', function () {
        $httpBackend.expectPOST(HOST.URL + '/auth/forgotpassword').respond({'status': 'SUCCESS'});
        var result = '',
          success = function (response) {
            result = response.status;
          },
          error = function (err) {
            result = err;
          };
        var _payload = {email: 'sandeep.kumar1@tothenew.com'};
        ForgetPasswordApiService.forgetPassword(_payload).$promise.then(success, error);
        $httpBackend.flush();
        $rootScope.$digest();
        expect(result).toEqual('SUCCESS');
      });
      it('Should return error', function () {
        $httpBackend.expectPOST(HOST.URL + '/auth/forgotpassword').respond(500, {errors: ['Internal server error']});
        var result = '',
          success = function (response) {
            result = response;
          },
          error = function (err) {
            result = err.data.errors[0];
          };
        var _payload = {email: 'sandeep.kumar1@tothenew.com'};
        ForgetPasswordApiService.forgetPassword(_payload).$promise.then(success, error);
        $httpBackend.flush();
        $rootScope.$digest();
        expect(result).toEqual('Internal server error');
      });
    });
  });
});