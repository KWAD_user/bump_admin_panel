'use strict';

describe('Unit : upload.api.service.js ', function () {
  var AddAssetsService, FetchAssetsService, DeleteAssetsService, $rootScope, $q, $httpBackend, HOST, fakeResponse;

  beforeAll(function () {
    fakeResponse = {
      fakeFetchedAsset: {
        'aws_key': 'ASIAJI6VWSFKI7F2MOGA',
        'aws_secret': 'JoCzgnWIP0/r7FSVQKkJKLCf9YRI/OfAQ5l7GWCl',
        'aws_token': 'AQoDYXdzEP7//////////wEaoAPQke+NYBcofGYrUWPK4NoXxtS24JGi6bCQY5S9S1yeohMusn9q8pJwAxw5BORiuyFJbG6+G/S592QDGoMczFr7hU+ozwpNYdBJ6yHO+IZc3pDcU7U1WVaD/oINCIqSesovFtnyRKOd+rSvYo/3uOWjf4XKFAqAiAiAjAg1tY+CEKe6e6Eg/JWzFJTC3fjtGIwK/kzLDVnaYmaHtwYm9+Cl30iFv0cqzQhWS1JojERQNLGthIor2ji8EYJvY7VxcYawFUxhIba5z0JQHcBD+P8z+4i1vPezF1g1eWOb0azsyULzFz1OF0kfdBhVZmQWq/JLyCQ/cfS1f7OkDrf3M+ZmZUEkwXj8rhyYDod86auhYdId5LYUSqLx7+U+iM+bh7QBEKzW7A4JxhUMhvE9q6XiU9Ni0hPEV8/Ma5PC0AdS46TsTkPE6jOXC7tLgGnMb24CN869AFDLYUJhQLJcE9KNEKXQCfV5yhQnpp4SCHX3aC7CrqZ5VousrrRErkHWKyyU5WasWhqpuRqc1dF8cIUwBFHXXEhEiAnD5ZFvr7CAvCDDio2iBQ==',
        's3_bucket': 'kwad-bump-assets',
        'prefixes': ['/54430675654b8d36b76a9ec0/544327ae654b29e81a0ec8e2']
      },
      fakeAddedAsset: {
        asset_id: '5672a991e4b02b658c9278dd',
        aws_key: 'ASIAJ64GQOFRALJOBWQA',
        aws_secret: 'fqN3PS7ao5e2aj38g3CYhVTXCn/R7VLXwTtXCthD',
        aws_token: 'AQoDYXdzEHYakAO+mVAbXL6gqWKmhtyx4IBROjvkzp3Zv2l1BcCVMI5BJ3ONeWhCzcaFp4gHv+72s4/UHUnMZjunMCvACoy/UZt90RnFdAUr6ly2UZofxNLOxcZLHb7kXikIsFocOAF72/KBRfEv+ntNuhaF0yJPVb4nWV1kdOJ0+7ZCLm1Ulz6AvdXrZIenhNxMl4jMRn5MctzXyq1mFNHlM1nyqsOrIiYstIiDv/Mdy4mKTsgqUBIxe2dKKXxvfiNx60FZfUNYr2M70j97+6TbJ9sfHD4Xh9vY9OiMZ+3zDpeX1NdL2BUjHHsyJlSnQVN2Q7U7DFatFOUqDnKOeChHybj8tRe4l2/po8mdGO4b3kQQvJ75VX2pPakIQ2Khn0x4tPRDBxsOzL9vlFwrzX1ehb5XAI4lWKnhaOXLvuFAAYkqzs2evFYShjnyVUnV/sr7cQ3w/nNrjbb2HZVueJzMzuwjnj06+h0QLthi8b/uNyat0A3Xl9IQEEEI/16dy5zNV+dZtDOD+w5Ynjt20Eu81GodKsAq0MxcIJHTyrMF',
        s3_bucket: 'kwad-bump-assets',
        s3_object: '/5592497703cef8958d68d1a5/5672a991e4b02b658c9278dd/raw_upload.jpeg'
      }
    }
  });
  beforeEach(module('kwadWeb'));
  beforeEach(inject(function (_AddAssetsService_, _FetchAssetsService_, _DeleteAssetsService_, _$rootScope_, _$q_, _$httpBackend_, _HOST_) {
    $rootScope = _$rootScope_;
    $q = _$q_;
    $httpBackend = _$httpBackend_;
    AddAssetsService = _AddAssetsService_;
    FetchAssetsService = _FetchAssetsService_;
    DeleteAssetsService = _DeleteAssetsService_;
    HOST = _HOST_;
  }));
  beforeEach(function () {
    $httpBackend.expectGET('app/login/login.html').respond(200);
    $httpBackend.flush();
  });

  describe('Unit : AddAssetsService Factory', function () {
    it('AddAssetsService should be defined', function () {
      expect(AddAssetsService).toBeDefined();
    });
    describe('Unit : AddAssetsService.addAsset', function () {
      it('Should exist and be a function', function () {
        expect(AddAssetsService.addAsset).toBeDefined();
        expect(typeof AddAssetsService.addAsset).toEqual('function');
      });
      it('Should return success', function () {
        $httpBackend.expectPOST(HOST.HOST_URL + '/api/ext/assets/add').respond({
          data: [fakeResponse.fakeAddedAsset],
          'status': 'SUCCESS'
        });
        var result = '',
          success = function (response) {
            result = response.data;
          },
          error = function (err) {
            result = err;
          };
        var _payload = [{
          file: 'sample.jpeg',
          type: 'image/jpeg',
          ep_id: '546aff5be4b0370c32328fbb'
        }];
        AddAssetsService.addAsset(_payload).$promise.then(success, error);
        $httpBackend.flush();
        $rootScope.$digest();
        expect(result.length).toBeGreaterThan(0);
      });
      it('Should return error', function () {
        $httpBackend.expectPOST(HOST.HOST_URL + '/api/ext/assets/add').respond(500, {errors: ['Internal server error']});
        var result = '',
          success = function (response) {
            result = response;
          },
          error = function (err) {
            result = err.data.errors[0];
          };
        var _payload = [{
          file: 'sample.jpeg',
          type: 'image/jpeg',
          ep_id: '546aff5be4b0370c32328fbb'
        }];
        AddAssetsService.addAsset(_payload).$promise.then(success, error);
        $httpBackend.flush();
        $rootScope.$digest();
        expect(result).toEqual('Internal server error');
      });
    });
  });

  describe('Unit : DeleteAssetsService Factory', function () {
    it('DeleteAssetsService should be defined', function () {
      expect(DeleteAssetsService).toBeDefined();
    });
    describe('Unit : DeleteAssetsService.removeAsset', function () {
      it('Should exist and be a function', function () {
        expect(DeleteAssetsService.removeAsset).toBeDefined();
        expect(typeof DeleteAssetsService.removeAsset).toEqual('function');
      });
      it('Should return success', function () {
        $httpBackend.expectPOST(HOST.URL + '/assets/delete').respond({'asset_ids': ['54b7b514e4b023bcb073c664']});
        var result = '',
          success = function (response) {
            result = response.asset_ids;
          },
          error = function (err) {
            result = err;
          };
        var _payload = [{'asset_id': '544327ae654b29e81a0ec8e2'}];
        DeleteAssetsService.removeAsset(_payload).$promise.then(success, error);
        $httpBackend.flush();
        $rootScope.$digest();
        expect(result.length).toBeGreaterThan(0);
      });
      it('Should return error', function () {
        $httpBackend.expectPOST(HOST.URL + '/assets/delete').respond(500, {errors: ['Internal server error']});
        var result = '',
          success = function (response) {
            result = response;
          },
          error = function (err) {
            result = err.data.errors[0];
          };
        var _payload = [{'asset_id': '544327ae654b29e81a0ec8e2'}];
        DeleteAssetsService.removeAsset(_payload).$promise.then(success, error);
        $httpBackend.flush();
        $rootScope.$digest();
        expect(result).toEqual('Internal server error');
      });
    });
  });

  describe('Unit : FetchAssetsService Factory', function () {
    it('FetchAssetsService should be defined', function () {
      expect(FetchAssetsService).toBeDefined();
    });
    describe('Unit : FetchAssetsService.fetchAsset', function () {
      it('Should exist and be a function', function () {
        expect(FetchAssetsService.fetchAsset).toBeDefined();
        expect(typeof FetchAssetsService.fetchAsset).toEqual('function');
      });
      it('Should return success', function () {
        $httpBackend.expectPOST(HOST.URL + '/assets/fetch').respond(fakeResponse.fakeFetchedAsset);
        var result = '',
          success = function (response) {
            result = response;
          },
          error = function (err) {
            result = err;
          };
        var _payload = [{'asset_id': '544327ae654b29e81a0ec8e2'}];
        FetchAssetsService.fetchAsset(_payload).$promise.then(success, error);
        $httpBackend.flush();
        $rootScope.$digest();
        expect(result.prefixes.length).toBeGreaterThan(0);
      });
      it('Should return error', function () {
        $httpBackend.expectPOST(HOST.URL + '/assets/fetch').respond(500, {errors: ['Internal server error']});
        var result = '',
          success = function (response) {
            result = response;
          },
          error = function (err) {
            result = err.data.errors[0];
          };
        var _payload = [{'asset_id': '544327ae654b29e81a0ec8e2'}];
        FetchAssetsService.fetchAsset(_payload).$promise.then(success, error);
        $httpBackend.flush();
        $rootScope.$digest();
        expect(result).toEqual('Internal server error');
      });
    });
  });
});
