'use strict';

(function (angular) {
  angular.module('kwadWeb')
    .factory('EventPlaceApiService', ['$resource', 'HOST',
      function ($resource, HOST) {
        return $resource(HOST.HOST_URL + '/api/ext/s/search', {}, {
          getEventsList: {method: 'POST', isArray: false},
          getPlacesList: {method: 'POST', isArray: false}
        });
      }])
    .factory('EventPlaceDetailsApiService', ['$resource', 'HOST',
      function ($resource, HOST) {
        return $resource(HOST.URL + '/ext/ep/details', {}, {
          getEventsDetails: {method: 'POST'},
          getPlacesDetails: {method: 'POST'}
        });
      }])
    .factory('EventAddEditApiService', ['$resource', 'HOST',
      function ($resource, HOST) {
        return $resource(HOST.HOST_URL + '/api/ext/ep/event', {}, {
          addEditEvent: {method: 'POST'}
        });
      }])
    .factory('PlaceAddEditApiService', ['$resource', 'HOST',
      function ($resource, HOST) {
        return $resource(HOST.HOST_URL + '/api/ext/ep/place', {}, {
          addEditPlace: {method: 'POST'}
        });
      }]);
})(angular);
