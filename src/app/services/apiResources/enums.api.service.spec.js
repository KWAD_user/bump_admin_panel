'use strict';

describe('Unit : Enums.api.service.js ', function () {
  var EnumsApiService, $rootScope, $q, $httpBackend, HOST, ToastrMessage, fakeResponse;
  beforeAll(function () {
    fakeResponse = {
      fakeUserRoles: ['ANONYMOUS', 'REGULAR', 'LAUNCHSQUAD', 'ADMIN', 'BUSINESS', 'BUSINESS_INDIVIDUAL', 'EDITOR']
    }
  });
  beforeEach(module('kwadWeb'));
  beforeEach(inject(function (_EnumsApiService_, _$rootScope_, _$q_, _$httpBackend_, _HOST_, _ToastrMessage_) {
    $rootScope = _$rootScope_;
    $q = _$q_;
    $httpBackend = _$httpBackend_;
    EnumsApiService = _EnumsApiService_;
    HOST = _HOST_;
    ToastrMessage = _ToastrMessage_;
  }));
  beforeEach(function () {
    $httpBackend.expectGET('app/login/login.html').respond(200);
    $httpBackend.flush();
  });

  describe('Unit : EnumsApiService Factory', function () {
    it('EnumsApiService should be defined', function () {
      expect(EnumsApiService).toBeDefined();
    });
    describe('Unit : EnumsApiService.getEnums', function () {
      it('Should exist and be a function', function () {
        expect(EnumsApiService.getEnums).toBeDefined();
        expect(typeof EnumsApiService.getEnums).toEqual('function');
      });
      it('Should return success', function () {
        $httpBackend.expectPOST(HOST.HOST_URL + '/api/ext/fetchenum').respond({valuesList: fakeResponse.fakeUserRoles});
        var result = '',
          success = function (response) {
            result = response.valuesList;
          },
          error = function (err) {
            result = err;
          };
        var _payload = {name: 'USER_ROLE'};
        EnumsApiService.getEnums(_payload).$promise.then(success, error);
        $httpBackend.flush();
        $rootScope.$digest();
        expect(result.length).toBeGreaterThan(0);
      });
      it('Should return error', function () {
        $httpBackend.expectPOST(HOST.HOST_URL + '/api/ext/fetchenum').respond(500, {errors: ['Internal server error']});
        var result = '',
          success = function (response) {
            result = response;
          },
          error = function (err) {
            result = err.data.errors[0];
          };
        var _payload = {name: 'USER_ROLE'};
        EnumsApiService.getEnums(_payload).$promise.then(success, error);
        $httpBackend.flush();
        $rootScope.$digest();
        expect(result).toEqual('Internal server error');
      });
    });
  });
});
