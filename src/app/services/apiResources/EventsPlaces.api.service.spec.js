'use strict';

describe('Unit : EventsPlaces.api.service.js', function () {
  var EventPlaceApiService, EventPlaceDetailsApiService, EventAddEditApiService, PlaceAddEditApiService, $rootScope, $q, $httpBackend, HOST, ToastrMessage, fakeResponse;
  beforeAll(function () {
    fakeResponse = {
      fakePlaceDetails: {
        'ep_id': '550f987fe4b09642124625a5',
        'cat_id': '54b83a2be4b06b211e892745',
        'type': 'PLACE',
        'title': '1865 Coffee',
        'description': 'xsxsxs',
        'phone': '4809678649',
        'website': '',
        'location': {'type': 'Point', 'coordinates': [33.42195744640333, -111.9392205029726]},
        'banner_asset': '5655d3dce4b0da91fa10f50b',
        'affiliation': 'NONE',
        'creator_id': '54e13604e4b072bb08b77438',
        'creator_name': 'Taylor Transtrum',
        'address': 'Arizona State University, Tempe Campus, 32-42 E University Dr, Tempe, AZ 85281',
        'creator_pic_asset': '54e13605e4b072bb08b7743c',
        'attending': 0,
        'positive_ratings': 0,
        'negative_ratings': 0,
        'ongoing': 0,
        'connections_data': [],
        'photos': ['5655d3dce4b0da91fa10f50b'],
        'bump_type': 'NON_BUMP',
        'positive_rating_count': 0,
        'negative_rating_count': 0,
        'invite_status': 'NONE',
        'event_timeline': 'UNDETERMINED',
        'place_schedule': {'periods': [], 'alwaysOpened': true},
        'private': false,
        'disabled': false
      },
      fakeEventDetails: {
        'ep_id': '55e78700e4b016c1bcd784ab',
        'cat_id': '54b83a2be4b06b211e89274e',
        'type': 'EVENT',
        'title': 'All White Affair',
        'description': 'One of the greatest 18+ parties of the year! Located at School of Rock on Mill. Guaranteed good time. updated',
        'p_id': '54f7d82ce4b0964212462037',
        'place_name': 'University of Washington',
        'location': {'type': 'Point', 'coordinates': [47.65394169259868, -122.3078935593367]},
        'banner_asset': '56559821e4b0da91f9e72234',
        'start_time': '2015-09-04T05:00:20.000+0000',
        'end_time': '2015-09-04T09:00:00.000+0000',
        'affiliation': 'NONE',
        'creator_id': '54df925ae4b072bb08b7733e',
        'creator_name': 'Elizabeth Cheney',
        'address': 'University of Washington, 1707 NE Grant Ln, Seattle, WA 98195',
        'creator_pic_asset': '5605b90ce4b0a9be00a15632',
        'attending': 2,
        'positive_ratings': 0,
        'negative_ratings': 0,
        'ongoing': -1,
        'connections_data': [{
          'id': '55e8f457e4b016c1bcda3de8',
          'first_name': 'Max',
          'last_name': 'Mao',
          'college': 'Macalester College',
          'year': 2019,
          'role': 'REGULAR',
          'email': 'mmao@macalester.edu',
          'friend': false,
          'state': 'ENABLED',
          'registration_date': '2015-09-04T01:34:04.488+0000',
          'full_name': 'Max Mao'
        }, {
          'id': '54df925ae4b072bb08b7733e',
          'first_name': 'Elizabeth',
          'last_name': 'Cheney',
          'college': 'Arizona State University',
          'year': 2015,
          'photo_asset': '5605b90ce4b0a9be00a15632',
          'role': 'LAUNCHSQUAD',
          'email': 'echeney1@asu.edu',
          'friend': false,
          'state': 'ENABLED',
          'registration_date': '2015-07-09T10:25:15.277+0000',
          'full_name': 'Elizabeth Cheney'
        }],
        'photos': ['56559821e4b0da91f9e72234', '56559819e4b0da91f9e71b29', '55e78700e4b016c1bcd784ae'],
        'bump_type': 'NON_BUMP',
        'keywords': ['sxsxsx'],
        'positive_rating_count': 0,
        'negative_rating_count': 0,
        'invite_status': 'NONE',
        'event_timeline': 'PAST',
        'recurrence_type': 'NEVER',
        'private': false,
        'disabled': false
      },
      fakePlaceList: [{
        'epId': '546aff5be4b0370c32328fbb',
        'name': 'Phoenix Rock Gym',
        'place': '1353 E University Drive, Tempe AZ',
        'creatorName': 'Cherry Lindz',
        'creatorId': '54bed6e2e4b026f0ebf5ab6b',
        'feedImpression': 0,
        'detailImpression': 0,
        'appCheckinCount': 26,
        'geoCheckinCount': 8,
        'shares': 26,
        'status': 'ENABLED',
        'associatedEvents': 2,
        'creatorRole': 'REGULAR',
        'eventDate': null,
        'eventTimeLine': 'UNDETERMINED'
      }, {
        'epId': '546aff5be4b0370c32328fbf',
        'name': 'The Vue on Apache',
        'place': '922 E Apache Blvd, Tempe AZ',
        'creatorName': 'Cherry Lindz',
        'creatorId': '54bed6e2e4b026f0ebf5ab6b',
        'feedImpression': 6,
        'detailImpression': 0,
        'appCheckinCount': 24,
        'geoCheckinCount': 283,
        'shares': 24,
        'status': 'ENABLED',
        'associatedEvents': 0,
        'creatorRole': 'REGULAR',
        'eventDate': null,
        'eventTimeLine': 'UNDETERMINED'
      }],
      fakeEventList: [{
        'epId': '5594cf91e4b044fb28d25c1a',
        'name': 'New Songs',
        'place': '1353 E University Drive, Tempe AZ',
        'creatorName': 'Cherry Lindz',
        'creatorId': '54bed6e2e4b026f0ebf5ab6b',
        'feedImpression': 0,
        'detailImpression': 0,
        'appCheckinCount': 1,
        'geoCheckinCount': 0,
        'shares': 1,
        'status': 'ENABLED',
        'associatedEvents': 0,
        'creatorRole': 'REGULAR',
        'eventDate': '2015-07-02T18:42:00.637+0000',
        'eventTimeLine': 'PAST'
      }, {
        'epId': '55fa4d3ee4b04e131361c62b',
        'name': 'Rock Climbing Student Day',
        'place': '1353 E University Drive, Tempe AZ',
        'creatorName': 'Cherry Lindz',
        'creatorId': '54bed6e2e4b026f0ebf5ab6b',
        'feedImpression': 0,
        'detailImpression': 0,
        'appCheckinCount': 1,
        'geoCheckinCount': 0,
        'shares': 1,
        'status': 'ENABLED',
        'associatedEvents': 0,
        'creatorRole': 'REGULAR',
        'eventDate': '2015-09-20T21:00:12.000+0000',
        'eventTimeLine': 'UPCOMING'
      }]
    }
  });
  beforeEach(module('kwadWeb'));
  beforeEach(inject(function (_EventPlaceApiService_, _EventPlaceDetailsApiService_, _EventAddEditApiService_, _PlaceAddEditApiService_, _$rootScope_, _$q_, _$httpBackend_, _HOST_, _ToastrMessage_) {
    $rootScope = _$rootScope_;
    $q = _$q_;
    $httpBackend = _$httpBackend_;
    EventPlaceApiService = _EventPlaceApiService_;
    EventPlaceDetailsApiService = _EventPlaceDetailsApiService_;
    EventAddEditApiService = _EventAddEditApiService_;
    PlaceAddEditApiService = _PlaceAddEditApiService_;
    HOST = _HOST_;
    ToastrMessage = _ToastrMessage_;
  }));
  beforeEach(function () {
    $httpBackend.expectGET('app/login/login.html').respond(200);
    $httpBackend.flush();
  });

  describe('Unit : EventPlaceApiService Factory', function () {
    it('EventPlaceApiService should be defined', function () {
      expect(EventPlaceApiService).toBeDefined();
    });
    describe('Unit : EventPlaceApiService.getEventsList', function () {
      it('Should exist and be a function', function () {
        expect(EventPlaceApiService.getEventsList).toBeDefined();
        expect(typeof EventPlaceApiService.getEventsList).toEqual('function');
      });
      it('Should return success', function () {
        $httpBackend.expectPOST(HOST.HOST_URL + '/api/ext/s/search').respond(fakeResponse.fakeEventList);
        var result = '',
          success = function (response) {
            result = response;
          },
          error = function (err) {
            result = err;
          };
        var _payload = {
          userId: '54bed6e2e4b026f0ebf5ab6b',
          type: 'EVENTS',
          searchBy: 'USER'
        };
        EventPlaceApiService.getEventsList(_payload).$promise.then(success, error);
        $httpBackend.flush();
        $rootScope.$digest();
        expect(result.length).toBeGreaterThan(0);
      });
      it('Should return error', function () {
        $httpBackend.expectPOST(HOST.HOST_URL + '/api/ext/s/search').respond(500, {errors: ['Internal server error']});
        var result = '',
          success = function (response) {
            result = response;
          },
          error = function (err) {
            result = err.data.errors[0];
          };
        var _payload = {
          userId: '54bed6e2e4b026f0ebf5ab6b',
          type: 'EVENTS',
          searchBy: 'USER'
        };
        EventPlaceApiService.getEventsList(_payload).$promise.then(success, error);
        $httpBackend.flush();
        $rootScope.$digest();
        expect(result).toEqual('Internal server error');
      });
    });
    describe('Unit : EventPlaceApiService.getPlacesList', function () {
      it('Should exist and be a function', function () {
        expect(EventPlaceApiService.getPlacesList).toBeDefined();
        expect(typeof EventPlaceApiService.getPlacesList).toEqual('function');
      });
      it('Should return success', function () {
        $httpBackend.expectPOST(HOST.HOST_URL + '/api/ext/s/search').respond(fakeResponse.fakePlaceList);
        var result = '',
          success = function (response) {
            result = response;
          },
          error = function (err) {
            result = err;
          };
        var _payload = {
          userId: '54bed6e2e4b026f0ebf5ab6b',
          type: 'PLACES',
          searchBy: 'USER'
        };
        EventPlaceApiService.getPlacesList(_payload).$promise.then(success, error);
        $httpBackend.flush();
        $rootScope.$digest();
        expect(result.length).toBeGreaterThan(0);
      });
      it('Should return error', function () {
        $httpBackend.expectPOST(HOST.HOST_URL + '/api/ext/s/search').respond(500, {errors: ['Internal server error']});
        var result = '',
          success = function (response) {
            result = response;
          },
          error = function (err) {
            result = err.data.errors[0];
          };
        var _payload = {
          userId: '54bed6e2e4b026f0ebf5ab6b',
          type: 'PLACES',
          searchBy: 'USER'
        };
        EventPlaceApiService.getPlacesList(_payload).$promise.then(success, error);
        $httpBackend.flush();
        $rootScope.$digest();
        expect(result).toEqual('Internal server error');
      });
    });
  });

  describe('Unit : EventPlaceDetailsApiService Factory', function () {
    it('EventPlaceDetailsApiService should be defined', function () {
      expect(EventPlaceDetailsApiService).toBeDefined();
    });
    describe('Unit : EventPlaceDetailsApiService.getEventsDetails', function () {
      it('Should exist and be a function', function () {
        expect(EventPlaceDetailsApiService.getEventsDetails).toBeDefined();
        expect(typeof EventPlaceDetailsApiService.getEventsDetails).toEqual('function');
      });
      it('Should return success', function () {
        $httpBackend.expectPOST(HOST.URL + '/ep/details').respond(fakeResponse.fakeEventDetails);
        var result = '',
          success = function (response) {
            result = response;
          },
          error = function (err) {
            result = err;
          };
        var _payload = {ep_id: '55e78700e4b016c1bcd784ab'};
        EventPlaceDetailsApiService.getEventsDetails(_payload).$promise.then(success, error);
        $httpBackend.flush();
        $rootScope.$digest();
        expect(result.ep_id).toEqual(_payload.ep_id);
      });
      it('Should return error', function () {
        $httpBackend.expectPOST(HOST.URL + '/ep/details').respond(500, {errors: ['Internal server error']});
        var result = '',
          success = function (response) {
            result = response;
          },
          error = function (err) {
            result = err.data.errors[0];
          };
        var _payload = {ep_id: '55e78700e4b016c1bcd784ab'};
        EventPlaceDetailsApiService.getEventsDetails(_payload).$promise.then(success, error);
        $httpBackend.flush();
        $rootScope.$digest();
        expect(result).toEqual('Internal server error');
      });
    });
    describe('Unit : EventPlaceDetailsApiService.getPlacesDetails', function () {
      it('Should exist and be a function', function () {
        expect(EventPlaceDetailsApiService.getPlacesDetails).toBeDefined();
        expect(typeof EventPlaceDetailsApiService.getPlacesDetails).toEqual('function');
      });
      it('Should return success', function () {
        $httpBackend.expectPOST(HOST.URL + '/ep/details').respond(fakeResponse.fakePlaceDetails);
        var result = '',
          success = function (response) {
            result = response;
          },
          error = function (err) {
            result = err;
          };
        var _payload = {ep_id: '550f987fe4b09642124625a5'};
        EventPlaceDetailsApiService.getPlacesDetails(_payload).$promise.then(success, error);
        $httpBackend.flush();
        $rootScope.$digest();
        expect(result.ep_id).toEqual(_payload.ep_id);
      });
      it('Should return error', function () {
        $httpBackend.expectPOST(HOST.URL + '/ep/details').respond(500, {errors: ['Internal server error']});
        var result = '',
          success = function (response) {
            result = response;
          },
          error = function (err) {
            result = err.data.errors[0];
          };
        var _payload = {ep_id: '550f987fe4b09642124625a5'};
        EventPlaceDetailsApiService.getPlacesDetails(_payload).$promise.then(success, error);
        $httpBackend.flush();
        $rootScope.$digest();
        expect(result).toEqual('Internal server error');
      });
    });
  });

  describe('Unit : EventAddEditApiService Factory', function () {
    it('EventAddEditApiService should be defined', function () {
      expect(EventAddEditApiService).toBeDefined();
    });
    describe('Unit : EventAddEditApiService.addEditEvent', function () {
      it('Should exist and be a function', function () {
        expect(EventAddEditApiService.addEditEvent).toBeDefined();
        expect(typeof EventAddEditApiService.addEditEvent).toEqual('function');
      });
      it('Should return success', function () {
        $httpBackend.expectPOST(HOST.HOST_URL + '/api/ext/ep/event').respond(fakeResponse.fakeEventDetails);
        var result = '',
          success = function (response) {
            result = response;
          },
          error = function (err) {
            result = err;
          };
        var _payload = {
          category_id: '54b83a2be4b06b211e89274e',
          creatorId: '54df925ae4b072bb08b7733e',
          e_id: '55e78700e4b016c1bcd784ab',
          title: 'All White Affair',
          private: false,
          disabled: false,
          edit_all: true
        };
        EventAddEditApiService.addEditEvent(_payload).$promise.then(success, error);
        $httpBackend.flush();
        $rootScope.$digest();
        expect(result.ep_id).toEqual(_payload.e_id);
      });
      it('Should return error', function () {
        $httpBackend.expectPOST(HOST.HOST_URL + '/api/ext/ep/event').respond(500, {errors: ['Internal server error']});
        var result = '',
          success = function (response) {
            result = response;
          },
          error = function (err) {
            result = err.data.errors[0];
          };
        var _payload = {
          category_id: '54b83a2be4b06b211e89274e',
          creatorId: '54df925ae4b072bb08b7733e',
          e_id: '550f987fe4b09642124625a5',
          title: 'All White Affair',
          private: false,
          disabled: false,
          edit_all: true
        };
        EventAddEditApiService.addEditEvent(_payload).$promise.then(success, error);
        $httpBackend.flush();
        $rootScope.$digest();
        expect(result).toEqual('Internal server error');
      });
    });
  });

  describe('Unit : PlaceAddEditApiService Factory', function () {
    it('PlaceAddEditApiService should be defined', function () {
      expect(PlaceAddEditApiService).toBeDefined();
    });
    describe('Unit : PlaceAddEditApiService.addEditPlace', function () {
      it('Should exist and be a function', function () {
        expect(PlaceAddEditApiService.addEditPlace).toBeDefined();
        expect(typeof PlaceAddEditApiService.addEditPlace).toEqual('function');
      });
      it('Should return success', function () {
        $httpBackend.expectPOST(HOST.HOST_URL + '/api/ext/ep/place').respond(fakeResponse.fakePlaceDetails);
        var result = '',
          success = function (response) {
            result = response;
          },
          error = function (err) {
            result = err;
          };
        var _payload = {
          category_id: '54b83a2be4b06b211e892745',
          creatorId: '54e13604e4b072bb08b77438',
          p_id: '550f987fe4b09642124625a5',
          title: '1865 Coffee',
          location: {'type': 'Point', 'coordinates': [33.42195744640333, -111.9392205029726]},
          private: false,
          disabled: false
        };
        PlaceAddEditApiService.addEditPlace(_payload).$promise.then(success, error);
        $httpBackend.flush();
        $rootScope.$digest();
        expect(result.ep_id).toEqual(_payload.p_id);
      });
      it('Should return error', function () {
        $httpBackend.expectPOST(HOST.HOST_URL + '/api/ext/ep/place').respond(500, {errors: ['Internal server error']});
        var result = '',
          success = function (response) {
            result = response;
          },
          error = function (err) {
            result = err.data.errors[0];
          };
        var _payload = {
          category_id: '54b83a2be4b06b211e892745',
          creatorId: '54e13604e4b072bb08b77438',
          p_id: '550f987fe4b09642124625a5',
          title: '1865 Coffee',
          location: {'type': 'Point', 'coordinates': [33.42195744640333, -111.9392205029726]},
          private: false,
          disabled: false
        };
        PlaceAddEditApiService.addEditPlace(_payload).$promise.then(success, error);
        $httpBackend.flush();
        $rootScope.$digest();
        expect(result).toEqual('Internal server error');
      });
    });
  });

});