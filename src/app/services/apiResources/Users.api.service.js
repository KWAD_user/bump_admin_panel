'use strict';

(function (angular) {
  angular.module('kwadWeb')
    .factory('UsersApiService', ['$resource', 'HOST',
      function ($resource, HOST) {
        return $resource(HOST.HOST_URL + '/api/ext/s/usersearch', {}, {
          getUsers: {method: 'POST'}
        });
      }])
    .factory('AddEditUserAPIService', ['$resource', 'HOST',
      function ($resource, HOST) {
        return $resource(HOST.HOST_URL + '/api/ext/auth/user', {}, {
          addEditUser: {method: 'POST'}
        });
      }])
    .factory('UserDetailsApiService', ['$resource', 'HOST',
      function ($resource, HOST) {
        return $resource(HOST.HOST_URL + '/api/ext/ics/profile', {}, {
          getUserProfileData: {method: 'POST'}
        });
      }]);
})(angular);
