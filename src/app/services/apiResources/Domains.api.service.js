'use strict';

(function (angular) {
  angular.module('kwadWeb')
    .factory('DomainApiService', ['$resource', 'HOST',
      function ($resource, HOST) {
        return $resource(HOST.HOST_URL + '/api/ext/s/domainsearch', {}, {
          getDomains: {method: 'POST', isArray: false}
        });
      }])
    .factory('DomainAddEditApiService', ['$resource', 'HOST',
      function ($resource, HOST) {
        return $resource(HOST.HOST_URL + '/api/ext/ics/university', {}, {
          addEditDomain: {method: 'POST'}
        });
      }]);
})(angular);
