'use strict';

(function (angular) {
  angular.module('kwadWeb')
    .factory('AddAssetsService', ['$resource', 'HOST',
      function ($resource, HOST) {
        return $resource(HOST.HOST_URL + '/api/ext/assets/add', {}, {
          addAsset: {method: 'POST'}
        });
      }])
    .factory('DeleteAssetsService', ['$resource', 'HOST',
      function ($resource, HOST) {
        return $resource(HOST.URL + '/ext/assets/delete', {}, {
          removeAsset: {method: 'POST'}
        });
      }])
    .factory('FetchAssetsService', ['$resource', 'HOST',
      function ($resource, HOST) {
        return $resource(HOST.URL + '/ext/assets/fetch', {}, {
          fetchAsset: {method: 'POST'}
        });
      }]);
})(angular);
