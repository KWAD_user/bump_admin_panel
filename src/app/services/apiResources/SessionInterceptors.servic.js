'use strict';

(function (angular, btoa) {
  angular.module('kwadWeb')
    .factory('SessionInterceptorsService', ['CookieService', function (CookieService) {
      var sessionToken = {
        request: function (request) {
          var userToken = CookieService.userSession.get();
          if (userToken) {
            request.headers.Authorization = 'Bearer ' + btoa(userToken);
          }
          return request;
        },
        response: function (response) {
          return response;
        }
      };
      return sessionToken;
    }]);
})(angular, btoa);
