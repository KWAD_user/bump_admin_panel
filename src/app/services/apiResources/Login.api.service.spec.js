'use strict';

describe('Unit : login.api.service.js ', function () {
  var LoginApiService, $rootScope, $q, $httpBackend, HOST, ToastrMessage;

  beforeEach(module('kwadWeb'));
  beforeEach(inject(function (_LoginApiService_, _$rootScope_, _$q_, _$httpBackend_, _HOST_, _ToastrMessage_) {
    $rootScope = _$rootScope_;
    $q = _$q_;
    $httpBackend = _$httpBackend_;
    LoginApiService = _LoginApiService_;
    HOST = _HOST_;
    ToastrMessage = _ToastrMessage_;
  }));
  beforeEach(function () {
    $httpBackend.expectGET('app/login/login.html').respond(200);
    $httpBackend.flush();
  });

  describe('Unit : LoginApiService Factory', function () {
    it('LoginApiService should be defined', function () {
      expect(LoginApiService).toBeDefined();
    });
    describe('Unit : LoginApiService.login Success', function () {
      it('Should exist and be a function', function () {
        expect(LoginApiService.login).toBeDefined();
        expect(typeof LoginApiService.login).toEqual('function');
      });
      it('Should return success', function () {
        $httpBackend.expectPOST(HOST.HOST_URL + '/api/ext/auth/login').respond({
          status: 'SUCCESS',
          token: '2256761cf0e4b02b658d19c3e5'
        });
        var result = '',
          success = function (response) {
            result = response.status;
          },
          error = function (err) {
            result = err;
          };
        var _payload = {email: 'sandeep.kumar1@tothenew.com', passwd: '12345xx'};
        LoginApiService.login(_payload).$promise.then(success, error);
        $httpBackend.flush();
        $rootScope.$digest();
        expect(result).toEqual('SUCCESS');
      });
      it('Should return error', function () {
        $httpBackend.expectPOST(HOST.HOST_URL + '/api/ext/auth/login').respond(500, {errors: ['Internal server error']});
        var result = '',
          success = function (response) {
            result = response;
          },
          error = function (err) {
            result = err.data.errors[0];
          };
        var _payload = {email: 'sandeep.kumar1@tothenew.com', passwd: '12345xx'};
        LoginApiService.login(_payload).$promise.then(success, error);
        $httpBackend.flush();
        $rootScope.$digest();
        expect(result).toEqual('Internal server error');
      });
    });
  });
});