'use strict';

describe('Unit : resetPassword.api.service.js ', function () {
  var ResetPasswordApiService, $rootScope, $q, $httpBackend, HOST, ToastrMessage;

  beforeEach(module('kwadWeb'));
  beforeEach(inject(function (_ResetPasswordApiService_, _$rootScope_, _$q_, _$httpBackend_, _HOST_, _ToastrMessage_) {
    $rootScope = _$rootScope_;
    $q = _$q_;
    $httpBackend = _$httpBackend_;
    ResetPasswordApiService = _ResetPasswordApiService_;
    HOST = _HOST_;
    ToastrMessage = _ToastrMessage_;
  }));
  beforeEach(function () {
    $httpBackend.expectGET('app/login/login.html').respond(200);
    $httpBackend.flush();
  });

  describe('Unit : ResetPasswordApiService Factory', function () {
    it('ResetPasswordApiService should be defined', function () {
      expect(ResetPasswordApiService).toBeDefined();
    });
    describe('Unit : ResetPasswordApiService.resetPassword Success', function () {
      it('Should exist and be a function', function () {
        expect(ResetPasswordApiService.resetPassword).toBeDefined();
        expect(typeof ResetPasswordApiService.resetPassword).toEqual('function');
      });
      it('Should return success', function () {
        $httpBackend.expectPOST(HOST.HOST_URL + '/api/ext/auth/resetpass').respond({'status': 'SUCCESS'});
        var result = '',
          success = function (response) {
            result = response.status;
          },
          error = function (err) {
            result = err;
          };
        var _payload = {userId: '54bed6e2e4b026f0ebf5ab6b'};
        ResetPasswordApiService.resetPassword(_payload).$promise.then(success, error);
        $httpBackend.flush();
        $rootScope.$digest();
        expect(result).toEqual('SUCCESS');
      });
      it('Should return error', function () {
        $httpBackend.expectPOST(HOST.HOST_URL + '/api/ext/auth/resetpass').respond(500, {errors: ['Internal server error']});
        var result = '',
          success = function (response) {
            result = response;
          },
          error = function (err) {
            result = err.data.errors[0];
          };
        var _payload = {userId: '54bed6e2e4b026f0ebf5ab6b'};
        ResetPasswordApiService.resetPassword(_payload).$promise.then(success, error);
        $httpBackend.flush();
        $rootScope.$digest();
        expect(result).toEqual('Internal server error');
      });
    });
  });
});