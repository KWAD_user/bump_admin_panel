'use strict';

(function (angular) {
  angular.module('kwadWeb')
    .factory('LoginApiService', ['$resource', 'HOST',
      function ($resource, HOST) {
        return $resource(HOST.HOST_URL + '/api/ext/auth/login', {}, {
          login: {method: 'POST'}
        });
      }]);
})(angular);
