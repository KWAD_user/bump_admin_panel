'use strict';

(function (angular) {
  angular.module('kwadWeb')
    .factory('ForgetPasswordApiService', ['$resource', 'HOST',
      function ($resource, HOST) {
        return $resource(HOST.URL + '/auth/forgotpassword', {}, {
          forgetPassword: {method: 'POST'}
        });
      }]);
})(angular);
