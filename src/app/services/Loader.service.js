'use strict';

/*
 *  userToken - Is use for Manage User session
 * appearance - Is user appearance first time or not ?
 * */
(function (angular) {
  angular.module('kwadWeb')
    .factory('LoaderService', [
      function () {
        var loader = {
          showSpinner: function () {
            angular.element('.loader').css('display', 'block');
            angular.element('body').css({'overflow': 'hidden'});
          },
          hideSpinner: function () {
            angular.element('.loader').css('display', 'none');
            angular.element('body').css({'overflow': 'visible'});
          }
        };

        return {
          Loader: loader
        };
      }]);
})(angular);
