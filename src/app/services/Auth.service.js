'use strict';

/*
 *  userToken - Is use for Manage User session
 * appearance - Is user appearance first time or not ?
 * */
(function (angular) {
  angular.module('kwadWeb')
    .factory('AuthService', ['$q', 'UserDetailsApiService',
      function ($q, UserDetailsApiService) {
        var currentUser = null;

        var getCurrentUser = function () {
            var deferred = $q.defer();
            if (currentUser) {
              deferred.resolve(currentUser);
            } else {
              UserDetailsApiService.getUserProfileData({}).$promise.then(function (result) {
                currentUser = result;
                deferred.resolve(currentUser);
              }, function (err) {
                deferred.reject(err);
              });
            }
            return deferred.promise;
          },
          setCurrentUser = function (user) {
            currentUser = user;
          };

        return {
          getCurrentUser: getCurrentUser,
          setCurrentUser: setCurrentUser
        };
      }]);
})(angular);
