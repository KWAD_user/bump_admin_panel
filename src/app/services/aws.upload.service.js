'use strict';
(function (angular, AWS) {
    angular.module('kwadWeb')
            .factory('AWSUploadService', ['$q', function ($q) {

                    function b64toBlob(b64Data, contentType, sliceSize) {
                        contentType = contentType || '';
                        sliceSize = sliceSize || 512;

                        var byteCharacters = atob(b64Data);
                        var byteArrays = [];
                        var blob = null;
                        var slice = 0;
                        var offset = 0;
                        var i = 0;
                        var byteNumbers = 0;
                        var byteArray = null;

                        for (offset = 0; offset < byteCharacters.length; offset += sliceSize) {
                            slice = byteCharacters.slice(offset, offset + sliceSize);

                            byteNumbers = new Array(slice.length);
                            for (i = 0; i < slice.length; i++) {
                                byteNumbers[i] = slice.charCodeAt(i);
                            }

                            byteArray = new Uint8Array(byteNumbers);

                            byteArrays.push(byteArray);
                        }

                        blob = new Blob(byteArrays, {type: contentType});
                        return blob;
                    }

                    var compressFile = function (file, options) {
                        var deferred = $q.defer();
                        var reader = null;
                        var image_obj = null;
                        var cvs = '';
                        var ctx = '';
                        var quality = '';
                        var compressedImageData = '';
                        var imageBase64 = '';
                        var blob = null;
                        var compressedFile = null;

                        if (!options || typeof options !== 'object') {
                            options = {};
                        }
                        options.quality = options.quality || 72;

                        if ((!file) || (file.type.indexOf('image/') === -1)) {
                            deferred.reject('Unable to compress: Not an image file.');
                        }

                        reader = new FileReader();
                        reader.onload = function (event) {
                            image_obj = new Image();
                            image_obj.src = event.target.result;
                            image_obj.onload = function () {
                                cvs = document.createElement('canvas');
                                cvs.width = image_obj.naturalWidth;
                                cvs.height = image_obj.naturalHeight;
                                ctx = cvs.getContext('2d').drawImage(image_obj, 0, 0);
                                quality = options.quality / 100;
                                compressedImageData = cvs.toDataURL(file.type, quality);
                                imageBase64 = compressedImageData.replace('data:' + file.type + ';base64,', '');
                                blob = b64toBlob(imageBase64, file.type);
                                compressedFile = new File([blob], file.name, {type: blob.type});
                                deferred.resolve(compressedFile);
                            };
                        };
                        reader.readAsDataURL(file);
                        return deferred.promise;
                    };

                    function uploadFile(file, AWSObj, options) {
                        var deferred = $q.defer();
                        var bucket = null;
                        var params = null;

                        if (!AWSObj.aws_key) {
                            deferred.reject('Credentials not defined');
                        }

                        AWS.config.update({
                            accessKeyId: AWSObj.aws_key,
                            secretAccessKey: AWSObj.aws_secret,
                            sessionToken: AWSObj.aws_token
                        });

                        bucket = new AWS.S3({
                            params: {
                                Bucket: AWSObj.s3_bucket //'dev-web.whatsbumpin.net'
                            }
                        });

                        compressFile(file, options).then(function (compressedFile) {
                            params = {
                                Key: AWSObj.s3_object,
                                ContentType: compressedFile.type,
                                Body: compressedFile
                            };
                            bucket.upload(params, function (err, data) {
                                if (err) {
                                    deferred.reject(err);
                                } else {
                                    deferred.resolve(data);
                                }
                            });
                        }, function (err) {
                            deferred.reject(err);
                        }
                        );

                        return deferred.promise;
                    }

                    return {
                        compressFile: compressFile,
                        uploadFile: uploadFile
                    };
                }]);
})(angular, window.AWS);