'use strict';

(function (angular) {
  angular.module('kwadWeb')
    .factory('EnumsService', ['$q', 'EnumsApiService', 'LoaderService',
      function ($q, EnumsApiService, LoaderService) {
        var AUTHORIZATION_STATE, USER_ROLE, EP_STATE, EP_TYPE, PLACE_CATEGORY, EVENT_CATEGORY, BUSINESS_TYPE, RECURRENCE_TYPE;
        var ASSET_TYPES = {
          IMAGE_JPG: 'IMAGE_JPG',
          IMAGE_JPEG: 'IMAGE_JPEG',
          VIDEO: 'VIDEO'
        };

        var getAuthorizationStateEnums = function () {
          var deferred = $q.defer();
          if (AUTHORIZATION_STATE) {
            deferred.resolve(AUTHORIZATION_STATE);
          } else {
            EnumsApiService.getEnums({name: 'AUTHORIZATION_STATE'}).$promise.then(function (result) {
              if (result.status === 'SUCCESS') {
                deferred.resolve(result.valuesList);
                setAuthorizationStateEnums(result.valuesList);
              } else {
                deferred.reject(result.status);
              }
            }, function (error) {
              deferred.reject(error);
            });
          }
          return deferred.promise;
        };
        var getUserRoleEnums = function () {
          var deferred = $q.defer();
          if (USER_ROLE) {
            deferred.resolve(USER_ROLE);
          } else {
            EnumsApiService.getEnums({name: 'USER_ROLE'}).$promise.then(function (result) {
              if (result.status === 'SUCCESS') {
                deferred.resolve(result.valuesList);
                setUserRoleEnums(result.valuesList);
              } else {
                deferred.reject(result.status);
              }
            }, function (error) {
              deferred.reject(error);
            });
          }
          return deferred.promise;
        };
        var getEpStateEnums = function () {
          var deferred = $q.defer();
          if (EP_STATE) {
            deferred.resolve(EP_STATE);
          } else {
            EnumsApiService.getEnums({name: 'EP_STATE'}).$promise.then(function (result) {
              if (result.status === 'SUCCESS') {
                deferred.resolve(result.valuesList);
                setEpStateEnums(result.valuesList);
              } else {
                deferred.reject(result.status);
              }
            }, function (error) {
              deferred.reject(error);
            });
          }
          return deferred.promise;
        };
        var getEpTypeEnums = function () {
          var deferred = $q.defer();
          if (EP_TYPE) {
            deferred.resolve(EP_TYPE);
          } else {
            EnumsApiService.getEnums({name: 'EP_TYPE'}).$promise.then(function (result) {
              if (result.status === 'SUCCESS') {
                deferred.resolve(result.valuesList);
                setEpTypeEnums(result.valuesList);
              } else {
                deferred.reject(result.status);
              }
            }, function (error) {
              deferred.reject(error);
            });
          }
          return deferred.promise;
        };
        var getRecurrenceTypeEnums = function () {
          var deferred = $q.defer();
          if (RECURRENCE_TYPE) {
            deferred.resolve(RECURRENCE_TYPE);
          } else {
            EnumsApiService.getEnums({name: 'RECURRENCE_TYPE'}).$promise.then(function (result) {
              if (result.status === 'SUCCESS') {
                deferred.resolve(result.valuesList);
                setRecurrenceTypeEnums(result.valuesList);
              } else {
                deferred.reject(result.status);
              }
            }, function (error) {
              deferred.reject(error);
            });
          }
          return deferred.promise;
        };
        var getPlaceCategoryEnums = function () {
          var deferred = $q.defer();
          if (PLACE_CATEGORY) {
            deferred.resolve(PLACE_CATEGORY);
          } else {
            EnumsApiService.getEnums({name: 'PLACE_CATEGORY'}).$promise.then(function (result) {
              if (result.status === 'SUCCESS') {
                deferred.resolve(result.categoryMap);
                setPlaceCategoryEnums(result.categoryMap);
              } else {
                deferred.reject(result.status);
              }
            }, function (error) {
              deferred.reject(error);
            });
          }
          return deferred.promise;
        };
        var getEventCategoryEnums = function () {
          var deferred = $q.defer();
          if (EVENT_CATEGORY) {
            deferred.resolve(EVENT_CATEGORY);
          } else {
            EnumsApiService.getEnums({name: 'EVENT_CATEGORY'}).$promise.then(function (result) {
              if (result.status === 'SUCCESS') {
                deferred.resolve(result.categoryMap);
                setEventCategoryEnums(result.categoryMap);
              } else {
                deferred.reject(result.status);
              }
            }, function (error) {
              deferred.reject(error);
            });
          }
          return deferred.promise;
        };
        var getBusinessTypeEnums = function () {
          var deferred = $q.defer();
          if (BUSINESS_TYPE) {
            deferred.resolve(BUSINESS_TYPE);
          } else {
            LoaderService.Loader.showSpinner();
            EnumsApiService.getEnums({name: 'BUSINESS_TYPE'}).$promise.then(function (result) {
              if (result.status === 'SUCCESS') {
                LoaderService.Loader.hideSpinner();
                setBusinessTypeEnums(result.valuesList);
                deferred.resolve(result.valuesList);
              } else {
                LoaderService.Loader.hideSpinner();
                deferred.reject(result.status);
              }
            }, function (error) {
              LoaderService.Loader.hideSpinner();
              deferred.reject(error);
            });
          }
          return deferred.promise;
        };
        var getAssetTypeEnums = function () {
          return ASSET_TYPES;
        };

        var setAuthorizationStateEnums = function (_AUTHORIZATION_STATE) {
          AUTHORIZATION_STATE = _AUTHORIZATION_STATE;
        };
        var setUserRoleEnums = function (_USER_ROLE) {
          USER_ROLE = _USER_ROLE;
        };
        var setEpStateEnums = function (_EP_STATE) {
          EP_STATE = _EP_STATE;
        };
        var setEpTypeEnums = function (_EP_TYPE) {
          EP_TYPE = _EP_TYPE;
        };
        var setRecurrenceTypeEnums = function (_RECURRENCE_TYPE) {
          RECURRENCE_TYPE = _RECURRENCE_TYPE;
        };
        var setPlaceCategoryEnums = function (_PLACE_CATEGORY) {
          PLACE_CATEGORY = _PLACE_CATEGORY;
        };
        var setEventCategoryEnums = function (_EVENT_CATEGORY) {
          EVENT_CATEGORY = _EVENT_CATEGORY;
        };
        var setBusinessTypeEnums = function (_BUSINESS_TYPE) {
          BUSINESS_TYPE = _BUSINESS_TYPE;
        };

        return {
          getAuthorizationStateEnums: getAuthorizationStateEnums,
          getUserRoleEnums: getUserRoleEnums,
          getEpStateEnums: getEpStateEnums,
          getEpTypeEnums: getEpTypeEnums,
          getRecurrenceTypeEnums: getRecurrenceTypeEnums,
          getPlaceCategoryEnums: getPlaceCategoryEnums,
          getEventCategoryEnums: getEventCategoryEnums,
          getBusinessTypeEnums: getBusinessTypeEnums,
          getAssetTypeEnums: getAssetTypeEnums
        };
      }]);
})(angular);
