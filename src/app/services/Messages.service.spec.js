'use strict';

describe('Unit: Messages factory', function () {
  var MessageService,
    $rootScope,
    $httpBackend;

  beforeEach(module('kwadWeb'));
  beforeEach(inject(function (_$rootScope_, _$httpBackend_, _MessageService_) {
    $rootScope = _$rootScope_;
    $httpBackend = _$httpBackend_;
    MessageService = _MessageService_;
    $httpBackend.expectGET('app/login/login.html').respond(200);
    $httpBackend.flush();
  }));

  it('MessageService should be defined', function () {
    expect(MessageService).toBeDefined();
  });

  it('MessageService.showSuccessfulMessage should be called with defined message', function () {
    var message = 'Data received successfully';
    MessageService.showSuccessfulMessage(message);
    $rootScope.$digest();
  });

  it('MessageService.showFailedMessage should be called with defined message', function () {
    var message = 'Something going wrong, Please try again later';
    MessageService.showFailedMessage(message);
    $rootScope.$digest();
  });

  it('MessageService.showFailedMessage should be called with default message', function () {
    MessageService.showFailedMessage();
    $rootScope.$digest();
  });
});