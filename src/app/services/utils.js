'use strict';

Array.prototype.toObject = function () {
  if (this.length <= 0) {
    return;
  }

  return this.reduce(function (obj, k) {
    obj[k] = k;
    return obj;
  }, {});
};

Array.prototype.chunks = function (limit) {
  var _that = this;

  if (_that.length <= 0) {
    return null;
  }

  var listLength = _that.length;
  var groupNum = (listLength % limit === 0) ? listLength / limit : Math.ceil(listLength / limit);

  _that.chunkList = [];
  for (var i = 0; i < groupNum; i++) {
    _that.chunkList[i] = _that.slice(i * limit, (i + 1) * limit);
  }

  return _that.chunkList;
};
