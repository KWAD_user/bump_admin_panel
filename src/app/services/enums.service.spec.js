'use strict';

describe('Unit: EnumsService factory', function () {
  describe('Unit: EnumsService factory Return success for each fetch enums call', function () {
    var EnumsService,
      $rootScope,
      $controller,
      $httpBackend,
      $q,
      LoaderService = {Loader: {}},
      fakeResponse,
      EnumsApiService,
      HOST;

    beforeAll(function () {
      fakeResponse = {
        fakeAuthorizationStates: ['ENABLED', 'DISABLED', 'AWAITING_VERIFICATION', 'CLOSED'],
        fakeRoles: ['ANONYMOUS', 'REGULAR', 'LAUNCHSQUAD', 'ADMIN', 'BUSINESS', 'BUSINESS_INDIVIDUAL', 'EDITOR'],
        fakeEpStates: ['ENABLED', 'DISABLED'],
        fakeEpTypes: ['BUMP', 'NON_BUMP', 'SPONSORED'],
        fakeRecurrenceType: ['DAILY', 'WEEKLY', 'TWO_WEEKLY', 'MONTHLY', 'YEARLY', 'NEVER'],
        fakePlaceCategories: {
          '54b83a2be4b06b211e892749': 'Music',
          '54d56cceeca14131c5fb3d9c': 'House',
          '54b83a2be4b06b211e892747': 'Bar/Club',
          '54b83a2be4b06b211e89274b': 'Sports',
          '54b83a2be4b06b211e892745': 'Coffee',
          '54b83a2be4b06b211e892751': 'Group',
          '54b83a2be4b06b211e892743': 'Food',
          '54b83a2be4b06b211e892753': 'Outdoors',
          '54b83a2be4b06b211e89274f': 'Party',
          '54b83a2be4b06b211e892755': 'General',
          '54b83a2be4b06b211e89274d': 'School'
        },
        fakeEventCategories: {
          '54b83a2be4b06b211e892748': 'Music',
          '54b83a2be4b06b211e892750': 'Group',
          '54b83a2be4b06b211e89274a': 'Sports',
          '54b83a2be4b06b211e892746': 'Bar/Club',
          '54b83a2be4b06b211e892752': 'Outdoors',
          '54b83a2be4b06b211e892744': 'Coffee',
          '54b83a2be4b06b211e892754': 'General',
          '54b83a2be4b06b211e89274e': 'Party',
          '54b83a2be4b06b211e892742': 'Food',
          '54b83a2be4b06b211e89274c': 'School'
        },
        fakeBusinessTypes: ['PROFIT', 'NON_PROFIT', 'UNIVERSITY']
      };
    });
    beforeEach(module('kwadWeb', function ($provide) {
      $provide.factory('EnumsApiService', function () {
        var EnumsApiService = jasmine.createSpyObj('EnumsApiService', ['getEnums']);
        EnumsApiService.getEnums.and.callFake(function (payload) {
          var deferred = $q.defer();
          switch (payload.name) {
            case 'AUTHORIZATION_STATE':
              deferred.resolve({status: 'SUCCESS', valuesList: fakeResponse.fakeAuthorizationStates});
              break;
            case 'USER_ROLE':
              deferred.resolve({status: 'SUCCESS', valuesList: fakeResponse.fakeRoles});
              break;
            case 'EP_STATE':
              deferred.resolve({status: 'SUCCESS', valuesList: fakeResponse.fakeEpStates});
              break;
            case 'EP_TYPE':
              deferred.resolve({status: 'SUCCESS', valuesList: fakeResponse.fakeEpTypes});
              break;
            case 'RECURRENCE_TYPE':
              deferred.resolve({status: 'SUCCESS', valuesList: fakeResponse.fakeRecurrenceType});
              break;
            case 'PLACE_CATEGORY':
              deferred.resolve({status: 'SUCCESS', categoryMap: fakeResponse.fakePlaceCategories});
              break;
            case 'EVENT_CATEGORY':
              deferred.resolve({status: 'SUCCESS', categoryMap: fakeResponse.fakeEventCategories});
              break;
            case 'BUSINESS_TYPE':
              deferred.resolve({status: 'SUCCESS', valuesList: fakeResponse.fakeBusinessTypes});
              break;
            default :
              deferred.reject();
              break;
          }
          deferred.$promise = deferred.promise;
          return deferred;
        });
        return EnumsApiService;
      });
    }));
    beforeEach(inject(function (_$rootScope_, _$controller_, _$q_, _$httpBackend_, _EnumsService_, _HOST_) {
      $rootScope = _$rootScope_;
      $controller = _$controller_;
      $q = _$q_;
      $httpBackend = _$httpBackend_;
      EnumsService = _EnumsService_;
      HOST = _HOST_;
      LoaderService.Loader = jasmine.createSpyObj('LoaderService.Loader', ['hideSpinner', 'showSpinner']);
      LoaderService.Loader.hideSpinner.and.callFake(function () {
      });
      LoaderService.Loader.showSpinner.and.callFake(function () {
      });
    }));
    beforeEach(function () {
      $httpBackend.expectGET('app/login/login.html').respond(200);
      $httpBackend.flush();
    });

    describe('Units Should be defined', function () {
      it('UserDetails should be defined', function () {
        expect(EnumsService).toBeDefined();
      });
      it('EventPlaceApiService should be defined', function () {
        expect($q).toBeDefined();
      });
      it('LoaderService should be defined', function () {
        expect(LoaderService).toBeDefined();
      });
    });

    describe('Service Unit: getAuthorizationStateEnums', function () {
      it('Should be exist and be a function', function () {
        expect(EnumsService.getAuthorizationStateEnums).toBeDefined();
        expect(typeof EnumsService.getAuthorizationStateEnums).toBe('function');
      });
      it('Should be resolve Authorization State Enums', function () {
        var result = '',
          success = function (data) {
            result = data;
          },
          error = function (err) {
            result = err;
          };
        EnumsService.getAuthorizationStateEnums().then(success, error); //fetch and cached in service
        $rootScope.$digest();
        EnumsService.getAuthorizationStateEnums().then(success, error); //fetched from cached data
        expect(result.length).toBeGreaterThan(0);
      });
    });

    describe('Service Unit: getUserRoleEnums', function () {
      it('Should be exist and be a function', function () {
        expect(EnumsService.getUserRoleEnums).toBeDefined();
        expect(typeof EnumsService.getUserRoleEnums).toBe('function');
      });
      it('Should be resolve User Roles Enums', function () {
        var result = '',
          success = function (data) {
            result = data;
          },
          error = function (err) {
            result = err;
          };
        EnumsService.getUserRoleEnums().then(success, error); //fetch and cached in service
        $rootScope.$digest();
        EnumsService.getUserRoleEnums().then(success, error); //fetched from cached data
        expect(result.length).toBeGreaterThan(0);
      });
    });

    describe('Service Unit: getEpStateEnums', function () {
      it('Should be exist and be a function', function () {
        expect(EnumsService.getEpStateEnums).toBeDefined();
        expect(typeof EnumsService.getEpStateEnums).toBe('function');
      });
      it('Should be resolve Ep States Enums', function () {
        var result = '',
          success = function (data) {
            result = data;
          },
          error = function (err) {
            result = err;
          };
        EnumsService.getEpStateEnums().then(success, error); //fetch and cached in service
        $rootScope.$digest();
        EnumsService.getEpStateEnums().then(success, error); //fetched from cached data
        expect(result.length).toBeGreaterThan(0);
      });
    });

    describe('Service Unit: getEpTypeEnums', function () {
      it('Should be exist and be a function', function () {
        expect(EnumsService.getEpTypeEnums).toBeDefined();
        expect(typeof EnumsService.getEpTypeEnums).toBe('function');
      });
      it('Should be resolve Ep Types Enums', function () {
        var result = '',
          success = function (data) {
            result = data;
          },
          error = function (err) {
            result = err;
          };
        EnumsService.getEpTypeEnums().then(success, error); //fetch and cached in service
        $rootScope.$digest();
        EnumsService.getEpTypeEnums().then(success, error); //fetched from cached data
        expect(result.length).toBeGreaterThan(0);
      });
    });

    describe('Service Unit: getRecurrenceTypeEnums', function () {
      it('Should be exist and be a function', function () {
        expect(EnumsService.getRecurrenceTypeEnums).toBeDefined();
        expect(typeof EnumsService.getRecurrenceTypeEnums).toBe('function');
      });
      it('Should be resolve Recurrence Type Enums', function () {
        var result = '',
          success = function (data) {
            result = data;
          },
          error = function (err) {
            result = err;
          };
        EnumsService.getRecurrenceTypeEnums().then(success, error); //fetch and cached in service
        $rootScope.$digest();
        EnumsService.getRecurrenceTypeEnums().then(success, error); //fetched from cached data
        expect(result.length).toBeGreaterThan(0);
      });
    });

    describe('Service Unit: getPlaceCategoryEnums', function () {
      it('Should be exist and be a function', function () {
        expect(EnumsService.getPlaceCategoryEnums).toBeDefined();
        expect(typeof EnumsService.getPlaceCategoryEnums).toBe('function');
      });
      it('Should be resolve Place Categories Enums', function () {
        var result = null,
          success = function (data) {
            result = data;
          },
          error = function (err) {
            result = err;
          };
        EnumsService.getPlaceCategoryEnums().then(success, error); //fetch and cached in service
        $rootScope.$digest();
        EnumsService.getPlaceCategoryEnums().then(success, error); //fetched from cached data
        expect(result).not.toEqual(null);
      });
    });

    describe('Service Unit: getEventCategoryEnums', function () {
      it('Should be exist and be a function', function () {
        expect(EnumsService.getEventCategoryEnums).toBeDefined();
        expect(typeof EnumsService.getEventCategoryEnums).toBe('function');
      });
      it('Should be resolve Event Categories Enums', function () {
        var result = null,
          success = function (data) {
            result = data;
          },
          error = function (err) {
            result = err;
          };
        EnumsService.getEventCategoryEnums().then(success, error); //fetch and cached in service
        $rootScope.$digest();
        EnumsService.getEventCategoryEnums().then(success, error); //fetched from cached data
        expect(result).not.toEqual(null);
      });
    });

    describe('Service Unit: getBusinessTypeEnums', function () {
      it('Should be exist and be a function', function () {
        expect(EnumsService.getBusinessTypeEnums).toBeDefined();
        expect(typeof EnumsService.getBusinessTypeEnums).toBe('function');
      });
      it('Should be resolve Business Types Enums', function () {
        var result = '',
          success = function (data) {
            result = data;
          },
          error = function (err) {
            result = err;
          };
        EnumsService.getBusinessTypeEnums().then(success, error); //fetch and cached in service
        $rootScope.$digest();
        EnumsService.getBusinessTypeEnums().then(success, error); //fetched from cached data
        expect(result.length).toBeGreaterThan(0);
      });
    });

    describe('Service Unit: getAssetTypeEnums', function () {
      it('Should be exist and be a function', function () {
        expect(EnumsService.getAssetTypeEnums).toBeDefined();
        expect(typeof EnumsService.getAssetTypeEnums).toBe('function');
      });
      it('Should be resolve Asset Types Enums', function () {
        var result = null;
        result = EnumsService.getAssetTypeEnums();
        $rootScope.$digest();
        expect(result).not.toEqual(null);
      });
    });
  });
  describe('Unit: EnumsService factory rejected  each fetch enums call', function () {
    var EnumsService,
      $rootScope,
      $controller,
      $httpBackend,
      $q,
      LoaderService = {Loader: {}},
      EnumsApiService,
      HOST;

    beforeEach(module('kwadWeb', function ($provide) {
      $provide.factory('EnumsApiService', function () {
        var EnumsApiService = jasmine.createSpyObj('EnumsApiService', ['getEnums']);
        EnumsApiService.getEnums.and.callFake(function (payload) {
          var deferred = $q.defer();
          switch (payload.name) {
            case 'AUTHORIZATION_STATE':
              deferred.reject('ERROR: AUTHORIZATION_STATE ENUMS NOT FOUND');
              break;
            case 'USER_ROLE':
              deferred.reject('ERROR: USER_ROLE ENUMS NOT FOUND');
              break;
            case 'EP_STATE':
              deferred.reject('ERROR: EP_STATE ENUMS NOT FOUND');
              break;
            case 'EP_TYPE':
              deferred.reject('ERROR: EP_TYPE ENUMS NOT FOUND');
              break;
            case 'RECURRENCE_TYPE':
              deferred.reject('ERROR: RECURRENCE_TYPE ENUMS NOT FOUND');
              break;
            case 'PLACE_CATEGORY':
              deferred.reject('ERROR: PLACE_CATEGORY ENUMS NOT FOUND');
              break;
            case 'EVENT_CATEGORY':
              deferred.reject('ERROR: EVENT_CATEGORY ENUMS NOT FOUND');
              break;
            case 'BUSINESS_TYPE':
              deferred.reject('ERROR: BUSINESS_TYPE ENUMS NOT FOUND');
              break;
            default :
              deferred.reject();
              break;
          }
          deferred.$promise = deferred.promise;
          return deferred;
        });
        return EnumsApiService;
      });
    }));
    beforeEach(inject(function (_$rootScope_, _$controller_, _$q_, _$httpBackend_, _EnumsService_, _HOST_) {
      $rootScope = _$rootScope_;
      $controller = _$controller_;
      $q = _$q_;
      $httpBackend = _$httpBackend_;
      EnumsService = _EnumsService_;
      HOST = _HOST_;
      LoaderService.Loader = jasmine.createSpyObj('LoaderService.Loader', ['hideSpinner', 'showSpinner']);
      LoaderService.Loader.hideSpinner.and.callFake(function () {
      });
      LoaderService.Loader.showSpinner.and.callFake(function () {
      });
    }));
    beforeEach(function () {
      $httpBackend.expectGET('app/login/login.html').respond(200);
      $httpBackend.flush();
    });

    describe('Function Units Should not be resolve enums', function () {
      it('UserDetails should be defined', function () {
        expect(EnumsService).toBeDefined();
      });
      it('EventPlaceApiService should be defined', function () {
        expect($q).toBeDefined();
      });
      it('LoaderService should be defined', function () {
        expect(LoaderService).toBeDefined();
      });

      it('Should be reject Authorization State Enums', function () {
        var result = '',
          success = function (data) {
            result = data;
          },
          error = function (err) {
            result = err;
          };
        EnumsService.getAuthorizationStateEnums().then(success, error);
        $rootScope.$digest();
        expect(result).toEqual('ERROR: AUTHORIZATION_STATE ENUMS NOT FOUND');
      });
      it('Should be reject User Roles Enums', function () {
        var result = '',
          success = function (data) {
            result = data;
          },
          error = function (err) {
            result = err;
          };
        EnumsService.getUserRoleEnums().then(success, error);
        $rootScope.$digest();
        expect(result).toEqual('ERROR: USER_ROLE ENUMS NOT FOUND');
      });
      it('Should be reject Ep States Enums', function () {
        var result = '',
          success = function (data) {
            result = data;
          },
          error = function (err) {
            result = err;
          };
        EnumsService.getEpStateEnums().then(success, error);
        $rootScope.$digest();
        expect(result).toEqual('ERROR: EP_STATE ENUMS NOT FOUND');
      });
      it('Should be reject Ep Types Enums', function () {
        var result = '',
          success = function (data) {
            result = data;
          },
          error = function (err) {
            result = err;
          };
        EnumsService.getEpTypeEnums().then(success, error);
        $rootScope.$digest();
        expect(result).toEqual('ERROR: EP_TYPE ENUMS NOT FOUND');
      });
      it('Should be reject Recurrence Type Enums', function () {
        var result = '',
          success = function (data) {
            result = data;
          },
          error = function (err) {
            result = err;
          };
        EnumsService.getRecurrenceTypeEnums().then(success, error);
        $rootScope.$digest();
        expect(result).toEqual('ERROR: RECURRENCE_TYPE ENUMS NOT FOUND');
      });
      it('Should be reject Place Categories Enums', function () {
        var result = '',
          success = function (data) {
            result = data;
          },
          error = function (err) {
            result = err;
          };
        EnumsService.getPlaceCategoryEnums().then(success, error);
        $rootScope.$digest();
        expect(result).toEqual('ERROR: PLACE_CATEGORY ENUMS NOT FOUND');
      });
      it('Should be reject Event Categories Enums', function () {
        var result = null,
          success = function (data) {
            result = data;
          },
          error = function (err) {
            result = err;
          };
        EnumsService.getEventCategoryEnums().then(success, error);
        $rootScope.$digest();
        expect(result).toEqual('ERROR: EVENT_CATEGORY ENUMS NOT FOUND');
      });
      it('Should be reject Business Types Enums', function () {
        var result = '',
          success = function (data) {
            result = data;
          },
          error = function (err) {
            result = err;
          };
        EnumsService.getBusinessTypeEnums().then(success, error);
        $rootScope.$digest();
        expect(result).toEqual('ERROR: BUSINESS_TYPE ENUMS NOT FOUND');
      });
    });
  });
  describe('Unit: EnumsService factory Return success not with status SUCCESS', function () {
    var EnumsService,
      $rootScope,
      $controller,
      $httpBackend,
      $q,
      LoaderService = {Loader: {}},
      EnumsApiService,
      HOST;

    beforeEach(module('kwadWeb', function ($provide) {
      $provide.factory('EnumsApiService', function () {
        var EnumsApiService = jasmine.createSpyObj('EnumsApiService', ['getEnums']);
        EnumsApiService.getEnums.and.callFake(function (payload) {
          var deferred = $q.defer();
          switch (payload.name) {
            case 'AUTHORIZATION_STATE':
            case 'USER_ROLE':
            case 'EP_STATE':
            case 'EP_TYPE':
            case 'RECURRENCE_TYPE':
            case 'PLACE_CATEGORY':
            case 'EVENT_CATEGORY':
            case 'BUSINESS_TYPE':
            default :
              deferred.resolve({status: 'ERROR'});
          }
          deferred.$promise = deferred.promise;
          return deferred;
        });
        return EnumsApiService;
      });
    }));
    beforeEach(inject(function (_$rootScope_, _$controller_, _$q_, _$httpBackend_, _EnumsService_, _HOST_) {
      $rootScope = _$rootScope_;
      $controller = _$controller_;
      $q = _$q_;
      $httpBackend = _$httpBackend_;
      EnumsService = _EnumsService_;
      HOST = _HOST_;
      LoaderService.Loader = jasmine.createSpyObj('LoaderService.Loader', ['hideSpinner', 'showSpinner']);
      LoaderService.Loader.hideSpinner.and.callFake(function () {
      });
      LoaderService.Loader.showSpinner.and.callFake(function () {
      });
    }));
    beforeEach(function () {
      $httpBackend.expectGET('app/login/login.html').respond(200);
      $httpBackend.flush();
    });

    describe('Function Units Should not be resolve enums', function () {
      it('UserDetails should be defined', function () {
        expect(EnumsService).toBeDefined();
      });
      it('EventPlaceApiService should be defined', function () {
        expect($q).toBeDefined();
      });
      it('LoaderService should be defined', function () {
        expect(LoaderService).toBeDefined();
      });

      it('Should be reject Authorization State Enums', function () {
        var result = '',
          success = function (data) {
            result = data;
          },
          error = function (err) {
            result = err;
          };
        EnumsService.getAuthorizationStateEnums().then(success, error);
        $rootScope.$digest();
        expect(result).toEqual('ERROR');
      });
      it('Should be reject User Roles Enums', function () {
        var result = '',
          success = function (data) {
            result = data;
          },
          error = function (err) {
            result = err;
          };
        EnumsService.getUserRoleEnums().then(success, error);
        $rootScope.$digest();
        expect(result).toEqual('ERROR');
      });
      it('Should be reject Ep States Enums', function () {
        var result = '',
          success = function (data) {
            result = data;
          },
          error = function (err) {
            result = err;
          };
        EnumsService.getEpStateEnums().then(success, error);
        $rootScope.$digest();
        expect(result).toEqual('ERROR');
      });
      it('Should be reject Ep Types Enums', function () {
        var result = '',
          success = function (data) {
            result = data;
          },
          error = function (err) {
            result = err;
          };
        EnumsService.getEpTypeEnums().then(success, error);
        $rootScope.$digest();
        expect(result).toEqual('ERROR');
      });
      it('Should be reject Recurrence Type Enums', function () {
        var result = '',
          success = function (data) {
            result = data;
          },
          error = function (err) {
            result = err;
          };
        EnumsService.getRecurrenceTypeEnums().then(success, error);
        $rootScope.$digest();
        expect(result).toEqual('ERROR');
      });
      it('Should be reject Place Categories Enums', function () {
        var result = '',
          success = function (data) {
            result = data;
          },
          error = function (err) {
            result = err;
          };
        EnumsService.getPlaceCategoryEnums().then(success, error);
        $rootScope.$digest();
        expect(result).toEqual('ERROR');
      });
      it('Should be reject Event Categories Enums', function () {
        var result = null,
          success = function (data) {
            result = data;
          },
          error = function (err) {
            result = err;
          };
        EnumsService.getEventCategoryEnums().then(success, error);
        $rootScope.$digest();
        expect(result).toEqual('ERROR');
      });
      it('Should be reject Business Types Enums', function () {
        var result = '',
          success = function (data) {
            result = data;
          },
          error = function (err) {
            result = err;
          };
        EnumsService.getBusinessTypeEnums().then(success, error);
        $rootScope.$digest();
        expect(result).toEqual('ERROR');
      });
    });
  });
});
