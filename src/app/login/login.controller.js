'use strict';

(function (angular) {
  angular.module('kwadWeb')
    .controller('LoginCtrl', ['$rootScope', '$state', 'LoginApiService', 'AuthService', 'ToastrMessage', 'MessageService', 'CookieService',
      function ($rootScope, $state, LoginApiService, AuthService, ToastrMessage, MessageService, CookieService) {
        // Define variables
        var Login = this;
        Login.credentials = {
          username: '',
          password: ''
        };

        var _successLogin = function (data) {
          if (data.token) {
            CookieService.userSession.create(data.token);
            AuthService.getCurrentUser();
            $rootScope.isLoggedInUser = true;
            $state.go('domains');
          } else {
            $rootScope.isLoggedInUser = false;
            MessageService.showFailedMessage(ToastrMessage.LOGIN[data.message]);
          }
        };

        var _failLogin = function (error) {
          AuthService.setCurrentUser(null);
          $rootScope.isLoggedInUser = false;
          if (error.data && error.data.errors[0]) {
            MessageService.showFailedMessage(error.data.errors[0]);
          }
          else {
            MessageService.showFailedMessage();
          }
        };

        Login.login = function () {
          if (!Login.credentials.username) {
            MessageService.showFailedMessage(ToastrMessage.DATA_VALIDATE.EMPTY_USERNAME);
            return;
          }
          if (!Login.credentials.password) {
            MessageService.showFailedMessage(ToastrMessage.DATA_VALIDATE.EMPTY_PASSWORD);
            return;
          }
          var _payload = {
            email: Login.credentials.username,
            passwd: Login.credentials.password
          };
          LoginApiService.login(_payload).$promise.then(_successLogin, _failLogin);
        };
      }]);
})(angular);
