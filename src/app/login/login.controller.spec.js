'use strict';

describe('Unit: Login Controller', function () {
  var Login,
    $rootScope,
    $scope,
    $controller,
    $state,
    $q,
    $httpBackend,
    fakeResponse,
    CookieService,
    LoginApiService,
    AuthService,
    ToastrMessage,
    MessageService,
    LoaderService = {Loader: {}};

  beforeAll(function () {
    fakeResponse = {
      fakeLoggedInUser: {token: '562e38bc03ce722dce2065e3'}
    }
  });
  beforeEach(module('kwadWeb'));
  beforeEach(inject(function (_$rootScope_, _$controller_, _$q_, _$state_, _ToastrMessage_, _CookieService_, _$httpBackend_) {
    $rootScope = _$rootScope_;
    $scope = _$rootScope_.$new();
    $controller = _$controller_;
    $state = _$state_;
    $q = _$q_;
    $httpBackend = _$httpBackend_;
    CookieService = _CookieService_;
    LoginApiService = jasmine.createSpyObj('LoginApiService', ['login']);
    AuthService = jasmine.createSpyObj('AuthService', ['getCurrentUser', 'setCurrentUser']);
    ToastrMessage = _ToastrMessage_;
    MessageService = jasmine.createSpyObj('MessageService', ['showSuccessfulMessage', 'showFailedMessage']);
    LoaderService.Loader = jasmine.createSpyObj('LoaderService.Loader', ['hideSpinner', 'showSpinner']);
    MessageService.showSuccessfulMessage.and.callFake(function () {
    });
    MessageService.showFailedMessage.and.callFake(function () {
    });
    LoaderService.Loader.hideSpinner.and.callFake(function () {
    });
    LoaderService.Loader.showSpinner.and.callFake(function () {
    });
    AuthService.getCurrentUser.and.callFake(function () {
    });
    AuthService.setCurrentUser.and.callFake(function () {
    });
  }));
  beforeEach(function () {
    Login = $controller('LoginCtrl', {
      $scope: $scope,
      $state: $state,
      CookieService: CookieService,
      LoginApiService: LoginApiService,
      AuthService: AuthService,
      ToastrMessage: ToastrMessage,
      MessageService: MessageService,
      LoaderService: LoaderService
    });
    $httpBackend.expectGET('app/login/login.html').respond(200);
  });

  describe('Units Should be defined', function () {
    it('Login should be defined', function () {
      expect(Login).toBeDefined();
    });
    it('$state should be defined', function () {
      expect($state).toBeDefined();
    });
    it('CookieService should be defined', function () {
      expect(CookieService).toBeDefined();
    });
    it('LoginApiService should be defined', function () {
      expect(LoginApiService).toBeDefined();
    });
    it('ToastrMessage should be defined', function () {
      expect(ToastrMessage).toBeDefined();
    });
    it('MessageService should be defined', function () {
      expect(MessageService).toBeDefined();
    });
    it('LoaderService should be defined', function () {
      expect(LoaderService).toBeDefined();
    });
  });

  describe('Function Unit: Login.login', function () {
    it('Should be defined and be a function', function () {
      expect(Login.login).toBeDefined();
      expect(typeof Login.login).toEqual('function');
    });
    it('Should user be logged in', function () {
      $httpBackend.flush();
      $httpBackend.expectGET('app/domains/domains.html').respond(200);
      LoginApiService.login.and.callFake(function () {
        var deferred = $q.defer();
        deferred.resolve(fakeResponse.fakeLoggedInUser);
        deferred.$promise = deferred.promise;
        return deferred;
      });
      Login.credentials.username = 'sandeep.kumar1@tthnew.com';
      Login.credentials.password = '123456xxx';
      $rootScope.isLoggedInUser = false;
      Login.login();
      $rootScope.$digest();
      $httpBackend.flush();
      expect(LoginApiService.login).toHaveBeenCalled();
      expect($rootScope.isLoggedInUser).toEqual(true);
    });
    describe('Should user neo be logged in', function () {
      it('When username is blank', function () {
        Login.credentials.username = '';
        Login.login();
        $rootScope.$digest();
        $httpBackend.flush();
        expect(MessageService.showFailedMessage).toHaveBeenCalledWith(ToastrMessage.DATA_VALIDATE.EMPTY_USERNAME);
      });
      it('When password is blank', function () {
        Login.credentials.username = 'sandeep.kumar1@tthnew.com';
        Login.credentials.password = '';
        Login.login();
        $rootScope.$digest();
        $httpBackend.flush();
        expect(MessageService.showFailedMessage).toHaveBeenCalledWith(ToastrMessage.DATA_VALIDATE.EMPTY_PASSWORD);
      });
      it('When LoginApiService.login returns error message', function () {
        LoginApiService.login.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve({message: 'BLANK_EMAIL_FIELD'});
          deferred.$promise = deferred.promise;
          return deferred;
        });
        Login.credentials.username = 'sandeep.kumar1@tthnew.com';
        Login.credentials.password = '123456xxx';
        $rootScope.isLoggedInUser = false;
        Login.login();
        $rootScope.$digest();
        $httpBackend.flush();
        expect($rootScope.isLoggedInUser).toEqual(false);
      });

      it('When LoginApiService.login rejected and have data.errors', function () {
        LoginApiService.login.and.callFake(function () {
          var deferred = $q.defer();
          deferred.reject({data: {errors: ['500, Internal server error.']}});
          deferred.$promise = deferred.promise;
          return deferred;
        });
        Login.credentials.username = 'sandeep.kumar1@tthnew.com';
        Login.credentials.password = '123456xxx';
        $rootScope.isLoggedInUser = false;
        Login.login();
        $rootScope.$digest();
        $httpBackend.flush();
        expect($rootScope.isLoggedInUser).toEqual(false);
      });

      it('When LoginApiService.login rejected and error.data is null', function () {
        LoginApiService.login.and.callFake(function () {
          var deferred = $q.defer();
          deferred.reject({data: null});
          deferred.$promise = deferred.promise;
          return deferred;
        });
        Login.credentials.username = 'sandeep.kumar1@tthnew.com';
        Login.credentials.password = '123456xxx';
        $rootScope.isLoggedInUser = false;
        Login.login();
        $rootScope.$digest();
        $httpBackend.flush();
        expect($rootScope.isLoggedInUser).toEqual(false);
      });
    });
  });
});
