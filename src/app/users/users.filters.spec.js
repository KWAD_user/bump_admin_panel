'use strict';

describe('Unit: Users.filters.js', function () {
  var $rootScope, $filter, $httpBackend, fakeResponse;

  beforeAll(function () {
    fakeResponse = {
      fakeRoles: ['ANONYMOUS', 'REGULAR', 'LAUNCHSQUAD', 'ADMIN', 'BUSINESS', 'BUSINESS_INDIVIDUAL', 'EDITOR'],
      fakeAuthorizedStates: ['AWAITING_VERIFICATION', 'ENABLED', 'DISABLED', 'CLOSED']
    };
  });
  beforeEach(module('kwadWeb'));
  beforeEach(inject(function (_$rootScope_, _$filter_, _$httpBackend_) {
    $filter = _$filter_;
    $rootScope = _$rootScope_;
    $httpBackend = _$httpBackend_;
    $httpBackend.expectGET('app/login/login.html').respond(200);
  }));

  describe('Units should be defined', function () {
    it('$filter should be defined', function () {
      expect($filter).toBeDefined();
    })
  });

  describe('Filter Unit: regularUserRole', function () {
    it('Should be exist and be a function', function () {
      expect($filter('regularUserRole')).toBeDefined();
      expect(typeof $filter('regularUserRole')).toBe('function');
    });
    it('Should be filter user roles', function () {
      var result = $filter('regularUserRole')(fakeResponse.fakeRoles);
      $rootScope.$digest();
      expect(result.indexOf('ANONYMOUS')).toEqual(-1);
      expect(result.indexOf('BUSINESS')).toEqual(-1);
      expect(result.indexOf('BUSINESS_INDIVIDUAL')).toEqual(-1);
    })
  });

  describe('Filter Unit: isEditableRole', function () {
    it('Should be exist and be a function', function () {
      expect($filter('isEditableRole')).toBeDefined();
      expect(typeof $filter('isEditableRole')).toBe('function');
    });
    it('Should return false when role is either ANONYMOUS, BUSINESS_INDIVIDUAL or BUSINESS', function () {
      var isAnonymous = $filter('isEditableRole')('ANONYMOUS'),
        isBusinessIndividual = $filter('isEditableRole')('BUSINESS_INDIVIDUAL'),
        isBusiness = $filter('isEditableRole')('BUSINESS');
      $rootScope.$digest();
      expect(isAnonymous).toBe(false);
      expect(isBusinessIndividual).toBe(false);
      expect(isBusiness).toBe(false);
    });
    it('Should return true when role is neither ANONYMOUS, BUSINESS_INDIVIDUAL nor BUSINESS', function () {
      var isAdmin = $filter('isEditableRole')('ADMIN'),
        isEditor = $filter('isEditableRole')('EDITOR'),
        isRegular = $filter('isEditableRole')('REGULAR'),
        isBump = $filter('isEditableRole')('LAUNCHSQUAD');
      $rootScope.$digest();
      expect(isAdmin).toBe(true);
      expect(isEditor).toBe(true);
      expect(isRegular).toBe(true);
      expect(isBump).toBe(true);
    })
  });

  describe('Filter Unit: regularUserState', function () {
    it('Should be exist and be a function', function () {
      expect($filter('regularUserState')).toBeDefined();
      expect(typeof $filter('regularUserState')).toBe('function');
    });
    it('Should be filter user roles', function () {
      var result = $filter('regularUserState')(fakeResponse.fakeAuthorizedStates);
      $rootScope.$digest();
      expect(result.indexOf('AWAITING_VERIFICATION')).toEqual(-1);
    })
  });

  describe('Filter Unit: isEditableRole', function () {
    it('Should be exist and be a function', function () {
      expect($filter('isEditableState')).toBeDefined();
      expect(typeof $filter('isEditableState')).toBe('function');
    });
    it('Should return false when state is AWAITING_VERIFICATION', function () {
      var result = $filter('isEditableState')('AWAITING_VERIFICATION');
      $rootScope.$digest();
      expect(result).toBe(false);
    });
    it('Should return true when role is not AWAITING_VERIFICATION', function () {
      var isEnabled = $filter('isEditableState')('ENABLED'),
        isDisabled = $filter('isEditableState')('DISABLED'),
        isClosed = $filter('isEditableState')('CLOSED');
      $rootScope.$digest();
      expect(isEnabled).toBe(true);
      expect(isDisabled).toBe(true);
      expect(isClosed).toBe(true);
    })
  });
});