'use strict';

/**
 * @ngdoc function
 * @name kwadWeb.Filter:regularUserRole filter
 * @description
 * # used to filter user roles
 */
(function (angular) {
  angular.module('kwadWeb')
    .filter('regularUserRole', [function () {
      return function (input) {
        if (Array.isArray(input) && input.length) {
          input = input.filter(function (value) {
            return !!(value !== 'ANONYMOUS' && value !== 'BUSINESS' && value !== 'BUSINESS_INDIVIDUAL');
          });
        }
        return input;
      };
    }])
    .filter('isEditableRole', [function () {
      return function (input) {
        return !!(input !== 'ANONYMOUS' && input !== 'BUSINESS' && input !== 'BUSINESS_INDIVIDUAL');
      };
    }])
    .filter('isEditableState', [function () {
      return function (input) {
        return input !== 'AWAITING_VERIFICATION';
      };
    }])
    .filter('regularUserState', [function () {
      return function (input) {
        if (Array.isArray(input) && input.length) {
          input = input.filter(function (value) {
            return value !== 'AWAITING_VERIFICATION';
          });
        }
        return input;
      };
    }]);
})(angular);
