'use strict';

describe('Unit: Users Controller', function () {
  var Users, $scope, $rootScope, $q, $controller, $state, $httpBackend, $filter, $modal, $log, AuthService, UsersList, EnumsService, ToastrMessage, MessageService, DTOptionsBuilder, DTColumnDefBuilder, UserDetailsService, UsersApiService, AddEditUserAPIService, LoaderService, fakeResponse;

  beforeAll(function () {
    UsersList = {
      REGULAR: [{
        'id': '56557c27e4b03b2a581a6963',
        'email': 'sk@tothenew.com',
        'role': 'REGULAR',
        'status': 'DISABLED',
        'registrationDate': '2015-11-25T09:15:19.872+0000',
        'contactNumber': '2213234232',
        'businessName': null,
        'bPlaces': 0,
        'type': null,
        'contactNames': [],
        'address': null,
        'website': null,
        'notes': null,
        'associatedBusinessId': null,
        'associatedBusinessName': null,
        'firstName': 'Ssss1',
        'lastName': 'Ssss',
        'friends': 0,
        'sessions': 0,
        'events': 0,
        'places': 0
      }, {
        'id': '56557c84e4b03b2a581a69ba',
        'email': 'dk@tothenew.com',
        'role': 'REGULAR',
        'status': 'DISABLED',
        'registrationDate': '2015-11-25T09:16:52.099+0000',
        'contactNumber': '2389432843',
        'businessName': null,
        'bPlaces': 0,
        'type': null,
        'contactNames': [],
        'address': null,
        'website': null,
        'notes': null,
        'associatedBusinessId': null,
        'associatedBusinessName': null,
        'firstName': 'Ddddd',
        'lastName': 'Ddddd',
        'friends': 0,
        'sessions': 0,
        'events': 0,
        'places': 0
      }],
      LAUNCHSQUAD: [{
        'id': '54d5843ae4b0e8031d901a4c',
        'email': 'nemmert1@asu.edu',
        'role': 'LAUNCHSQUAD',
        'status': 'ENABLED',
        'registrationDate': '2015-07-09T10:25:15.277+0000',
        'contactNumber': null,
        'businessName': null,
        'bPlaces': 0,
        'type': null,
        'contactNames': [],
        'address': null,
        'website': null,
        'notes': null,
        'associatedBusinessId': null,
        'associatedBusinessName': null,
        'firstName': 'Natalie',
        'lastName': 'Emmert',
        'friends': 2,
        'sessions': 0,
        'events': 12,
        'places': 0
      }, {
        'id': '54bed6e2e4b026f0ebf5ab6b',
        'email': 'rfishersmith@ucdavis.edu',
        'role': 'LAUNCHSQUAD',
        'status': 'ENABLED',
        'registrationDate': '2015-07-09T10:25:15.277+0000',
        'contactNumber': null,
        'businessName': null,
        'bPlaces': 0,
        'type': null,
        'contactNames': [],
        'address': null,
        'website': null,
        'notes': null,
        'associatedBusinessId': null,
        'associatedBusinessName': null,
        'firstName': 'RFisher',
        'lastName': 'Smith',
        'friends': 5,
        'sessions': 0,
        'events': 10,
        'places': 5
      }],
      ADMIN: [{
        'id': '546aff58e4b0370c32328f40',
        'email': 'demo1@whatsbumpin.com',
        'role': 'ADMIN',
        'status': 'ENABLED',
        'registrationDate': '2015-07-09T10:25:15.277+0000',
        'contactNumber': null,
        'businessName': null,
        'bPlaces': 0,
        'type': null,
        'contactNames': [],
        'address': null,
        'website': null,
        'notes': null,
        'associatedBusinessId': null,
        'associatedBusinessName': null,
        'firstName': 'Demo ',
        'lastName': 'User ',
        'friends': 0,
        'sessions': 0,
        'events': 10,
        'places': 320
      }, {
        'id': '54cab8a8e4b0b752858628c7',
        'email': 'dschisler@ucdavis.edu',
        'role': 'ADMIN',
        'status': 'ENABLED',
        'registrationDate': '2015-07-09T10:25:15.277+0000',
        'contactNumber': null,
        'businessName': null,
        'bPlaces': 0,
        'type': null,
        'contactNames': [],
        'address': null,
        'website': null,
        'notes': null,
        'associatedBusinessId': null,
        'associatedBusinessName': null,
        'firstName': 'Darren',
        'lastName': 'Schisler',
        'friends': 1,
        'sessions': 0,
        'events': 0,
        'places': 0
      }],
      BUSINESS: [{
        'id': '56497837e4b0290cecbedfca',
        'email': 'mycar@gmail.com',
        'role': 'BUSINESS',
        'status': 'DISABLED',
        'registrationDate': '2015-11-16T06:31:19.968+0000',
        'contactNumber': '1234567890',
        'businessName': 'mycar',
        'bPlaces': 0,
        'type': 'PROFIT',
        'contactNames': ['Sandy Chhapola'],
        'address': 'mycar, delhi',
        'website': 'www.mycar.com',
        'notes': null,
        'associatedBusinessId': null,
        'associatedBusinessName': null,
        'firstName': null,
        'lastName': null,
        'friends': 0,
        'sessions': 0,
        'events': 0,
        'places': 0
      }, {
        'id': '56497f6fe4b0290cecc0d1bf',
        'email': 'ss@ss.ss',
        'role': 'BUSINESS',
        'status': 'ENABLED',
        'registrationDate': '2015-11-16T07:02:07.994+0000',
        'contactNumber': '',
        'businessName': 'ss',
        'bPlaces': 0,
        'type': 'UNIVERSITY',
        'contactNames': ['ss'],
        'address': 'ss',
        'website': 'ss',
        'notes': null,
        'associatedBusinessId': null,
        'associatedBusinessName': null,
        'firstName': null,
        'lastName': null,
        'friends': 0,
        'sessions': 0,
        'events': 0,
        'places': 0
      }],
      EDITOR: [{
        'id': '54c6bf04e4b0b2818191f8cf',
        'email': 'jusa8337@cc.peralta.edu',
        'role': 'EDITOR',
        'status': 'CLOSED',
        'registrationDate': '2015-07-09T10:25:15.277+0000',
        'contactNumber': null,
        'businessName': null,
        'bPlaces': 0,
        'type': null,
        'contactNames': [],
        'address': null,
        'website': null,
        'notes': null,
        'associatedBusinessId': null,
        'associatedBusinessName': null,
        'firstName': 'Sam',
        'lastName': 'singh',
        'friends': 6,
        'sessions': 0,
        'events': 63,
        'places': 12
      }, {
        'id': '54dc32d2e4b065d7c43bb77e',
        'email': 'schang4@berkeley.edu',
        'role': 'EDITOR',
        'status': 'ENABLED',
        'registrationDate': '2015-07-09T10:25:15.277+0000',
        'contactNumber': null,
        'businessName': null,
        'bPlaces': 0,
        'type': null,
        'contactNames': [],
        'address': null,
        'website': null,
        'notes': null,
        'associatedBusinessId': null,
        'associatedBusinessName': null,
        'firstName': 'qq',
        'lastName': 'aa',
        'friends': 0,
        'sessions': 0,
        'events': 28,
        'places': 30
      }]
    };
    fakeResponse = {
      fakeAdminUser: {
        'id': '546aff58e4b0370c32328f40',
        'email': 'demo1@whatsbumpin.com',
        'role': 'ADMIN',
        'status': 'ENABLED',
        'registrationDate': '2015-07-09T10:25:15.277+0000',
        'contactNumber': null,
        'businessName': null,
        'bPlaces': 0,
        'type': null,
        'contactNames': [],
        'address': null,
        'website': null,
        'notes': null,
        'associatedBusinessId': null,
        'associatedBusinessName': null,
        'firstName': 'Demo ',
        'lastName': 'User ',
        'friends': 0,
        'sessions': 0,
        'events': 10,
        'places': 320
      },
      fakeBumpUser: {
        'id': '54d5843ae4b0e8031d901a4c',
        'email': 'nemmert1@asu.edu',
        'role': 'LAUNCHSQUAD',
        'status': 'ENABLED',
        'registrationDate': '2015-07-09T10:25:15.277+0000',
        'contactNumber': null,
        'businessName': null,
        'bPlaces': 0,
        'type': null,
        'contactNames': [],
        'address': null,
        'website': null,
        'notes': null,
        'associatedBusinessId': null,
        'associatedBusinessName': null,
        'firstName': 'Natalie',
        'lastName': 'Emmert',
        'friends': 2,
        'sessions': 0,
        'events': 12,
        'places': 0
      },
      fakeBusinessUser: {
        'id': '56497837e4b0290cecbedfca',
        'email': 'mycar@gmail.com',
        'role': 'BUSINESS',
        'status': 'DISABLED',
        'registrationDate': '2015-11-16T06:31:19.968+0000',
        'contactNumber': '1234567890',
        'businessName': 'mycar',
        'bPlaces': 0,
        'businessType': 'PROFIT',
        'contactNames': ['Sandy Chhapola'],
        'address': 'mycar, delhi',
        'website': 'www.mycar.com',
        'notes': null,
        'associatedBusinessId': null,
        'associatedBusinessName': null,
        'firstName': null,
        'lastName': null,
        'friends': 0,
        'sessions': 0,
        'events': 0,
        'places': 0
      },
      fakeBusinessIndividualUser: {
        'id': '56497837e4b0290cecbedfca',
        'email': 'mycar@gmail.com',
        'role': 'BUSINESS_Individual',
        'status': 'DISABLED',
        'registrationDate': '2015-11-16T06:31:19.968+0000',
        'address': 'mycar, delhi',
        'firstName': null,
        'lastName': null,
        'friends': 0,
        'sessions': 0,
        'events': 0,
        'places': 0
      },
      fakeUserRoles: ['ANONYMOUS', 'REGULAR', 'LAUNCHSQUAD', 'ADMIN', 'BUSINESS', 'BUSINESS_INDIVIDUAL', 'EDITOR'],
      fakeUserRolesObj: {
        ANONYMOUS: 'ANONYMOUS',
        REGULAR: 'REGULAR',
        LAUNCHSQUAD: 'LAUNCHSQUAD',
        ADMIN: 'ADMIN',
        BUSINESS: 'BUSINESS',
        BUSINESS_INDIVIDUAL: 'BUSINESS_INDIVIDUAL',
        EDITOR: 'EDITOR'
      },
      fakeAuthorizedStates: ['ENABLED', 'DISABLED', 'AWAITING_VERIFICATION', 'CLOSED']
    };
  });
  beforeEach(module('kwadWeb'));
  beforeEach(inject(function (_$rootScope_, _$controller_, _$q_, _$log_, _$httpBackend_, _$filter_, _UserDetailsService_, _DTOptionsBuilder_, _DTColumnDefBuilder_, _ToastrMessage_) {
    $q = _$q_;
    $rootScope = _$rootScope_;
    $scope = _$rootScope_.$new();
    $httpBackend = _$httpBackend_;
    $controller = _$controller_;
    $filter = _$filter_;
    $log = _$log_;
    DTOptionsBuilder = _DTOptionsBuilder_;
    DTColumnDefBuilder = _DTColumnDefBuilder_;
    UsersApiService = jasmine.createSpyObj('UsersApiService', ['getUsers']);
    AddEditUserAPIService = jasmine.createSpyObj('AddEditUserAPIService', ['addEditUser']);
    AuthService = jasmine.createSpyObj('AuthService', ['getCurrentUser']);
    UserDetailsService = _UserDetailsService_;
    EnumsService = jasmine.createSpyObj('EnumsService', ['getUserRoleEnums', 'getAuthorizationStateEnums']);
    $state = {};
    $state = jasmine.createSpyObj('$state', ['go']);
    $state.current = {};
    $modal = jasmine.createSpyObj('$modal', ['open']);
    MessageService = jasmine.createSpyObj('MessageService', ['showSuccessfulMessage', 'showFailedMessage']);
    LoaderService = {};
    LoaderService.Loader = jasmine.createSpyObj('LoaderService.Loader', ['showSpinner', 'hideSpinner']);
    ToastrMessage = _ToastrMessage_;
    MessageService.showSuccessfulMessage.and.callFake(function () {
    });
    MessageService.showFailedMessage.and.callFake(function () {
    });
    LoaderService.Loader.showSpinner.and.callFake(function () {
    });
    LoaderService.Loader.hideSpinner.and.callFake(function () {
    });
    $state.go.and.callFake(function () {
    });
  }));
  beforeEach(function () {
    Users = $controller('UsersCtrl', {
      $scope: $scope,
      $state: $state,
      EnumsService: EnumsService,
      ToastrMessage: ToastrMessage,
      AuthService: AuthService,
      MessageService: MessageService,
      $filter: $filter,
      $modal: $modal,
      $log: $log,
      DTOptionsBuilder: DTOptionsBuilder,
      DTColumnDefBuilder: DTColumnDefBuilder,
      UserDetailsService: UserDetailsService,
      UsersApiService: UsersApiService,
      AddEditUserAPIService: AddEditUserAPIService,
      LoaderService: LoaderService
    });
    $httpBackend.expectGET('app/login/login.html')
      .respond(200);
  });

  describe('Units Should be defined', function () {
    it('Users should be defined', function () {
      expect(Users).toBeDefined();
    });
    it('$scope should be defined', function () {
      expect($scope).toBeDefined();
    });
    it('$state should be defined', function () {
      expect($state).toBeDefined();
    });
    it('EnumsService should be defined', function () {
      expect(EnumsService).toBeDefined();
    });
    it('ToastrMessage should be defined', function () {
      expect(ToastrMessage).toBeDefined();
    });
    it('MessageService should be defined', function () {
      expect(MessageService).toBeDefined();
    });
    it('$filter should be defined', function () {
      expect($filter).toBeDefined();
    });
    it('$modal should be defined', function () {
      expect($modal).toBeDefined();
    });
    it('$log should be defined', function () {
      expect($log).toBeDefined();
    });
    it('DTOptionsBuilder should be defined', function () {
      expect(DTOptionsBuilder).toBeDefined();
    });
    it('DTColumnDefBuilder should be defined', function () {
      expect(DTColumnDefBuilder).toBeDefined();
    });
    it('UserDetailsService should be defined', function () {
      expect(UserDetailsService).toBeDefined();
    });
    it('UsersApiService should be defined', function () {
      expect(UsersApiService).toBeDefined();
    });
    it('AddEditUserAPIService should be defined', function () {
      expect(AddEditUserAPIService).toBeDefined();
    });
    it('LoaderService should be defined', function () {
      expect(LoaderService).toBeDefined();
    });
  });

  describe('Function Unit: Users.changeCurrentFilter', function () {
    it('Should be defined and be a function', function () {
      expect(Users.changeCurrentFilter).toBeDefined();
      expect(typeof Users.changeCurrentFilter).toEqual('function');
    });
    it('Should change active class', function () {
      var triggeredEvent = {target: '<a class=\'active\' ui-sref=\'users.regular\'>Regular</a>'};
      Users.changeCurrentFilter(triggeredEvent);
      $rootScope.$digest();
    });
  });

  describe('Function Unit: Users.init', function () {
    it('Should be defined and be a function', function () {
      expect(Users.init).toBeDefined();
      expect(typeof Users.init).toEqual('function');
    });
    describe('When Success Callbacks called', function () {
      beforeEach(function () {
        UsersApiService.getUsers.and.callFake(function (payload) {
          var deferred = $q.defer();
          var response = {
            data: UsersList[payload.role]
          };
          deferred.resolve(response);
          deferred.$promise = deferred.promise;
          return deferred;
        });
        EnumsService.getUserRoleEnums.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve(fakeResponse.fakeUserRoles);
          return deferred.promise;
        });
        EnumsService.getAuthorizationStateEnums.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve(fakeResponse.fakeAuthorizedStates);
          return deferred.promise;
        });
      });
      describe('when current user role admin', function () {
        beforeEach(function () {
          AuthService.getCurrentUser.and.callFake(function () {
            var deferred = $q.defer();
            deferred.resolve(fakeResponse.fakeAdminUser);
            return deferred.promise;
          });
        });
        it('Should not fetch user list data when state is : users ', function () {
          $state.current.name = 'users';
          Users.init();
          $httpBackend.flush();
          $rootScope.$digest();
          expect(Users.filterByRole).toEqual('');
          expect(Users.userList).toEqual(null);
          expect(Users.AUTHORIZATION_STATE.length).toEqual(4);
          expect(Users.USER_ROLES.length).toEqual(7);
        });
        it('Should fetch user list data when state is : users.regular ', function () {
          $state.current.name = 'users.regular';
          Users.init();
          $rootScope.$digest();
          $httpBackend.flush();
          expect(Users.filterByRole).toEqual('REGULAR');
          expect(Users.userList.length).toBeGreaterThan(0);
          expect(Users.AUTHORIZATION_STATE.length).toEqual(4);
          expect(Users.USER_ROLES.length).toEqual(7);
        });
        it('Should fetch user list data when state is : users.admin ', function () {
          $state.current.name = 'users.admin';
          Users.init();
          $rootScope.$digest();
          $httpBackend.flush();
          expect(Users.filterByRole).toEqual('ADMIN');
          expect(Users.userList.length).toBeGreaterThan(0);
          expect(Users.AUTHORIZATION_STATE.length).toEqual(4);
          expect(Users.USER_ROLES.length).toEqual(7);
        });
        it('Should fetch user list data when state is : users.bump ', function () {
          $state.current.name = 'users.bump';
          Users.init();
          $rootScope.$digest();
          $httpBackend.flush();
          expect(Users.filterByRole).toEqual('LAUNCHSQUAD');
          expect(Users.userList.length).toBeGreaterThan(0);
          expect(Users.AUTHORIZATION_STATE.length).toEqual(4);
          expect(Users.USER_ROLES.length).toEqual(7);
        });
        it('Should fetch user list data state is : users.business ', function () {
          $state.current.name = 'users.business';
          Users.init();
          $rootScope.$digest();
          $httpBackend.flush();
          expect(Users.filterByRole).toEqual('BUSINESS');
          expect(Users.userList.length).toBeGreaterThan(0);
          expect(Users.AUTHORIZATION_STATE.length).toEqual(4);
          expect(Users.USER_ROLES.length).toEqual(7);
        });
        it('Should fetch user list data when state is : users.editor ', function () {
          $state.current.name = 'users.editor';
          Users.init();
          $rootScope.$digest();
          $httpBackend.flush();
          expect(Users.filterByRole).toEqual('EDITOR');
          expect(Users.userList.length).toBeGreaterThan(0);
          expect(Users.AUTHORIZATION_STATE.length).toEqual(4);
          expect(Users.USER_ROLES.length).toEqual(7);
        });
        it('Should be passed when userList is not null and role equals to current role.', function () {
          $state.current.name = 'users.regular';
          Users.userList = UsersList.REGULAR;
          Users.init();
          $rootScope.$digest();
          $httpBackend.flush();
          expect(LoaderService.Loader.hideSpinner).toHaveBeenCalled();
        });
      });
      describe('when current user role LAUNCHSQUAD', function () {
        beforeEach(function () {
          AuthService.getCurrentUser.and.callFake(function () {
            var deferred = $q.defer();
            deferred.resolve(fakeResponse.fakeBumpUser);
            return deferred.promise;
          });
        });
        it('Should not fetch user list data when state is : users ', function () {
          $state.current.name = 'users';
          Users.init();
          $httpBackend.flush();
          $rootScope.$digest();
          expect(Users.filterByRole).toEqual('');
          expect(Users.userList).toEqual(null);
          expect(Users.AUTHORIZATION_STATE.length).toEqual(4);
          expect(Users.USER_ROLES.length).toEqual(7);
        });
        it('Should fetch user list data when state is : users.regular ', function () {
          $state.current.name = 'users.regular';
          Users.init();
          $rootScope.$digest();
          $httpBackend.flush();
          expect(Users.filterByRole).toEqual('REGULAR');
          expect(Users.userList).toEqual(null);
        });
        it('Should fetch user list data when state is : users.admin ', function () {
          $state.current.name = 'users.admin';
          Users.init();
          $rootScope.$digest();
          $httpBackend.flush();
          expect(Users.filterByRole).toEqual('ADMIN');
          expect(Users.userList).toEqual(null);
        });
        it('Should fetch user list data when state is : users.bump ', function () {
          $state.current.name = 'users.bump';
          Users.init();
          $rootScope.$digest();
          $httpBackend.flush();
          expect(Users.filterByRole).toEqual('LAUNCHSQUAD');
          expect(Users.userList.length).toBeGreaterThan(0);
        });
        it('Should fetch user list data state is : users.business ', function () {
          $state.current.name = 'users.business';
          Users.init();
          $rootScope.$digest();
          $httpBackend.flush();
          expect(Users.filterByRole).toEqual('BUSINESS');
          expect(Users.userList).toEqual(null);
        });
        it('Should fetch user list data when state is : users.editor ', function () {
          $state.current.name = 'users.editor';
          Users.init();
          $rootScope.$digest();
          $httpBackend.flush();
          expect(Users.filterByRole).toEqual('EDITOR');
          expect(Users.userList).toEqual(null);
        });
      });
      describe('when current user role BUSINESS', function () {
        beforeEach(function () {
          AuthService.getCurrentUser.and.callFake(function () {
            var deferred = $q.defer();
            deferred.resolve(fakeResponse.fakeBusinessUser);
            return deferred.promise;
          });
        });
        it('Should not fetch user list data when state is : users ', function () {
          $state.current.name = 'users';
          Users.init();
          $httpBackend.flush();
          $rootScope.$digest();
          expect(Users.filterByRole).toEqual('');
          expect(Users.userList).toEqual(null);
        });
        it('Should fetch user list data when state is : users.regular ', function () {
          $state.current.name = 'users.regular';
          Users.init();
          $rootScope.$digest();
          $httpBackend.flush();
          expect(Users.filterByRole).toEqual('REGULAR');
          expect(Users.userList).toEqual(null);
        });
        it('Should fetch user list data when state is : users.admin ', function () {
          $state.current.name = 'users.admin';
          Users.init();
          $rootScope.$digest();
          $httpBackend.flush();
          expect(Users.filterByRole).toEqual('ADMIN');
          expect(Users.userList).toEqual(null);
        });
        it('Should fetch user list data when state is : users.bump ', function () {
          $state.current.name = 'users.bump';
          Users.init();
          $rootScope.$digest();
          $httpBackend.flush();
          expect(Users.filterByRole).toEqual('LAUNCHSQUAD');
          expect(Users.userList).toEqual(null);
        });
        it('Should fetch user list data state is : users.business ', function () {
          $state.current.name = 'users.business';
          Users.init();
          $rootScope.$digest();
          $httpBackend.flush();
          expect(Users.filterByRole).toEqual('BUSINESS');
          expect(Users.userList.length).toBeGreaterThan(0);
        });
        it('Should fetch user list data when state is : users.editor ', function () {
          $state.current.name = 'users.editor';
          Users.init();
          $rootScope.$digest();
          $httpBackend.flush();
          expect(Users.filterByRole).toEqual('EDITOR');
          expect(Users.userList).toEqual(null);
        });
      });
      describe('when current user role BUSINESS_INDIVIDUAL Should not fetch user list data', function () {
        beforeEach(function () {
          AuthService.getCurrentUser.and.callFake(function () {
            var deferred = $q.defer();
            deferred.resolve(fakeResponse.fakeBusinessIndividualUser);
            return deferred.promise;
          });
        });
        it('when state is : users ', function () {
          $state.current.name = 'users';
          Users.init();
          $httpBackend.flush();
          $rootScope.$digest();
          expect(Users.filterByRole).toEqual('');
          expect(Users.userList).toEqual(null);
        });
        it('when state is : users.regular ', function () {
          $state.current.name = 'users.regular';
          Users.init();
          $rootScope.$digest();
          $httpBackend.flush();
          expect(Users.filterByRole).toEqual('REGULAR');
          expect(Users.userList).toEqual(null);
        });
        it('when state is : users.admin ', function () {
          $state.current.name = 'users.admin';
          Users.init();
          $rootScope.$digest();
          $httpBackend.flush();
          expect(Users.filterByRole).toEqual('ADMIN');
          expect(Users.userList).toEqual(null);
        });
        it('when state is : users.bump ', function () {
          $state.current.name = 'users.bump';
          Users.init();
          $rootScope.$digest();
          $httpBackend.flush();
          expect(Users.filterByRole).toEqual('LAUNCHSQUAD');
          expect(Users.userList).toEqual(null);
        });
        it('when state is : users.business ', function () {
          $state.current.name = 'users.business';
          Users.init();
          $rootScope.$digest();
          $httpBackend.flush();
          expect(Users.filterByRole).toEqual('BUSINESS');
          expect(Users.userList).toEqual(null);
        });
        it('when state is : users.editor ', function () {
          $state.current.name = 'users.editor';
          Users.init();
          $rootScope.$digest();
          $httpBackend.flush();
          expect(Users.filterByRole).toEqual('EDITOR');
          expect(Users.userList).toEqual(null);
        });
      });
    });
    describe('When Failure Callbacks called', function () {
      beforeEach(function () {
        UsersApiService.getUsers.and.callFake(function () {
          var deferred = $q.defer();
          deferred.reject();
          deferred.$promise = deferred.promise;
          return deferred;
        });
        EnumsService.getAuthorizationStateEnums.and.callFake(function () {
          var deferred = $q.defer();
          deferred.reject();
          return deferred.promise;
        });
      });
      it('Users.userList and Users.AUTHORIZATION_STATE should be null', function () {
        AuthService.getCurrentUser.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve(fakeResponse.fakeAdminUser);
          return deferred.promise;
        });
        EnumsService.getUserRoleEnums.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve(fakeResponse.fakeUserRoles);
          return deferred.promise;
        });
        $state.current.name = 'users.admin';
        Users.init();
        $rootScope.$digest();
        $httpBackend.flush();
        expect(Users.userList).toEqual(null);
        expect(Users.AUTHORIZATION_STATE).toEqual(null);
      });
      it('Users.userList and Users.currentUser should be null', function () {
        AuthService.getCurrentUser.and.callFake(function () {
          var deferred = $q.defer();
          deferred.reject();
          return deferred.promise;
        });
        EnumsService.getUserRoleEnums.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve(fakeResponse.fakeUserRoles);
          return deferred.promise;
        });
        $state.current.name = 'users.admin';
        Users.init();
        $rootScope.$digest();
        $httpBackend.flush();
        expect(Users.userList).toEqual(null);
        expect(Users.currentUser).toEqual(null);
      });
      it('Users.USER_ROLES Should be null', function () {
        EnumsService.getUserRoleEnums.and.callFake(function () {
          var deferred = $q.defer();
          deferred.reject();
          return deferred.promise;
        });
        Users.init();
        $rootScope.$digest();
        $httpBackend.flush();
        expect(Users.USER_ROLES).toEqual(null);
      });
    });
  });

  describe('Function Unit: Users.saveUserDetails', function () {
    it('Should be defined and be a function', function () {
      expect(Users.saveUserDetails).toBeDefined();
      expect(typeof  Users.saveUserDetails).toEqual('function');
    });
    it('Should update user info', function () {
      UserDetailsService.setUsersList(angular.copy(UsersList.ADMIN));
      AddEditUserAPIService.addEditUser.and.callFake(function () {
        var deferred = $q.defer();
        deferred.resolve({status: 'SUCCESS', data: fakeResponse.fakeAdminUser});
        deferred.$promise = deferred.promise;
        return deferred;
      });
      var index = 0, data = {
        id: '546aff58e4b0370c32328f40',
        firstName: 'demo1',
        lastName: 'User',
        email: 'demo1@whatsbumpin.com',
        role: 'ADMIN',
        state: 'ENABLED'
      };
      Users.saveUserDetails(0, data);
      $rootScope.$digest();
      expect(MessageService.showSuccessfulMessage).toHaveBeenCalledWith(ToastrMessage.USER.UPDATE.SUCCESS)
    });
    describe('Should not update user info', function () {
      it('when respond 200 with error status', function () {
        UserDetailsService.setUsersList(angular.copy(UsersList.ADMIN));
        AddEditUserAPIService.addEditUser.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve({status: 'INSUFFICIENT_RIGHTS'});
          deferred.$promise = deferred.promise;
          return deferred;
        });
        var index = 0, data = {
          id: '546aff58e4b0370c32328f40',
          firstName: 'demo1',
          lastName: 'User',
          email: 'demo1@whatsbumpin.com',
          role: 'ADMIN',
          state: 'ENABLED'
        };
        Users.saveUserDetails(0, data);
        $rootScope.$digest();
        expect(MessageService.showFailedMessage).toHaveBeenCalledWith(ToastrMessage.USER.UPDATE.INSUFFICIENT_RIGHTS)
      });
      it('When AddEditUserAPIService.addEditUser rejected', function () {
        UserDetailsService.setUsersList(angular.copy(UsersList.ADMIN));
        AddEditUserAPIService.addEditUser.and.callFake(function () {
          var deferred = $q.defer();
          deferred.reject();
          deferred.$promise = deferred.promise;
          return deferred;
        });
        var index = 0, data = {
          id: '546aff58e4b0370c32328f40',
          firstName: 'demo1',
          lastName: 'User',
          email: 'demo1@whatsbumpin.com',
          role: 'ADMIN',
          state: 'ENABLED'
        };
        Users.saveUserDetails(0, data);
        $rootScope.$digest();
        expect(MessageService.showFailedMessage).toHaveBeenCalledWith()
      });
    });
  });

  describe('Function Unit: Users.editUserDetails', function () {
    it('Should be defined and be a function', function () {
      expect(Users.editUserDetails).toBeDefined();
      expect(typeof  Users.editUserDetails).toEqual('function');
    });
    it('Should send to userDetails page when role is other than BUSINESS', function () {
      var index = 0, data = {
        id: '546aff58e4b0370c32328f40',
        firstName: 'demo1',
        lastName: 'User',
        email: 'demo1@whatsbumpin.com',
        role: 'ADMIN',
        state: 'ENABLED'
      };
      Users.editUserDetails(0, data);
      $rootScope.$digest();
      expect($state.go).toHaveBeenCalledWith('users.userdetails', {userId: data.id})
    });
    it('Should send to userDetails page when role is other than BUSINESS', function () {
      var index = 0, data = {
        'id': '56497837e4b0290cecbedfca',
        'email': 'mycar@gmail.com',
        'role': 'BUSINESS',
        'status': 'DISABLED',
        'registrationDate': '2015-11-16T06:31:19.968+0000',
        'type': 'NON_PROFIT',
        'contactNames': ['Sandy Chhapola'],
        'address': 'mycar, delhi',
        'website': 'www.mycar.com'
      };
      Users.editUserDetails(0, data);
      $rootScope.$digest();
      expect($state.go).toHaveBeenCalledWith('users.businessdetails', {userId: data.id});
    });
  });

  describe('Function Unit: Users.addUser', function () {
    it('Should be defined and be a function', function () {
      expect(Users.addUser).toBeDefined();
      expect(typeof  Users.addUser).toEqual('function');
    });
    it('Should not invoke $modal.open', function () {
      Users.currentUser = {role: 'BUSINESS'};
      Users.USER_ROLES_OBJ = fakeResponse.fakeUserRolesObj;
      $modal.open.and.callFake(function (obj) {
        var deferred = $q.defer();
        obj.resolve.UserRoles();
        deferred.resolve(UsersList.ADMIN[0]);
        return {
          result: deferred.promise
        };
      });
      Users.addUser();
      $rootScope.$digest();
      expect($modal.open).not.toHaveBeenCalled()
    });
    describe('Should add a user if Users.currentUser.role is ADMIN', function () {
      it('when $modal.open invoked and clicked Ok', function () {
        Users.currentUser = {role: 'ADMIN'};
        Users.USER_ROLES_OBJ = fakeResponse.fakeUserRolesObj;
        $modal.open.and.callFake(function (obj) {
          var deferred = $q.defer();
          obj.resolve.UserRoles();
          deferred.resolve(UsersList.ADMIN[0]);
          return {
            result: deferred.promise
          };
        });
        Users.addUser();
        $rootScope.$digest();
        expect($modal.open).toHaveBeenCalled()
      });
      it('when $modal.open invoked and clicked Cancel', function () {
        Users.currentUser = {role: 'ADMIN'};
        Users.USER_ROLES_OBJ = fakeResponse.fakeUserRolesObj;
        $modal.open.and.callFake(function (obj) {
          var deferred = $q.defer();
          obj.resolve.UserRoles();
          deferred.reject();
          return {
            result: deferred.promise
          };
        });
        Users.addUser();
        $rootScope.$digest();
        expect($modal.open).toHaveBeenCalled()
      });
    });
    describe('Should add a Business user if Users.currentUser.role is EDITOR', function () {
      it('when $modal.open invoked and clicked Ok', function () {
        Users.currentUser = {role: 'EDITOR'};
        Users.USER_ROLES_OBJ = fakeResponse.fakeUserRolesObj;
        $modal.open.and.callFake(function (obj) {
          var deferred = $q.defer();
          obj.resolve.UserRoles();
          deferred.resolve(UsersList.BUSINESS[0]);
          return {
            result: deferred.promise
          };
        });
        Users.addUser();
        $rootScope.$digest();
        expect($modal.open).toHaveBeenCalled()
      });
      it('when $modal.open invoked and clicked Cancel', function () {
        Users.currentUser = {role: 'EDITOR'};
        Users.USER_ROLES_OBJ = fakeResponse.fakeUserRolesObj;
        $modal.open.and.callFake(function (obj) {
          var deferred = $q.defer();
          obj.resolve.UserRoles();
          deferred.reject();
          return {
            result: deferred.promise
          };
        });
        Users.addUser();
        $rootScope.$digest();
        expect($modal.open).toHaveBeenCalled()
      });
    });
  });
});