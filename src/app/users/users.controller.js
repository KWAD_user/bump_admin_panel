'use strict';

(function (angular) {
	angular.module('kwadWeb')
			.controller('UsersCtrl', ['$state', '$scope', '$filter', '$compile', '$modal', '$log', 'UserDetailsApiService', 'AuthService', 'DTOptionsBuilder', 'DTColumnBuilder', 'EnumsService', 'UserDetailsService', 'UsersApiService', 'AddEditUserAPIService', 'MessageService', 'ToastrMessage', 'LoaderService', 'HOST', 'CookieService',
				function ($state, $scope, $filter, $compile, $modal, $log, UserDetailsApiService, AuthService, DTOptionsBuilder, DTColumnBuilder, EnumsService, UserDetailsService, UsersApiService, AddEditUserAPIService, MessageService, ToastrMessage, LoaderService, HOST, CookieService) {
					// Define variables
					var Users = this,
							AUTHORIZATION_STATE_OBJ = null,
							currentFilter = UserDetailsService.getCurrentUserFilter() || 'REGULAR';
					Users.USER_ROLES_OBJ = null;
					Users.filterByRole = '';
					Users.USER_ROLES = null;
					Users.ALLOWED_USER_ROLES = null;
					Users.AUTHORIZATION_STATE = null;
					Users.ALLOWED_AUTHORIZATION_STATE = null;
					Users.viewName = 'Users';
					Users.isMoreButtonClicked = false;
					Users.currentUser = null;
					Users.isAuthorisedUser = false;
					Users.dtInstance = {};
					Users.showDT = false; //fix for only showing table when other data is ready

					Users.userList = UserDetailsService.getUsersList() || null;

					var userToken = CookieService.userSession.get();

					var changeUserFilter = function () {
						var _name = $state.current.name;
						switch (_name) {
							case 'users.admin':
								Users.filterByRole = Users.USER_ROLES_OBJ.ADMIN;
								break;
							case 'users.bump':
								Users.filterByRole = Users.USER_ROLES_OBJ.LAUNCHSQUAD;
								break;
							case 'users.business':
								Users.filterByRole = Users.USER_ROLES_OBJ.BUSINESS;
								break;
							case 'users.editor':
								Users.filterByRole = Users.USER_ROLES_OBJ.EDITOR;
								break;
							case 'users.regular':
								Users.filterByRole = Users.USER_ROLES_OBJ.REGULAR;
								break;
							default:
								Users.filterByRole = '';
								break;
						}
					};

					var _successUsers = function (result) {
						Users.userList = result.data;
						UserDetailsService.setUsersList(Users.userList);
						UserDetailsService.setCurrentUserFilter(Users.filterByRole);
						LoaderService.Loader.hideSpinner();
					};
					var _failUsers = function () {
						LoaderService.Loader.hideSpinner();
					};

					var userRoleEnumsSuccess = function (userRoles) {
						Users.USER_ROLES = angular.copy(userRoles);
						Users.ALLOWED_USER_ROLES = $filter('regularUserRole')(Users.USER_ROLES);
						Users.USER_ROLES_OBJ = userRoles.toObject();
						changeUserFilter();
						AuthService.getCurrentUser().then(function (user) {
							Users.currentUser = user;
							if (Users.filterByRole) {
								checkUserPermission(user);
							}
						}, function () {
							Users.currentUser = null;
							MessageService.showFailedMessage();
							LoaderService.Loader.hideSpinner();
						});
					};
					var userRoleEnumsFailure = function () {
						currentFilter = Users.filterByRole;
						LoaderService.Loader.hideSpinner();
					};
					var authorizationStateEnumsSuccess = function (authorizationStates) {
						Users.AUTHORIZATION_STATE = angular.copy(authorizationStates);
						AUTHORIZATION_STATE_OBJ = authorizationStates.toObject();
						Users.ALLOWED_AUTHORIZATION_STATE = $filter('regularUserState')(Users.AUTHORIZATION_STATE);
					};
					var authorizationStateEnumsFailure = function (error) {
						$log.info('Error: ', error);
					};

					function checkUserPermission(user) {
						var _payload = null;
						switch (user.role) {
							case Users.USER_ROLES_OBJ.ADMIN:
							case Users.USER_ROLES_OBJ.EDITOR:
								_payload = {};
								Users.isAuthorisedUser = true;
								if (currentFilter !== Users.filterByRole || !Users.userList) {
									currentFilter = Users.filterByRole;
									Users.getUsers(_payload);
								} else {
									LoaderService.Loader.hideSpinner();
								}
								break;
							case Users.USER_ROLES_OBJ.LAUNCHSQUAD:
								_payload = {
									userId: user.id,
									role: user.role
								};
								currentFilter = Users.filterByRole;
								angular.element('a.underline').removeClass('underline');
								angular.element('#bump').addClass('underline');
								if (Users.USER_ROLES_OBJ.LAUNCHSQUAD === Users.filterByRole) {
									Users.getUsers(_payload);
								} else {
									$state.go('users.bump');
									Users.userList = null;
								}
								UserDetailsService.setUsersList(Users.userList);
								UserDetailsService.setCurrentUserFilter(Users.filterByRole);
								LoaderService.Loader.hideSpinner();
								break;
							case Users.USER_ROLES_OBJ.BUSINESS:
								_payload = {
									userId: user.id,
									role: user.role
								};
								currentFilter = Users.filterByRole;
								angular.element('a.underline').removeClass('underline');
								angular.element('#business').addClass('underline');
								if (Users.USER_ROLES_OBJ.BUSINESS === Users.filterByRole) {
									if (user.businessType === 'PROFIT')
										Users.getUsers(_payload);
								} else {
									$state.go('users.business');
									Users.userList = null;
								}
								UserDetailsService.setUsersList(Users.userList);
								UserDetailsService.setCurrentUserFilter(Users.filterByRole);
								LoaderService.Loader.hideSpinner();
								break;
							default:
								currentFilter = Users.filterByRole;
								_payload = null;
								Users.userList = null;
								UserDetailsService.setUsersList(Users.userList);
								UserDetailsService.setCurrentUserFilter(Users.filterByRole);
								LoaderService.Loader.hideSpinner();
								break;
						}
					}


					function addUser(USER_ROLES) {
						var modalInstance = $modal.open({
							templateUrl: 'components/modals/addUserContent/addUserContent.html',
							controllerAs: 'AddUserModal',
							controller: 'AddUserModalCtrl',
							size: 'lg',
							resolve: {
								UserRoles: function () {
									return USER_ROLES;
								}
							}
						});

						modalInstance.result.then(function (user) {
							$log.info('saved user info: ', user);
						}, function () {
							$log.info('Modal dismissed at: ' + new Date());
						});
					}

					Users.init = function () {
						LoaderService.Loader.showSpinner();
						
						$scope.$emit('SHOW_ACTION_BAR');
						
						EnumsService.getUserRoleEnums().then(userRoleEnumsSuccess, userRoleEnumsFailure);
						EnumsService.getAuthorizationStateEnums().then(authorizationStateEnumsSuccess, authorizationStateEnumsFailure);
						
					};

					Users.getUsers = function (_payload) {														
						Users.dtOptions = DTOptionsBuilder.newOptions()
								.withOption('ajax', {
									url: HOST.HOST_URL + '/api/ext/s/usersearch',
									type: 'POST',
									headers: {'Authorization': 'Bearer ' + btoa(userToken)},
									data: function (data) {
										_payload.limit = data.length;
										_payload.offSet = data.start;
										_payload.role = Users.filterByRole;
										_payload.query = data.search.value;

										return JSON.stringify(_payload);
									},
									dataFilter: function (data) {
										var json = jQuery.parseJSON(data);
										json.recordsTotal = json.count;
										json.recordsFiltered = json.count;

										return JSON.stringify(json);
									},
									dataType: 'json',
									contentType: 'application/json'
								})
								.withDataProp('data')
								.withOption('processing', true)
								.withOption('serverSide', true)
								.withOption('createdRow', createdRow)
								.withPaginationType('full_numbers');

						Users.dtColumns = [
							DTColumnBuilder.newColumn('firstName').withTitle('User Name').renderWith(function (data, type, full, meta) {
								var html = '<p>' + full.firstName + ' ' + full.lastName + '</p>';
								return html;
							}),
							DTColumnBuilder.newColumn('email').withTitle('Email'),
							DTColumnBuilder.newColumn('sessions').withTitle('Sessions'),
							DTColumnBuilder.newColumn('events').withTitle('Events'),
							DTColumnBuilder.newColumn('places').withTitle('Places'),
							DTColumnBuilder.newColumn('friends').withTitle('Friends'),
							DTColumnBuilder.newColumn('role').withTitle('Role'),
							DTColumnBuilder.newColumn('status').withTitle('Status')
						];
						
						Users.showDT = true;

						LoaderService.Loader.hideSpinner();												
					};

					function createdRow(row, data, dataIndex) {
						if (Users.checkUserPermissions(data)) {
							$(row).addClass('edit table-row cursor-pointer');
							$(row).unbind('click');
							$(row).bind('click', function () {
								$scope.$apply(function () {
									Users.editUserDetails(dataIndex, data);
								});
							});
						}

						// Recompiling so we can bind Angular directive to the DT
						$compile(angular.element(row).contents())($scope);
					}

					Users.addUser = function () {
						var USER_ROLES = null;
						switch (Users.currentUser.role) {
							case Users.USER_ROLES_OBJ.ADMIN:
								USER_ROLES = $filter('addUserPopupRole')(Users.USER_ROLES);
								addUser(USER_ROLES);
								break;
							case Users.USER_ROLES_OBJ.EDITOR:
								USER_ROLES = [Users.USER_ROLES_OBJ.BUSINESS];
								addUser(USER_ROLES);
								break;
							default :
								USER_ROLES = null;
								MessageService.showFailedMessage(ToastrMessage.USER.ADD.INSUFFICIENT_RIGHTS);
								break;
						}
					};

					Users.editUserDetails = function (index, data) {
						var _user = {data: data, index: index};
						UserDetailsService.setCurrentUserFilter(Users.filterByRole);
						UserDetailsService.setUserDetails(_user);
						UserDetailsService.setUsersList(Users.userList);
						$scope.$emit('HIDE_ACTION_BAR');
						if (_user.data.role === 'BUSINESS') {
							$state.go('businessdetails', {userId: _user.data.id});
						} else {
							$state.go('userdetails', {userId: _user.data.id});
						}
					};

					Users.saveUserDetails = function (index, userData) {
						var payload = {
							userId: userData.id,
							firstName: userData.firstName ? userData.firstName : userData.first_name ? userData.first_name : '',
							lastName: userData.lastName ? userData.lastName : userData.last_name ? userData.last_name : '',
							email: userData.email,
							role: userData.role,
							state: userData.status
						};
						var addEditUserSuccess = function (result) {
							if (result.status === 'SUCCESS') {
								MessageService.showSuccessfulMessage(ToastrMessage.USER.UPDATE[result.status]);
							} else {
								MessageService.showFailedMessage(ToastrMessage.USER.UPDATE[result.status]);
							}
							if (userData.role !== currentFilter) {
								UserDetailsService.removeUserFromUsersList(index);
							}
							LoaderService.Loader.hideSpinner();
						};
						var addEditUserFailure = function () {
							LoaderService.Loader.hideSpinner();
							MessageService.showFailedMessage();
						};
						LoaderService.Loader.showSpinner();
						AddEditUserAPIService.addEditUser(payload).$promise.then(addEditUserSuccess, addEditUserFailure);
					};

					Users.changeCurrentFilter = function (e, filter) {
						angular.element('a.underline').removeClass('underline');
						angular.element(e.target).addClass('underline');

						Users.filterByRole = filter; // update filter			
						Users.dtInstance.rerender();
					};
					
					/**
					 * check if the current user is allowed to edit
					 * @returns {bool}
					 */
					Users.checkUserPermissions = function(data){						
						//admins can always edit
						if(Users.currentUser.role === Users.USER_ROLES_OBJ.ADMIN){							
							return true;
						}									
						
						var state = $state.current.name;
						var bool = false;
						
						switch (state){
							case state = 'users.bump':
								if(Users.currentUser.role === Users.USER_ROLES_OBJ.LAUNCHSQUAD){
									bool = true;									
								}
							case state = 'users.business':
								if(Users.isAuthorisedUser || data.type === 'PROFIT'){
									bool = true;									
								}
						}
						
						return bool;
					};

					$scope.$on('HIDE_ACTION_BAR', function () {
						Users.isMoreButtonClicked = true;
					});
					$scope.$on('SHOW_ACTION_BAR', function () {
						Users.isMoreButtonClicked = false;
					});

				}]);
})(angular);
