jQuery(document).ready(function($) {
    
    if($('.user-type').val() == 'business'){
        $('#add-user-form').show();
        $('#add-business-form').hide();
    }
   
    //shows the add business form
    $('.user-type').change(function(){       
        if($(this).val() == 'business'){
            $('#add-user-form').hide();
            $('#add-business-form').show();
        }else {
            $('#add-user-form').show();
            $('#add-business-form').hide();
        } 
    });
    
    $('.tabs').tabs();
    
});