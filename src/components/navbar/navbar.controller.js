'use strict';

(function (angular) {
  angular.module('kwadWeb')
    .controller('NavbarCtrl', ['$rootScope', '$scope', '$state', 'CookieService', 'LogoutApiService', 'AuthService', 'MessageService', '$location',
      function ($rootScope, $scope, $state, CookieService, LogoutApiService, AuthService, MessageService, $location) {
        // Define variables
        var Navbar = this;
        Navbar.stateNew = {
          domains: 0,
          users: 1,
          events: 2,
          places: 3
        };

        Navbar.state = {
          current: Navbar.stateNew[$location.url().split('/')[1] || 'domains']
        };

        Navbar.menuItem = [{
          name: 'Domains Admin',
          goToState: 'domains'
        }, {
          name: 'User Admin',
          goToState: 'users'
        }, {
          name: 'Events',
          goToState: 'events'
        }, {
          name: 'Places',
          goToState: 'places'
        }];

        if ($rootScope.isLoggedInUser) {
          AuthService.getCurrentUser();
        }

        var _successLogout = function () {
          CookieService.userSession.clear();
          AuthService.setCurrentUser(null);
          $rootScope.isLoggedInUser = false;
          Navbar.state.current = 0;
          $state.go('login');
        };
        var _failLogout = function (error) {
          MessageService.showFailedMessage(error.data);
        };

        Navbar.logout = function () {
          LogoutApiService.logout({}).$promise.then(_successLogout, _failLogout);
        };

        $scope.$on('EVENT_CANCEL', function () {
          Navbar.state.current = Navbar.stateNew[$location.url().split('/')[1] || 'domains']
        });
      }]);
})(angular);
