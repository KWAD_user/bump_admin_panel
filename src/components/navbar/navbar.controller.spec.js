'use strict';

describe('Unit: NavbarCtrl Controller', function () {
  var Navbar,
    $rootScope,
    $scope,
    $controller,
    $state,
    $q,
    $httpBackend,
    CookieService = {},
    AuthService,
    LogoutApiService,
    ToastrMessage,
    MessageService,
    LoaderService = {Loader: {}};

  beforeEach(module('kwadWeb'));
  beforeEach(inject(function (_$rootScope_, _$controller_, _$q_, _ToastrMessage_, _$httpBackend_) {
    $rootScope = _$rootScope_;
    $scope = _$rootScope_.$new();
    $controller = _$controller_;
    $state = jasmine.createSpyObj('$state', ['go']);
    $q = _$q_;
    $httpBackend = _$httpBackend_;
    CookieService.userSession = jasmine.createSpyObj('userSession', ['clear']);
    AuthService = jasmine.createSpyObj('AuthService', ['getCurrentUser', 'setCurrentUser']);
    LogoutApiService = jasmine.createSpyObj('LogoutApiService', ['logout']);
    ToastrMessage = _ToastrMessage_;
    MessageService = jasmine.createSpyObj('MessageService', ['showSuccessfulMessage', 'showFailedMessage']);
    LoaderService.Loader = jasmine.createSpyObj('LoaderService.Loader', ['hideSpinner', 'showSpinner']);
    MessageService.showSuccessfulMessage.and.callFake(function () {
    });
    MessageService.showFailedMessage.and.callFake(function () {
    });
    LoaderService.Loader.hideSpinner.and.callFake(function () {
    });
    LoaderService.Loader.showSpinner.and.callFake(function () {
    });
    AuthService.getCurrentUser.and.callFake(function () {
    });
    AuthService.setCurrentUser.and.callFake(function () {
    });
    $state.go.and.callFake(function () {
    });
  }));
  beforeEach(function () {
    $rootScope.isLoggedInUser = true;
    Navbar = $controller('NavbarCtrl', {
      $scope: $scope,
      $state: $state,
      CookieService: CookieService,
      AuthService: AuthService,
      LogoutApiService: LogoutApiService,
      ToastrMessage: ToastrMessage,
      MessageService: MessageService,
      LoaderService: LoaderService
    });
    $httpBackend.expectGET('app/login/login.html').respond(200);
  });

  describe('Units Should be defined', function () {
    it('ToastrMessage should be defined', function () {
      expect(ToastrMessage).toBeDefined();
    });
    it('CookieService should be defined', function () {
      expect(CookieService).toBeDefined();
    });
  });

  describe('Function Unit: Navbar.logout', function () {
    it('Should be defined and be a function', function () {
      expect(Navbar.logout).toBeDefined();
      expect(typeof Navbar.logout).toEqual('function');
    });

    it('Should assign value false to isLoggedInUser when LogoutApiService.logout get resolved.', function () {
      CookieService.userSession.clear.and.callFake(function () {
      });
      LogoutApiService.logout.and.callFake(function () {
        var deferred = $q.defer();
        deferred.resolve({});
        deferred.$promise = deferred.promise;
        return deferred;
      });
      Navbar.logout();
      $rootScope.$digest();
      $httpBackend.flush();
      expect($rootScope.isLoggedInUser).toBeFalsy();
      expect(Navbar.state.current).toEqual(0);
    });

    it('Should not assign value false to isLoggedInUser when LogoutApiService.logout get rejected.', function () {
      LogoutApiService.logout.and.callFake(function () {
        var deferred = $q.defer();
        deferred.reject({data: 'error'});
        deferred.$promise = deferred.promise;
        return deferred;
      });
      Navbar.logout();
      $rootScope.$digest();
      $httpBackend.flush();
      expect($rootScope.isLoggedInUser).not.toBeFalsy();
    });
  });

  describe('Unit: $scope Events', function () {
    it('$scope should capture event \'EVENT_CANCEL\'', function () {
      Navbar.state.current = 2;
      $rootScope.$broadcast('EVENT_CANCEL');
      $rootScope.$digest();
      $httpBackend.flush();
      expect(Navbar.state.current).toEqual(0);
    });
  });
});
