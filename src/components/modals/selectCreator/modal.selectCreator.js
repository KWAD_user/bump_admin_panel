'use strict';

(function (angular) {
  angular.module('kwadWeb')
    .controller('SelectCreatorModalCtrl', ['$modalInstance', 'CreatorList',
      function ($modalInstance, CreatorList) {
        // Define variables
        var SelectCreatorModal = this;

        CreatorList.forEach(function (creator) {
          creator.fullName = creator.first_name || creator.last_name ? creator.first_name + ' ' + creator.last_name : (creator.firstName || creator.lastName ? creator.firstName + ' ' + creator.lastName : null);
          creator.fullName = creator.fullName ? creator.fullName : creator.businessName;
        });

        SelectCreatorModal.creatorList = CreatorList.length ? CreatorList : [{
          fullName: 'No Creator Found',
          id: null
        }];

        SelectCreatorModal.selectedCreator = SelectCreatorModal.creatorList[0];

        SelectCreatorModal.ok = function () {
          $modalInstance.close(SelectCreatorModal.selectedCreator);
        };

        SelectCreatorModal.cancel = function () {
          $modalInstance.dismiss('cancel');
        };

      }]);
})(angular);
