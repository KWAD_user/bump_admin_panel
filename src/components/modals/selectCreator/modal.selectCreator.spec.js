'use strict';

describe('Unit: SelectCreatorModal Controller', function () {
  var SelectCreatorModal,
    $rootScope,
    $controller,
    $httpBackend,
    $modalInstance,
    CreatorList;

  beforeAll(function () {
    CreatorList = [{
      'id': '5600d67ce4b037ce4d25269c',
      'email': 'vkvang@ucdavis.edu',
      'role': 'REGULAR',
      'status': 'ENABLED',
      'registrationDate': '2015-09-22T04:18:04.277+0000',
      'contactNumber': '2092440733',
      'businessName': null,
      'bPlaces': 0,
      'type': null,
      'contactNames': [],
      'address': null,
      'website': null,
      'notes': null,
      'associatedBusinessId': null,
      'associatedBusinessName': null,
      'firstName': 'Kazoua',
      'lastName': 'Vang',
      'friends': 0,
      'sessions': 0,
      'events': 0,
      'places': 0
    }, {
      'id': '5605f79be4b0a9be00b29c0a',
      'email': 'katrinao@uw.edu',
      'role': 'REGULAR',
      'status': 'DISABLED',
      'registrationDate': '2015-09-26T01:40:43.858+0000',
      'contactNumber': '4088354776',
      'businessName': null,
      'bPlaces': 0,
      'type': null,
      'contactNames': [],
      'address': null,
      'website': null,
      'notes': null,
      'associatedBusinessId': null,
      'associatedBusinessName': null,
      'firstName': 'Katrina',
      'lastName': 'Odfina',
      'friends': 0,
      'sessions': 0,
      'events': 0,
      'places': 0
    }]
  });
  beforeEach(module('kwadWeb'));
  beforeEach(inject(function (_$rootScope_, _$controller_, _$httpBackend_) {
    $rootScope = _$rootScope_;
    $controller = _$controller_;
    $httpBackend = _$httpBackend_;
    $modalInstance = jasmine.createSpyObj('$modalInstance', ['close', 'dismiss']);
    $modalInstance.close.and.callFake(function () {
    });
    $modalInstance.dismiss.and.callFake(function () {
    });
  }));
  beforeEach(function () {
    SelectCreatorModal = $controller('SelectCreatorModalCtrl', {
      $modalInstance: $modalInstance,
      CreatorList: CreatorList
    });
    $httpBackend.expectGET('app/login/login.html').respond(200);
  });

  describe('Units Should be defined', function () {
    it('SelectCreatorModal should be defined', function () {
      expect(SelectCreatorModal).toBeDefined();
    });
    it('$modalInstance should be defined', function () {
      expect($modalInstance).toBeDefined();
    });
    it('CreatorList should be defined', function () {
      expect(CreatorList).toBeDefined();
    });
  });

  describe('Function Unit: SelectCreatorModal.cancel', function () {
    it('Should be defined and be a function', function () {
      expect(SelectCreatorModal.cancel).toBeDefined();
      expect(typeof SelectCreatorModal.cancel).toEqual('function');
    });
    it('Should invoke $modalInstance.dismiss', function () {
      SelectCreatorModal.cancel();
      $rootScope.$digest();
      expect($modalInstance.dismiss).toHaveBeenCalledWith('cancel');
    });
  });

  describe('Function Unit: SelectCreatorModal.ok', function () {
    it('Should be defined and be a function', function () {
      expect(SelectCreatorModal.ok).toBeDefined();
      expect(typeof SelectCreatorModal.ok).toEqual('function');
    });
    it('Should invoke $modalInstance.close', function () {
      SelectCreatorModal.ok();
      $rootScope.$digest();
      expect($modalInstance.close).toHaveBeenCalledWith(SelectCreatorModal.creatorList[0]);
    });
  });
});