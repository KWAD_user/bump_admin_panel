'use strict';

describe('Unit: SelectPlaceModal Controller', function () {
  var SelectPlaceModal,
    $rootScope,
    $controller,
    $httpBackend,
    $modalInstance,
    PlaceResult;

  beforeAll(function () {
    PlaceResult = [{
      'epId': '546aff5be4b0370c32328fbb',
      'name': 'Phoenix Rock Gym',
      'place': '1353 E University Drive, Tempe AZ',
      'creatorName': 'Demo  User ',
      'creatorId': '546aff58e4b0370c32328f40',
      'feedImpression': 0,
      'detailImpression': 0,
      'appCheckinCount': 26,
      'geoCheckinCount': 8,
      'shares': 26,
      'status': 'ENABLED',
      'associatedEvents': 2,
      'creatorRole': 'ADMIN',
      'eventDate': null,
      'eventTimeLine': 'UNDETERMINED'
    }, {
      'epId': '546aff5be4b0370c32328fbf',
      'name': 'The Vue on Apache',
      'place': '922 E Apache Blvd, Tempe AZ',
      'creatorName': 'Brandy Smith',
      'creatorId': '546aff59e4b0370c32328f43',
      'feedImpression': 6,
      'detailImpression': 0,
      'appCheckinCount': 24,
      'geoCheckinCount': 283,
      'shares': 24,
      'status': 'ENABLED',
      'associatedEvents': 0,
      'creatorRole': 'BUMP',
      'eventDate': null,
      'eventTimeLine': 'UNDETERMINED'
    }]
  });
  beforeEach(module('kwadWeb'));
  beforeEach(inject(function (_$rootScope_, _$controller_, _$httpBackend_) {
    $rootScope = _$rootScope_;
    $controller = _$controller_;
    $httpBackend = _$httpBackend_;
    $modalInstance = jasmine.createSpyObj('$modalInstance', ['close', 'dismiss']);
    $modalInstance.close.and.callFake(function () {
    });
    $modalInstance.dismiss.and.callFake(function () {
    });
  }));
  beforeEach(function () {
    SelectPlaceModal = $controller('SelectPlaceModalCtrl', {
      $modalInstance: $modalInstance,
      PlaceResult: PlaceResult
    });
    $httpBackend.expectGET('app/login/login.html').respond(200);
  });

  describe('Units Should be defined', function () {
    it('SelectPlaceModal should be defined', function () {
      expect(SelectPlaceModal).toBeDefined();
    });
    it('$modalInstance should be defined', function () {
      expect($modalInstance).toBeDefined();
    });
    it('PlaceResult should be defined', function () {
      expect(PlaceResult).toBeDefined();
    });
  });

  describe('Function Unit: SelectPlaceModal.cancel', function () {
    it('Should be defined and be a function', function () {
      expect(SelectPlaceModal.cancel).toBeDefined();
      expect(typeof SelectPlaceModal.cancel).toEqual('function');
    });
    it('Should invoke $modalInstance.dismiss', function () {
      SelectPlaceModal.cancel();
      $rootScope.$digest();
      expect($modalInstance.dismiss).toHaveBeenCalledWith('cancel');
    });
  });

  describe('Function Unit: SelectPlaceModal.ok', function () {
    it('Should be defined and be a function', function () {
      expect(SelectPlaceModal.ok).toBeDefined();
      expect(typeof SelectPlaceModal.ok).toEqual('function');
    });
    it('Should invoke $modalInstance.close', function () {
      SelectPlaceModal.ok();
      $rootScope.$digest();
      expect($modalInstance.close).toHaveBeenCalledWith(SelectPlaceModal.PlaceResult[0]);
    });
  });
});
