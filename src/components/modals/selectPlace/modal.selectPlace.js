'use strict';

(function (angular) {
  angular.module('kwadWeb')
    .controller('SelectPlaceModalCtrl', ['$modalInstance', 'PlaceResult',
      function ($modalInstance, PlaceResult) {
        var SelectPlaceModal = this;

        SelectPlaceModal.PlaceResult = PlaceResult.length ? PlaceResult : [{
          name: 'No Place Found',
          epId: null
        }];

        SelectPlaceModal.selectedPlace = SelectPlaceModal.PlaceResult[0];

        SelectPlaceModal.ok = function () {
          $modalInstance.close(SelectPlaceModal.selectedPlace);
        };

        SelectPlaceModal.cancel = function () {
          $modalInstance.dismiss('cancel');
        };
      }]);
})(angular);
