describe('Unit: AddUser modal filter', function () {
  var $filter, $rootScope, fakeResponse, $httpBackend;

  beforeAll(function () {
    fakeResponse = {
      fakeRoles: ['ANONYMOUS', 'REGULAR', 'LAUNCHSQUAD', 'ADMIN', 'BUSINESS', 'BUSINESS_INDIVIDUAL', 'EDITOR']
    };
  });
  beforeEach(module('kwadWeb'));
  beforeEach(inject(function (_$rootScope_, _$filter_, _$httpBackend_) {
    $filter = _$filter_;
    $rootScope = _$rootScope_;
    $httpBackend = _$httpBackend_;
    $httpBackend.expectGET('app/login/login.html').respond(200);
  }));

  describe('Units should be defined', function () {
    it('$filter should be defined', function () {
      expect($filter).toBeDefined();
    })
  });

  describe('Filter Unit: addUserPopupRole', function () {
    it('Should be exist and be a function', function () {
      expect($filter('addUserPopupRole')).toBeDefined();
      expect(typeof $filter('addUserPopupRole')).toBe('function');
    });
    it('Should be filter user roles', function () {
      var result = $filter('addUserPopupRole')(fakeResponse.fakeRoles);
      $rootScope.$digest();
      expect(result.indexOf('ANONYMOUS')).toEqual(-1);
    })
  });
});