'use strict';

describe('Unit: AddUserModal Controller', function () {
  var AddUserModal,
    $rootScope,
    $controller,
    $modalInstance,
    $q,
    $httpBackend,
    $filter,
    MessageService,
    ToastrMessage,
    LoaderService = {Loader: {}},
    UserRoles,
    UserDetailsService,
    AddEditUserAPIService,
    fakeResponse,
    EnumsService;

  beforeAll(function () {
    fakeResponse = {
      fakeBusinessTypes: ['PROFIT', 'NON_PROFIT', 'UNIVERSITY'],
      fakeAddedUser: {
        id: '54bed6e2e4b026f0ebf5ab6b',
        attributes: [],
        college: '',
        first_name: 'Cherry',
        full_name: 'Cherry Lindz',
        last_name: 'Lindz',
        role: 'REGULAR',
        state: 'ENABLED'
      },
      fakeAddedBusinessUser: {
        address: 'Delhi, India',
        contactNames: ['test user'],
        registrationDate: '2015-09-04T05:00:20.000+0000',
        website: 'www.test.test',
        businessName: 'Test Business',
        email: 'test@gmail.com',
        id: '50bas6r4e4b024t0dff6sc9c',
        role: 'BUSINESS',
        state: 'ENABLED',
        type: 'PROFIT'
      }
    }
  });
  beforeEach(module('kwadWeb'));
  beforeEach(inject(function (_$rootScope_, _$controller_, _$filter_, _$q_, _$httpBackend_, _UserDetailsService_, _ToastrMessage_) {
    $rootScope = _$rootScope_;
    $controller = _$controller_;
    $filter = _$filter_;
    $modalInstance = jasmine.createSpyObj('$modalInstance', ['close', 'dismiss']);
    $q = _$q_;
    $httpBackend = _$httpBackend_;
    UserDetailsService = _UserDetailsService_;
    AddEditUserAPIService = jasmine.createSpyObj('AddEditUserAPIService', ['addEditUser']);
    EnumsService = jasmine.createSpyObj('EnumsService', ['getBusinessTypeEnums']);
    MessageService = jasmine.createSpyObj('MessageService', ['showSuccessfulMessage', 'showFailedMessage']);
    ToastrMessage = _ToastrMessage_;
    LoaderService.Loader = jasmine.createSpyObj('LoaderService.Loader', ['hideSpinner', 'showSpinner']);
    MessageService.showSuccessfulMessage.and.callFake(function () {
    });
    MessageService.showFailedMessage.and.callFake(function () {
    });
    LoaderService.Loader.hideSpinner.and.callFake(function () {
    });
    LoaderService.Loader.showSpinner.and.callFake(function () {
    });
    $modalInstance.close.and.callFake(function () {
    });
    $modalInstance.dismiss.and.callFake(function () {
    });
  }));

  describe('when default user role is REGULAR/ADMIN/LAUNCHSQUAD/EDITOR', function () {
    beforeAll(function () {
      UserRoles = ['REGULAR', 'LAUNCHSQUAD', 'ADMIN', 'ANONYMOUS', 'BUSINESS', 'BUSINESS_INDIVIDUAL', 'EDITOR'];
    });
    beforeEach(function () {
      AddUserModal = $controller('AddUserModalCtrl', {
        $q: $q,
        $modalInstance: $modalInstance,
        $filter: $filter,
        AddEditUserAPIService: AddEditUserAPIService,
        UserDetailsService: UserDetailsService,
        EnumsService: EnumsService,
        MessageService: MessageService,
        ToastrMessage: ToastrMessage,
        LoaderService: LoaderService,
        UserRoles: UserRoles
      });
      $httpBackend.expectGET('app/login/login.html').respond(200);
      $httpBackend.flush();
    });

    describe('Units Should be defined', function () {
      it('AddUserModal should be defined', function () {
        expect(AddUserModal).toBeDefined();
      });
      it('$modalInstance should be defined', function () {
        expect($modalInstance).toBeDefined();
      });
      it('$modalInstance should be defined', function () {
        expect($filter).toBeDefined();
      });
      it('AddEditUserAPIService should be defined', function () {
        expect(AddEditUserAPIService).toBeDefined();
      });
      it('UserDetailsService should be defined', function () {
        expect(UserDetailsService).toBeDefined();
      });
      it('EnumsService should be defined', function () {
        expect(EnumsService).toBeDefined();
      });
      it('MessageService should be defined', function () {
        expect(MessageService).toBeDefined();
      });
      it('ToastrMessage should be defined', function () {
        expect(ToastrMessage).toBeDefined();
      });
      it('LoaderService should be defined', function () {
        expect(LoaderService).toBeDefined();
      });
      it('UserRoles should be defined', function () {
        expect(UserRoles).toBeDefined();
      });
    });

    describe('Function Unit: AddUserModal.cancel', function () {
      it('Should be defined and be a function', function () {
        expect(AddUserModal.cancel).toBeDefined();
        expect(typeof AddUserModal.cancel).toEqual('function');
      });
      it('Should invoke $modalInstance.dismiss', function () {
        AddUserModal.cancel();
        $rootScope.$digest();
        expect($modalInstance.dismiss).toHaveBeenCalledWith('cancel');
      });
    });

    describe('Function Unit: AddUserModal.init', function () {
      it('Should be defined and be a function', function () {
        expect(AddUserModal.init).toBeDefined();
        expect(typeof AddUserModal.init).toEqual('function');
      });
      it('Should invoke EnumsService.getBusinessTypeEnums and respond success', function () {
        EnumsService.getBusinessTypeEnums.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve(fakeResponse.fakeBusinessTypes);
          return deferred.promise;
        });
        AddUserModal.init();
        $rootScope.$digest();
        expect(AddUserModal.BUSINESS_TYPES.length).toBeGreaterThan(0);
      });
      it('Should invoke EnumsService.getBusinessTypeEnums and respond failure', function () {
        EnumsService.getBusinessTypeEnums.and.callFake(function () {
          var deferred = $q.defer();
          deferred.reject();
          return deferred.promise;
        });
        AddUserModal.init();
        $rootScope.$digest();
        expect(AddUserModal.BUSINESS_TYPES).toEqual(null);
      });
    });

    describe('Function Unit: AddUserModal.ok', function () {
      it('Should be defined and be a function', function () {
        expect(AddUserModal.ok).toBeDefined();
        expect(typeof AddUserModal.ok).toEqual('function');
      });
      it('Should add a regular user successfully', function () {
        AddEditUserAPIService.addEditUser.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve({status: 'SUCCESS', data: fakeResponse.fakeAddedUser});
          deferred.$promise = deferred.promise;
          return deferred;
        });

        AddUserModal.userInfo = {
          role: 'REGULAR',
          status: 'ENABLED',
          firstName: 'Cherry',
          lastName: 'Lindz',
          email: 'cherry_lindz@gmail.com'
        };

        AddUserModal.ok();
        $rootScope.$digest();
        expect($modalInstance.close).toHaveBeenCalledWith(AddUserModal.userInfo);
      });
      describe('Should not add a regular user', function () {
        it('when AddEditUserAPIService.addEditUser respond 200 with failure status', function () {
          AddEditUserAPIService.addEditUser.and.callFake(function () {
            var deferred = $q.defer();
            deferred.resolve({status: 'ALREADY_REGISTERED'});
            deferred.$promise = deferred.promise;
            return deferred;
          });
          AddUserModal.userInfo = {
            role: 'REGULAR',
            status: 'ENABLED',
            firstName: 'Cherry',
            lastName: 'Lindz',
            email: 'cherry_lindz@gmail.com'
          };
          AddUserModal.ok();
          $rootScope.$digest();
          expect(MessageService.showFailedMessage).toHaveBeenCalledWith(ToastrMessage.USER.ADD.ALREADY_REGISTERED);
          expect(LoaderService.Loader.hideSpinner).toHaveBeenCalled();
        });
        it('when new user\'s email is blank', function () {
          AddUserModal.userInfo = {
            role: 'REGULAR',
            status: 'ENABLED',
            firstName: 'Cherry',
            lastName: 'Lindz',
            email: ''
          };
          AddUserModal.ok();
          $rootScope.$digest();
          expect(MessageService.showFailedMessage).toHaveBeenCalledWith(ToastrMessage.USER.ADD.BLANK_EMAIL_FIELD);
          expect(LoaderService.Loader.hideSpinner).toHaveBeenCalled();
        });
        it('when AddEditUserAPIService.addEditUser respond failure', function () {
          AddEditUserAPIService.addEditUser.and.callFake(function () {
            var deferred = $q.defer();
            deferred.reject();
            deferred.$promise = deferred.promise;
            return deferred;
          });
          AddUserModal.userInfo = {
            role: 'REGULAR',
            status: 'ENABLED',
            firstName: 'Cherry',
            lastName: 'Lindz',
            email: 'cherry_lindz@gmail.com'
          };
          AddUserModal.ok();
          $rootScope.$digest();
          expect(MessageService.showFailedMessage).toHaveBeenCalled();
          expect(LoaderService.Loader.hideSpinner).toHaveBeenCalled();
        });
      });
    });
  });

  describe('when default user role is BUSINESS', function () {
    beforeEach(function () {
      UserRoles = ['BUSINESS', 'REGULAR', 'LAUNCHSQUAD', 'ADMIN', 'ANONYMOUS', 'BUSINESS_INDIVIDUAL', 'EDITOR'];
      AddUserModal = $controller('AddUserModalCtrl', {
        $q: $q,
        $modalInstance: $modalInstance,
        $filter: $filter,
        AddEditUserAPIService: AddEditUserAPIService,
        UserDetailsService: UserDetailsService,
        EnumsService: EnumsService,
        MessageService: MessageService,
        ToastrMessage: ToastrMessage,
        LoaderService: LoaderService,
        UserRoles: UserRoles
      });
      $httpBackend.expectGET('app/login/login.html').respond(200);
      $httpBackend.flush();
    });
    describe('Function Unit: AddUserModal.ok', function () {
      it('Should be defined and be a function', function () {
        expect(AddUserModal.ok).toBeDefined();
        expect(typeof AddUserModal.ok).toEqual('function');
      });
      it('Should add a business user successfully', function () {
        AddEditUserAPIService.addEditUser.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve({status: 'SUCCESS', data: fakeResponse.fakeAddedBusinessUser});
          deferred.$promise = deferred.promise;
          return deferred;
        });
        AddUserModal.businessUserInfo = {
          businessType: 'PROFIT',
          businessName: 'Test Business',
          email: 'test@gmail.com',
          address: 'Delhi, India',
          contactNumber: '0123456789',
          website: 'www.test.test',
          state: 'ENABLED',
          contactNames: ['test user'],
          role: 'BUSINESS'
        };
        AddUserModal.ok();
        $rootScope.$digest();
        expect($modalInstance.close).toHaveBeenCalledWith(AddUserModal.businessUserInfo);
      });
      describe('Should not add a business user', function () {
        it('when AddEditUserAPIService.addEditUser respond 200 with failure status', function () {
          AddEditUserAPIService.addEditUser.and.callFake(function () {
            var deferred = $q.defer();
            deferred.resolve({status: 'ALREADY_REGISTERED'});
            deferred.$promise = deferred.promise;
            return deferred;
          });
          AddUserModal.businessUserInfo = {
            businessType: 'PROFIT',
            businessName: 'Test Business',
            email: 'test@gmail.com',
            address: 'Delhi, India',
            contactNumber: '0123456789',
            website: 'www.test.test',
            state: 'ENABLED',
            contactNames: ['test user'],
            role: 'BUSINESS'
          };
          AddUserModal.ok();
          $rootScope.$digest();
          expect(MessageService.showFailedMessage).toHaveBeenCalledWith(ToastrMessage.USER.ADD.ALREADY_REGISTERED);
          expect(LoaderService.Loader.hideSpinner).toHaveBeenCalled();
        });
        it('when new user\'s email is blank', function () {
          AddUserModal.businessUserInfo = {
            businessType: 'PROFIT',
            businessName: 'Test Business',
            email: '',
            address: 'Delhi, India',
            contactNumber: '0123456789',
            website: 'www.test.test',
            state: 'ENABLED',
            contactNames: ['test user'],
            role: 'BUSINESS'
          };
          AddUserModal.ok();
          $rootScope.$digest();
          expect(MessageService.showFailedMessage).toHaveBeenCalledWith(ToastrMessage.USER.ADD.BLANK_EMAIL_FIELD);
          expect(LoaderService.Loader.hideSpinner).toHaveBeenCalled();
        });
        it('when AddEditUserAPIService.addEditUser respond failure', function () {
          AddEditUserAPIService.addEditUser.and.callFake(function () {
            var deferred = $q.defer();
            deferred.reject();
            deferred.$promise = deferred.promise;
            return deferred;
          });
          AddUserModal.businessUserInfo = {
            businessType: 'PROFIT',
            businessName: 'Test Business',
            email: 'test@gmail.com',
            address: 'Delhi, India',
            contactNumber: '0123456789',
            website: 'www.test.test',
            state: 'ENABLED',
            contactNames: ['test user'],
            role: 'BUSINESS'
          };
          AddUserModal.ok();
          $rootScope.$digest();
          expect(MessageService.showFailedMessage).toHaveBeenCalled();
          expect(LoaderService.Loader.hideSpinner).toHaveBeenCalled();
        });
      });
    });
  });
});
