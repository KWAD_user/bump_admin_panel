'use strict';

/**
 * @ngdoc function
 * @name kwadWeb.Filter:regularUserRole filter
 * @description
 * # used to filter user roles
 */
(function (angular) {
  angular.module('kwadWeb')
    .filter('addUserPopupRole', [function () {
      return function (input) {
        if (Array.isArray(input) && input.length) {
          input = input.filter(function (value) {
            return !!(value !== 'ANONYMOUS' && value !== 'BUSINESS_INDIVIDUAL');
          });
        }
        return input;
      };
    }]);
})(angular);
