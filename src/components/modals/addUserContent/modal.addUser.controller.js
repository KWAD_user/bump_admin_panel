'use strict';

(function (angular) {
  angular.module('kwadWeb')
    .controller('AddUserModalCtrl', ['$q', '$modalInstance', '$filter', '$log', 'UserRoles', 'UserDetailsService', 'AddEditUserAPIService', 'EnumsService', 'MessageService', 'ToastrMessage', 'LoaderService',
      function ($q, $modalInstance, $filter, $log, UserRoles, UserDetailsService, AddEditUserAPIService, EnumsService, MessageService, ToastrMessage, LoaderService) {
        // Define variables
        var AddUserModal = this;
        AddUserModal.USER_ROLES = UserRoles;
        AddUserModal.BUSINESS_TYPES = null;
        AddUserModal.userRole = AddUserModal.USER_ROLES ? AddUserModal.USER_ROLES[0] : '';
        AddUserModal.userInfo = {
          role: '',
          status: 'ENABLED',
          firstName: '',
          lastName: '',
          email: '',
          events: 0,
          sessions: 0,
          friends: 0,
          places: 0
        };
        AddUserModal.businessUserInfo = {
          businessType: '',
          businessName: '',
          email: '',
          address: '',
          contactNumber: '',
          website: '',
          state: 'ENABLED',
          contactNames: [],
          role: ''
        };

        var BusinessTypeEnumsSuccess = function (result) {
          AddUserModal.BUSINESS_TYPES = result;
          AddUserModal.businessUserInfo.businessType = AddUserModal.BUSINESS_TYPES ? AddUserModal.BUSINESS_TYPES[0] : '';
        };
        var BusinessTypeEnumsFailure = function (err) {
          $log.info('Error: ', err);
        };
        var updateUserSuccess = function (res) {
          AddUserModal.userInfo.id = res.data.id;
          LoaderService.Loader.hideSpinner();
          UserDetailsService.addNewUserToUsersList(AddUserModal.userInfo);
          $modalInstance.close(AddUserModal.userInfo);
        };
        var updateUserFailure = function (err) {
          $log.info('Error: ', err);
          LoaderService.Loader.hideSpinner();
        };
        var addBusinessUserSuccess = function (res) {
          var _user = {
            address: res.data.address,
            contactNames: res.data.contactNames,
            registrationDate: res.data.registrationDate,
            website: res.data.website,
            bPlaces: 0,
            businessName: res.data.businessName,
            email: res.data.email,
            events: 0,
            id: res.data.id,
            places: 0,
            role: res.data.role,
            sessions: 0,
            status: res.data.state,
            type: res.data.businessType
          };
          LoaderService.Loader.hideSpinner();
          UserDetailsService.addNewUserToUsersList(_user);
          $modalInstance.close(AddUserModal.businessUserInfo);
        };
        var addBusinessUserFailure = function (err) {
          $log.info('Error: ', err);
          LoaderService.Loader.hideSpinner();
        };

        function updateUserDetails(payload) {
          var deferred = $q.defer();
          var addEditUserSuccess = function (result) {
            if (result.status === 'SUCCESS') {
              MessageService.showSuccessfulMessage(ToastrMessage.USER.ADD[result.status]);
              deferred.resolve(result);
            } else {
              MessageService.showFailedMessage(ToastrMessage.USER.ADD[result.status]);
              deferred.reject(result);
            }
          };
          var addEditUserFailure = function (err) {
            MessageService.showFailedMessage();
            deferred.reject(err);
          };
          AddEditUserAPIService.addEditUser(payload).$promise.then(addEditUserSuccess, addEditUserFailure);
          return deferred.promise;
        }

        function addUser() {
          LoaderService.Loader.showSpinner();
          AddUserModal.userInfo.role = AddUserModal.userRole;
          if (!AddUserModal.userInfo.email) {
            MessageService.showFailedMessage(ToastrMessage.USER.ADD.BLANK_EMAIL_FIELD);
            LoaderService.Loader.hideSpinner();
            return;
          }

          var payload = {
            email: AddUserModal.userInfo.email,
            role: AddUserModal.userInfo.role,
            state: AddUserModal.userInfo.status,
            firstName: AddUserModal.userInfo.firstName,
            lastName: AddUserModal.userInfo.lastName,
            passwd: AddUserModal.userInfo.password
          };
          updateUserDetails(payload).then(updateUserSuccess, updateUserFailure);
        }

        function addBusinessUser() {
          LoaderService.Loader.showSpinner();
          AddUserModal.businessUserInfo.role = AddUserModal.userRole;
          if (!AddUserModal.businessUserInfo.email) {
            MessageService.showFailedMessage(ToastrMessage.USER.ADD.BLANK_EMAIL_FIELD);
            LoaderService.Loader.hideSpinner();
            return;
          }
          updateUserDetails(AddUserModal.businessUserInfo).then(addBusinessUserSuccess, addBusinessUserFailure);
        }

        AddUserModal.init = function () {
          EnumsService.getBusinessTypeEnums().then(BusinessTypeEnumsSuccess, BusinessTypeEnumsFailure);
        };

        AddUserModal.ok = function () {
          switch (AddUserModal.userRole) {
            case 'REGULAR':
            case 'LAUNCHSQUAD':
            case 'ADMIN':
            case 'EDITOR':
              addUser();
              break;
            case 'BUSINESS':
              addBusinessUser();
              break;
          }
        };

        AddUserModal.cancel = function () {
          $modalInstance.dismiss('cancel');
        };

      }]);
})(angular);