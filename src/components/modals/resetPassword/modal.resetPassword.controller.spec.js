'use strict';

describe('Unit: ResetPasswordModal Controller', function () {
  var ResetPasswordModal,
    $rootScope,
    $controller,
    $modalInstance,
    $q,
    $httpBackend,
    UserInfo,
    ResetPasswordApiService,
    MessageService,
    ToastrMessage,
    LoaderService = {Loader: {}};

  beforeAll(function () {
    UserInfo = {'id': '5600d67ce4b037ce4d25269c'}
  });
  beforeEach(module('kwadWeb'));
  beforeEach(inject(function (_$rootScope_, _$controller_, _$q_, _$httpBackend_, _ToastrMessage_) {
    $rootScope = _$rootScope_;
    $controller = _$controller_;
    $modalInstance = jasmine.createSpyObj('$modalInstance', ['close', 'dismiss']);
    $q = _$q_;
    $httpBackend = _$httpBackend_;
    ResetPasswordApiService = jasmine.createSpyObj('ResetPasswordApiService', ['resetPassword']);
    ToastrMessage = _ToastrMessage_;
    MessageService = jasmine.createSpyObj('MessageService', ['showSuccessfulMessage', 'showFailedMessage']);
    LoaderService.Loader = jasmine.createSpyObj('LoaderService.Loader', ['hideSpinner', 'showSpinner']);
    MessageService.showSuccessfulMessage.and.callFake(function () {
    });
    MessageService.showFailedMessage.and.callFake(function () {
    });
    LoaderService.Loader.hideSpinner.and.callFake(function () {
    });
    LoaderService.Loader.showSpinner.and.callFake(function () {
    });
    $modalInstance.close.and.callFake(function () {
    });
    $modalInstance.dismiss.and.callFake(function () {
    });
  }));
  beforeEach(function () {
    ResetPasswordModal = $controller('ResetPasswordModalCtrl', {
      $modalInstance: $modalInstance,
      ResetPasswordApiService: ResetPasswordApiService,
      MessageService: MessageService,
      ToastrMessage: ToastrMessage,
      LoaderService: LoaderService,
      UserInfo: UserInfo
    });
    $httpBackend.expectGET('app/login/login.html').respond(200);
  });

  describe('Units Should be defined', function () {
    it('ResetPasswordModal should be defined', function () {
      expect(ResetPasswordModal).toBeDefined();
    });
    it('$modalInstance should be defined', function () {
      expect($modalInstance).toBeDefined();
    });
    it('UserInfo should be defined', function () {
      expect(UserInfo).toBeDefined();
    });
  });

  describe('Function Unit: ResetPasswordModal.cancel', function () {
    it('Should be defined and be a function', function () {
      expect(ResetPasswordModal.cancel).toBeDefined();
      expect(typeof ResetPasswordModal.cancel).toEqual('function');
    });
    it('Should invoke $modalInstance.dismiss', function () {
      ResetPasswordModal.cancel();
      $rootScope.$digest();
      expect($modalInstance.dismiss).toHaveBeenCalledWith('cancel');
    });
  });

  describe('Function Unit: ResetPasswordModal.ok', function () {
    it('Should be defined and be a function', function () {
      expect(ResetPasswordModal.ok).toBeDefined();
      expect(typeof ResetPasswordModal.ok).toEqual('function');
    });
    describe('Should invoke $modalInstance.close', function () {
      it('when reset password successfully', function () {
        ResetPasswordApiService.resetPassword.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve({status: 'SUCCESS'});
          deferred.$promise = deferred.promise;
          return deferred;
        });
        ResetPasswordModal.ok();
        $rootScope.$digest();
        expect($modalInstance.close).toHaveBeenCalledWith(ToastrMessage.PASSWORD.RESET.SUCCESS);
      });
      it('when reset password return 200 with status INSUFFICIENT_RIGHTS', function () {
        ResetPasswordApiService.resetPassword.and.callFake(function () {
          var deferred = $q.defer();
          deferred.resolve({status: 'INSUFFICIENT_RIGHTS'});
          deferred.$promise = deferred.promise;
          return deferred;
        });
        ResetPasswordModal.ok();
        $rootScope.$digest();
        expect($modalInstance.close).toHaveBeenCalledWith(ToastrMessage.PASSWORD.RESET.INSUFFICIENT_RIGHTS);
      });
      it('when reset password get rejected', function () {
        ResetPasswordApiService.resetPassword.and.callFake(function () {
          var deferred = $q.defer();
          deferred.reject();
          deferred.$promise = deferred.promise;
          return deferred;
        });
        ResetPasswordModal.ok();
        $rootScope.$digest();
        expect($modalInstance.close).toHaveBeenCalledWith('Something going wrong, Please try again.');
      });
    });
  });
});