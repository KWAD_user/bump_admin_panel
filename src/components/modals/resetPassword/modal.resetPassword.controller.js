'use strict';

(function (angular) {
  angular.module('kwadWeb')
    .controller('ResetPasswordModalCtrl', ['$modalInstance', 'UserInfo', 'ResetPasswordApiService', 'MessageService', 'ToastrMessage', 'LoaderService',
      function ($modalInstance, UserInfo, ResetPasswordApiService, MessageService, ToastrMessage, LoaderService) {
        // Define variables
        var ResetPasswordModal = this;

        ResetPasswordModal.ok = function () {
          var payload = {
              userId: UserInfo.id
            },
            success = function (response) {
              LoaderService.Loader.hideSpinner();
              if (response.status === 'SUCCESS') {
                MessageService.showSuccessfulMessage(ToastrMessage.PASSWORD.RESET[response.status]);
              } else {
                MessageService.showFailedMessage(ToastrMessage.PASSWORD.RESET[response.status]);
              }
              $modalInstance.close(ToastrMessage.PASSWORD.RESET[response.status]);
            },
            failure = function () {
              LoaderService.Loader.hideSpinner();
              MessageService.showFailedMessage();
              $modalInstance.close('Something going wrong, Please try again.');
            };
          LoaderService.Loader.showSpinner();
          ResetPasswordApiService.resetPassword(payload).$promise.then(success, failure);
        };

        ResetPasswordModal.cancel = function () {
          $modalInstance.dismiss('cancel');
        };

      }]);
})(angular);
