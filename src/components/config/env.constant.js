'use strict';

(function (window, document, angular, jQuery) {

  //Variables
  var hostname = window.location.hostname;

  //Set Default Constants
  var apiUrl = '';
  var hostUrl = '';

  //Define Application Level Constants
  //Set LOCALHOST Constants
  if (hostname.match(/dev-web.whatsbumpin.net/)) {
    apiUrl = 'http://development.whatsbumpin.net/api';   // dev server
    hostUrl = 'http://development.whatsbumpin.net';   // dev server
  } else if (hostname.match(/web.whatsbumpin.net/)) {
    apiUrl = 'http://apistg.whatsbumpin.net/api';
    hostUrl = 'http://apistg.whatsbumpin.net';
  } else {
    apiUrl = 'http://development.whatsbumpin.net/api';   // dev server
    hostUrl = 'http://development.whatsbumpin.net';   // dev server
  }
  // noFollow
  jQuery('html head').append('<meta name="robots" content="nofollow" />');

  //Constants Binding
  angular.module('kwadWeb')
    .constant('HOST', {
      URL: apiUrl,
      HOST_URL: hostUrl
    })
    .constant('AWS_UPLOAD', {
      URL: 'https://s3.amazonaws.com/'
    });


})(window, document, angular, jQuery);
