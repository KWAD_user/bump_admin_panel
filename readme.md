# kwad-admin

This project is generated with [yo angular generator](https://github.com/yeoman/generator-angular)

## Build & development

      1. Clone repository from:
         https://KWAD_user@bitbucket.org/KWAD_user/bump_admin_panel.git
      2. bower install
      3. npm install
      4. Run app :
         gulp serve :- Will run app without minification.
         gulp serve:dist :- Will run app in minified version

      Note: You can run `gulp build` for building minification then serve as 'gulp serve:dist' and
         run 'gulp clean' to clean minification.

## Testing

      Running `gulp test` will run the unit tests with karma.
      To view coverage, run 'coverage/index.html' file in your browser after running command 'gulp test'

## app directory structure

    src/
    |--> app
          |--> domains
          |--> events
          |--> forgetPassword
          |--> login
          |--> main
          |--> places
          |--> services
                |-->apiResources
          |--> styles
          |--> userDetails
          |--> users
    |--> assets
    |--> components
          |--> config
          |--> directives
          |--> modals
                |--> addUserContent
                |--> resetPassword
                |--> selectCreator
                |--> selectPlace
          |--> navbar
    |--> fonts
    |--> images
    |--> scripts
    |--> index.js
